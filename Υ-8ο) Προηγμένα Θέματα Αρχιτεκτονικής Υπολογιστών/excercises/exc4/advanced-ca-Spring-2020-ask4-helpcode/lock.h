#ifndef LOCK_H_
#define LOCK_H_

typedef volatile int spinlock_t;

#define UNLOCKED  0 
#define LOCKED    1

static inline void spin_lock_init(spinlock_t *spin_var)
{
    *spin_var = UNLOCKED; 
}

static inline void spin_lock_tas_cas(spinlock_t *spin_var)
{
    /* complete your implementation here */
}

static inline void spin_lock_ttas_cas(spinlock_t *spin_var)
{
    /* complete your implementation here */
}

static inline void spin_lock_tas_ts(spinlock_t *spin_var)
{
    /* complete your implementation here */
}

static inline void spin_lock_ttas_ts(spinlock_t *spin_var)
{
    /* complete your implementation here */
}

static inline void spin_unlock(spinlock_t *spin_var)
{
    __sync_lock_release(spin_var);
}


#endif
