/* CLIENT SIDE - index.php */

<div id="results"></div>

<div id="panel">
	<button>Get</button>
</div>

<script>
	var getButton = $("#panel button")[0]; // to "#panel button" legetai query selector. eksigiseis apo katw
	getButton.onclick = function() {
		$.post("server.php", {
			getResults: "whatever"
		}, function(str) {
			$("#results")[0].innerHTML = str;
		});
	};
</script>











/* SERVER SIDE - server.php */

<?php
if (isset($_POST['getResults'])) {
	$query = mysqli_query($link, "SELECT id, name FROM users WHERE 1");
	$html = '<table>
	<tr><th>ID</th><th>NAME</th></tr>'; // column titles
	while ($row = mysqli_fetch_assoc($query)) {	// scan through sql table rows
		// se kathe loupa to $row einai ena php assoc array pou antiprosopeuei mia sql row
		// kai exei kleidia $row['id'] kai $row['name']
		$html .= '<tr><td>'.$row['id'].'</td><td>'.$row['name'].'</td></tr>';
	}
	$html .= '</table>';
	echo $html; // to html table tha to parei i ajax twra.
}
?>






















NOTES:

1) mesa sto head bazoume tin jquery
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

2) ti einai to query selector?
dialegoume elements pou to ena einai mesa sto allo.
#abc simainei element me id="abc"
.abc simainei element me class="abc"
abc simainei element abc <abc>innerHTML</abc>
to [0] to bazoume gia na dialeksoume to prwto, epidi to query selector kanei return array

3) pws kanoume mia forma submit me jquery anti gia <form>?
<form method="post" action="server.php">
	<input name="name" value="Liakos"/>
	<input name="password" value="" type="password"/>
	<button type="submit" name="login">Login</button>
</form>
	
tips for function "$.post": $.post(urlTarget, postVariables, callback)
	
ara:
	
$.post("server.php", {
	name: "Liakos",
	password: "",
	login: 1
}, function(str) {
	// to string pou epestrepse o server	
});