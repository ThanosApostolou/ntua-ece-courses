/* Count records in employees table */
SELECT COUNT(*)
FROM employees;

/* Change the first name of employee with id=2 
to 'Steven'
*/
UPDATE employees SET firstname='Steven'
WHERE employeeid=2;


/* Count UNIQUE first names in employees table */
SELECT COUNT(DISTINCT firstname)
FROM employees;

/* OUTPUT employees WITH firstname STARTS WITH 'Ste' */
SELECT *
FROM employees
WHERE firstname LIKE 'Ste%'
;


/* CREATE  A VIEW with the UNIQUE combinations of firstname + space + lastname  
*/
CREATE VIEW employeesflnames AS 
SELECT DISTINCT firstname || ' ' || lastname AS flname
FROM employees;

/* DROP the VIEW employeesflnames */
DROP VIEW employeesflnames;

/* Find how many orders per employee */
SELECT employeeid,COUNT(orderid) 
  FROM orders
  GROUP BY employeeid
ORDER BY employeeid;

/* OUTPUT THE top-4 employees
WITH THE MAXIMUM ORDERS */ 
SELECT employeeid,COUNT(orderid) 
FROM orders
GROUP BY employeeid
ORDER BY COUNT(orderid) DESC
LIMIT 4;

/* EXPLAIN WHY productids ARE STORED ON DB table order_details
AND NOT ON TABLE orders */

/* JOIN THE TABLES ORDERS AND ORDER DETAILS 
ORDERED BY orderid
*/
SELECT n1.*,n2.*
  FROM order_details n1, orders n2
WHERE n1.orderid=n2.orderid
ORDER BY n1.orderid;

/* CREATE A VIEW FROM THE PREVIOUS QUERY
THAT HAS ALSO THE NAMEOF EACH PRODUCT */
CREATE VIEW ord_orddet_prod AS
SELECT n2.*,n1.productid,n1.unitprice,n1.quantity,n1.discount,
n3.productname
  FROM order_details n1, orders n2,products n3 
WHERE n1.orderid=n2.orderid
AND n1.productid=n3.productid
ORDER BY n1.orderid;


/* USING THE previous view CALCULATE THE REVENUE PER orderid AND productid */  
SELECT * FROM
ord_orddet_prod;

SELECT orderid,productid,unitprice*quantity*(1-discount) AS revenue FROM
ord_orddet_prod;

/* CALCULATE TOTAL REVENUE */
SELECT SUM(unitprice*quantity*(1-discount)) AS total_revenue FROM
ord_orddet_prod;

/* CALCULATE TOTAL REVENUE PER productid */
SELECT productid,SUM(unitprice*quantity*(1-discount)) AS total_revenue FROM
ord_orddet_prod
GROUP BY productid
ORDER BY productid;

/* OUTPUT productid AND productname FOR THE top-10 profitable products */  
SELECT productid,SUM(unitprice*quantity*(1-discount)) AS total_revenue FROM
ord_orddet_prod
GROUP BY productid
ORDER BY SUM(unitprice*quantity*(1-discount)) DESC
LIMIT 10;


/* OUTPUT MAXIMUM TOTAL REVENUE OF PRODUCTS */
SELECT productid,SUM(unitprice*quantity*(1-discount)) AS total_revenue FROM
ord_orddet_prod
GROUP BY productid
ORDER BY SUM(unitprice*quantity*(1-discount)) DESC
LIMIT 1;

SELECT MAX(total_revenue) FROM
(SELECT productid,SUM(unitprice*quantity*(1-discount)) AS total_revenue FROM
ord_orddet_prod
GROUP BY productid) s;


/* SELECT productids WITH NO orders */
INSERT INTO products VALUES('78','Copied Frankfurter grüne Soße',12,2,'12 boxes',13,32,0,15,0);

SELECT  n1.productid,n2.orderid
  FROM products n1 LEFT JOIN order_details n2 ON n1.productid=n2.productid
  WHERE orderid IS NULL;


/* CALCULATE TOTAL REVENUE PER employeeid */
SELECT employeeid,SUM(unitprice*quantity*(1-discount)) AS total_revenue FROM
ord_orddet_prod
GROUP BY employeeid
ORDER BY employeeid;

/* OUTPUT employeeids THAT PRODUCED total_revenue>170000*/
SELECT employeeid,SUM(unitprice*quantity*(1-discount)) AS total_revenue FROM
ord_orddet_prod
GROUP BY employeeid
HAVING SUM(unitprice*quantity*(1-discount))>170000
ORDER BY employeeid;


/* Table required for trigger */
CREATE TABLE product_inserts (
   productid INTEGER,
   productname TEXT,
   inserted_on timestamp NOT NULL DEFAULT now()
);


/* Trigger */
--DROP TABLE product_inserts;
CREATE TABLE product_inserts (
   productid INTEGER,
   productname TEXT,
   inserted_on timestamp NOT NULL DEFAULT now()
);


/* 2nd Trigger */
CREATE OR REPLACE FUNCTION log_products_inserts()
  RETURNS trigger AS
$BODY$
BEGIN
 INSERT INTO product_inserts(productid ,productname)
 VALUES(NEW.productid,NEW.productname);
 
 RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE TRIGGER product_inserts
  AFTER INSERT ON products 
  FOR EACH ROW
  EXECUTE PROCEDURE log_products_inserts();

--DELETE FROM  products WHERE productid=79;
INSERT INTO products VALUES('79','Copied Frankfurter grüne Soße v2',12,2,'12 boxes',13,32,0,15,0);



CREATE TABLE products_audits (
   id SERIAL PRIMARY KEY,
   productid INTEGER NOT NULL,
   productname_before TEXT NOT NULL,
   productname_after TEXT NOT NULL,
   changed_on timestamp NOT NULL DEFAULT now()
);

/* 2nd Trigger */
CREATE OR REPLACE FUNCTION log_products_changes()
  RETURNS trigger AS
$BODY$
BEGIN
 IF NEW.productname <> OLD.productname THEN
 INSERT INTO products_audits(productid,productname_before,productname_after)
 VALUES(OLD.productid,OLD.productname,NEW.productname);
 END IF;
 
 RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE TRIGGER productname_changes
  AFTER UPDATE ON products
  FOR EACH ROW
  EXECUTE PROCEDURE log_products_changes();

UPDATE products SET productname='Dummy' WHERE productid=79;



