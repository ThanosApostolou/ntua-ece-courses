; Read 2 decimals
; calculate sum=x+y (hex)
; if sum>=16 (dec) prin "OVF" else print "SUM="sum
ORG 100H

.DATA
msg1 db 0Dh,0Ah, "ENTER THE INPUT:$"
msg2 db 0Dh,0Ah, "ABSOLUTE: $"
msg3 db 0Dh,0Ah, "$"
A DB 4 DUP(?)         ; array with the 4 numbers

.CODE
START:
CALL PRINT_MSG1 

MOV DI,02h
CALL READ_HEXALS

MOV DL,A[00h]
CALL PRINT_CHAR
MOV DL,' '
CALL PRINT_CHAR
MOV DL,A[01h]
CALL PRINT_CHAR 

MOV DL,A[00H]
CALL ASCII_TO_HEX
MOV DH,DL           ; DH = x

MOV DL,A[01H]
CALL ASCII_TO_HEX   ; DL = y

CMP DH,DL
JGE DH_GREATER_DL
SUB DL,DH
JMP FINISH
DH_GREATER_DL:
SUB DH,DL
MOV DL,DH

FINISH:
CALL PRINT_MSG2

CALL PRINT_HEX

JMP START

GREATER_15:
CALL PRINT_MSG3

JMP START 
END



; Read 2 decimals
; calculate sum=x+y (hex)
; if sum>=16 (dec) prin "OVF" else print "SUM="sum
ORG 100H

.DATA
msg1 db 0Dh,0Ah, "COUNTER=$"
msg2 db 0Dh,0Ah, "$"
msg3 db 0Dh,0Ah, "$"
A DB 4 DUP(?)         ; array with the 4 numbers

.CODE
MOV DX,0000h    ; counter
START:

CALL PRINT_MSG1
MOV AX,DX
CALL HEX2_TO_4DEC
MOV A[03h],CL
MOV A[02h],CH
MOV A[01h],BL
MOV A[00h],BH
MOV DI,04h 
CALL PRINT_NUMBERS

MOV AH,08H
READ_LOOP:
INT 21H       
CMP AL,'T'
JE  END           ; if T is given jump to END
CMP AL,'I'
JE  INCREASE
CMP AL,'D'
JE  DECREASE
JMP READ_LOOP

INCREASE: 
INC DX 
JMP FINISH
 
DECREASE:
CMP DX,00h
JE FINISH
DEC DX

FINISH:
JMP START
END



; Read 2 decimals
; calculate sum=x+y (hex)
; if sum>=16 (dec) prin "OVF" else print "SUM="sum
ORG 100H

.DATA
msg1 db 0Dh,0Ah, "DOSE 2 ARITHMOUS: $"
msg2 db 0Dh,0Ah, "SUM: $"
msg3 db 0Dh,0Ah, "OVF$"
A DB 4 DUP(?)         ; array with the 4 numbers

.CODE
START:
CALL PRINT_MSG1 

MOV DI,02h
CALL READ_DECS

MOV DI,02h
CALL PRINT_CHARS    ; print 2 chars

MOV AL,A[00H]
SUB AL,30H          ; AL = x

MOV AH,A[01H]
SUB AH,30H          ; AH = y

ADD AL,AH           ; AL = x+y

CMP AL,0x10h
JAE GREATER_15
CALL PRINT_MSG2


; Turn AL hex to decimal in BH-BL-AL
CALL HEX_TO_3DEC

MOV A[00h],BL
MOV A[01h],AL
MOV DI,02h
CALL PRINT_NUMBERS
JMP START

GREATER_15:
CALL PRINT_MSG3

JMP START
END



; Read 2 decimals
; calculate sum=x+y (hex)
; if sum>=16 (dec) prin "OVF" else print "SUM="sum
ORG 100H

.DATA
msg1 db 0Dh,0Ah, "DOSE 2 ARITHMOUS: $"
msg2 db 0Dh,0Ah, "SUM: $"
msg3 db 0Dh,0Ah, "OVF$"
A DB 4 DUP(?)         ; array with the 4 numbers

.CODE
START:
CALL PRINT_MSG1 

MOV DI,02h
CALL READ_DECS

MOV DI,02h
CALL PRINT_CHARS    ; print 2 chars

MOV DL,A[00H]
SUB DL,30H          ; AL = x

MOV DH,A[01H]
SUB DH,30H          ; AH = y

ADD DL,DH           ; AL = x+y

CMP DL,0x10h
JAE GREATER_15
CALL PRINT_MSG2

CALL PRINT_HEX

JMP START

GREATER_15:
CALL PRINT_MSG3

JMP START 
END
