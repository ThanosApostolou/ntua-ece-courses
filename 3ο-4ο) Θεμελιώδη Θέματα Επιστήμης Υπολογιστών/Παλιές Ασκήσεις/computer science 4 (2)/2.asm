;We need to compute (XOR(R1,R2))
;   XOR(R1,R2)=       OR(    AND(NOT(R1),R2),     AND(R1,NOT(R2))) =
;             =       NOT(AND(NOT(AND(NOT(R1),R2)),NOT(AND(R1,NOT(R2)))))


                         .ORIG X3000
                        LD R0,NUM   ;for the time being,we have to load the values.
                        LD R1,NUM2  
                        NOT	R3, R0		; R2 = X'
			AND	R3, R3, R1	; R2 = X'Y
			NOT	R1, R1		; R1 = Y'
			AND	R1, R1, R0	; R1 = XY'
			NOT	R3, R3		; R2 = NOT(X'Y)
			NOT	R1, R1		; R1 = NOT(XY')
			AND	R3, R3, R1	; R2 = (X'Y)'*(XY')'
			NOT	R3, R3		; R2 = ( (X'Y)' (XY')' )'

NUM .FILL #34  ;random values
NUM2 .FILL #28

        HALT
.end