;1.In the beginning,we decide whether the devision is possible or not with the BRZ command after R1.
 ;    According to that decision,we set R2 to 0 (IMPOSSIBLE) or to 1
;2.If the devision is possible,we have to decide whether the devided number is positive-zero or negative.
;3.If it is negative,we go to NEG and make it zero-positive by applying the XmodK=(X +n*K)modK relationship.
;       To be more specific,we begin to increase R0 by R1 until it is >=0
;4.Once we have a positive devided number,we store -R4 to R1.We use R3 to temporary reduce R0
;5.We continuously decrease R0 via R3,until R3 becomes negative.At that point,the value left to R0 is the modula.


.ORIG X3000
       AND R2,R2,#0   ;initialize resister R2 to 0
       LD R1,NUM2     ;we must give a value to R1.We won't need that for the final program though.
       BRZ IMPOSSIBLE ;if the devider is 0,then impossible devision
       ADD R2,R2,#1   ;R2=1,which means normal devision
       LD R0,NUM      ;We must give a value to R0
       BRN NEG        ;check for negative case
 POS   NOT R4,R1      ;Once we have possitive mumber to be devided,
       ADD R4,R4,#1   ;R4=-R1       
LOOP   ADD R3,R0,R4   ;R3=R0-R1 .With R3 we investigate if it is possible to reduce R0 without becoming negative
       BRN FINISH  
       ADD R0,R3,#0   ;R0=R0-R1 .We can afford to reduce R0.
       BRNZP LOOP     ;if R0 is still possitive,keep on decreasing it

IMPOSSIBLE  AND R2,R2,#0  ;R0=0 ,which means impossible devision
            BRNZP FINISH

FINISH  ADD R1,R0,#0  ;R1 must have the result,so we copy it from R0.
        HALT

NEG  ADD R0,R0,R1  ;R0=R0+R1, because XmodK=(X +n*K)modK .We increase n step by step,starting from n=1
     BRZP POS
     BRNZP NEG

NUM    .FILL #-4    ;random numbers.We won't need them later for the complete program.
NUM2   .FILL #26

 .END


;NOTE 1:  I take for granted the fact that R1 is positive.If we deny that fact,we can simply  
;         make the following changes to the code:
;         .....
;        AND R5,R5,#0  0 means positive,1 negative
;        AND R6,R6,#0
;        ADD R1,R1,#0
;        BRN L2        
;        .....
;        L4 ...
;        .....
;        ADD R0,RO,#0
;        BRN L3
;        .....
;  L2   ADD R5,R5,#1
;       BRNZP L4
;       .....
; L3    ADD R6,R6,#1
;       BRNZP L5
;       ....
;    L5 ...
;      ....
;     NOT R5,R5
;     ADD R5,R5,#1
;     ADD R5,R5,R6  if R6-R5=0,then we have either 2 positive numbers or 2 negative numbers
;     BRNP NEG
;     BRZNP POS
;     ......      



;NOTE 2:This program receives input from NUM and NUM2.I made it that way so that it can function as an independant program



;NOTE 3:As a fanatical advocate of roman civilization an a leading member of the International Roman Community,
;please allow me to pay my depts to the greatest men the world has ever seen!
;HAIL CAESAR!

