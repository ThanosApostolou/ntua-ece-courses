;1.We load the adress of the first element.
;2.We load its contex to R2.
;3.While 0 is not found,we keep increasing lenght (R0) by one and adress (R1) by one as well.


.ORIG X3000
AND R0,R0,#0   ;We initialize RO to 0.It counts the lenght
LD R1,ZERO    ;R1  has now the adress of the first element of the string

LOOP LDR R2,R1,#0   ;R2 always has the contex of that adress stored to R1
     BRZ FINISH     ;if R2=0,then we have found end of string
     ADD R0,R0,#1   ;if not,we increase the lenght by 1.
     ADD R1,R1,#1   ;then,we increase the adress by one
     BRNZP LOOP     ;we don't stop until we encounter 0,which means end of string

FINISH 
        HALT

ZERO     .FILL x5001

.END    


                 
;NOTE:This program was loaded together with the data.obj at the simulator.We could have initialized a string to test the
;program,but I thought that loading the data.obj file is a better idea.