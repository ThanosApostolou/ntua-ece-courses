;1.We load the adress of the first element.
;2.We store the number of elements that must be read till the end to R4.This data always remains to R4,
  ; with a brief interuption during PROG1. COUNT is used to temporary store that data.
;3.We use LOOP2 until we read all the elements.
;4.While in LOOP2,we use PROG3 to count the length of each string.This length will be our key for that string.
 ;  At the same time,we find the end of the string with PROG3 and store it to TEMP
;5.We begin the decoding process with LOOP3.R6 loads the KEY and then acts as counter.We will decode the same number
; of characters that we read,R6.Of course,if R6=0,there is no decoding to be done.
;6.We use XOR decoding (PROG2)
;7.After we reduce the number by 65,we use Caesar's decoding.(PROG1).Then,we add 65 again.This value is the decoded result.
;8.We print the character,reduce the counter and load the next adress.
;9.Once we decode all characters of the string,we move to L23.
;10.We use TEMP to load the adress of the last element that we have already read.Then,we increase it by one.The result is
; the beginning of the new string.Simultaneously,we load EMPTYSPACE to print the ' ' character,in order to make strings
;descernible.
;11.We return to LOOP2 and repeat the following steps.We won't stop until all elements are read.
;12.More details about PROG1,PROG2 and PROG3 can be found at the previous programs.





.ORIG X3000
 LD R5,DATA                             ;R5 stores the adress of the beginning of the data file
  LD  R4,COUNT                          ;R4 has the number of elements that have to be read till the end                                   
      LOOP2 ADD R1,R5,#0
            LDR R0,R5,#0                ;R5 should contain the beginning of the word we are decrypting
            JSR PROG3                   ;PROG3 counts the lenght.R1 is set to the last element,R0 has the length
            ST R1,TEMP                  ;However,we need R5,so we store it to TEMP instead.
            ADD R6,R0,#0                ;R6 stores temporary the lenght,in order to act as up-down counter.
            ST R6,KEY                   ;KEY stores the lenght of the string we are decrypting each time.
            ADD R6,R0,#0                ;R6 will now act as a counter
            BRZ L23                     ;if the counter is 0,it is time to move to the next string.
            BRNZP LOOP3




            LOOP3
               LDR R0,R5,#0     ;We start decoding the letters of the string one by one.
                 LD R1,KEY      ;R1 has now the key
                 JSR PROG2      ;R3 has the result of the XOR decrytion
                 LD R2,KEY
                 NOT R2,R2     
                 ADD R2,R2,#1  ;R2=-KEY 
                 LD R1,NUM     ;We set the devider's value to 26,because we have 26 letters.
                 ADD R0,R3,R2  ;R0=R3-KEY,because result=(oldnumber-key)mod26
                 ST R4,TEMP2   ;PROG1 uses R4,so we must make sure that its value isn't lost.
                 LD R4,EX1     ;We reduce R0 by 65,because of the ASCII capital letters.
                 ADD R0,R0,R4
                 JSR PROG1     ;R1 has the result of Ceasar's decryption
                 ADD R0,R1,#0  ;The output must be tranfered to RO so that it can be print.
                 LD R4,EX2     ;To get the real output though,we need to add the 65 value that we reduced.
                 ADD R0,R0,R4  ;add 65
                 LD R4,TEMP2   ;R4 gets back its desired value.
                 TRAP X21      ;print letter

                 ADD R6,R6,#-1 ;reduce counter
                 BRZ L23       ;We return to LOOP2 after we update a few things,if counter=0.
                 ADD R5,R5,#1  ;If the counter is not 0,we move to next adress.
                 BRNZP LOOP3

L23  LD R5,TEMP                ;R5 now instead of the beginning of the string that we just decrypted
     ADD R5,R5,#1              ;contains the first element of the new string.
     LD R0,EMPTYSPACE          ;We print an empty space between the words
     TRAP X21           
     BRNZP LOOP2               ;We return to LOOP2 


PROG3     AND R0,R0,#0   ;R0 has the output(lenght)
          REP3     LDR R2,R1,#0   ;R2 has the contex of that adress
                   BRZ FIN3       ;if R2=0,then we have found end of string
                   ADD R4,R4,#-1
                    BRZ FINISH
                   ADD R0,R0,#1   ;if not,increase the lenght by 1.
                   ADD R1,R1,#1   ;increase the adress by one
                   BRNZP REP3

FIN3  ADD R4,R4,#-1
      BRZ FINISH
      RET


PROG2     ;XOR decrytion.R3 has the result in the end.R0 is the decrypted number and R1 is the key.
                     
                        NOT	R3, R0		; R2 = A'
			AND	R3, R3, R1	; R2 = A'B
			NOT	R1, R1		; R1 = B'
			AND	R1, R1, R0	; R1 = AB'
			NOT	R3, R3		; R2 = NOT(A'B)
			NOT	R1, R1		; R1 = NOT(AB')
			AND	R3, R3, R1	; R2 = (A'B)'*(AB')'
			NOT	R3, R3		; R2 = ( (A'B)' (AB')' )'
                        RET


PROG1                     ;Caesar's decryption.R0 is the decrypted number from XOR reduced by the key.       
         ADD R1,R1,#0     ;R1 is equal to 26.In the end,the encrypted number is stored to R1.
         BRZ IMPOSSIBLE   ;It is importnant to notice that before PROG1 we reduce the value by 65 and
         AND R2,R2,#0     ;after its execution we increase it by 65.
         ADD R2,R2,#1   
         ADD R0,R0,#0
         BRN NEG        
 POS        NOT R4,R1     
            ADD R4,R4,#1      
REP1             ADD R3,R0,R4 
                 BRN FIN1 
                 ADD R0,R3,#0   
                 BRNZP REP1   

IMPOSSIBLE  AND R2,R2,#0  
            BRNZP FINISH
FIN1  ADD R1,R0,#0  
        RET

NEG  ADD R0,R0,R1  
     BRZP POS
     BRNZP NEG


FINISH  HALT

DATA .FILL X5001         ;beginning of file 
TEMP .FILL X4000         ;TEMP is used to store the the end of each string.
KEY .FILL X3000          ;Key is used to save the key-length of each word.
COUNT .FILL X5000        ;number of elements to read before end
TEMP2 .FILL X4200        ;Used to temporary store R4's contex.
EMPTYSPACE .FILL #32     ;Used to produce empty space.
NUM   .FILL #26          ;Used to load the 26 value
EX1  .FILL #-65          ;Used to load the -65 value
EX2  .FILL #65           ;Used to load the +65 value
.END


   


                 
            
            
            
            
            
     



