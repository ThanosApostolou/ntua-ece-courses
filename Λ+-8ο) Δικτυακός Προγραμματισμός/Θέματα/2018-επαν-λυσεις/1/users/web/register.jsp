<%@ page import="java.sql.*" %>
<html>
<head>
	<title>Register</title>
</head>
<body>
	<%
		String newUser = request.getParameter("newuser");
		String username = request.getParameter("username");
		String fullname = request.getParameter("fullname");
		String birth = request.getParameter("birth");
		String address = request.getParameter("address");
		if (newUser == null) {
			// already filled and submitted the register form
			try (Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/account", "root", "")) {
				Statement stmt = con.createStatement();
				String sql = "INSERT INTO selection (username, fullname, birth, address) VALUES (\""
					+ username + "\", \"" + fullname + "\", \"" + birth + "\", \"" + address + "\");";
				stmt.executeUpdate(sql);
				response.sendRedirect("login.jsp?username=" + username);
			}
		}
	%>
	<form method="get">
		Username: <input type="text" name="username" value="<%= newUser %>"/><br/>
		Full Name: <input type="text" name="fullname"/><br/>
		Birth Date: <input type="text" name="birth"/><br/>
		Address: <input type="text" name="address"/><br/>
		<input type="submit" value="Register"/>
	</form>
</body>
</html>
