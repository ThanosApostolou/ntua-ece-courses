<%@ page import="java.sql.*" %>
<html>
<head>
	<title>Update</title>
	<script>
	function fullFocus() {
	 document.getElementById("fullname").innerHTML = "<%= session.getAttribute("fullname") %>";
	}
	
	function fullBlur() {
	 document.getElementById("fullname").innerHTML = "";
	}
	
	function birthFocus() {
	 document.getElementById("birth").innerHTML = "<%= session.getAttribute("birth") %>";
	}
	
	function birthBlur() {
	 document.getElementById("birth").innerHTML = "";
	}
	
	function addrFocus() {
	 document.getElementById("address").innerHTML = "<%= session.getAttribute("address") %>";
	}
	
	function addrBlur() {
	 document.getElementById("address").innerHTML = "";
	}
	
	</script
</head>
<body>
	<%
	String fullname = request.getParameter("fullname");
	String birth = request.getParameter("birth");
	String address = request.getParameter("address");
	if (fullname != null && birth != null && address != null) {
		try (Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/account", "root", "")) {
			Statement stmt = con.createStatement();
			String sql = "UPDATE selection SET address=\"" + address
				+ "\", fullname=\"" + fullname
				+ "\", birth=\"" + birth
				+ "\" WHERE username=\"" + session.getAttribute("username") +"\";";
			stmt.executeUpdate(sql);
			response.sendRedirect("login.jsp?username=" + session.getAttribute("username"));
		}
	}
	%>
	<form>
		Full Name: <input type="text" name="fullname" onfocus="fullFocus()" onblur="fullBlur()"/><span id="fullname"></span></br>
		Birth Date: <input type="text" name="birth" onfocus="birthFocus()" onblur="birthBlur()"/><span id="birth"></span></br>
		Address: <input type="text" name="address" onfocus="addrFocus()" onblur="addrBlur()"/><span id="address"></span></br>
		<input type="submit" name="Submit"/>
	</form>
</body>
</html>
