<%@ page import="java.sql.*,java.util.*" %>
<html>
<head>
	<title>Statistics</title>
</head>
<body>
	<h3>Grade statistics:</h3>
	<%
	try (Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/test", "root", "")) {
		Statement stmt = con.createStatement();
		String sqlA = "SELECT * FROM students where grade=\"A\";";
		String sqlB = "SELECT * FROM students where grade=\"B\";";
		String sqlC = "SELECT * FROM students where grade=\"C\";";
		int As = 0, Bs = 0, Cs = 0;
		ArrayList<String> namesA = new ArrayList<String>();
		ArrayList<String> namesB = new ArrayList<String>();
		ArrayList<String> namesC = new ArrayList<String>();
		for (ResultSet rs = stmt.executeQuery(sqlA); rs.next(); As++)
			namesA.add(rs.getString("name"));
		for (ResultSet rs = stmt.executeQuery(sqlB); rs.next(); Bs++)
			namesB.add(rs.getString("name"));
		for (ResultSet rs = stmt.executeQuery(sqlC); rs.next(); Cs++)
			namesC.add(rs.getString("name"));
		%>
		<form method="get" action="statistics.jsp">
		Number of "A"s: <%= As %>
			<input type="hidden" name="grade" value="A"/>
			<input type="submit" value="Show students">
		</form>
		<br>
		<form method="get" action="statistics.jsp">
		Number of "B"s: <%= Bs %>
			<input type="hidden" name="grade" value="B"/>
			<input type="submit" value="Show students">
		</form>
		<br>
		<form method="get" action="statistics.jsp">
		Number of "C"s: <%= Cs %>
			<input type="hidden" name="grade" value="C"/>
			<input type="submit" value="Show students">
		</form>
		<br>
		<%
		String grade = request.getParameter("grade");
		ArrayList<String> names = null;
		if (grade != null) {
			if (grade.equals("A"))
				names = namesA;
			else if (grade.equals("B"))
				names = namesB;
			else if (grade.equals("C"))
				names = namesC;
		}
		if (names != null) {
			%>
			<h3>Students with grade "<%= grade %>":</h3>
			<ul>
			<%
			for (String name: names) {
				%>
				<li><%= name %></li>
				<%
			}
			%>
			</ul>
			<%
		}
	}
	%>
</body>
</html>
