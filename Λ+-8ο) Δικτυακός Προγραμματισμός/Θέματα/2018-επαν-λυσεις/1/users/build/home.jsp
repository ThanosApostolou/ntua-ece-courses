<%@ page import="java.sql.*" %>
<html>
<head>
	<title>Home</title>
	<script>
	function login() {
		var user = document.getElementById("username").value;
		if (user == "admin") {
			window.location.href = "admin.jsp";
		} else {
			window.location.href = "vote.jsp?user=" + user;
		}
	 	return false;
	}
	</script>
</head>
<body>
	<h3>Poll text:</h3>
	<%
	try (Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/polls", "root", "")) {
		Statement stmt = con.createStatement();

		String newText = request.getParameter("text");
		if (newText != null) {
			String sqlDelPoll = "DELETE from poll;";
			String sqlDelPart = "DELETE from participation;";
			String sqlUpdPoll = "INSERT INTO poll (text) VALUES (\"" + newText + "\");";
			stmt.executeUpdate(sqlDelPoll);
			stmt.executeUpdate(sqlDelPart);
			stmt.executeUpdate(sqlUpdPoll);
		}

		String sql = "SELECT * FROM poll;";
		ResultSet rs = stmt.executeQuery(sql);
		rs.next();
		String text = rs.getString("text");
		%>
		<%= text %>
		<h3>Participation:</h3>
		<%
		String sqlYes = "SELECT * FROM participation WHERE voted=\"yes\";";
		String sqlNo = "SELECT * FROM participation WHERE voted=\"no\";";
		int yes = 0, no = 0;
		for (rs = stmt.executeQuery(sqlYes); rs.next(); yes++)
			;
		for (rs = stmt.executeQuery(sqlNo); rs.next(); no++)
			;
		%>
		<ul>
			<li>"Yes": <%= yes %></li>
			<li>"No": <%= no %></li>
		</ul>
		<%
	}
	%>
	<form onsubmit="return login();">
		Username: <input type="text" id="username"><br>
		<input type="submit" value="Log in">
	</form>
</body>
</html>
