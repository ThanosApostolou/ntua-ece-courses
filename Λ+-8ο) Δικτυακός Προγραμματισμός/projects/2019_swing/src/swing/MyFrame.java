package swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class MyFrame extends JFrame implements KeyListener {
	Canvas canvas = new Canvas();

	public Canvas getCanvas() {
		return canvas;
	}

	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}
	public static void main(String[] args) {
		new MyFrame();

	}

	public MyFrame() throws HeadlessException {
		super("This is my frame");
		setVisible(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getContentPane().setBackground(Color.black);
		addKeyListener(this);
		getContentPane().add(canvas);
		
		pack();
	}
	
	public void keyTyped(KeyEvent e) {
		if(e.getKeyChar() == 'T') {
			Random r = new Random();
			int col = r.nextInt(canvas.columns);
			canvas.depth[col] -= 4;
			int x = col*4;
			int y=0;
			
			Shape newShape = new Shape(x, y, canvas.depth[col]);
			canvas.shapes.add(newShape);
			MyThread t = new MyThread(this.canvas, newShape);
			t.start();
			canvas.repaint();
		} else if (e.getKeyChar() == 'S') {
			System.out.println(""+canvas.max);
		}
    }
    public void keyPressed(KeyEvent e) {
    }
    public void keyReleased(KeyEvent e) {
    }  
}
