package swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.event.MouseEvent;
import java.util.Random;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MyFrame extends JFrame  implements KeyListener {
	private static final long serialVersionUID = 1L;
	Canvas canvas = new Canvas();
	int msec = 100;
	int cols = 500 / 4;

	public Canvas getCanvas() {
		return canvas;
	}

	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}
	public static void main(String[] args) {
		new MyFrame();
	}

	public MyFrame() throws HeadlessException {
		super("This is my frame");
		getContentPane().setBackground(Color.black);
		addKeyListener(this);

		setVisible(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getContentPane().add(canvas);

		pack();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getKeyChar()=='T') {
			Random r = new Random();
			int n = r.nextInt(cols);
			MyThread t = new MyThread(this.canvas, n * 4, 0, msec);
			canvas.addThread(t);
			canvas.repaint();
		}
		if (e.getKeyChar()=='S') {
			int max = 500;
			for (MyThread t: canvas.threads) {
				if (t.y < max) max = t.y;
			}
			System.out.println("MAX HEIGHT:" + (500 - max));
		}
	}
}