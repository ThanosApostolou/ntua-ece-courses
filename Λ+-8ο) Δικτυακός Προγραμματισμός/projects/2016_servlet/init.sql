drop database if exists exams2016;
create database exams2016;
use exams2016;
CREATE TABLE students (name varchar(45) NOT NULL, grade varchar(1), id int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY);
INSERT INTO students (name, grade) VALUES
('thanos1','A'),
('thanos2','A'),
('maria','A'),
('thanos3','B'),
('thanos4','B'),
('george',null);
