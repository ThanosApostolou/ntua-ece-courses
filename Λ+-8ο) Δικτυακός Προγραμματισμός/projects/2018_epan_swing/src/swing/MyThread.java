package swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class MyThread extends Thread {

	public final Canvas canvas;
	Shape s;
	int msec=0;
	int step=10;

	public MyThread(Canvas canvas, Shape s, int msec) {
		this.canvas = canvas;
		this.s = s;
		this.msec = msec;
		this.setDaemon(true);
	}
	public void run(){
		while (true){
			try {
				// System.out.println("going to sleep for " + msec);
				sleep(msec);
			} catch (InterruptedException e) {
				System.out.println("sleep interrupted");
				e.printStackTrace();
			}
			// move right
			s.x += step;
			// change colors if collision
			for (int i=0; i<canvas.shapes.size(); i++) {
				Shape s1 = canvas.shapes.get(i);
				boolean collision=false;
				for (int j=0; j<canvas.shapes.size(); j++) {
					if (i != j ) {
						Shape s2 = canvas.shapes.get(j);
						if (collides(s1, s2)) {
							collision=true;
							s2.color=Color.red;
						}
					}
				}
				if (collision) {
					s1.color=Color.red;
				} else {
					s1.color=Color.black;
				}
			}
			canvas.repaint();
		}
	}
	
	boolean collides (Shape s1, Shape s2) {
		int xdistance=s1.x - s2.x;
		int ydistance=s1.y - s2.y;
		if (Math.sqrt(xdistance * xdistance + ydistance * ydistance) < 2 * s1.rad) {
			return true;
		} else {
			return false;
		}
	}
}
