#include <iostream>
#include <cstdio>
#include <cstring>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define B 256
#define W 1031

using namespace std;

class list_node
{
    int line;
    int position;

public:
    list_node *next;

    void  set_node(int a,int b)
    {
        line=a;
        position=b;
    }
    void print_node()
    {
        printf("(%d,%d)\n",line,position);
    }
    list_node()
    {
        next=NULL;
    }
};

class list
{

    list_node *list_head;
public:
    void add_node(int a,int b);
    void print_list();
    void erase();
    list();
    ~list();
};

void list::add_node(int a,int b)
{
    list_node *neos;
    neos=new(nothrow) list_node;
    if(neos==NULL)
    {
        printf("Not Enough Space in List");
        exit(1);
    }
    neos->set_node(a,b);
    neos->next=NULL;
    list_node *temp;
    temp=list_head;
    if(temp==NULL)
    {
        list_head=neos;
    }
    else
    {
        while(temp->next!=NULL) temp=temp->next;
        temp->next=neos;
    }
    return;
}
void list::print_list()
{
    list_node *temp;
    temp=list_head;
    while(temp!=NULL)
    {
        temp->print_node();
        temp=temp->next;
    }
}
list::list()
{
    list_head=NULL;
}
list::~list()
{
    list_node *temp;
    temp=list_head;
    while(list_head!=NULL)
    {
        temp=list_head->next;
        delete list_head;
        list_head=temp;
    }
}
void list::erase()
{

    list_node *temp;
    temp=list_head;
    while(list_head!=NULL)
    {
        temp=list_head->next;
        delete list_head;
        list_head=temp;
    }
}


//dn exw valei akoma tree_delete
class tree_node
{
public:
    char word[21];
    list lista;
    tree_node *left;
    tree_node *right;
    void add_value(int a,int b)
    {
        lista.add_node(a,b);
    }
};
class tree

{


    tree_node *root;
public:
    tree()
    {
        root=NULL;
    }
    tree_node *newnode(char data[],int a,int b);
    tree_node *find_node(char data[]);
    bool add_node(char data[],int a,int b);
    void print_node(char data[]);


};

tree_node* tree::newnode(char data[],int a,int b)
{
    tree_node *neos;
    neos = new(nothrow) tree_node;
    if(neos==NULL)
    {
        printf("Not Enough Space in Tree");
        exit(1);
    }
    strcpy(neos->word,data);
    neos->add_value(a,b);
    neos->left=NULL;
    neos->right=NULL;
    return(neos);
}
tree_node* tree::find_node(char data[])
{
    tree_node *current;
    current=root;
    if(root==NULL) return NULL;
    while(strcmp(current->word,data)!=0)
    {
        if(strcmp(current->word,data)>0)
            current=current->left;
        else
            current=current->right;
        if(current==NULL)
            return NULL;
    }
    return current;
}

bool tree::add_node(char data[],int a,int b)
{

    tree_node *temp;
    temp=find_node(data);
    if(temp==NULL)
    {
        tree_node *next, *current, *ptr;
        int isleft;
        next=root;
        current=root;
        ptr=newnode(data,a,b);
        if(root==NULL)
        {
            root=ptr;
            return true;
        }
        while(1)
        {
            if(strcmp(data,current->word)<0)
            {
                next=current->left;
                isleft=1;
            }
            else
            {
                next=current->right;
                isleft=0;
            }
            if(next==NULL)
            {
                if(isleft)
                    current->left=ptr;
                else
                    current->right=ptr;
                return true;
            }
            current=next;
        }
    }
    else
        temp->add_value(a,b);

}
void tree::print_node(char data[])
{
    tree_node *temp;
    temp=find_node(data);
    printf("%s\n",data);
    if(temp==NULL)
        printf("NOT FOUND\n");

    else temp->lista.print_list();
    return;
}

class hash_table
{

public:
    tree *table;
    hash_table(int a);
    void add_data(char data[],int a,int b);
    void find_word(char data[]);
    int position(char data[]);

};
hash_table::hash_table(int a)
{
    table= new tree[a];
}

void hash_table::add_data(char data[],int a,int b)
{
    table[position(data)].add_node(data,a,b);
}
void hash_table::find_word(char data[])
{
    table[position(data)].print_node(data);
}

int hash_table::position(char data[])
{
    int i, s=0, b=1;
    for(i=strlen(data)-1; i>=0; i--)
    {
        s+=data[i]*b;
        b=(b*B)%W;
    }
    s=s%W;
    return s;
}


int main(void)
{
    char line[150];
    char keyword[25];
    hash_table pinakas(W);
    scanf("%s",keyword);
    while (getchar() != '\n');
    int count_line=0;
    char *delims = " \n\t";
    char *ptr;
    while(gets(line)!=NULL)
    {
        count_line++;
        ptr=strtok(line,delims);
        while(ptr!=NULL)
        {
            pinakas.add_data(ptr,count_line,(int)(ptr-line)+1);
            ptr=strtok(NULL,delims);
        }
    }
    pinakas.find_word(keyword);
    return 0;
}

