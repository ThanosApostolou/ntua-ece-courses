#include <stdlib.h>
#include <stdio.h>

typedef struct node{
  long long int s;
  long long int f;
  //long long int time;
  long long int pour;
} source;

source sources[100000] = {{0}};
long long int volume[100000000] = {0};

void swap(long long int i, long long int j){
	source temp;
	
	temp = sources[i];
	sources[i] = sources[j];
	sources[j] = temp;  
}

int partition(long long int left,long long int right){
	long long int pivot, i, j;
	
	pivot = sources[left].s;
	i = left-1;
	j = right + 1;
	
	while(1){
		while(sources[++i].s < pivot);
		while(sources[--j].s > pivot);
		if (i < j){
			swap(i,j);
		}
		else return j;
	}
}

int partition2(long long int left,long long int right){
	long long int pivot, i, j;
	
	pivot = sources[left].f;
	i = left-1;
	j = right + 1;
	
	while(1){
		while(sources[++i].f < pivot);
		while(sources[--j].f > pivot);
		if (i < j){
			swap(i,j);
		}
		else return j;
	}
}

void quicksort(long long int left,long long int right){
	long long int q;
	
	if (left>=right) return; //1 element at most
	q = partition(left, right);
	quicksort(left, q);
	quicksort(q+1, right);
}

void quicksort2(long long int left,long long int right){
	long long int q;
	
	if (left>=right) return; //1 element at most
	q = partition2(left, right);
	quicksort2(left, q);
	quicksort2(q+1, right);
}

int main(int argc, char *argv[]){
	FILE *f;
	long long int N, K, i, temp1, temp2,maxf, time;
  
	/*open input file*/
	f = fopen(argv[1],"r");
	if (f == NULL){
		perror(argv[1]);
		exit(1);
	}
	
	fscanf(f, "%lld %lld", &N, &K);
	//printf("N = %lld, K= %lld\n", N, K);
	maxf = 0;
	for (i=0; i<N; i++){
		fscanf(f, "%lld %lld", &sources[i].s, &sources[i].f);
		if (sources[i].f > maxf) maxf = sources[i].f
		//sources[i].time = sources[i].f - sources[i].s;
		sources[i].pour = sources[i].time + 1;
		//printf("source %lld starts pouring at %lld and stops at %lld. Total time = %lld and quantity = %lld.\n", i, sources[i].s, sources[i].f, sources[i].time, sources[i].pour);
	}
	
	quicksort(0, N-1);
	/*for (i=0; i<N; i++){
		printf("source %lld starts pouring at %lld and stops at %lld. Total time = %lld and quantity = %lld.\n", i, sources[i].s, sources[i].f, sources[i].time, sources[i].pour);
	}*/
	for (i=0; i<N; i++){
		temp1 = sources[i].s;
		temp2 = 0;
		while(sources[i].s == temp1){ temp2++; i++;}
		if (temp2>0) quicksort2(i-temp2, i-1);
		i = i-1;
	}
	/*for (i=0; i<N; i++){
		printf("source %lld starts pouring at %lld and stops at %lld. Total time = %lld and quantity = %lld.\n", i, sources[i].s, sources[i].f, sources[i].time, sources[i].pour);
	}*/
	
	time = 0;
	i = 0;
	while (i <= maxf){
		
		while (temp1<N){
			if (sources[temp1].s<= i && sources[temp1].f>=i){
				volume[i] += volume[i-1] + 1;
			}
			temp1++;
		}
		i++;
	}
	
	
  
	return(0);
}