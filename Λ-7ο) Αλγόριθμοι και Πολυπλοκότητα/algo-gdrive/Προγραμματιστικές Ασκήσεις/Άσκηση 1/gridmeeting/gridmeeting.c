#include <stdio.h>
#include <stdlib.h>

struct dimensions{
	long long int x;
	long long int y;
}; typedef struct dimensions house;

house houses[500000] = {{0}};
long long int r[500000] = {0};
long long int s[500000] = {0};

int partition(long long int left,long long int right){
	long long int pivot, i, j;
	house temp;
	
	pivot = houses[left].x;
	i = left-1;
	j = right + 1;
	
	while(1){
		while(houses[++i].x < pivot);
		while(houses[--j].x > pivot);
		if (i < j) {
			temp = houses[i];
			houses[i] = houses[j];
			houses[j] = temp;
		}
		else return j;
	}
}

int partition2(long long int left, long long int right){
	long long int pivot, i, j;
	house temp;
	long long int temp1;
	
	pivot = houses[left].y;
	i = left-1;
	j = right + 1;

	while(1){
		while(houses[++i].y < pivot);
		while(houses[--j].y > pivot);
		if (i < j) {
			temp = houses[i];
			houses[i] = houses[j];
			houses[j] = temp;
		/*
		 *Swap the array of distance's sums, 
		 *witch currently has only the distances
		 *from x axis, accordingly so that x and y
		 *match on the sums (manhattan distance)
		 */
			temp1 = s[i];
			s[i] = s[j];
			s[j] = temp1;
		}
		else return j;
	}
}

void quicksort(long long int left,long long int right){
	long long int q;
	
	//printf("inside quicksort\n");
	if (left>=right) return; //1 element at most
	q = partition(left, right);
	//printf("inside quicksort after partition q = %d\n", q);
	quicksort(left, q);
	quicksort(q+1, right);
}

void quicksort2(long long int left, long long int right){
	long long int q;
	
	//printf("inside quicksort\n");
	if (left>=right) return; //1 element at most
	q = partition2(left, right);
	//printf("inside quicksort after partition q = %d\n", q);
	quicksort2(left, q);
	quicksort2(q+1, right);
}

int main(int argc, char *argv[])
{
	long long int N, i, j;
	long long int min;
	
	if (scanf("%lld", &N)<0){
		printf("input failed\n");
		exit(1);
	}//the number of student's houses
	
	//insert coordinates of each house in an array of type houses
	for(i=0; i < N; i++){
		if (scanf("%lld %lld", &houses[i].x, &houses[i].y)<0){
			printf("input failed\n");
			exit(1);
		}
	}
	//sort the array houses for x
	quicksort(0, N-1);
	
	/*
	 * Distances on axis x from each house
	 */
	r[0] = 0;
	s[0] = 0;
	for (i = 1; i < N; i++){//from left to right
		r[i] = r[i-1] + i*(houses[i].x - houses[i-1].x);
		s[i] = r[i];
	}
	r[N-1] = 0;
	j = 1;
	for (i = N-2; i>=0; --i){//from right to left
		r[i] = j*(houses[i+1].x - houses[i].x) + r[i+1];
		s[i] += r[i];
		j++;
	}
	
	//sort the array houses for y
	quicksort2(0, N-1);
	
	/*
	 * Distances on axis y from each house 
	 */
	r[0] = 0;
	for (i = 1; i < N; i++){//from left to right
		r[i] = r[i-1] + i*(houses[i].y - houses[i-1].y);
		s[i] += r[i];//add y distances to previous x accordingly (manhattan distance)
	}
	r[N-1] = 0;
	j = 1;
	min = s[N-1];
	for (i = N-2; i>=0; --i){//from right to left
		r[i] = j*(houses[i+1].y - houses[i].y) + r[i+1];
		s[i] += r[i];//add y distances to previous x accordingly (manhattan distance)
		j++;
		if (s[i]<min) min = s[i];//find minimum distance
	}
	
	printf("%lld\n", min);
	
	return 0;
}