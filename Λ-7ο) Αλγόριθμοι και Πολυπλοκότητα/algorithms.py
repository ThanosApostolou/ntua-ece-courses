#!/bin/env python3

import math
import heapq

def mergesort(arr):
    def merge(arr, L, R) :
        i = j = k = 0
        # Copy data to temp arrays L[] and R[]
        while i < len(L) and j < len(R):
            if L[i] < R[j]:
                arr[k] = L[i]
                i+=1
            else:
                arr[k] = R[j]
                j+=1
            k+=1
        # Checking if any element was left
        while i < len(L):
            arr[k] = L[i]
            i+=1
            k+=1
        while j < len(R):
            arr[k] = R[j]
            j+=1
            k+=1
        return (arr)

    if len(arr)>1:
        mid = len(arr)//2 #Finding the mid of the array
        L = arr[:mid] # Dividing the array elements
        R = arr[mid:] # into 2 halves
        mergesort(L) # Sorting the first half
        mergesort(R) # Sorting the second half
        return merge(arr, L, R)

def binary_search (arr, item) :
    def inner_binary_search (l, r) :
        m = (l+r)//2
        if  r<l  :
            return (None)
        if item == arr[m] :
            return m
        elif item < arr[m] :
            return inner_binary_search(l, m-1)
        else :
            return inner_binary_search(m+1, r)
    return (inner_binary_search(0, len(arr)-1))

def print_tree(arr) :
    n = len(arr)
    h = int(math.log(n,2))
    index=0
    for i in range (1, h+2):
        for j in  range(2**i) :
            if (index < n) :
                print (arr[index], end=" ")
            index+=1
        print()

# driver
input = [1,2,10,3,5,3,8,11,13,12,12,12]
output = input.copy()
output = mergesort(output)
print ("input: ", input)
print ("output: ", output)

heapq.heapify(input)
print("heap: ", input)

print (binary_search(input, 10))

print_tree(input)