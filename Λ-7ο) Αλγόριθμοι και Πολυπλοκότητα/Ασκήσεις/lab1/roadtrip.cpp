#include <iostream>
#include <algorithm>
#include <vector>

int N,K,D,T;
std::vector<int> P_table, J_table, K_table;
int Ts,Cs,Tf,Cf;

void read_input () {
	std::cin >> N >> K >> D >> T;
	int temp1;
	int temp2;
	for (int i=0; i<N; i++) {
		std::cin >> temp1 >> temp2;
		P_table.push_back (temp1);
		J_table.push_back (temp2);
	}
	for (int i=0; i<K; i++) {
		std::cin >> temp1;
		K_table.push_back (temp1);
	}
	std::cin >> Ts >> Cs >> Tf >> Cf;
}

void print_variables () {
	std::cout << N << " " << K << " " << D << " " << T
			  << std::endl;
	for (int i=0; i<N; i++) {
		std::cout << P_table[i] << " " << J_table[i] << std::endl;
	}
	for (int i=0; i<K; i++) {
		std::cout << K_table[i] << std::endl;
	}
	std::cout << Ts << " " << Cs << " " << Tf << " " << Cf
			  << std::endl;
}

int main (int argc, char* argv[]) {
	read_input();

	//print_variables();
	std::cout << P_table.front() << std::endl;
	make_heap (P_table.begin(), P_table.end());
	std::cout << P_table.front()  << std::endl;
	return 0;
}
