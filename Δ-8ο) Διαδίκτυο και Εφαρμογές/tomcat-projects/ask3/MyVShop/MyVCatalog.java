package MyVShop;
/*if present 'package MUST be the first statement,*/
/* (possibly) followed by 'import'*/
import java.util.Hashtable;

public class MyVCatalog {
     /**The vehicles as Hashtable*/
     /** the car Model is the key for finding entries  */
     private Hashtable catalog;

     public MyVCatalog(){
         /** the old catalog remains*/
         /** but now the catalog (Hashtable) will contain also beans !!!*/
         catalog=new Hashtable();

         /**Some content - we NOW polulate with some objects!!*/
         addV(new VehicleBean("Buick","General Motors","1948", new MotorBean("1500", "200", "100")));
         addV(new VehicleBean("Mustang","Ford","1960", new MotorBean("1500", "200", "100")));
         addV(new VehicleBean("4CV","Citroen","1950", new MotorBean("1500", "200", "100")));
         addV(new VehicleBean("Jeep","General Motors", "1942", new MotorBean("1500", "200", "100")));
         addV(new VehicleBean("Jeep2","General Motors", "1952", new MotorBean("1500", "200", "100")));
         addV(new VehicleBean("Beatle","Volkswagen","1938", new MotorBean("1500", "200", "100")));
     }
     /** FIRST METHOD TO BE EXPOSED - addV */
     /** input argument is now a single ('complex') object !!*/
     /** nothing is returned*/
     public void addV(VehicleBean  vObj) {
         if (vObj == null){
              throw new IllegalArgumentException("The object provided cannot be null.");
         }
         /** entry format for the Hashtable: key,data */
         /** vObj.getVModel gets the 'key' out of the bean object*/
         catalog.put(vObj.getVModel(),vObj);
         System.out.println("Addition at server side: " + vObj.getVModel());
      }

     /** SECOND METHOD TO BE EXPOSED - getVehicleBean */
     /** input argument is a String */
     /** a ('complex') object is returned */
     public VehicleBean getVehicleBean(String model) {
         if (model==null){
              throw new IllegalArgumentException("carModel cannot be null.");
         }

         //Return the requested vehicle - NOW AS AN OBJECT !!!!
         return (VehicleBean)catalog.get(model);
     }

     public void delVehicleBean (String model) {
          if (model == null){
               throw new IllegalArgumentException("The model provided cannot be null.");
          }
          catalog.remove(model);
          System.out.println("Removal at server side: " + model);
     }

     /** THIRD METHOD TO BE EXPOSED - listV */
     /** NO input argument - a whole table is returned */
     /** ... , and that with ('complex') objects as entries !!!!*/
     public Hashtable listV(){return catalog;}
}