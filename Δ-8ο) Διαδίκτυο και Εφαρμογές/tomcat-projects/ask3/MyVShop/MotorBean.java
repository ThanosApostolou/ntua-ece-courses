package MyVShop;
//if present 'package' MUST be the first statement (possibly) followed by 'import'
// The following class follows the structure of a 'Java Bean'

public class MotorBean {
     /**The properties of the VehicleBean*/
     String MCc;
     String MNo_cylinders;
     String MPs;

     /** The following non-argument constructor MUST be always present*/
     /** for this class to be a Java Bean*/
     public MotorBean(){}

     /** Another constructor CAN also be always present*/
     /**  the following initializes all properties whenever a new object is instantiated (with 'new') - */
     public MotorBean(String MCc,String MNo_cylinders,String MPs){
     	this.MCc= MCc; this.MNo_cylinders = MNo_cylinders; this.MPs= MPs;}

     /** get & set methods for all properties MUST be present*/
     public String getMCc(){return MCc;}
     public void setMCc(String MCc){this.MCc= MCc;}
     public String getMNo_cylinders(){return MNo_cylinders;}
     public void setMNo_cylinders(String MNo_cylinders){this.MNo_cylinders= MNo_cylinders;}
     public String getMPs(){return MPs;}
     public void setMPs(String MPs){this.MPs= MPs;}

     //toString is an in-built method for every Java object. Outputs an informative string
     public String toString(){
      	return "'" + MCc + "' by " + MNo_cylinders + " (" + MPs + ")";}
}