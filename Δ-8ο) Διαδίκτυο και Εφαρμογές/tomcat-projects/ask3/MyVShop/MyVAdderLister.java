package MyVShop;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.soap.Constants;
import org.apache.soap.Fault;
import org.apache.soap.SOAPException;

import org.apache.soap.encoding.SOAPMappingRegistry;
import org.apache.soap.encoding.soapenc.BeanSerializer;

import org.apache.soap.rpc.Call;
import org.apache.soap.rpc.Parameter;
import org.apache.soap.rpc.Response;

import org.apache.soap.util.xml.QName;


public class MyVAdderLister {

     public void addlist(URL url, String model, String manufacturer, String year, MotorBean mObj)
         throws SOAPException{

         // Build the object with the data given by user
         VehicleBean vObj = new VehicleBean(model, manufacturer, year, mObj);

         //MotorBean and VehicleBean must now be mapped ... so SOAP can use them
         SOAPMappingRegistry reg = new SOAPMappingRegistry();
         BeanSerializer serializer1 = new BeanSerializer();
         reg.mapTypes(Constants.NS_URI_SOAP_ENC, new QName("urn:VBean_xmlns","mObj"), MotorBean.class, serializer1, serializer1);
         BeanSerializer serializer2 = new BeanSerializer();
         reg.mapTypes(Constants.NS_URI_SOAP_ENC, new QName("urn:VBean_xmlns","vObj"), VehicleBean.class, serializer2, serializer2);

         //Build the Call object
         Call call = new Call();
         //How to map, where to send, method to call, encoding "style"
         call.setSOAPMappingRegistry(reg);
         call.setTargetObjectURI("urn:MyVehicleCatalog");
         call.setMethodName("addV");
         call.setEncodingStyleURI(Constants.NS_URI_SOAP_ENC);

          //------------  A D D I N G ------------
          System.out.println("Adding vehicle: '" + model + "' by " + manufacturer +
                    ", year " + year + ", MCc " + mObj.MCc + ", MNo_cylinders " + mObj.MNo_cylinders + ", MPs " + mObj.MPs);

         // Set up the parameters of the call
         Vector params = new Vector();

         //in the instructions given to the 'Serializer' - see Depl. Descr.
         params.addElement(new Parameter("vObj", VehicleBean.class, vObj, null));
         call.setParams(params);

          // Invoke the call
          Response response;
          response = call.invoke(url, "");
          //We do not expect something back, unless there is a fault!!
          if (!response.generatedFault()) {
               System.out.println("Server reported NO FAULT while adding vehicle");
          } else {
               Fault fault = response.getFault();
               System.out.println("Server reported FAULT while adding:");
               System.out.println(fault.getFaultString());
          }

          //------------  L I S T I N G ------------
          list(url);
     }

     public void removelist(URL url, String model) throws SOAPException {
          //Build the Call object
          Call call = new Call();
          //How to map, where to send, method to call, encoding "style"
          call.setTargetObjectURI("urn:MyVehicleCatalog");
          call.setMethodName("delVehicleBean");
          call.setEncodingStyleURI(Constants.NS_URI_SOAP_ENC);


          //------------  R E M O V I N G ------------
          System.out.println("Removing vehicle: '" + model + "'");

          // Set up the parameters of the call
          Vector params = new Vector();

          //in the instructions given to the 'Serializer' - see Depl. Descr.
          //params.addElement(new Parameter("model", String, String, null));
          params.addElement(new Parameter("model", String.class, model, null));
          call.setParams(params);

          // Invoke the call
          Response response;
          response = call.invoke(url, "");
          //We do not expect something back, unless there is a fault!!
          if (!response.generatedFault()) {
               System.out.println("Server reported NO FAULT while deleting vehicle");
          } else {
               Fault fault = response.getFault();
               System.out.println("Server reported FAULT while deleting:");
               System.out.println(fault.getFaultString());
          }

          //------------  L I S T I N G ------------
          list(url);
     }

     public static void list (URL url) throws SOAPException {
          Call call = new Call();

          //MotorBean and VehicleBean must now be mapped ... so SOAP can use them
          SOAPMappingRegistry reg = new SOAPMappingRegistry();
          BeanSerializer serializer1 = new BeanSerializer();
          reg.mapTypes(Constants.NS_URI_SOAP_ENC, new QName("urn:VBean_xmlns","mObj"), MotorBean.class, serializer1, serializer1);
          BeanSerializer serializer2 = new BeanSerializer();
          reg.mapTypes(Constants.NS_URI_SOAP_ENC, new QName("urn:VBean_xmlns","vObj"), VehicleBean.class, serializer2, serializer2);

          call.setSOAPMappingRegistry(reg);
          call.setTargetObjectURI("urn:MyVehicleCatalog");
          call.setMethodName("listV");
          call.setEncodingStyleURI(Constants.NS_URI_SOAP_ENC);

          /* NO parameters here !!*/
          /*(we cannot have a call with arguments as before)*/
          call.setParams(null);
          // Invoke the call; here we expect something back !!
          Response response;
          response = call.invoke(url, "");

          /*Extract the value returned in the form of a 'Parameter' Object*/
          Parameter returnValue = response.getReturnValue();
          /*Cast the 'Parameter' Object onto a Hashtabel Object*/
          Hashtable catalog = (Hashtable)returnValue.getValue();
          Enumeration e = catalog.keys();
          while (e.hasMoreElements()) {
               String VModel = (String)e.nextElement();
               VehicleBean vo = (VehicleBean)catalog.get(VModel);
               System.out.println("  '" + vo.getVModel() + "' by " + vo.getVManufacturer() +
                    ", year " + vo.getVYear() + ", MCc " + vo.getVMotor().MCc + ", MNo_cylinders " + vo.getVMotor().MNo_cylinders + ", MPs " + vo.getVMotor().MPs);
          }
     }

     public static void main(String[] args) {
          if (args.length == 8 && args[1].equals("add")) {
               try {
               // URL for SOAP server to connect to
               URL urlink = new URL(args[0]);

               // Get values for the new vehicle
               String model = args[2];
               String manufacturer = args[3];
               String year = args[4];
               MotorBean motor = new MotorBean(args[5], args[6], args[7]);

               //Add the new vehicle,
               MyVAdderLister adderlister = new MyVAdderLister();
               adderlister.addlist(urlink, model, manufacturer, year, motor);
               }  catch (Exception e) {e.printStackTrace();}
          } else if (args.length == 3 && args[1].equals("remove")) {
               try {
               // URL for SOAP server to connect to
               URL urlink = new URL(args[0]);

               // Get values for the new vehicle
               String model = args[2];
               MyVAdderLister adderlister = new MyVAdderLister();
               adderlister.removelist(urlink, model);
               }  catch (Exception e) {e.printStackTrace();}
          } else {
               System.out.println("USAGE: url 'add' model manufacturer year MCs MNo_cylinders MPs\nOR     url 'remove' model");
          }
     }
}