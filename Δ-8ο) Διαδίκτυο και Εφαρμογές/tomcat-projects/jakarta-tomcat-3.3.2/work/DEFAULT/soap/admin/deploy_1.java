package admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.io.*;
import  java.util.*;
import  org.w3c.dom.*;
import org.apache.soap.Constants;
import  org.apache.soap.util.*;
import  org.apache.soap.util.xml.*;
import  org.apache.soap.server.*;


public class deploy_1 extends org.apache.jasper.runtime.HttpJspBase {


    static {
    }
    public deploy_1( ) {
    }

    private boolean _jspx_inited = false;

    public final synchronized void _jspx_init() throws org.apache.jasper.JasperException {
        if (! _jspx_inited) {
            _jspx_inited = true;
        }
    }

    public void _jspService(HttpServletRequest request, HttpServletResponse  response)
        throws java.io.IOException, ServletException {

        JspFactory _jspxFactory = null;
        PageContext pageContext = null;
        HttpSession session = null;
        ServletContext application = null;
        ServletConfig config = null;
        JspWriter out = null;
        Object page = this;
        String  _value = null;
        try {
            try {

                _jspx_init();
                _jspxFactory = JspFactory.getDefaultFactory();
                response.setContentType("text/html;charset=ISO-8859-1");
                pageContext = _jspxFactory.getPageContext(this, request, response,
                			"", true, 8192, true);

                application = pageContext.getServletContext();
                config = pageContext.getServletConfig();
                session = pageContext.getSession();
                out = pageContext.getOut();

                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/deploy.jsp";from=(0,58);to=(1,0)]
                    out.write("\n");

                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/deploy.jsp";from=(1,124);to=(5,0)]
                    out.write("\n\n<h1>Deploy a Service</h1>\n\n");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/deploy.jsp";from=(5,2);to=(14,0)]
                     
                      String configFilename = config.getInitParameter(Constants.CONFIGFILENAME);
                      if (configFilename == null)
                        configFilename = application.getInitParameter(Constants.CONFIGFILENAME);
                    
                    ServiceManager serviceManager =
                      org.apache.soap.server.http.ServerHTTPUtils.getServiceManagerFromContext(application, configFilename);
                    
                    if (!request.getMethod().equals ("POST")) { 
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/deploy.jsp";from=(14,2);to=(324,0)]
                    out.write("\n\n<form action=\"deploy.jsp\" method=\"POST\">\n    <table border=\"1\" width=\"100%\">\n        <tr>\n            <th colspan=\"2\"><h2>Service Deployment Descriptor\n            Template</h2>\n            </th>\n        </tr>\n        <tr>\n            <th>Property</th>\n            <th>Details</th>\n        </tr>\n        <tr>\n            <td>ID</td>\n            <td><input type=\"text\" size=\"60\" name=\"id\"></td>\n        </tr>\n        <tr>\n            <td>Scope</td>\n            <td><select name=\"scope\" size=\"1\">\n                <option selected value=\"0\">Request</option>\n                <option value=\"1\">Session</option>\n                <option value=\"2\">Application</option>\n            </select></td>\n        </tr>\n        <tr>\n            <td>Methods</td>\n            <td><input type=\"text\" size=\"60\" name=\"methods\"><br>\n            (Whitespace separated list of method names) </td>\n        </tr>\n        <tr>\n            <td>Provider Type</td>\n      \n            <td><select name=\"providerType\" size=\"1\">\n                <option selected value=\"0\">Java</option>\n                <option value=\"1\">Script</option>\n\t\t<option value=\"3\">User-Defined</option>\n            \t</select></td>\n        </tr>\n\t<tr>\n\t\t<td> </td>\n\t\t<td><div align=\"left\"><table border=\"0\">\n                <tr>\n                    <td>For User-Defined Provider Type, Enter FULL Class Name:</td>\n\t\t</tr>\n\t\t<tr>\n                    <td><input type=\"text\" size=\"60\" name=\"userProviderClassString\"></td>\n                </tr>\n\t\t</table></div></td>\n\t</tr>\n\t<tr>\n\t\t<td> </td>\n\t\t<td>Number of Options: <input type=\"text\" size=\"10\" name=\"noOpts\" /><br>\n            \t<div align=\"center\"><center><table border=\"0\">\n                <tr>\n                    <td align=\"center\" >Key</td>\n                    <td align=\"center\" >Value</td>\n\t\t</tr>\n                <tr>\n                    <td><input type=\"text\" size=\"15\" name=\"optionkey1\"></td>\n                    <td><input type=\"text\" size=\"20\" name=\"optionvalue1\"></td>\n                </tr>\n                <tr>\n                    <td><input type=\"text\" size=\"15\" name=\"optionkey2\"></td>\n                    <td><input type=\"text\" size=\"20\" name=\"optionvalue2\"></td>\n                </tr>\n                <tr>\n                    <td><input type=\"text\" size=\"15\" name=\"optionkey3\"></td>\n                    <td><input type=\"text\" size=\"20\" name=\"optionvalue3\"></td>\n                </tr>\n                <tr>\n                    <td><input type=\"text\" size=\"15\" name=\"optionkey4\"></td>\n                    <td><input type=\"text\" size=\"20\" name=\"optionvalue4\"></td>\n                </tr>\n\t\t</table></center></div></td>\n\t</tr>\n        <tr>\n            <td>Java Provider</td>\n            <td><div align=\"left\"><table border=\"0\">\n                <tr>\n                    <td>Provider Class</td>\n                    <td><input type=\"text\" size=\"40\"\n                    name=\"providerClass\"></td>\n                </tr>\n                <tr>\n                    <td width=\"150\">Static?</td>\n                    <td><select name=\"isStatic\" size=\"1\">\n                        <option value=\"true\">Yes</option>\n                        <option selected value=\"no\">No</option>\n                    </select></td>\n                </tr>\n            </table>\n            </div></td>\n        </tr>\n        <tr>\n            <td>Script Provider</td>\n            <td><div align=\"left\"><table border=\"0\">\n                <tr>\n                    <td>Script Language</td>\n                    <td><select name=\"scriptLanguage\" size=\"1\">\n                        <option value=\"bml\">BML</option>\n                        <option value=\"jacl\">Jacl</option>\n                        <option value=\"javascript\">JavaScript (Rhino)</option>\n                        <option value=\"jpython\">JPython</option>\n                        <option value=\"jscript\">JScript</option>\n                        <option value=\"perlscript\">PerlScript</option>\n                        <option value=\"vbscript\">VBScript</option>\n                        <option value=\"other\">Other .. (type in)</option>\n                    </select><input type=\"text\" size=\"20\"\n                    name=\"scriptLanguageTypein\"></td>\n                </tr>\n                <tr>\n                    <td width=\"150\">Script Filename, or</td>\n                    <td><input type=\"text\" size=\"40\"\n                    name=\"scriptFilename\"></td>\n                </tr>\n                <tr>\n                    <td>Script</td>\n                    <td><textarea name=\"script\" rows=\"10\"\n                    cols=\"40\"></textarea></td>\n                </tr>\n            </table>\n            </div></td>\n        </tr>\n        <tr>\n            <td>Type Mappings</td>\n            <td>Number of mappings: <input type=\"text\" size=\"10\"\n            name=\"nmaps\" /><br>\n            <div align=\"center\"><center><table border=\"0\">\n                <tr>\n                    <td align=\"center\" rowspan=\"2\">Encoding Style</td>\n                    <td align=\"center\" colspan=\"2\">Element Type </td>\n                    <td align=\"center\" rowspan=\"2\">Java Type</td>\n                    <td align=\"center\" rowspan=\"2\">Java to XML\n                    Serializer </td>\n                    <td align=\"center\" rowspan=\"2\">XML to Java\n                    Deserializer </td>\n                </tr>\n                <tr>\n                    <td align=\"center\">Namespace URI</td>\n                    <td align=\"center\">Local Part</td>\n                </tr>\n                <tr>\n                    <td><select name=\"encstyle1\" size=\"1\">\n                        <option selected value=\"0\">SOAP</option>\n                        <option value=\"1\">XMI</option>\n                    </select></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"nsuri1\"></td>\n                    <td><input type=\"text\" size=\"10\"\n                    name=\"localpart1\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"classname1\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"java2xml1\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"xml2java1\"></td>\n                </tr>\n                <tr>\n                    <td><select name=\"encstyle2\" size=\"1\">\n                        <option selected value=\"0\">SOAP</option>\n                        <option value=\"1\">XMI</option>\n                    </select></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"nsuri2\"></td>\n                    <td><input type=\"text\" size=\"10\"\n                    name=\"localpart2\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"classname2\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"java2xml2\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"xml2java2\"></td>\n                </tr>\n                <tr>\n                    <td><select name=\"encstyle3\" size=\"1\">\n                        <option selected value=\"0\">SOAP</option>\n                        <option value=\"1\">XMI</option>\n                    </select></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"nsuri3\"></td>\n                    <td><input type=\"text\" size=\"10\"\n                    name=\"localpart3\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"classname3\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"java2xml3\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"xml2java3\"></td>\n                </tr>\n                <tr>\n                    <td><select name=\"encstyle4\" size=\"1\">\n                        <option selected value=\"0\">SOAP</option>\n                        <option value=\"1\">XMI</option>\n                    </select></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"nsuri4\"></td>\n                    <td><input type=\"text\" size=\"10\"\n                    name=\"localpart4\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"classname4\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"java2xml4\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"xml2java4\"></td>\n                </tr>\n                <tr>\n                    <td><select name=\"encstyle5\" size=\"1\">\n                        <option selected value=\"0\">SOAP</option>\n                        <option value=\"1\">XMI</option>\n                    </select></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"nsuri5\"></td>\n                    <td><input type=\"text\" size=\"10\"\n                    name=\"localpart5\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"classname5\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"java2xml5\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"xml2java5\"></td>\n                </tr>\n                <tr>\n                    <td><select name=\"encstyle6\" size=\"1\">\n                        <option selected value=\"0\">SOAP</option>\n                        <option value=\"1\">XMI</option>\n                    </select></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"nsuri6\"></td>\n                    <td><input type=\"text\" size=\"10\"\n                    name=\"localpart6\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"classname6\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"java2xml6\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"xml2java6\"></td>\n                </tr>\n                <tr>\n                    <td><select name=\"encstyle7\" size=\"1\">\n                        <option selected value=\"0\">SOAP</option>\n                        <option value=\"1\">XMI</option>\n                    </select></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"nsuri7\"></td>\n                    <td><input type=\"text\" size=\"10\"\n                    name=\"localpart7\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"classname7\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"java2xml7\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"xml2java7\"></td>\n                </tr>\n                <tr>\n                    <td><select name=\"encstyle8\" size=\"1\">\n                        <option selected value=\"0\">SOAP</option>\n                        <option value=\"1\">XMI</option>\n                    </select></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"nsuri8\"></td>\n                    <td><input type=\"text\" size=\"10\"\n                    name=\"localpart8\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"classname8\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"java2xml8\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"xml2java8\"></td>\n                </tr>\n                <tr>\n                    <td><select name=\"encstyle9\" size=\"1\">\n                        <option selected value=\"0\">SOAP</option>\n                        <option value=\"1\">XMI</option>\n                    </select></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"nsuri9\"></td>\n                    <td><input type=\"text\" size=\"10\"\n                    name=\"localpart9\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"classname9\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"java2xml9\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"xml2java9\"></td>\n                </tr>\n                <tr>\n                    <td><select name=\"encstyle10\" size=\"1\">\n                        <option selected value=\"0\">SOAP</option>\n                        <option value=\"1\">XMI</option>\n                    </select></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"nsuri10\"></td>\n                    <td><input type=\"text\" size=\"10\"\n                    name=\"localpart10\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"classname10\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"java2xml10\"></td>\n                    <td><input type=\"text\" size=\"15\"\n                    name=\"xml2java10\"></td>\n                </tr>\n            </table>\n            </center></div></td>\n        </tr>\n\t<tr><td>Default Mapping Registry Class</td><td><input type=\"text\" size=\"60\" name=\"defaultSMR\"></td></tr>\n    </table>\n    <p><input type=\"submit\" value=\"Deploy\" /></p>\n</form>\n\n");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/deploy.jsp";from=(324,2);to=(442,0)]
                    
                    } else {
                      String id = request.getParameter ("id");
                      DeploymentDescriptor dd = new DeploymentDescriptor ();
                      dd.setID (id);
                    
                      // get the provider info
                      int scope = Integer.parseInt (request.getParameter ("scope"));
                      String providerTypeStr = request.getParameter ("providerType");
                      String className = request.getParameter ("providerClass");
                      boolean isStatic = request.getParameter ("isStatic").equals ("true");
                      String scriptLang = request.getParameter ("scriptLanguage");
                      String userClass = request.getParameter ("userProviderClassString");
                    
                      if (scriptLang.equals ("other")) {
                        scriptLang = request.getParameter ("scriptLanguageTypeIn");
                      }
                      String scriptFilename = request.getParameter ("scriptFilename");
                      String script = request.getParameter ("script");
                      String methodsStr = request.getParameter ("methods");
                      StringTokenizer st = new StringTokenizer (methodsStr);
                      int nTokens = st.countTokens ();
                      String[] methods = new String[nTokens];
                      for (int i = 0; i < nTokens; i++) {
                        methods[i] = st.nextToken ();
                      }
                    
                      dd.setScope (scope);
                      dd.setMethods (methods);
                    
                      if (providerTypeStr.equals ("0")) {
                        dd.setProviderType (DeploymentDescriptor.PROVIDER_JAVA);
                        dd.setProviderClass (className);
                        dd.setIsStatic (isStatic);
                      } else {
                    	if (providerTypeStr.equals("3")) {
                    		dd.setProviderType (DeploymentDescriptor.PROVIDER_USER_DEFINED);
                    		dd.setServiceClass(userClass);
                    		dd.setProviderClass (className);
                    		dd.setIsStatic (isStatic);
                    
                    		// get any options
                    		int optnum = 0;
                    
                    
                    		try {
                    		    optnum = Integer.parseInt (request.getParameter ("noOpts"));
                    		} catch (NumberFormatException e) {
                    			optnum = 0;
                    		}
                    
                    		if (optnum != 0) {
                    		
                    			Hashtable optionsTble = new Hashtable();
                    		
                    			for (int j = 1; j <= optnum; j++) { 
                    			      String keyS= request.getParameter ("optionkey" + j);
                    			      String valueS= request.getParameter ("optionvalue" + j);
                    				optionsTble.put(keyS, valueS);
                    			}
                    			dd.setProps(optionsTble);
                        		}
                    
                    	
                       	} else {
                        		if (!scriptFilename.equals ("")) { // filename specified
                          			dd.setProviderType (DeploymentDescriptor.PROVIDER_SCRIPT_FILE);
                        		} else { // there better be a script to run
                          			dd.setProviderType (DeploymentDescriptor.PROVIDER_SCRIPT_STRING);
                        		}
                        		dd.setScriptLanguage (scriptLang);
                        		dd.setScriptFilenameOrString (scriptFilename);
                      	}
                      }
                      String[] encs = {org.apache.soap.Constants.NS_URI_SOAP_ENC,
                           org.apache.soap.Constants.NS_URI_XMI_ENC};
                    
                      // set up any type mappings
                      int nmaps = 0;
                    
                      try {
                        nmaps = Integer.parseInt (request.getParameter ("nmaps"));
                      } catch (NumberFormatException e) {
                      }
                    
                      if (nmaps != 0) {
                        TypeMapping[] mappings = new TypeMapping[nmaps];
                        for (int i = 1; i <= nmaps; i++) { // the form is hard-coded to a max of 10
                          int encStyle = Integer.parseInt (request.getParameter ("encstyle" + i));
                          String nsuri = request.getParameter ("nsuri" + i);
                          String localPart = request.getParameter ("localpart" + i);
                          String classNameStr = request.getParameter ("classname" + i);
                          String java2XMLClass = request.getParameter ("java2xml" + i);
                          String xml2JavaClass = request.getParameter ("xml2java" + i);
                          QName elementType = (nsuri.equals ("") || localPart.equals ("")) 
                                              ? null : new QName (nsuri, localPart);
                          // map "" to null (the unfilled params come as empty strings because
                          // they are infact actual parameters)
                          classNameStr = classNameStr.equals ("") ? null : classNameStr;
                          java2XMLClass = java2XMLClass.equals ("") ? null : java2XMLClass;
                          xml2JavaClass = xml2JavaClass.equals ("") ? null : xml2JavaClass;
                          mappings[i-1] = new TypeMapping (encs[encStyle], elementType,
                    				       classNameStr, java2XMLClass,
                    				       xml2JavaClass);
                        }
                        dd.setMappings (mappings);
                      }
                    
                      String defaultSMR = request.getParameter("defaultSMR");
                      if (defaultSMR != null) {
                    	dd.setDefaultSMRClass(defaultSMR);
                      }
                      // ok now deploy it
                      serviceManager.deploy (dd);
                    
                      // show what was deployed
                      out.println ("<p>Service <b>" + dd.getID () + "</b> deployed.</p>");
                    }
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/deploy.jsp";from=(442,2);to=(443,0)]
                    out.write("\n");

                // end

            } catch (Exception ex) {
                if (out != null && out.getBufferSize() != 0)
                    out.clearBuffer();
                if (pageContext != null) pageContext.handlePageException(ex);
            } catch (Error error) {
                throw error;
            } catch (Throwable throwable) {
                throw new ServletException(throwable);
            }
        } finally {
            if (out instanceof org.apache.jasper.runtime.JspWriterImpl) { 
                ((org.apache.jasper.runtime.JspWriterImpl)out).flushBuffer();
            }
            if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
        }
    }
}
