package admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import  org.apache.soap.Constants;
import  org.apache.soap.server.*;


public class list_1 extends org.apache.jasper.runtime.HttpJspBase {


    static {
    }
    public list_1( ) {
    }

    private boolean _jspx_inited = false;

    public final synchronized void _jspx_init() throws org.apache.jasper.JasperException {
        if (! _jspx_inited) {
            _jspx_inited = true;
        }
    }

    public void _jspService(HttpServletRequest request, HttpServletResponse  response)
        throws java.io.IOException, ServletException {

        JspFactory _jspxFactory = null;
        PageContext pageContext = null;
        HttpSession session = null;
        ServletContext application = null;
        ServletConfig config = null;
        JspWriter out = null;
        Object page = this;
        String  _value = null;
        try {
            try {

                _jspx_init();
                _jspxFactory = JspFactory.getDefaultFactory();
                response.setContentType("text/html;charset=ISO-8859-1");
                pageContext = _jspxFactory.getPageContext(this, request, response,
                			"", true, 8192, true);

                application = pageContext.getServletContext();
                config = pageContext.getServletConfig();
                session = pageContext.getSession();
                out = pageContext.getOut();

                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(0,85);to=(4,0)]
                    out.write("\n\n<h1>Service Listing</h1>\n\n");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(4,2);to=(18,4)]
                     
                      String configFilename = config.getInitParameter(Constants.CONFIGFILENAME);
                      if (configFilename == null)
                        configFilename = application.getInitParameter(Constants.CONFIGFILENAME);
                    
                      ServiceManager serviceManager =
                        org.apache.soap.server.http.ServerHTTPUtils.getServiceManagerFromContext(application, configFilename);
                    
                      String[] serviceNames = serviceManager.list ();
                      if (serviceNames.length == 0) {
                        out.println ("<p>Sorry, there are no services currently deployed.</p>");
                      } else {
                        out.println ("<p>Here are the deployed services (select one to see");
                        out.println ("details)</p>");
                        
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(18,6);to=(20,4)]
                    out.write("\n    <ul>\n    ");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(20,6);to=(23,4)]
                    
                        for (int i = 0; i < serviceNames.length; i++) {
                          String id = serviceNames[i];
                        
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(23,6);to=(24,38)]
                    out.write("\n      <li><a href=\"showdetails.jsp?id=");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(24,41);to=(24,43)]
                    out.print(id);
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(24,45);to=(24,47)]
                    out.write("\">");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(24,50);to=(24,53)]
                    out.print( id);
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(24,55);to=(25,4)]
                    out.write("</a></li>\n    ");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(25,6);to=(27,4)]
                    
                        }
                        
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(27,6);to=(29,4)]
                    out.write("\n    </ul>\n    ");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(29,6);to=(31,0)]
                    
                      }
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/list.jsp";from=(31,2);to=(32,0)]
                    out.write("\n");

                // end

            } catch (Exception ex) {
                if (out != null && out.getBufferSize() != 0)
                    out.clearBuffer();
                if (pageContext != null) pageContext.handlePageException(ex);
            } catch (Error error) {
                throw error;
            } catch (Throwable throwable) {
                throw new ServletException(throwable);
            }
        } finally {
            if (out instanceof org.apache.jasper.runtime.JspWriterImpl) { 
                ((org.apache.jasper.runtime.JspWriterImpl)out).flushBuffer();
            }
            if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
        }
    }
}
