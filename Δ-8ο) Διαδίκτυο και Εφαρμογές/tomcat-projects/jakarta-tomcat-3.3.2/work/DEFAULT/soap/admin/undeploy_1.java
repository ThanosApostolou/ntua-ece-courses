package admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.soap.*;
import org.apache.soap.server.*;


public class undeploy_1 extends org.apache.jasper.runtime.HttpJspBase {


    static {
    }
    public undeploy_1( ) {
    }

    private boolean _jspx_inited = false;

    public final synchronized void _jspx_init() throws org.apache.jasper.JasperException {
        if (! _jspx_inited) {
            _jspx_inited = true;
        }
    }

    public void _jspService(HttpServletRequest request, HttpServletResponse  response)
        throws java.io.IOException, ServletException {

        JspFactory _jspxFactory = null;
        PageContext pageContext = null;
        HttpSession session = null;
        ServletContext application = null;
        ServletConfig config = null;
        JspWriter out = null;
        Object page = this;
        String  _value = null;
        try {
            try {

                _jspx_init();
                _jspxFactory = JspFactory.getDefaultFactory();
                response.setContentType("text/html;charset=ISO-8859-1");
                pageContext = _jspxFactory.getPageContext(this, request, response,
                			"", true, 8192, true);

                application = pageContext.getServletContext();
                config = pageContext.getServletConfig();
                session = pageContext.getSession();
                out = pageContext.getOut();

                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(0,38);to=(1,0)]
                    out.write("\n");

                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(1,45);to=(5,0)]
                    out.write("\n\n<h1>Un-Deploy a Service</h1>\n\n");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(5,2);to=(20,4)]
                     
                      String configFilename = config.getInitParameter(Constants.CONFIGFILENAME);
                      if (configFilename == null)
                        configFilename = application.getInitParameter(Constants.CONFIGFILENAME);
                    
                    ServiceManager serviceManager =
                      org.apache.soap.server.http.ServerHTTPUtils.getServiceManagerFromContext(application, configFilename);
                    
                    String id = request.getParameter ("id");
                    if (id == null) {
                      String[] serviceNames = serviceManager.list ();
                      if (serviceNames.length == 0) {
                        out.println ("<p>Sorry, there are no services currently deployed.</p>");
                      } else {
                        out.println ("<p>Select the service to be undeployed:</p>");
                        
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(20,6);to=(22,4)]
                    out.write("\n    <ul>\n    ");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(22,6);to=(25,4)]
                    
                        for (int i = 0; i < serviceNames.length; i++) {
                          id = serviceNames[i];
                        
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(25,6);to=(26,35)]
                    out.write("\n      <li><a href=\"undeploy.jsp?id=");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(26,38);to=(26,40)]
                    out.print(id);
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(26,42);to=(26,44)]
                    out.write("\">");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(26,47);to=(26,50)]
                    out.print( id);
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(26,52);to=(27,4)]
                    out.write("</li>\n    ");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(27,6);to=(29,4)]
                    
                        }
                        
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(29,6);to=(31,4)]
                    out.write("\n    </ul>\n    ");

                // end
                // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(31,6);to=(42,0)]
                    
                      }
                    } else {
                      try {
                        DeploymentDescriptor dd = serviceManager.undeploy (id);
                        out.println ("OK, service named '" + id + "' undeployed successfully!");
                      } catch (SOAPException e) {
                        out.println ("Ouch, coudn't undeploy service '" + id + "' because: ");
                        e.getMessage ();
                      }
                    }
                // end
                // HTML // begin [file="/home/thanos/Documents/shmmy-di2019/jakarta-tomcat-3.3.2/webapps/soap/admin/undeploy.jsp";from=(42,2);to=(43,0)]
                    out.write("\n");

                // end

            } catch (Exception ex) {
                if (out != null && out.getBufferSize() != 0)
                    out.clearBuffer();
                if (pageContext != null) pageContext.handlePageException(ex);
            } catch (Error error) {
                throw error;
            } catch (Throwable throwable) {
                throw new ServletException(throwable);
            }
        } finally {
            if (out instanceof org.apache.jasper.runtime.JspWriterImpl) { 
                ((org.apache.jasper.runtime.JspWriterImpl)out).flushBuffer();
            }
            if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
        }
    }
}
