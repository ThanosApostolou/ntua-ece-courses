import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class DateForm extends HttpServlet {

   public void doPost( HttpServletRequest request,
                       HttpServletResponse response )   throws ServletException, IOException
   {
        PrintWriter output;
        String input_date = request.getParameter("input_date");
        String next_page = request.getParameter( "next_page" );
        if (input_date != null && !input_date.equals("")) {
            Cookie c1 = new Cookie("input_date", input_date);
            c1.setMaxAge( 360 );
            response.addCookie( c1 );
        }
        response.setContentType( "text/html" );  
        response.sendRedirect("/ask1/"+next_page);
   }
   
   public void doGet( HttpServletRequest request,
                      HttpServletResponse response )
                      throws ServletException, IOException
   {
        PrintWriter output;
        Cookie cookies[];
         Boolean set_info_done = false;
        Boolean set_category_done = false;
        Boolean set_location_done = false;
        
        cookies = request.getCookies();

        response.setContentType( "text/html" ); 
        output = response.getWriter();

        output.println( "<HTML><HEAD><TITLE>" );
        output.println( "Date form!" );
        output.println( "</TITLE></HEAD><BODY>" );

        if ( cookies.length != 0 ) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("first_name")) {
                    output.println("<p>Your First Name is: " + cookies[i].getValue() + "</p>");
                    set_info_done = true;
                }
                if (cookies[i].getName().equals("last_name")) {
                    output.println("<p>Your Last Name is: " + cookies[i].getValue() + "</p>");
                }
                if (cookies[i].getName().equals("age")) {
                    output.println("<p>Your Age is: " + cookies[i].getValue() + "</p>");
                }
                if (cookies[i].getName().equals("select_category")) {
                    output.println("<p>You have chosen category: " + cookies[i].getValue() + "</p>");
                    set_category_done = true;
                }
                if (cookies[i].getName().equals("input_date")) {
                    output.println("<p>You have chosen date: " + cookies[i].getValue() + "</p>");
                }
                if (cookies[i].getName().equals("select_location")) {
                    output.println("<p>You have chosen location: " + cookies[i].getValue() + "</p>");
                    set_location_done = true;
                }
            }
        }

        output.println(""+
        "<FORM ACTION=\"/ask1/DateForm\" METHOD=\"POST\">"+
        "<STRONG>Put the date which interests you:<br> </STRONG>"+
        "<input type=\"date\" name=\"input_date\">"+
        "<p>Choose your next step</p>"+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"index.html\">");
        if (set_info_done) {
            output.println("<del>Set Personal Info</del><BR>");
        } else {
            output.println("Set Personal Info<BR>");
        }
        output.println(""+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"CategoriesForm\">");
        if (set_category_done) {
            output.println("<del>Set Category</del><BR>");
        } else {
            output.println("Set Category<BR>");
        }
        output.println(""+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"LocationForm\" CHECKED>");
        if (set_location_done) {
            output.println("<del>Set Location</del><BR>");
        } else {
            output.println("Set Location<BR>");
        }
        output.println(""+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"VehiclesForm\">Show Vehicles<BR>"+
        "<INPUT TYPE=\"submit\" VALUE=\"Submit\">"+
        "<INPUT TYPE=\"reset\">"+
        "</FORM>");


        output.println( "</BODY></HTML>" );
        output.close();
   }

}
