import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class VehiclesForm extends HttpServlet {

   public void doPost( HttpServletRequest request,
                       HttpServletResponse response )   throws ServletException, IOException
   {
      String next_page = request.getParameter( "next_page" );
      response.setContentType( "text/html" );  
      response.sendRedirect("/ask1/"+next_page);
   }
   
   public void doGet( HttpServletRequest request,
                      HttpServletResponse response )
                      throws ServletException, IOException
   {
        String[] motorbikes = {"moto1", "moto2", "moto3"};
        String[] vans = {"van1", "van2", "van3"};
        String[] sedans = {"sedan1", "sedan2", "sedan3"};
        String vehicle_category = "";

        PrintWriter output;
        Cookie cookies[];
        Boolean set_info_done = false;
        Boolean set_category_done = false;
        Boolean set_date_done = false;
        Boolean set_location_done = false;
        
        cookies = request.getCookies();

        response.setContentType( "text/html" ); 
        output = response.getWriter();

        output.println( "<HTML><HEAD><TITLE>" );
        output.println( "Vehicles form!" );
        output.println( "</TITLE></HEAD><BODY>" );

        if ( cookies.length != 0 ) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("first_name")) {
                    output.println("<p>Your First Name is: " + cookies[i].getValue() + "</p>");
                    set_info_done = true;
                }
                if (cookies[i].getName().equals("last_name")) {
                    output.println("<p>Your Last Name is: " + cookies[i].getValue() + "</p>");
                }
                if (cookies[i].getName().equals("age")) {
                    output.println("<p>Your Age is: " + cookies[i].getValue() + "</p>");
                }
                if (cookies[i].getName().equals("select_category")) {
                    output.println("<p>You have chosen category: " + cookies[i].getValue() + "</p>");
                    set_category_done = true;
                    vehicle_category = cookies[i].getValue();
                }
                if (cookies[i].getName().equals("input_date")) {
                    output.println("<p>You have chosen date: " + cookies[i].getValue() + "</p>");
                    set_date_done = true;
                }
                if (cookies[i].getName().equals("select_location")) {
                    output.println("<p>You have chosen location: " + cookies[i].getValue() + "</p>");
                    set_location_done = true;
                }
            }
        }

        output.println(""+
        "<FORM ACTION=\"/ask1/DateForm\" METHOD=\"POST\">"+
        "<p>Choose your next step</p>"+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"index.html\" CHECKED>");
        if (set_info_done) {
            output.println("<del>Set Personal Info</del><BR>");
        } else {
            output.println("Set Personal Info<BR>");
        }
        output.println(""+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"CategoriesForm\">");
        if (set_category_done) {
            output.println("<del>Set Category</del><BR>");
        } else {
            output.println("Set Category<BR>");
        }
        output.println(""+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"DateForm\">");
        if (set_date_done) {
            output.println("<del>Set Date</del><BR>");
        } else {
            output.println("Set Date<BR>");
        }
        output.println(""+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"LocationForm\">");
        if (set_location_done) {
            output.println("<del>Set Location</del><BR>");
        } else {
            output.println("Set Location<BR>");
        }
        output.println(""+
        "<INPUT TYPE=\"submit\" VALUE=\"Submit\">"+
        "<INPUT TYPE=\"reset\">"+
        "</FORM>");


        output.println("<h1> Available vehicles</h1>");
        if (vehicle_category.equals("van")) {
            for (int i=0; i<vans.length; i++) {
                output.println("<p>" + vans[i] + "</p>");
            }
        } else if (vehicle_category.equals("motorbike")) {
            for (int i=0; i<motorbikes.length; i++) {
                output.println("<p>" + motorbikes[i] + "</p>");
            }
        } else if (vehicle_category.equals("sedan")) {
            for (int i=0; i<sedans.length; i++) {
                output.println("<p>" + sedans[i] + "</p>");
            }
        } else {
            for (int i=0; i<vans.length; i++) {
                output.println("<p>" + vans[i] + "</p>");
            }
            for (int i=0; i<motorbikes.length; i++) {
                output.println("<p>" + motorbikes[i] + "</p>");
            }
            for (int i=0; i<sedans.length; i++) {
                output.println("<p>" + sedans[i] + "</p>");
            }
        }
        output.println( "</BODY></HTML>" );
        output.close();
   }

}
