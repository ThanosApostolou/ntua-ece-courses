import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class CategoriesForm extends HttpServlet {

   public void doPost( HttpServletRequest request,
                       HttpServletResponse response )   throws ServletException, IOException
   {
      PrintWriter output;
      String select_category = request.getParameter("select_category");
      String next_page = request.getParameter( "next_page" );
      Cookie c1 = new Cookie("select_category", select_category);
      c1.setMaxAge( 360 );
      response.addCookie( c1 );
      response.setContentType( "text/html" );  
      response.sendRedirect("/ask1/"+next_page);
   }
   
   public void doGet( HttpServletRequest request,
                      HttpServletResponse response )
                      throws ServletException, IOException
   {
        PrintWriter output;
        Cookie cookies[];
        Boolean set_info_done = false;
        Boolean set_date_done = false;
        Boolean set_location_done = false;

        cookies = request.getCookies(); // get client's cookies

        response.setContentType( "text/html" ); 
        output = response.getWriter();

        output.println( "<HTML><HEAD><TITLE>" );
        output.println( "Categories form!" );
        output.println( "</TITLE></HEAD><BODY>" );

        if ( cookies.length != 0 ) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("first_name")) {
                    output.println("<p>Your First Name is: " + cookies[i].getValue() + "</p>");
                    set_info_done = true;
                }
                if (cookies[i].getName().equals("last_name")) {
                    output.println("<p>Your Last Name is: " + cookies[i].getValue() + "</p>");
                }
                if (cookies[i].getName().equals("age")) {
                    output.println("<p>Your Age is: " + cookies[i].getValue() + "</p>");
                }
                if (cookies[i].getName().equals("select_category")) {
                    output.println("<p>You have chosen category: " + cookies[i].getValue() + "</p>");
                }
                if (cookies[i].getName().equals("input_date")) {
                    output.println("<p>You have chosen date: " + cookies[i].getValue() + "</p>");
                    set_date_done = true;
                }
                if (cookies[i].getName().equals("select_location")) {
                    output.println("<p>You have chosen location: " + cookies[i].getValue() + "</p>");
                    set_location_done = true;
                }
            }
        }
        output.println(""+
        "<FORM ACTION=\"/ask1/CategoriesForm\" METHOD=\"POST\">"+
        "<STRONG>Select a vehicle category:<br> </STRONG>"+
        "<select name=\"select_category\">"+
            "<option value=\"van\">Van</option>"+
            "<option value=\"motorbike\">Motorbike</option>"+
            "<option value=\"sedan\">Sedan</option>"+
        "</select>"+
        "<p>Choose your next step</p>"+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"index.html\">");
        if (set_info_done) {
            output.println("<del>Set Personal Info</del><BR>");
        } else {
            output.println("Set Personal Info<BR>");
        }
        output.println(""+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"DateForm\" CHECKED>");
        if (set_date_done) {
            output.println("<del>Set Date</del><BR>");
        } else {
            output.println("Set Date<BR>");
        }
        output.println(""+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"LocationForm\">");
        if (set_location_done) {
            output.println("<del>Set Location</del><BR>");
        } else {
            output.println("Set Location<BR>");
        }
        output.println(""+
        "<INPUT TYPE=\"radio\" NAME=\"next_page\" VALUE=\"VehiclesForm\">Show Vehicles<BR>"+
        "<INPUT TYPE=\"submit\" VALUE=\"Submit\">"+
        "<INPUT TYPE=\"reset\">"+
        "</FORM>");

        output.println( "</BODY></HTML>" );
        output.close();
   }

}
