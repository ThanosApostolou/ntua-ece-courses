// Setting and Retrieving Cookies
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class IndexForm extends HttpServlet {

   public void doPost( HttpServletRequest request,                  // reaction to the reception of POST
                       HttpServletResponse response )   throws ServletException, IOException
   {
      PrintWriter output;
      String first_name = request.getParameter( "first_name" );
      String last_name = request.getParameter( "last_name" );
      String age = request.getParameter( "age" );
      String next_page = request.getParameter( "next_page" );
      Cookie c1 = new Cookie("first_name", first_name);
      c1.setMaxAge( 120 );
      response.addCookie( c1 );
      Cookie c2 = new Cookie("last_name", last_name);
      c2.setMaxAge( 120 );
      response.addCookie( c2 );
      Cookie c3 = new Cookie("age", age);
      c3.setMaxAge( 120 );
      response.addCookie( c3 );
      response.setContentType( "text/html" );
      response.sendRedirect("/ask1/"+next_page);
   }

}
