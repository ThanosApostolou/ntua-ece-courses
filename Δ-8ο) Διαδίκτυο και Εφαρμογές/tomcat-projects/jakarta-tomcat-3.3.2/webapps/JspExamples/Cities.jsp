<%!private String names[] = { "Europe", "Asia", "America", "Africa" };
      private String cities[] = {"Athens and Rome", "Peking and Amman", 
                                                                   "New York", "Cairo" };       
      private String getCities( String conString)
       {
          for ( int i = 0; i < names.length; ++i )
              if ( conString.equals( names[ i ] ) ) return cities[ i ];
           return "";  // no matching string found
       }
%>

<HTML><HEAD><TITLE> JSP Second Page </TITLE></HEAD>
<BODY>
    <% String contiNames[];
    if ( session != null )  contiNames=session.getValueNames();
    else
        contiNames = null;
    if ( contiNames != null && contiNames.length != 0 ) { %>
    
    <H1>Recommended Destinations</H1>
    <%
    for ( int i = 0; i < contiNames.length; i++ ) {
        String val = (String) session.getValue(contiNames[i]); 
        out.println(
                "In " + contiNames[i] + " we recommend to visit "
                + val + "<BR>" );
        }
    }     
    else { %>
        <H1>Sorry, no recommendation possible !</H1>
            You did not select a continent or the session has been terminated.
    <% }
    %>
</BODY></HTML>
