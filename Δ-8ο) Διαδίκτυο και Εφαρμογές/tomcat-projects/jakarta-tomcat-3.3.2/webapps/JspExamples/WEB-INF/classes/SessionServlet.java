// Using Session
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class SessionServlet extends HttpServlet {
   private String names[] = { "Europe", "Asia", "America", "Africa" };
   private String cities[] = { "Athens and Rome", "Peking and Amman",  "New York", "Cairo" };

   public void doPost( HttpServletRequest request,
                       HttpServletResponse response )
      throws ServletException, IOException
   {
      PrintWriter output;
      String conti = request.getParameter( "continent" );

//                             Replace the following 3 'cookie' lines
//Cookie c = new Cookie( conti, getCities( conti ) );
//c.setMaxAge( 120 );  // seconds until cookie removed
//response.addCookie( c );  // must precede getWriter
// BY
      //create a new session (with arg 'true')
      HttpSession session  = request.getSession(true);
      session.putValue(conti, getCities( conti ));
 //
      
      response.setContentType( "text/html" );
      output = response.getWriter();         

      // send HTML page to client
      output.println( "<HTML><HEAD><TITLE>" );
      output.println( "Session" );
      output.println( "</TITLE></HEAD><BODY>" );
      output.println( "<P>Welcome to Sessions!<BR>" );
      output.println( "<P>" );
      output.println( conti );
      output.println( " is an interesting continent !" );
      output.println( "</BODY></HTML>" );
      output.close();    // close stream
   }

   public void doGet( HttpServletRequest request,
                      HttpServletResponse response )
                      throws ServletException, IOException
   {
      PrintWriter output;

//                             Replace the following 2 'cookie' lines
//      Cookie cookies[];    
//      cookies = request.getCookies(); // get client's cookies
// BY
      //do not create a new session (with arg 'false')
      HttpSession session  = request.getSession(false);
      String contiNames[];
          
       if ( session != null )
            contiNames=session.getValueNames();
       else
         contiNames = null;
      
      
      response.setContentType( "text/html" ); 
      output = response.getWriter();

      output.println( "<HTML><HEAD><TITLE>" );
 //   output.println( "Cookie with cities has been read !" );
      output.println( "</TITLE></HEAD><BODY>" );

 //     if ( cookies.length != 0 ) {

        if ( contiNames != null && contiNames.length != 0 ) {
         output.println( "<H1>Recommended Destinations</H1>" );

//       for ( int i = 0; i < contiNames.length; i++ ) 
//           output.println(
//               "In " + cookies[ i ].getName() + " we recommend to visit "
//                + cookies[ i ].getValue() + "<BR>" );
//     }
        for ( int i = 0; i < contiNames.length; i++ ) {
           String val = (String) session.getValue(contiNames[i]);
           output.println(
                  "In " + contiNames[i] + " we recommend to visit "
                  + val + "<BR>" );
            }
        }     
            
      else {
         output.println( "<H1>Sorry, no recommendation possible !</H1>" );
         output.println( "You did not select a continent or " );
         output.println( "the session has been terminated." );
      }

      output.println( "</BODY></HTML>" );
      output.close();    // close stream
   }

   private String getCities( String conString)
   {
      for ( int i = 0; i < names.length; ++i )
         if ( conString.equals( names[ i ] ) )
            return cities[ i ];

      return "";  // no matching string found
   }
}
