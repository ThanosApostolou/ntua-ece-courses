<html>
<%@ page isErrorPage="true" %>

<!--
  Copyright 1999-2004 The Apache Software Foundation
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<head>
    <title>Admin Error</title>
</head>

<body bgcolor="white">
<table border=0 cellspacing=5>
  <tr>
    <td><a href="/">
        <img SRC="../tomcat.gif" height=92 width=130 align=LEFT border=0 alt="Tomcat Home Page"></a>
    <td valign=center><h2>Tomcat Administration Tools</h2>
  </tr>
  <tr>
      <td valign=top align=center>
        <b><font face="Arial, Helvetica, sans-serif" size=-5>
        <a href="/">Tomcat Home Page</a>
        </b></font>
  </tr>
</table>

<br>

<h3>Error in Admin WebApp, probably you doesn't have the Admin Context as trusted, follow instructions at <a href="..">Tomcat Admin Homepage</a> <br/></h3>

</body>
</html>
