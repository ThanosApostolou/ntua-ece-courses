<html>
<!--   
    Copyright 1999-2004 The Apache Software Foundation
  
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
  
        http://www.apache.org/licenses/LICENSE-2.0
  
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->

<body bgcolor="white">
<html>
<h1>Security Examples</h1>
<table border=0>
    <tr><td>
        <a href='<%= response.encodeURL("protected/index.jsp") %>'>Protected Directory, browse it with cookies disabled</a><br/>
    </td></tr>
    <tr><td>
        <a href='protected/index.jsp'>Protected Directory, Use with cookies enabled browser</a>
    </td></tr>
</table>

</html>
