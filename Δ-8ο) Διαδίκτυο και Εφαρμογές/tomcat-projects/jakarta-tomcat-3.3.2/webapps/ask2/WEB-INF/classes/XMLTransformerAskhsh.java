import java.io.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
// for servlet:
import javax.servlet.*;
import javax.servlet.http.*;

import org.w3c.dom.*;  // for DOM (Java DOM)
import javax.xml.parsers.*;

import javax.xml.transform.dom.*;// for transformations

public class XMLTransformerAskhsh extends HttpServlet {
	ServletContext ctx;
	String absPath;          //absolute path to our files - see below
	TransformerFactory tF;
	Transformer myTransformer;// will be built at init, will be used by doGet
	Document doc;

	public void init(ServletConfig config) throws UnavailableException {
		System.out.println("Init start");
		try {
			ctx = config.getServletContext();   // we will use the 'contex' below
			absPath = ctx.getRealPath("/WEB-INF/Presentor.xsl");
			tF = TransformerFactory.newInstance();
			DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
			// absolutely important, to understand the meaning of prefix 'xslt' !!!!
			fact.setNamespaceAware(true);
			DocumentBuilder builder = fact.newDocumentBuilder();
			doc = builder.parse(absPath);
			System.out.println("Name of document element is " +  doc.getDocumentElement().getNodeName());
		      } catch (Exception e) {	e.printStackTrace(); }
		System.out.println("Init end");
	}

	private void changeDomByColor(Document doc, String color, String table_color, String font_size) {
		NodeList h1_el = doc.getElementsByTagName("h1");
		NodeList table_el = doc.getElementsByTagName("table");

		Attr a = doc.createAttribute("style");
		a.setValue("background-color: "+color);
		h1_el.item(0).getAttributes().setNamedItem(a);

		Attr b = doc.createAttribute("style");
		if (table_color.equals("light")) {
			b.setValue("background-color: #ffffff; color: #000000; font-size: " + font_size + "px");
		} else {
			b.setValue("background-color: #000000; color: #ffffff; font-size: " + font_size + "px");
		}
		table_el.item(0).getAttributes().setNamedItem(b);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("dopost start");
		System.out.println("Name of document element (at the post) is " + doc.getDocumentElement().getNodeName());
		String color = request.getParameter("color");
		String table_color = request.getParameter("table_color");
		String font_size = request.getParameter("font_size");
		String file = request.getParameter("file");

		System.out.println("You selected the color "  + color);
		System.out.println(doc.getElementsByTagName("h1").item(0).getAttributes().getNamedItem("style").getNodeValue());
		changeDomByColor(doc, color, table_color, font_size);
		System.out.println(doc.getElementsByTagName("h1").item(0).getAttributes().getNamedItem("style").getNodeValue());
		System.out.println(doc.getElementsByTagName("table").item(0).getAttributes().getNamedItem("style").getNodeValue());
		PrintWriter pwr = response.getWriter();
		try {
			DOMSource ds = new DOMSource(doc) ;
	       	System.out.println( ((Document)ds.getNode()).getDocumentElement().getNodeName() +" " +((Document)ds.getNode()).getDocumentElement().getNodeValue() ) ;
			myTransformer = tF.newTransformer(ds);

			StreamSource xmlSource = new StreamSource(ctx.getResourceAsStream("/WEB-INF/" + file));
			System.out.println("Sending back the xml tranformed into html");
			response.setContentType("text/html"); //in order to put in http body
			myTransformer.transform(xmlSource, new StreamResult(pwr));
			pwr.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("dopost stop");
	}

}