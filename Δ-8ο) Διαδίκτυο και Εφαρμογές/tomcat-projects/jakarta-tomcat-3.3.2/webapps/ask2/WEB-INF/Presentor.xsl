<?xml version="1.0"?>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <head>
        <title>Printout Table</title>
      </head>
      <body>
        <h1 style="background-color: #446600" align="center"> Table of  <xsl:value-of select="name(//*[1])"/> </h1>
        <table style="background-color: #446600" align="center" border="2">
          <tr><xsl:apply-templates select="/*/*[1]/*|/*/*[1]/@*" mode="column_names"/></tr>
          <xsl:apply-templates select="/*/*"/>
        </table>

        <p align="center"><a href="/ask2">Home</a></p>
      </body>
    </html>
  </xsl:template>

<!--   .......... .....  This template for column names .....................   -->
<xsl:template match="/*/*[1]/*|/*/*[1]/@*" mode="column_names">
  <td><xsl:value-of select="name()"/></td>
</xsl:template>
<!-- .................................................................................................... -->

<!--  ......................... These templates for values ........................  -->
<xsl:template match="/*/*">
  <tr><xsl:apply-templates select="*|@*"/></tr>
</xsl:template>

<xsl:template match="/*/*/*|/*/*/@*">
  <td><xsl:value-of select="."/></td>
</xsl:template>
<!-- .................................................................................................... -->

</xsl:stylesheet>