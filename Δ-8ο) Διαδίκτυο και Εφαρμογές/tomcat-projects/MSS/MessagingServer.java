package MSS;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;
import java.util.List;
import java.util.Iterator;
import javax.mail.MessagingException;

//for SOAP
import org.apache.soap.Constants;
import org.apache.soap.Envelope; //*New!!! for messaging */
import org.apache.soap.Fault;
import org.apache.soap.SOAPException;
import org.apache.soap.encoding.SOAPMappingRegistry;
import org.apache.soap.encoding.soapenc.BeanSerializer;
import org.apache.soap.rpc.Call;
import org.apache.soap.rpc.Parameter;
import org.apache.soap.rpc.Response;
import org.apache.soap.rpc.SOAPContext;
import org.apache.soap.util.xml.QName;

// DOM related
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class MessagingServer {
     public void vehiclesSOAP(Envelope env, SOAPContext  req, SOAPContext  res)
                throws java.io.IOException, javax.mail.MessagingException   {

         System.out.println(".... inside MessagingServer's vehiclesSOAP method");

         // Get the vehicle element as the first body entry of the SOAP message;
         // here there is one body element - in general could be many!
         // so in general we put these in a vector,
         // then iterate though it and finally get the XML in DOM form into memory!


         Vector bodyEntries = env.getBody().getBodyEntries();
         Element vehicles_in_DOM = (Element)bodyEntries.iterator().next();
         System.out.println(vehicles_in_DOM.getTagName() +
                          ".... first body entry extracted out of SOAP");

         // DOM processing in Java for transforming the XML

         // A test that we actually have access to vehicles through DOM:
         // Go to all vehicles>jeeps>jeep and print all colour attributes
         Element jeeps_in_DOM =
              (Element)vehicles_in_DOM.getElementsByTagName("jeeps").item(0);
         NodeList jeepList = jeeps_in_DOM.getElementsByTagName("jeep");

         for (int i=0, len=jeepList.getLength(); i<len; i++){
         Element jeep = (Element)jeepList.item(i);
         System.out.println("Printing jeep colour from within DOM: "
                                            + jeep.getAttribute("colour"));
       	 jeep.getAttributeNode("user").setValue("military");
       	 System.out.println("Jeep 'user' changed - check that in the response");

       	 // Build the response SOAP message
       	 bodyEntries.clear(); // reuse of same Vector object
      	 bodyEntries.add(jeeps_in_DOM);
         StringWriter writerObj = new StringWriter();
         env.marshall(writerObj,null);

         // Send envelope back to client
         res.setRootPart(writerObj.toString(),"text/xml");
       	 }
     }
}