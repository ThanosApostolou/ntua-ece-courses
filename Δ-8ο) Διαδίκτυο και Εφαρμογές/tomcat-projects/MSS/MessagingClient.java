import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
// for SAX & DOM
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
/// for SOAP
import org.apache.soap.Constants;
import org.apache.soap.Envelope;
import org.apache.soap.SOAPException;
import org.apache.soap.messaging.Message;
import org.apache.soap.transport.SOAPTransport;
import org.apache.soap.util.xml.XMLParserUtils;

public class MessagingClient {
   // the following method coverts the .xml file into a SOAP message
   // and the sends this to seviceURL
   public void sender (URL serviceURL, String msgFilename)
              throws IOException, SAXException, SOAPException {

              // Handle the XML document supplied in the input file
              FileReader readerObj = new FileReader(msgFilename);
              DocumentBuilder builder = XMLParserUtils.getXMLDocBuilder();
              Document doc = builder.parse(new InputSource(readerObj));

              if (doc == null) {
              	throw new SOAPException(Constants.FAULT_CODE_CLIENT,"Error in XML parsing");
              }

              // Create Message Envelope
              Envelope env = Envelope.unmarshall(doc.getDocumentElement());

              // Send the Message
              Message msg = new Message();
              msg.send(serviceURL, "urn:Messaging_Service", env);

              // Receive response (e.g. to the previous message sent)
    }


  public static void main(String args[])   {
      if (args.length !=2) {
      	System.out.println("Please give the url and the .xml message file");
      	return; }

      try {
      	URL serviceURL = new URL(args[0]);

      	MessagingClient messagingClientObj = new MessagingClient();
      	messagingClientObj.sender(serviceURL, args[1]);

      } catch (Exception e) {e.printStackTrace();}
  }
 }