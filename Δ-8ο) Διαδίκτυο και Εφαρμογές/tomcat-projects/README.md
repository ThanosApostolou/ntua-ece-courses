# shmmy-di2019

## Classpath
```
CLASSPATH=".:/home/thanos/Documents/shmmy-di2019/jars/*"
```
or use depending on your folder:
```
javac -cp ".:../../../../../jars/*" *.java
javac -cp ".:../jars/*" *.java
java -cp ".:../jars/*" CLASS
```

## TCP Monitor
```
java -cp ".:../jars/*"  org.apache.axis.utils.tcpmon
```

## Create WAR
```
cd jakarta-tomcat-3.3.2/webapps/ask1
jar cvfM ask1.war WEB-INF index.html
```

## create jar
jar cvf  NAME.jar NAME.class
copy NAME.jar to **jakarta-tomcat-3.3.2/lib/common**

## BVShop
```
cd BVShop
javac *.java
jar cvf BV.jar VehicleBean.class BVCatalog.class
cp BV.jar ../jakarta-tomcat-3.3.2/lib/common/
java org.apache.soap.server.ServiceManagerClient http://localhost:8080/soap/servlet/rpcrouter deploy BVCatalogDD.xml
cd ..
#java org.apache.axis.utils.tcpmon #tcpmonitor listens to 8081
java BVShop.BVAdderLister http://localhost:8081/soap/servlet/rpcrouter  “Smart” “Swatch”  “2001”
```

## MSS
```
cd MSS
javac *.java
jar cvf MServer.jar MessagingServer.class
cp MServer.jar ../jakarta-tomcat-3.3.2/lib/common/
java org.apache.soap.server.ServiceManagerClient http://localhost:8080/soap/servlet/rpcrouter deploy MessagingServiceDD.xml
#java org.apache.axis.utils.tcpmon #tcpmonitor listens to 8081
java MessagingClient http://localhost:8081/soap/servlet/messagerouter vehiclesSOAP.xml
```

## MyVShop
  - Service
```
cd ask3
javac MyVShop/*.java
jar cvf MyV.jar MyVShop/VehicleBean.class MyVShop/MotorBean.class MyVShop/MyVCatalog.class
cp MyV.jar ../jakarta-tomcat-3.3.2/lib/common/
java org.apache.soap.server.ServiceManagerClient http://localhost:8080/soap/servlet/rpcrouter deploy MyVCatalogDD.xml
```
restart tomcat
  - Client
```
#java org.apache.axis.utils.tcpmon #tcpmonitor listens to 8081
java MyVShop.MyVAdderLister http://localhost:8081/soap/servlet/rpcrouter add Smart Swatch 2001 1550 200 150
java MyVShop.MyVAdderLister http://localhost:8081/soap/servlet/rpcrouter remove Smart
```
