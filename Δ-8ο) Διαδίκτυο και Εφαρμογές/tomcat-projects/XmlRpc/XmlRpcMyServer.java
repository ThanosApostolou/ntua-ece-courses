import java.io.IOException;
import org.apache.xmlrpc.*;
import org.apache.xerces.parsers.*;

public class XmlRpcMyServer {

    public static void main(String args[]) throws  ClassNotFoundException {
        //Specify driver for parsing/encoding from/to XML
        //(XmlRpc is static !)
        XmlRpc.setDriver("org.apache.xerces.parsers.SAXParser");

        //Instantiate & start the server
        System.out.println("Server geting up");
        WebServer server = new WebServer(Integer.parseInt(args[0]));
        server.start();

        //Register the handler
        server.addHandler("hello", new HelloHandler());
        System.out.println("Handler registered");
    }

}