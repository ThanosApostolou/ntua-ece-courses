import java.io.IOException;
import java.util.Vector;
import org.apache.xmlrpc.*;
import org.apache.xerces.parsers.*;


public class XmlRpcMyClient {

    public static void main(String args[])
            throws ClassNotFoundException,IOException, XmlRpcException
    {
        String serverPort = args[0];
        XmlRpc.setDriver("org.apache.xerces.parsers.SAXParser");
        //Specify the server and port
        XmlRpcClient client=new XmlRpcClient("http://localhost:" + serverPort  + "/");

        //Create a request
        Vector params= new Vector();
        params.addElement(args[1]);

        //Make request and print result
        String result = (String)client.execute ("hello.sayHello",params);
        System.out.println("Response from Server: " + result);
    }

}