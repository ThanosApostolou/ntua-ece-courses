import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.soap.Constants;
import org.apache.soap.Fault;
import org.apache.soap.SOAPException;
import org.apache.soap.rpc.Call;
import org.apache.soap.rpc.Parameter;
import org.apache.soap.rpc.Response;


public class VAdderLister {

     public void addlist(URL url, String model, String manufacturer)
         throws SOAPException{

         //Build the Call object
         Call call = new Call();
         call.setTargetObjectURI("urn:VehicleCatalog");
         call.setMethodName("addV");
         call.setEncodingStyleURI(Constants.NS_URI_SOAP_ENC);

 //------------  A D D I N G ---------------------------------------------------
          System.out.println("Adding vehicle model '" + model + "' by " + manufacturer);

         // Set up the parameters of the call
         Vector params = new Vector();
         params.addElement(new Parameter("model", String.class, model, null));
         params.addElement(new Parameter("manufacturer", String.class, manufacturer, null));
         call.setParams(params);

         // Invoke the call
         Response response;
         response = call.invoke(url, "");
         if (!response.generatedFault())
              {        System.out.println("Successful Vehicle addition");}
         else {        Fault fault = response.getFault();
                       System.out.println("Error: " + fault.getFaultString());}

 //------------  L I S T I N G ---------------------------------------------------
               System.out.println("Listing the new updated catalog");

         // The Call object already in place,
         // updating only the relevant properties of this object
         call.setMethodName("listV");
         call.setParams(null); //the listV method has no input arguments

           // Invoke the call (using the same Response object)

         response = call.invoke(url, "");
         if (!response.generatedFault())
              {        System.out.println("Successful invocation of the listV method");
                       Parameter returnValue = response.getReturnValue();
                       Hashtable catalog = (Hashtable)returnValue.getValue();
                       Enumeration e = catalog.keys();
                       while (e.hasMoreElements()){
                       	model =(String)e.nextElement();
                       	manufacturer= (String) catalog.get(model);
                       	System.out.println("  '" + model + "' by " + manufacturer);}
               } else {
                 Fault fault = response.getFault();
                 System.out.println("Error reported: " + fault.getFaultString());
               }

     }

     public static void main(String[] args) {
	if (args.length !=3)
           {System.out.println("Put url, model and manufacturer as arguments !!");
           return;}

        try {
               // URL for SOAP server to connect to
               URL urll = new URL(args[0]);

               // Get values for the new vehicle
               String model = args[1];
               String manufacturer= args[2];

               //Add the new vehicle, then list catalog
               VAdderLister adderlister = new VAdderLister();
               adderlister.addlist(urll, model, manufacturer);
        }  catch (Exception e) {e.printStackTrace();}
     }
}