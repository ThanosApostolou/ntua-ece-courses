from __future__ import print_function

import sys

from pyspark import SparkContext
from pyspark.streaming import StreamingContext

#def flatmapper (line) :
#   return line.split("")

def mapper (line) :
    line_list = line.split(",")
    start_date = line_list[1]
    hour = start_date.split(" ")[1].split(":")[0]
    int_hour = int(hour)
    #finish_date = line.split(",")[2]
    return (int_hour, 1)

def reducer (a, b) :
    return a+b

if __name__ == "__main__":
    sc = SparkContext(appName="Q1-MR")

    text_file = sc.textFile("hdfs://master:9000/tripdata-min.csv")

    # flatMap(flatmapper)
    counts = text_file.map(mapper).reduceByKey(reducer)
    print("Saving to file Q1-MR-out.txt\n")
    counts.saveAsTextFile("hdfs://master:9000/Q1-MR-out.txt")
