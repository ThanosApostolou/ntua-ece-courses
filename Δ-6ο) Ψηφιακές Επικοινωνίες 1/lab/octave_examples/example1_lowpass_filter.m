%% Lowpass FIR filter ? closed-form and windowing
clear all; close all;
load sima;   % Fs=8192;
% figure; pwelch(s,[],[],[],Fs);   % for MATLAB
figure; pwelch(s,[],[],[],Fs,  'semilogy');  % for octave
fo=1200; Ts=1/Fs;
N=128;   % ���� �������)
t=[-(N-1):2:N-1]*Ts/2;
hlp=1/Fs*sin(2*pi*fo*t)/pi./t;
hlpw=hlp.*kaiser(length(hlp),5)';
wvtool(hlp,hlpw);
sima_lp=conv(s,hlpw);
% figure; pwelch(sima_lp,[],[],[],Fs);   % for MATLAB
figure; pwelch(sima_lp,[],[],[],Fs,  'semilogy');  % for octave
sima_lp=conv(s,hlp);
% figure; pwelch(sima_lp,[],[],[],Fs);   % for MATLAB
figure; pwelch(sima_lp,[],[],[],Fs. 'semilogy');  % for octave
