%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 1 Δημιουργήστε το σήμα
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all %MINE   % κλείστε όλες τις γραφικές παραστάσεις
clear all %MINE   % καθαρίστε τον χώρο εργασίας
clc %MINE         % καθαρίστε το παράθυρο εντολών
Fs=1000;          % συχνότητα δειγματοληψίας 1000 Hz
Ts=1/Fs; %MINE    % περίοδος δειγματοληψίας
L=1000;           % μήκος σήματος (αριθμός δειγμάτων)
T=L*Ts;           % διάρκεια σήματος
t=0:Ts:(L-1)*Ts;  % χρονικές στιγμές υπολογισμού το σήματος

x=sin(2*pi*30*t)...           % ημιτονικό σήμα συχνότητας 30 Hz
+ 0.5*sin(2*pi*80*(t-2))...   % συνιστώσα 80 Hz
+ sin(2*pi*60*t);             % συνιστώσα 60 Hz

figure(1); % νοιγμα παραθύρου για γραφική παράσταση
plot(t,x); % γραφική παράσταση του σήματος
title('Time domain plot of x - ΑΘΑΝΑΣΙΟΣ ΑΠΟΣΤΟΛΟΥ'); % τίτλος γραφικής παράστασης
xlabel('t (sec)');          % λεζάντα στον άξονα x
ylabel('Amplitude');    % λεζάντα στον άξονα y
waitforbuttonpress(); % αναμονή, πιέστε ένα πλήκτρο για να συνεχίσετε
axis([0 0.3 -2 2]);       % εμφάνιση του σήματος από 0 έως 0.3 sec και
                                  % κλίμακα από -2 έως 2
waitforbuttonpress(); % αναμονή, πιέστε ένα πλήκτρο για να συνεχίσετε


% Υπολογίστε τον διακριτό μετασχηματισμό Fourier
N = 2^nextpow2(L);   % μήκος μετασχηματισμού Fourier.
                                  % η nextpow2 βρίσκει τον εκθέτη της δύναμης του 2 που
                                  % είναι μεγαλύτερη ή ίση από το όρισμα L
                                  % εναλλακτικά, =ceil(log2(L))
Fo=Fs/N; %MINE      % ανάλυση συχνότητας              
f=(0:N-1)*Fo;       % διάνυσμα συχνοτήτων             
X=fft(x,N); %MINE   % αριθμητικός υπολογισμός του διακριτού μετασχηματισμού
                    % Fourier (DFT) για Ν σημεία

% Σχεδιάστε το σήμα στο πεδίο συχνότητας
% Αφού το σήμα είναι πραγματικό μπορείτε
% να σχεδιάσετε μόνο τις θετικές συχνότητες

figure(2);                         % άνοιγμα παραθύρου για γραφική παράστα-ση
plot(f(1:N),abs(X(1:N))); %MINE    % γραφική παράσταση των θετικών συχνοτή-των
title('Frequency domain plot of x - ΑΘΑΝΑΣΙΟΣ ΑΠΟΣΤΟΛΟΥ'); % τίτλος γραφικής παράστασης
xlabel('f (Hz)');                  % λεζάντα στον άξονα x
ylabel('Amplitude');               % λεζάντα στον άξονα y
waitforbuttonpress();              % αναμονή για να δείτε το σχήμα
                                  % πιέστε ένα πλήκτρο για να συνεχίσετε

% για τη γραφική παράσταση του αμφίπλευρου φάσματος
% πρέπει να χρησιμοποιήσετε την fftshift ώστε ο όρος για
% τη συχνότητα μηδέν να μετακινηθεί στην αρχή των αξόνων
% δείτε help fftshift για περισσότερες λεπτομέρειες

figure(3);       % άνοιγμα παραθύρου για γραφική παράσταση
f=f-Fs/2;       % ολίσθηση συχνοτήτων προς τα αριστερά κατά –Fs/2
X=fftshift(X);  % ολίσθηση της μηδενικής συχνότητας στο κέντρο
                % του φάσματος
                % (ακολουθούν πολλές εντολές σε μια γραμμή)
plot(f,abs(X));
title('Two sided spectrum of x - ΑΘΑΝΑΣΙΟΣ ΑΠΟΣΤΟΛΟΥ');
xlabel('f (Hz)');
ylabel('Amplitude');
waitforbuttonpress();        % αναμονή, πιέστε ένα πλήκτρο για να συνεχίσετε

power=X.*conj(X)/N/L;       % υπολογισμός πυκνότητας ισχύος
figure(4);                   % άνοιγμα παραθύρου για γραφική παράσταση
plot(f,power);               % ισχύς ανά συνιστώσα συχνότητα
xlabel('Frequency (Hz)');    % λεζάντα στον άξονα x
ylabel('Power');             % λεζάντα στον άξονα y
title('{\bf Periodogram}  - ΑΘΑΝΑΣΙΟΣ ΑΠΟΣΤΟΛΟΥ');  % τίτλος διαγράμματος με παχιά γράμματα
waitforbuttonpress();
disp('Part2');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 2 Προσθέστε θόρυβο στο σήμα
% Συμπληρώστε τον κώδικα για τη δημιουργία του σήματος θορύβου n με τη
% βοήθεια της συνάρτησης randn.
% Το διάνυσμα θορύβου n θα πρέπει να είναι του ίδιου μεγέθους με αυτό της
% ημιτονοειδούς κυματομορφής x του πρώτου μέρους. Δείτε help size.
% Σχεδιάστε το σήμα θορύβου στο διάστημα από 0 έως 0.3 sec και κλίμακα
% από -2 έως 2
% Υπολογίστε το περιοδόγραμμα του n και σχεδιάστε την πυκνότητα φάσματος
% ισχύος του σήματος θορύβου.
% Προσθέστε το σήμα θορύβου και το x για να λάβετε το σήμα με θόρυβο s.
% Σχεδιάσατε το σήμα με θόρυβο s στο πεδίο του χρόνου στην περιοχή 0 έως
% 0.2 sec και κλίμακα από -2 έως 2 καθώς και το αμφίπλευρο φάσμα του
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Σχεδιάστε το σήμα θορύβου στο διάστημα από 0 έως 0.3 sec και κλίμακα
% από -2 έως 2
n=randn(size(x));     % θόρυβος με μέγεθος ίσο του σήματος x
figure(5);                  % Ανοιγμα παραθύρου για γραφική παράσταση
plot(t,n);                    % γραφική παράσταση του θορύβου
title('Time domain plot of n - ΑΘΑΝΑΣΙΟΣ ΑΠΟΣΤΟΛΟΥ') % τίτλος γραφικής παράστασης
xlabel('t (sec)');          % λεζάντα στον άξονα x
ylabel('Amplitude');    % λεζάντα στον άξονα y
axis([0 0.3 -2 2])        % εμφάνιση του σήματος από 0 έως 0.3 sec και κλίμακα από -2 έως 2
waitforbuttonpress()  % αναμονή, πιέστε ένα πλήκτρο για να συνεχίσετε

% Υπολογίστε το περιοδόγραμμα του n και σχεδιάστε την πυκνότητα φάσματος
% ισχύος του σήματος θορύβου.
FN=fft(n,N);          % fourier του n
power=FN.*conj(FN)/N/L;       % υπολογισμός πυκνότητας ισχύος
figure(6);                   % άνοιγμα παραθύρου για γραφική παράσταση
plot(f,power);               % ισχύς ανά συνιστώσα συχνότητα
xlabel('Frequency (Hz)');    % λεζάντα στον άξονα x
ylabel('Power');             % λεζάντα στον άξονα y
title('{\bf Periodogram of s}  - ΑΘΑΝΑΣΙΟΣ ΑΠΟΣΤΟΛΟΥ');  % τίτλος διαγράμματος με παχιά γράμματα
waitforbuttonpress();

% Προσθέστε το σήμα θορύβου και το x για να λάβετε το σήμα με θόρυβο s.
% Σχεδιάσατε το σήμα με θόρυβο s στο πεδίο του χρόνου στην περιοχή 0 έως
% 0.2 sec και κλίμακα από -2 έως 2
s=x+n;
figure(6);                  % νοιγμα παραθύρου για γραφική παράσταση
plot(t,s);                    % γραφική παράσταση του θορύβου
title('Time domain plot of s - ΑΘΑΝΑΣΙΟΣ ΑΠΟΣΤΟΛΟΥ'); % τίτλος γραφικής παράστασης
xlabel('t (sec)');          % λεζάντα στον άξονα x
ylabel('Amplitude');    % λεζάντα στον άξονα y
axis([0 0.2 -2 2])        % εμφάνιση του σήματος από 0 έως 0.3 sec, κλίμακα από -2 έως 2
waitforbuttonpress();  % αναμονή, πιέστε ένα πλήκτρο για να συνεχίσετε

% καθώς και το αμφίπλευρο φάσμα του
S=fft(s,N);
f=(0:N-1)*Fo;      % διάνυσμα συχνοτήτων 
f=f-Fs/2;             % ολίσθηση συχνοτήτων προς τα αριστερά κατά –Fs/2
S=fftshift(S);      % ολίσθηση της μηδενικής συχνότητας στο κέντρο
                          % του φάσματος
                          % (ακολουθούν πολλές εντολές σε μια γραμμή)
figure(7)             % άνοιγμα παραθύρου για γραφική παράσταση
plot(f,abs(S));
title('Two sided spectrum of s - ΑΘΑΝΑΣΙΟΣ ΑΠΟΣΤΟΛΟΥ');
xlabel('f (Hz)');
ylabel('Amplitude');
waitforbuttonpress();        % αναμονή, πιέστε ένα πλήκτρο για να συνεχίσετε
disp('Part3');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 3. Πολλαπλασιασμός σημάτων
% Συμπληρώστε τον κώδικα δημιουργίας ενός ημιτονοειδούς σήματος συχνότητας
% 100 Hz και πολλαπλασιάστε με το προηγούμενο σήμα s.
% Τα δύο σήματα θα πρέπει να είναι του ίδιου μεγέθους και να χρησιμοποιηθεί
% ο τελεστής '.*' για ανά στοιχείο πολλαπλασιασμό.
% Σχεδιάστε το αποτέλεσμα στο πεδίο του χρόνου στην περιοχή 0 έως 0.2 sec
% και κλίμακα από -2 έως 2 καθώς και στο πεδίο της συχνότητας
% χρησιμοποιώντας τη συνάρτηση fftshift.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Συμπληρώστε τον κώδικα δημιουργίας ενός ημιτονοειδούς σήματος συχνότητας
% 100 Hz και πολλαπλασιάστε με το προηγούμενο σήμα s.
f_y=100;                     % hertz
y=cos(2*pi*f_y*t);
y=y.*s;

% Σχεδιάστε το αποτέλεσμα στο πεδίο του χρόνου στην περιοχή 0 έως 0.2 sec
% και κλίμακα από -2 έως 2
figure(8);                    % άνοιγμα παραθύρου για γραφική παράσταση
plot(t,y);
title('Time domain plot of y - ΑΘΑΝΑΣΙΟΣ ΑΠΟΣΤΟΛΟΥ');
xlabel('t (sec)');
ylabel('Amplitude');
axis([0 0.2 -2 2]); 
waitforbuttonpress();        % αναμονή, πιέστε ένα πλήκτρο για να συνεχίσετε

% καθώς και στο πεδίο της συχνότητας
Y=fft(y,N);
f=(0:N-1)*Fo;      % διάνυσμα συχνοτήτων 
f=f-Fs/2;             % ολίσθηση συχνοτήτων προς τα αριστερά κατά –Fs/2
Y=fftshift(Y);      % ολίσθηση της μηδενικής συχνότητας στο κέντρο

figure(9);                     % άνοιγμα παραθύρου για γραφική παράσταση
plot(f,abs(Y));
title('Spectrum of y - ΑΘΑΝΑΣΙΟΣ ΑΠΟΣΤΟΛΟΥ');
xlabel('f (Hz)');
ylabel('Amplitude');
waitforbuttonpress();        % αναμονή, πιέστε ένα πλήκτρο για να συνεχίσετε