function [ber,numBits] = ber_func_12910(EbNo, maxNumErrs, maxNumBits)
% Import Java class for BERTool.
import com.mathworks.toolbox.comm.BERTool;
% Initialize variables related to exit criteria.
totErr = 0; % Number of errors observed
numBits = 0; % Number of bits processed
% A. --- Set up parameters. ---
% --- INSERT YOUR CODE HERE.
bps=1; % number of bits per symbol
M=2^bps;
Nsymb=10000; % number of symbols in each run
ns=2*M*4; % oversampling,i.e. number of samples per T
% Simulate until number of errors exceeds maxNumErrs
% or number of bits processed exceeds maxNumBits.
while((totErr < maxNumErrs) && (numBits < maxNumBits))
    % Check if the user clicked the Stop button of BERTool.
    if (BERTool.getSimulationStop)
        break;
    end
    % Β. --- INSERT YOUR CODE HERE.
    %errors=fsk_errors_coh_12910(bps,Nsymb,ns,EbNo);
    %errors=fsk_errors_noncoh_12910(bps,Nsymb,ns,EbNo);
    errors=msk_errors_12910(Nsymb,ns,EbNo);
    % Assume Gray coding: 1 symbol error ==> 1 bit error
    totErr=totErr+errors;
    numBits=numBits + bps*Nsymb;
end % End of loop
% Compute the BER
ber = totErr/numBits;
