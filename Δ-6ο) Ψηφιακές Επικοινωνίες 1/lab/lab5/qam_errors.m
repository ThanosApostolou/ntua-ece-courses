function errors=qam_errors(k,Nsymb,nsamp,EbNo)
% Η συνάρτηση αυτή εξομοιώνει την παραγωγή και αποκωδικοποίηση
% θορυβώδους ακολουθίας M-QAM πλήρους ορθογωνικού πλέγματος LxL,
% μετράει δε και επιστρέφει τον αριθμό των παρατηρούμενων λαθών.
% k είναι ο αριθμός των bits ανά σύμβολο (άρτιος), έτσι L=2^(k/2).
% Nsymb είναι το μήκος της παραγόμενης ακολουθίας συμβόλων,
% nsamp είναι ο συντελεστής υπερδειγμάτισης (αριθ. δειγμάτων/Τ),
% EbNo είναι ο ανηγμένος σηματοθορυβικός λόγος Eb/No, σε db.
%
M=2^k; L=sqrt(M);
SNR=EbNo-10*log10(nsamp/k); % SNR ανά δείγμα σήματος
x=2*floor(L*rand(1,Nsymb))-L+1; % in-phase
y=2*floor(L*rand(1,Nsymb))-L+1; % quadrature
u=x+i*y;
% Αναλυτικό σήμα QAM (βασικής ζώνης)
h=ones(1,nsamp);% κρουστ. αποκρ. φίλτρου πομπού (ορθογ. παλμός)
ztx=upsample(u,nsamp); % μετατροπή στο πυκνό πλέγμα
ztx=conv(ztx,h); % το προς εκπομπή σήμα
ztx=ztx(1:Nsymb*nsamp);% περικόπτεται η ουρά που αφήνει η συνέλιξη
Pztx=10*log10(ztx*ztx'/length(ztx));%ισχύς μιγαδικού σήματος,σε db
Pn=Pztx-SNR; % αντίστοιχη ισχύς μιγαδικού θορύβου, σε db
Pnx=10^(Pn/10)/2; Pny=Pnx; % ισχύς κάθε μιας συνιστώσας θορύβου
nx=sqrt(Pnx)*randn(1,length(ztx));
ny=sqrt(Pny)*randn(1,length(ztx));
n=nx+i*ny;
% μιγαδικό σήμα θορύβου
znoisy=ztx+n; % θορυβώδες μιγαδικό QAM σήμα
%%
z=reshape(znoisy,nsamp,length(znoisy)/nsamp);
matched=h;
zrx=matched*z/nsamp;
zi=real(zrx); zq=imag(zrx); % in phase & quadrature components
l=[-L+1:2:L-1];
for p=1:length(zrx)
    [m,j]=min(abs(l-zi(p)));
    zi(p)=l(j);
    [m,j]=min(abs(l-zq(p)));
    zq(p)=l(j);
end
err=not(u==(zi+i*zq));
errors=sum(err);
% Θεωρητικός υπολογισμός Pe & Pb
% Pe=2*(L-1)/L*erfc(sqrt(3*k/2/(L^2-1)*10^(EbNo/10)));
% Pb=Pe/log2(M);
end
