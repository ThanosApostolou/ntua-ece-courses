function mapping=psk_scatterplot_gray_12910(L)
% k is the number of bits per symbol
% mapping is the vector of psk points, in the gray-coding order
% i.e. mapping(1)<->00...00, mapping(2)<->00...01,
%      mapping(3)<->00...10, ..
% For 16-PSK, L=16 so k=4;
k=log2(L);
ph1=[pi/4];
theta=[ph1; -ph1; pi-ph1; -pi+ph1];
mapping=exp(1j*theta); % τετριμμένη κωδικοποίηση, L=4
if(k>2)
    for j=3:k
        theta=theta/2;
        mapping=exp(1j*theta);
        mapping=[mapping;-conj(mapping)];
        theta=log(mapping)./1j;
    end
end
scatterplot(mapping);
code=0;
for Z=mapping.'
    text(real(Z),imag(Z),num2str(de2bi(code,k,'left-msb')), 'FontSize', 6);
    code=code+1;
end
end