clear all; close all;
% Το αρχείο "sima.mat" περιέχει το σήμα s και τη συχνότητα
% δειγματοληψίας Fs. Το φάσμα του σήματος εκτείνεται σχεδόν σε όλη την
% περιοχή συχνοτήτων μέχρι 4 KHz. Πάνω από 1 KHz, όμως, είναι θόρυβος 
% και πρέπει να φιλτραριστεί.
%load sima;  %initial sima
% άθροισμα 3 ημιτόνων μοναδιαίου πλάτους για ερώτημα 5
Fs=8192;                    % συχνότητα δειγματοληψίας
Ts=1/Fs;                    % περίοδος δειγματοληψίας
T=1;                        % χρόνος σήματος
t=0:Ts:T-Ts;                   % χρονικές στιγμές σήματος
s=sin(2*pi*800*t)...        % ημιτονικό σήμα συχνότητας 800 Hz
+ sin(2*pi*1000*t)...       % συνιστώσα 1000 Hz
+ sin(2*pi*3000*t);         % συνιστώσα 3000 Hz
figure(1); pwelch(s,[],[],[],Fs);
% Ορίζεται η ιδανική βαθυπερατή συνάρτηση Η, με συχνότητα αποκοπ. Fs/8.
H=[ones(1,Fs/8) zeros(1,Fs-Fs/4) ones(1,Fs/8)];
% Υπολογίζεται η κρουστική απόκριση με αντίστροφο μετασχ. Fourier
% Εναλλακτικά, μπορεί να χρησιμοποιηθεί η αναλυτική σχέση Sa(x)
h=ifft(H,'symmetric');
middle=length(h)/2;
h=fftshift(h); %h=[h(middle+1:end) h(1:middle)];
h32=h(middle+1-16:middle+17);
h64=h(middle+1-32:middle+33);
h128=h(middle+1-64:middle+65);
h256=h(middle+1-128:middle+129);
% figure; stem([0:length(h64)-1],h64); grid;
% figure; freqz(h64,1); % σχεδιάζουμε την απόκριση συχνότητας της h64
wvtool(h32,h64,h128,h256);  % αποκρίσεις συχνότητας των περικομμένων h
% Οι πλευρικοί λοβοί είναι υψηλοί!
% Πολλαπλασιάζουμε την περικομμένη κρουστική απόκριση με κατάλληλο 
% παράθυρο. Χρησιμοποιούμε την h64 και παράθυρα hamming και kaiser
wh=hamming(length(h256));
wk=kaiser(length(h256),5);
figure(4); plot(0:256,wk,'r',0:256,wh,'b'); grid;
h_hamming=h256.*wh';
% figure; stem([0:length(h64)-1],h_hamming); grid;
% figure; freqz(h_hamming,1);
h_kaiser=h256.*wk';
wvtool(h256,h_hamming,h_kaiser);
% Φιλτράρουμε το σήμα μας με καθένα από τα τρία φίλτρα
y_rect=conv(s,h256);
figure(5); pwelch(y_rect,[],[],[],Fs);
y_hamm=conv(s,h_hamming);
figure(6); pwelch(y_hamm,[],[],[],Fs);
y_kais=conv(s,h_kaiser);
figure(7); pwelch(y_kais,[],[],[],Fs);
%
% Βαθυπερατό Parks-MacClellan
hpm=firpm(64, [0 0.10 0.15 0.5]*2, [1 1 0 0]);
% figure; freqz(hpm,1);
s_pm=conv(s,hpm);
figure(8); pwelch(s_pm,[],[],[],Fs);

% Βαθυπερατό Parks-MacClellan 140+1
hpm2=firpm(140, [0 0.10 0.15 0.5]*2, [1 1 0 0]);
s_pm2=conv(s,hpm2);
figure(9); pwelch(s_pm2,[],[],[],Fs);

% Βαθυπερατό Parks-MacClellan 140+1 (0.11, 0.12)
hpm3=firpm(140, [0 0.11 0.12 0.5]*2, [1 1 0 0]);
s_pm3=conv(s,hpm3);
figure(10); pwelch(s_pm3,[],[],[],Fs);
