clear all; close all;
% Το αρχείο "sima.mat" περιέχει το σήμα s και τη συχνότητα
% δειγματοληψίας Fs. Το φάσμα του σήματος εκτείνεται σχεδόν σε όλη την
% περιοχή συχνοτήτων μέχρι 4 KHz. Πάνω από 1 KHz, όμως, είναι θόρυβος 
% και πρέπει να φιλτραριστεί.
load sima;  %initial sima
figure(1); pwelch(s,[],[],[],Fs);

f1=3000; f2=3500; % ζώνη διέλευσης
Ts=1/Fs;
f2m1=(f2-f1); f2p1=(f2+f1)/2; N=256;
t=[-(N-1):2:N-1]*Ts/2;
% απλό φίλτρο, hamming φίλτρο, kaiser φίλτρο
hbp=2/Fs*cos(2*pi*f2p1*t).*sin(pi*f2m1*t)/pi./t;
hbp_h=hbp.*hamming(length(hbp))';
hbp_k=hbp.*kaiser(length(hbp),5)';
wvtool(hbp,hbp_h, hbp_k);
% Φιλτράρουμε το σήμα μας με καθένα από τα τρία φίλτρα
y_rect=conv(s,hbp);
figure(3); pwelch(y_rect,[],[],[],Fs);
y_hamm=conv(s,hbp_h);
figure(4); pwelch(y_hamm,[],[],[],Fs);
y_kais=conv(s,hbp_k);
figure(5); pwelch(y_kais,[],[],[],Fs);

% Φίλτρο Parks-MacClellan
f=2*[0 f1*0.95 f1*1.05 f2*0.95 f2*1.05 Fs/2]/Fs;
hbp_pm=firpm(140, f, [0 0 1 1 0 0]);
wvtool(hbp_pm);
s_pm=conv(s,hbp_pm);
figure(7); pwelch(s_pm,[],[],[],Fs);
