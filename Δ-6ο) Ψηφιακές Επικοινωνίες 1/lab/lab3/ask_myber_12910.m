function [ber,numBits] = ask_myber_12910()
k=3; % number of bits per symbol
Nsymb=20000; % number of symbols in each run
nsamp=16; % oversampling,i.e. number of samples per T
L=2^k;
EbNos=[0:20];
BERs=[]
for EbNo = EbNos
    EbNoW = 10^(EbNo/10); % μετατροπή db
    Pe=((L-1)/L)*erfc(sqrt(3*log2(L)*EbNoW/(L^2-1)));
    ber = Pe/log2(L);
    BERs = [BERs ber];
end
BERs=BERs
figure(1);
semilogy(EbNos, BERs);
ylim([10^-8 10^0]);

BERs2 = [];
for EbNo = EbNos
    errors=ask_errors_12910(k,Nsymb,nsamp,EbNo);
    numBits=k*Nsymb;
    ber=errors/numBits;
    BERs2 = [ BERs2 ber];
end % End of loop
figure(2);
semilogy(EbNos, BERs2);
ylim([10^-8 10^0]);
end