function errors=ask_Nyq_filter(k,Nsymb,nsamp,EbNo)
% Η συνάρτηση αυτή εξομοιώνει την παραγωγή και αποκωδικοποίηση
% θορυβώδους ακολουθίας L-ASK και μετρά τα λαθεμένα σύμβολα,
% με μορφοποίηση παλμών μέσω φίλτρου τετρ. ρίζας Nyquist.
%%%%%% ΠΑΡΑΜΕΤΡΟΙ %%%%%%%
% k είναι ο αριθμός των bits ανά σύμβολο, έτσι L=2^k
% Nsymb είναι το μήκος της εξομοιούμενης ακολουθίας συμβόλων L-ASK
% nsamp είναι ο συντελεστής υπερδειγμάτισης, δηλ. #samples/Td
% EbNo είναι ο ανηγμένος σηματοθορυβικός λόγος, Εb/No, σε db
L=2^k;
SNR=EbNo-10*log10(nsamp/2/k); % SNR ανά δείγμα σήματος
% Διάνυσμα τυχαίων ακεραίων από το σύνολο {±1, ±3, ... ±(L-1)}
x=2*floor(L*rand(1,Nsymb))-L+1;
%% Ορισμός παραμέτρων φίλτρου
delay = 8; % Group delay (# of input symbols)
filtorder = delay*nsamp*2; % τάξη φίλτρου
rolloff = 0.25; % Συντελεστής πτώσης -- rolloff factor
% κρουστική απόκριση φίλτρου τετρ. ρίζας ανυψ. συνημιτόνου
rNyquist= rcosine(1,nsamp,'fir/sqrt',rolloff,delay);
% ----------------------
% Για φίλτρο γραμμικής πτώσης να χρησιμοποιηθεί η επόμενη εντολή
% (με την rtrapezium του Κώδικα 4.1 στο current directory)
% Πρέπει delay>5 για καλά αποτελέσματα στην αναγνώριση.
% rNyquist=rtrapezium(nsamp,rolloff,delay);
% ----------------------
%% ΕΚΠΕΜΠΟΜΕΝΟ ΣΗΜΑ
% Υπερδειγμάτιση και εφαρμογή φίλτρου rNyquist
y=upsample(x,nsamp);
ytx = conv(y,rNyquist);
% Διάγραμμα οφθαλμού για μέρος του φιλτραρισμένου σήματος
% eyediagram(ytx(1:2000),nsamp*2);
ynoisy=awgn(ytx,SNR,'measured'); % θορυβώδες σήμα
% ----------------------
%% ΛΑΜΒΑΝΟΜΕΝΟ ΣΗΜΑ
% Φιλτράρισμα σήματος με φίλτρο τετρ. ρίζας ανυψ. συνημ.
yrx=conv(ynoisy,rNyquist);
yrx = downsample(yrx,nsamp); % Υποδειγμάτιση
yrx = yrx(2*delay+1:end-2*delay); % περικοπή, λόγω καθυστέρησης
% Ανιχνευτής ελάχιστης απόστασης L πλατών
l=[-L+1:2:L-1];
for i=1:length(yrx)
    [m,j]=min(abs(l-yrx(i)));
end
end
