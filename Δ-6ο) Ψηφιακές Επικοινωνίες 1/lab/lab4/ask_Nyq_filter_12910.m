function errors=ask_Nyq_filter_12910(k,Nsymb,nsamp,EbNo)
% Η συνάρτηση αυτή εξομοιώνει την παραγωγή και αποκωδικοποίηση
% θορυβώδους ακολουθίας L-ASK και μετρά τα λαθεμένα σύμβολα,
% με μορφοποίηση παλμών μέσω φίλτρου τετρ. ρίζας Nyquist.
%%%%%% ΠΑΡΑΜΕΤΡΟΙ %%%%%%%
% k είναι ο αριθμός των bits ανά σύμβολο, έτσι L=2^k
% Nsymb είναι το μήκος της εξομοιούμενης ακολουθίας συμβόλων L-ASK
% nsamp είναι ο συντελεστής υπερδειγμάτισης, δηλ. #samples/Td
% EbNo είναι ο ανηγμένος σηματοθορυβικός λόγος, Εb/No, σε db
L=2^k;
SNR=EbNo-10*log10(nsamp/2/k); % SNR ανά δείγμα σήματος
% το διάνυσμα mapping έχει τις διαφορετικές τιμών παλμών {±1, ±3, ... ±(L-1)}
step=2;
mapping=[step/2; -step/2];
if(k>1)
    for j=2:k
        mapping=[mapping+2^(j-1)*step/2; ...
        -mapping-2^(j-1)*step/2];
    end
end
%mapping=-(L-1):step:(L-1);
% k * Nsymb τυχαία bits
x = randi(2,1,k*Nsymb)-1;
% ομαδοποιούμε ανά k για να πάρουμε την δεκαδική τιμή
xsym = bi2de(reshape(x,k,length(x)/k).','left-msb');
% χρησιμοποιούμε την δεκαδική τιμή ως δείκτη στο διάνυσμα mapping για να
% πάρουμε την κωδικοποίηση gray
y1=[];
for i=1:length(xsym)
    y1=[y1 mapping(xsym(i)+1)];
end
%% Ορισμός παραμέτρων φίλτρου
delay = 16; % Group delay (# of input symbols)
filtorder = delay*nsamp*2; % τάξη φίλτρου
rolloff = 0.33; % Συντελεστής πτώσης -- rolloff factor
% κρουστική απόκριση φίλτρου τετρ. ρίζας ανυψ. συνημιτόνου
rNyquist= rcosine(1,nsamp,'fir/sqrt',rolloff,delay);
% ----------------------
%% ΕΚΠΕΜΠΟΜΕΝΟ ΣΗΜΑ
% Eφαρμογή φίλτρου rNyquist
y=upsample(y1,nsamp);
ytx = conv(y,rNyquist);
% Διάγραμμα οφθαλμού για μέρος του φιλτραρισμένου σήματος
% eyediagram(ytx(1:2000),nsamp*2);
ynoisy=awgn(ytx,SNR,'measured'); % θορυβώδες σήμα
% ----------------------
%% ΛΑΜΒΑΝΟΜΕΝΟ ΣΗΜΑ
% Φιλτράρισμα σήματος με φίλτρο τετρ. ρίζας ανυψ. συνημ.
%figure;stem(ynoisy)
yrx=conv(ynoisy,rNyquist);
yrx = yrx(2*delay*nsamp+1:end-2*delay*nsamp); % περικοπή, λόγω καθυστέρησης
yr = downsample(yrx,nsamp); % Υποδειγμάτιση
%figure;
%hold on;
%plot(yrx(1:10*nsamp)); % σήμα εξόδου 10 περιόδων
%stem([1:nsamp:10*nsamp],y1(1:10),'filled'); % σήμα εισόδου 10 περιόδων πριν την υπερδειγματοληψία
%hold off;
%figure;pwelch(yrx,[],[],[],1);
% Ανιχνευτής ελάχιστης απόστασης L πλατών
xr=[];
for i=1:length(yr)
    [m,j]=min(abs(mapping-yr(i)));  
    xr = [xr de2bi(j-1,k,'left-msb')];
end
errors = sum(not(x==xr));
end
