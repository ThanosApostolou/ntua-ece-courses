function errors=psk_errors(k,Nsymb,nsamp,EbNo)
% Η συνάρτηση αυτή εξομοιώνει την παραγωγή και αποκωδικοποίηση
% θορυβώδους ακολουθίας M-QAM πλήρους ορθογωνικού πλέγματος LxL,
% μετράει δε και επιστρέφει τον αριθμό των παρατηρούμενων λαθών.
% k είναι ο αριθμός των bits ανά σύμβολο (άρτιος), έτσι L=2^(k/2).
% Nsymb είναι το μήκος της παραγόμενης ακολουθίας συμβόλων,
% nsamp είναι ο συντελεστής υπερδειγμάτισης (αριθ. δειγμάτων/Τ),
% EbNo είναι ο ανηγμένος σηματοθορυβικός λόγος Eb/No, σε db.
%
M=2^k; L=sqrt(M);
SNR=EbNo-10*log10(nsamp/2/k); % SNR ανά δείγμα σήματος

%% Random Nsymb symbols in gray encoding
% mapping vector for gray encoding
ph1=[pi/4];
theta=[ph1; -ph1; pi-ph1; -pi+ph1];
mapping=exp(1j*theta); % τετριμμένη κωδικοποίηση, L=4
if(k>2)
    for j=3:k
        theta=theta/2;
        mapping=exp(1j*theta);
        mapping=[mapping;-conj(mapping)];
        theta=log(mapping)./1j;
    end
end

x = randi(2,1,k*Nsymb)-1;
xsym = bi2de(reshape(x,k,length(x)/k).','left-msb'); % group per k
y1=[];
for n=1:length(xsym)
    y1=[y1; mapping(xsym(n)+1)];
end
%figure;plot(y1)

%% Filter Definition
delay = 8; % Group delay (# of input symbols)
filtorder = delay*nsamp*2; % τάξη φίλτρου
rolloff = 0.25; % Συντελεστής πτώσης -- rolloff factor
rNyquist= rcosine(1,nsamp,'fir/sqrt',rolloff,delay); % square root Nyquist filter

%% Transmitted Signal
% apply rNyquist filter
y=upsample(y1,nsamp);
ytx = conv(y,rNyquist);
%modulate to get the bandpass singal
fc=7.5;
W=3;
T=(1+rolloff)/W; Fs=nsamp/T;
m=(1:length(ytx))';
s=real(ytx.*exp(1j*2*pi*fc*m/Fs));
%figure;pwelch(s,[],[],[],Fs);
snoisy=awgn(s,SNR,'measured');

%% Received Signal
yrx=2*snoisy.*exp(-1j*2*pi*fc*m/Fs);
yrx=conv(yrx,rNyquist);
yrx=yrx(2*delay*nsamp+1:end-2*delay*nsamp); % περικοπή, λόγω καθυστέρησης
yr=downsample(yrx,nsamp);

%% Find errors
xr=[];
for n=1:length(yr)
    [m,j]=min(abs(mapping-yr(n)));  
    xr = [xr de2bi(j-1,k,'left-msb')];
end
errors = sum(not(x==xr));
end