%% Γενικά
clear all; close all; clc;
x=0:0.1:0.5; y=linspace(0,0.5,6); %ισοδύναμα
size(x);length(x); % διαστάσεις, μεγαλύτερη διάσταση
x*y % πολλαπλασιασμός πινάκων
x.*y; x./y; x.^3; % πολλαπλασιασμός, διαίρεση, δύναμη ανά στοιχείο
fft(x); ifft; % fourier και αντίστροφος fourier
sum(x); % άθροισμα στοιχείων του πίνακα
fftshift(x); ifftshift(x); % 1η κεντρικό στοιχείο δεξιά, 2η κεντρικό στοιχείο αριστερά. Ίδιες αν x ζυγό length
for column=x
    column
end
for line=x.'
    line
end
for line=x.'
    for element=line.'
        element
    end
end
for column=x
    for element=column.'
        element
    end
end

%% Σήμα - Fourier
x; % σήμα
Fs; Ts=1/Fs; % συχνότητα - περίοδος δειγματοληψίας
L=length(x); T=L*Ts; t=0:Ts:(L-1)*Ts; % στο πεδίο του χρόνου
plot(t,x); % διάγραμμα στον χρόνο

N=2^nextpow2(L); Fo=Fs/N; f=0:Fo:(N-1)*Fo; % στο πεδίο της συχνότητας
X=fft(x,N); % μετασχηματισμός fourier
plot(f,abs(X)); % πλάτος των θετικών συχνοτή-των

power=X.*conj(X)/N/L; % υπολογισμός πυκνότητας ισχύος
plot(f,power);        % ισχύς ανά συνιστώσα συχνότητας

f_shifted=f-Fs/2;       % ολίσθηση συχνοτήτων προς τα αριστερά κατά –Fs/2
X_shifted=fftshift(X);  % ολίσθηση της μηδενικής συχνότητας στο κέντρο
plot(f_shifted,abs(X_shifted)); % πλάτος του αμφίπλευρου φάσματος

power=X_shifted.*conj(X_shifted)/N/L; % υπολογισμός πυκνότητας ισχύος αμφίπλευρου φάσματος
plot(f_shifted,power);                % ισχύς ανά συνιστώσα συχνότητας

pwelch(x,[],[],[],Fs); % φασματική πυκνότητα ισχύος
stem(x); % διακριτή ακολουθία
wvtool(x);
