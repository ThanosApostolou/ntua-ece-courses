function mapping=qam_scatterplot_gray(M)
% Διάνυσμα mapping για την κωδικοποίηση Gray M-QAM
% Αφορά σε πλήρες ορθογωνικό πλέγμα σημείων, διάστασης M=L^2
% l=log2(L): αριθμός bit ανά συνιστώσα (inphase, quadrature)l=log2(L);
L=sqrt(M)
l=log2(L);
core=[1+i;1-i;-1+i;-1-i]; % τετριμμένη κωδικοποίηση, M=4
mapping=core;
if(l>1)
    for j=1:l-1
        mapping=mapping+j*2*core(1);
        mapping=[mapping;conj(mapping)];
        mapping=[mapping;-conj(mapping)];
    end
end;
size(mapping)


scatterplot(mapping);
code=0;
for Z=mapping.'
    text(real(Z),imag(Z),num2str(de2bi(code,L,'left-msb')), 'FontSize', 6);
    code=code+1;
end
end