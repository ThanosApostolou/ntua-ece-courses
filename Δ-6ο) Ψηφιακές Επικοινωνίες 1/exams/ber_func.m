function [ber,numBits] = ber_func(EbNo, maxNumErrs, maxNumBits)
% Import Java class for BERTool.
import com.mathworks.toolbox.comm.BERTool;
% Initialize variables related to exit criteria.
totErr = 0; % Number of errors observed
numBits = 0; % Number of bits processed
% A. --- Set up parameters. ---
% --- INSERT YOUR CODE HERE.
k=1; % 2ASK  2PSK        2FSK  MSK
%k=2; % 4ASK  4PSK  4QAM  4FSK
%k=3; % 8ASK  8PSK        8FSK
%k=4; % 16ASK 16PSK 16QAM 16FSK
Nsymb=5000; % number of symbols in each run
nsamp=64; % oversampling,i.e. number of samples per T
% Simulate until number of errors exceeds maxNumErrs
% or number of bits processed exceeds maxNumBits.
while((totErr < maxNumErrs) && (numBits < maxNumBits))
    % Check if the user clicked the Stop button of BERTool.
    if (BERTool.getSimulationStop)
        break;
    end
    % Β. --- INSERT YOUR CODE HERE.
    %errors=ask_errors(k,Nsymb,nsamp,EbNo);
    %errors=ask_errors_nyq(k,Nsymb,nsamp,EbNo);
    %errors=psk_errors(k,Nsymb,nsamp,EbNo);
    %errors=qam_errors(k,Nsymb,nsamp,EbNo);
    %errors=psk_errors(k,Nsymb,nsamp,EbNo);
    %errors=qam_errors(k,Nsymb,nsamp,EbNo);
    %errors=fsk_errors_coh(k,Nsymb,nsamp,EbNo);
    %errors=fsk_errors_noncoh(k,Nsymb,nsamp,EbNo);
    errors=msk_errors(Nsymb,nsamp,EbNo);
    % Assume Gray coding: 1 symbol error ==> 1 bit error
    totErr=totErr+errors;
    numBits=numBits + k*Nsymb;
end % End of loop
% Compute the BER
ber = totErr/numBits;
