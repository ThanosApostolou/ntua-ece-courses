BEGIN {
	pkts_dropped_1 = 0;
	pkts_dropped_2 = 0;
}
/^d/&&/cbr/ {
	if($8 == 0) {
		pkts_dropped_1 ++ ;
	}
	if($8 == 1) {
		pkts_dropped_2 ++ ;
	}
}
END {
	printf("Total Packets dropped for flow 1 (blue): %d\n", pkts_dropped_1);
	printf("Total Packets dropped for flow 2 (red): %d\n", pkts_dropped_2);
}
