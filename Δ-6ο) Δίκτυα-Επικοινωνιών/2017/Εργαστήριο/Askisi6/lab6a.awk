BEGIN {
	total_pckts1 = 0 ;
	total_pckts2 = 0 ;
	time1 = 0 ;
	time2 = 0 ;
}
/^r/&&/tcp/ {
	if($6 == 540 && $4 == 5 && $8 == 1) {
		total_pckts1 ++ ;
		time1 = $2 ;
	}
	if($6 == 540 && $4 == 9 && $8 == 2) {
		total_pckts2 ++ ;
		time2 = $2 ;
	}
}
END {
	printf("Total packets for flow1: %d\n", total_pckts1);
	printf("Total packets for flow2: %d\n", total_pckts2);
	printf("Last packet for flow 1 received at: %f sec\n", time1);
	printf("Last packet for flow 2 received at: %f sec\n", time2);
}
