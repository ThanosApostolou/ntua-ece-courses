set ns [new Simulator]
set nf [open lab6a.nam w]
$ns namtrace-all $nf
set trf [open lab6a.tr w]
$ns trace-all $trf
Agent/rtProto/Direct set preference_ 200
$ns rtproto DV

proc finish {} {
	global ns nf
	$ns flush-trace
	close $nf
	exit 0
}
for {set i 0} {$i < 10} {incr i} {
	set n($i) [$ns node]
}
$ns duplex-link $n(0) $n(1) 1Mb 30ms DropTail
$ns cost $n(0) $n(1) 3
$ns cost $n(1) $n(0) 3
$ns duplex-link $n(1) $n(2) 1Mb 20ms DropTail
$ns cost $n(1) $n(2) 2
$ns cost $n(2) $n(1) 2
$ns duplex-link $n(2) $n(3) 1Mb 20ms DropTail
$ns cost $n(2) $n(3) 2
$ns cost $n(3) $n(2) 2
$ns duplex-link $n(3) $n(4) 1Mb 10ms DropTail
$ns duplex-link $n(4) $n(5) 1Mb 10ms DropTail
$ns duplex-link $n(5) $n(6) 1Mb 20ms DropTail
$ns cost $n(5) $n(6) 2
$ns cost $n(6) $n(5) 2
$ns duplex-link $n(6) $n(7) 1Mb 10ms DropTail
$ns duplex-link $n(7) $n(8) 1Mb 20ms DropTail
$ns cost $n(7) $n(8) 2
$ns cost $n(8) $n(7) 2
$ns duplex-link $n(8) $n(9) 1Mb 20ms DropTail
$ns cost $n(8) $n(9) 2
$ns cost $n(9) $n(8) 2
$ns duplex-link $n(9) $n(0) 1Mb 30ms DropTail
$ns cost $n(9) $n(0) 3
$ns cost $n(0) $n(9) 3
$ns duplex-link $n(1) $n(3) 1Mb 50ms DropTail
$ns cost $n(1) $n(3) 5
$ns cost $n(3) $n(1) 5
$ns duplex-link $n(3) $n(5) 1Mb 30ms DropTail
$ns cost $n(3) $n(5) 3
$ns cost $n(5) $n(3) 3
$ns duplex-link $n(5) $n(7) 1Mb 50ms DropTail
$ns cost $n(5) $n(7) 5
$ns cost $n(7) $n(5) 5
$ns duplex-link $n(7) $n(9) 1Mb 30ms DropTail
$ns cost $n(7) $n(9) 3
$ns cost $n(9) $n(7) 3
$ns duplex-link $n(9) $n(1) 1Mb 50ms DropTail
$ns cost $n(9) $n(1) 5
$ns cost $n(1) $n(9) 5

set tcp1 [new Agent/TCP]
$tcp1 set packetSize_ 500
$ns attach-agent $n(0) $tcp1
$tcp1 set fid_ 1
$ns color 1 green
set sink1 [new Agent/TCPSink]
$ns attach-agent $n(5) $sink1
$ns connect $tcp1 $sink1
set ftp1 [new Application/FTP]
$ftp1 attach-agent $tcp1

set tcp2 [new Agent/TCP]
$tcp2 set packetSize_ 500
$ns attach-agent $n(3) $tcp2
$tcp2 set fid_ 2
$ns color 2 yellow
set sink2 [new Agent/TCPSink]
$ns attach-agent $n(9) $sink2
$ns connect $tcp2 $sink2
set ftp2 [new Application/FTP]
$ftp2 attach-agent $tcp2

$ns at 0.5 "$ftp1 produce 150"
$ns at 0.8 "$ftp2 produce 150"
$ns at 4.0 "finish"
$ns run
