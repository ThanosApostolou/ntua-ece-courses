set ns [new Simulator]
set nf [open lab6b.nam w]
$ns namtrace-all $nf
set trf [open lab6b.tr w]
$ns trace-all $trf
set f0 [open lab6b1.tr w]
set f1 [open lab6b2.tr w]
Agent/rtProto/Direct set preference_ 200
$ns rtproto DV

proc record {} {
	global sink0 sink1 f0 f1
	set ns [Simulator instance]
	set time 0.25
	set bw0 [$sink0 set bytes_]
	set bw1 [$sink1 set bytes_]
	set now [$ns now]
	puts $f0 "$now [expr $bw0/$time*8/1000000]"
	puts $f1 "$now [expr $bw1/$time*8/1000000]"
	$sink0 set bytes_ 0
	$sink1 set bytes_ 0
	$ns at [expr $now+$time] "record"
}
proc finish {} {
	global ns nf f0 f1
	$ns flush-trace
	close $nf
	close $f0
	close $f1
	exit 0
}

set n0 [$ns node]
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
$ns duplex-link $n2 $n3 4Mb 20ms DropTail
$ns duplex-link $n2 $n1 4Mb 20ms DropTail
$ns duplex-link $n3 $n1 4Mb 20ms DropTail
$ns duplex-link $n1 $n0 1.5Mb 20ms DropTail
$ns duplex-link-op $n2 $n3 orient right
$ns duplex-link-op $n1 $n2 orient left-up
$ns duplex-link-op $n1 $n3 orient right-up
$ns duplex-link-op $n1 $n0 orient down

set udp2 [new Agent/UDP]
$udp2 set packetSize_ 540
$ns attach-agent $n2 $udp2
$udp2 set fid_ 0
$ns color 0 blue
set cbr2 [new Application/Traffic/CBR]
$cbr2 set packetSize_ 540
$cbr2 set interval_ 0.004
$cbr2 attach-agent $udp2
set sink0 [new Agent/LossMonitor]
$ns attach-agent $n0 $sink0
$ns connect $udp2 $sink0

set udp3 [new Agent/UDP]
$udp3 set packetSize_ 540
$ns attach-agent $n3 $udp3
$udp3 set fid_ 1
$ns color 1 red
set cbr3 [new Application/Traffic/CBR]
$cbr3 set packetSize_ 540
$cbr3 set interval_ 0.004
$cbr3 attach-agent $udp3
set sink1 [new Agent/LossMonitor]
$ns attach-agent $n1 $sink1
$ns connect $udp3 $sink1

$ns at 0.0 "record"
$ns at 0.4 "$cbr2 start"
$ns at 0.4 "$cbr3 start"
$ns rtmodel-at 1.5 down $n0 $n1
$ns rtmodel-at 2.0 up $n0 $n1
$ns at 3.2 "$cbr2 stop"
$ns at 3.2 "$cbr3 stop"
$ns at 3.8 "finish"
$ns run
