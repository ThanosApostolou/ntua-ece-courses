BEGIN {
	sum_delay = 0;
	bufferspace = 100000;
	total_pkts_sent = 0;
	total_pkts_recv = 0;
	total_pkts_dropped = 0;
	if (sprintf(sqrt(2)) ~ /,/) dmfix = 1;
}
{
	if (dmfix) sub(/\./, ",", $0);
}
/^h/&&/cbr/ {
	stations = $4;
	total_pkts_sent++;
}
/^d/&&/cbr/ {
	total_pkts_dropped++;
}
/^-/&&/cbr/ {
	sendtimes[$12%bufferspace] = $2;
}
/^r/&&/cbr/ {
	packet_size = $6;
	last_ts = $2;
	total_pkts_recv++;
	sum_delay += $2 - sendtimes[$12%bufferspace];
}
{
	tr_time = last_ts - 0.4;
	exp_ut = 100 * (total_pkts_recv * packet_size * 8)/ (tr_time *10000000);
	lost_packets_percentage = total_pkts_dropped * 100/total_pkts_sent;
}
END {
	printf("Percentage of lost packets\t: %f\n", lost_packets_percentage);
	printf("Number of stations\t\t: %d\n", stations);
	printf("Experimental Utilization\t: %f\n", exp_ut);
	printf("Total Packets sent\t\t: %d\n", total_pkts_sent);
	printf("Total Packets received\t\t: %d\n", total_pkts_recv);
	printf("Total Packets dropped\t\t: %d\n", total_pkts_dropped);
	printf("Average Delay\t\t\t: %f sec\n", (1.0 * sum_delay)/total_pkts_recv);
}
