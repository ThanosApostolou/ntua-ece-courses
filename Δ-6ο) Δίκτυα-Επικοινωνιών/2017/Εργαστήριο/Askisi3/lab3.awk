BEGIN {
	data_0=0;
	packets_0=0;
	data_1=0;
	packets_1=0;
}

/^r/&&/ack/ {
	flow_id = $8;
	if (flow_id == 0) {
		data_0 += $6;
		packets_0++;
		last_ts_0 = $2;
	}
	if (flow_id == 1) {
		data_1 += $6;
		packets_1++;
		last_ts_1 = $2;
	}
}

END {
	printf("Last ack packet received for flow ID 0\t: %s sec\n", last_ts_0);
	printf("Last ack packet received for flow ID 1\t: %s sec\n", last_ts_1);
}
