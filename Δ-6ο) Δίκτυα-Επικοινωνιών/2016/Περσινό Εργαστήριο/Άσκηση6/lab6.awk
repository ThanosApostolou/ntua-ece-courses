BEGIN {
	data = 0;
	packets = 0;
	buffersize = 10;
	sumDelay = 0;
	bufferspace = 10000;
	if(sprintf(sqrt(2)) ~ /,/) dmfix = 1;
}

{
	if (dmfix) sub(/\./, ",", $0);
}

/^-/&&/cbr/ {
	sendtimes[$12%bufferspace] = $2;
}

/^r/&&/cbr/ {
	data += $6;
	packets++;
	sumDelay += $2 - sendtimes[$12%bufferspace];
}

END {
	printf("Total Data received\t: %d Bytes\n", data);
	printf("Total Packets received\t: %d\n", packets);
	printf("Average Delay\t\t: %f sec\n", (1.0 * sumDelay) / packets);
}
