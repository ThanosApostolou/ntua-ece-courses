set ns [new Simulator]
set nf [open lab1.nam w]
$ns namtrace-all $nf
set f [open lab1.tr w]

proc record {} {
	global sink f
	set ns [Simulator instance]
	#ορισμός της ώρας που η διαδικασία θα ξανακληθει.
	set time 0.15
	#katagrafh bytes.
	set bw [$sink set bytes_]
	#lipsi ths trexousas wras
	set now [$ns now]
	#ypologismos tou bandwitdth kai katagrafh tou sto arxeio
	puts $f "$now [expr (($bw/$time)*8/1000000)]"
	#kanei thn timh bytes_ 0
	$sink set bytes_ 0
	#epanaprogrammatismos ths diadikasias
	$ns at [expr $now+$time] "record"
}

proc finish {} {
	global ns nf f
	$ns flush-trace
	close $nf
	close $f
	exit 0
}

set n0 [$ns node]
set n1 [$ns node]
$ns duplex-link $n0 $n1 2Mb 10ms DropTail

set udp0 [new Agent/UDP]
$udp0 set packetSize_ 1000
$ns attach-agent $n0 $udp0
set traffic0 [new Application/Traffic/Exponential]
$traffic0 set packetSize_ 1000
$traffic0 set interval_ 0.005
$traffic0 attach-agent $udp0

set sink [new Agent/LossMonitor]
$ns attach-agent $n1 $sink
$ns connect $udp0 $sink

$ns at 0.0 "record"
$ns at 1.0 "$traffic0 start"
$ns at 41.0 "$traffic0 stop"
$ns at 42.0 "finish"

$ns run
