set ns [new Simulator]
set nf [open lab3.nam w]
$ns namtrace-all $nf
Agent/rtProto/Direct set preference_ 200
$ns rtproto DV
set f0 [open out0.tr w]
set f3 [open out3.tr w]

proc record {} {
	global sink0 sink3 f0 f3
	set ns [Simulator instance]
	set time 0.075
	set bw0 [$sink3 set bytes_]
	set bw3 [$sink0 set bytes_]
	set now [$ns now]
	puts $f0 "$now [expr (($bw0/$time)*8/1000000)]"
	puts $f3 "$now [expr (($bw3/$time)*8/1000000)]"
	$sink0 set bytes_ 0
	$sink3 set bytes_ 0
	$ns at [expr $now+$time] "record"
}
proc finish {} {
	global ns nf f0 f3
	$ns flush-trace
	close $nf
	close $f0
	close $f3
	exit 0
}

for {set i 0} {$i < 11} {incr i} {
	set n($i) [$ns node]
}
for {set i 0} {$i < 9} {incr i} {
	$ns duplex-link $n($i) $n([expr ($i+1)%9]) 2Mb 30ms DropTail
}
$ns duplex-link $n(9) $n(1) 2Mb 30ms DropTail
$ns duplex-link $n(9) $n(5) 2Mb 10ms DropTail
$ns duplex-link $n(10) $n(5) 2Mb 10ms DropTail
$ns duplex-link $n(10) $n(2) 2Mb 30ms DropTail

for {set i 0} {$i < 9} {incr i} {
	$ns cost $n($i) $n([expr ($i+1)%9]) 3
	$ns cost $n([expr ($i+1)%9]) $n($i) 3
}
$ns cost $n(9) $n(1) 3
$ns cost $n(1) $n(9) 3
$ns cost $n(9) $n(5) 1
$ns cost $n(5) $n(9) 1
$ns cost $n(10) $n(5) 1
$ns cost $n(5) $n(10) 1
$ns cost $n(10) $n(2) 3
$ns cost $n(2) $n(10) 3

set udp0 [new Agent/UDP]
$udp0 set packetSize_ 1500
$ns attach-agent $n(0) $udp0
$udp0 set fid_ 0
$ns color 0 yellow
set sink0 [new Agent/LossMonitor]
$ns attach-agent $n(0) $sink0
set udp3 [new Agent/UDP]
$udp3 set packetSize_ 1500
$ns attach-agent $n(3) $udp3
$udp3 set fid_ 3
$ns color 3 blue
set sink3 [new Agent/LossMonitor]
$ns attach-agent $n(3) $sink3
$ns connect $udp0 $sink3
$ns connect $udp3 $sink0

set cbr0 [new Application/Traffic/CBR]
$cbr0 set packetSize_ 1500
$cbr0 set interval_ 0.015
$cbr0 attach-agent $udp0
set exp3 [new Application/Traffic/Exponential]
$exp3 set packetSize_ 2000
$exp3 set rate_ 640k
$exp3 attach-agent $udp3

$ns at 0.0 "record"
$ns at 1 "$cbr0 start"
$ns at 2 "$exp3 start"
$ns at 22 "$exp3 stop"
$ns at 24 "$cbr0 stop"
$ns at 24 "finish"
$ns run
