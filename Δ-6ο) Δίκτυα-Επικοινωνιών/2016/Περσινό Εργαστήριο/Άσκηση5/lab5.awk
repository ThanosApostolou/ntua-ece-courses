BEGIN {
	data=0;
	packets=0;
	start=0;
	finish=0;
}
/^+/&&/tcp/ {
	if(!start) start=$2;
}
/^r/&&/tcp/ {
	data+=$6;
	packets++;
}
	/^r/&&/ack/{
	finish=$2;
}
END {
	printf("First tcp packet sent\t: %f \n", start);
	printf("Last acknowledgement received\t: %f \n", finish);
	printf("Total Data received\t: %d Bytes\n", data);
	printf("Total Packets received\t: %d\n", packets);
	printf("Data transmission duration\t:%f\n",finish-start);
}
