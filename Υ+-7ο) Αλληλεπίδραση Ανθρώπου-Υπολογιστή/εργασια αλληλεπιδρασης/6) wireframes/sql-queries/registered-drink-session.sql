-- save usage after ended session (usageid is autoincrement)
INSERT INTO USAGE (userid, start, finish, drunk_percent, location)
VALUES (current_userid, current_start, current_finish, current_drunk_percent, current_location);

-- for each drink added insert it to DRUNK
INSERT DRUNK (userid, usageid, drinkid, quantity)
VALUES (current_userid, current_usageid, current_drinkid, current_quantity);
