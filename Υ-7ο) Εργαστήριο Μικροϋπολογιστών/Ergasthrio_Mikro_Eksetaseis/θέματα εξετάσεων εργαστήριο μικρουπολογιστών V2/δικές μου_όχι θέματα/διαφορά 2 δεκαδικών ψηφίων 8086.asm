
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

.data
msg1 db "Give two numbers: $" 
msg2 db "The difference of those two numbers is: $"
msg3 db "-$"
space db " $"
another db "*****************************************$"
first db 0
second db 0 
diff db 0

.code   
start:

mov ah, 0Eh       ; PRINT NEW LINE SEQUENCE
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h 

mov dx, offset msg1
mov ah, 09h
int 21h 

mov ah,01h
int 21h   
sub al,30h
mov first,al

mov dx, offset space
mov ah, 09h
int 21h

mov ah,01h
int 21h  
sub al,30h
mov second,al

mov al,second
neg al
add al,first           

rol al,1
jnc thetiko
ror al,1
neg al     
arnitiko: 
mov diff,al

mov ah, 0Eh       ; PRINT NEW LINE SEQUENCE
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h

mov dx, offset msg2
mov ah, 09h
int 21h 

mov dx, offset msg3
mov ah, 09h
int 21h 

mov dl,diff
add dl,30h
mov ah,02
int 21h  

mov ah, 0Eh       ; PRINT NEW LINE SEQUENCE
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h
 
mov dx, offset another
mov ah, 09h
int 21h
 
jmp start
thetiko:
ror al,1
mov diff,al

mov ah, 0Eh       ; PRINT NEW LINE SEQUENCE
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h 

mov dx, offset msg2
mov ah, 09h
int 21h
 
mov dl,diff
add dl,30h
mov ah,02
int 21h  

mov ah, 0Eh       ; PRINT NEW LINE SEQUENCE
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h 

mov dx, offset another
mov ah, 09h
int 21h 

jmp start

ret




