.include "m16def.inc"
.def prwto=r16
.def deutero=r17
.def trito=r18
.def tetarto=r19
.def sum=r20
.def temp=r21
.def temp2=r22

start:
ldi sum,0x00
ldi temp,0x00
ldi temp2,0x00

ser temp
out ddrc,temp

in prwto,pina
in trito,pinb
andi prwto,0x0f
andi trito,0x0f

in deutero,pina
andi deutero,0xf0
clc
ror deutero
clc
ror deutero
clc 
ror deutero
clc 
ror deutero

in tetarto,pinb
andi tetarto,0xf0
clc
ror tetarto
clc
ror tetarto
clc 
ror tetarto
clc 
ror tetarto


add sum,prwto
add sum,deutero
add sum,trito
add sum,tetarto

out portc,sum
mov temp,sum
andi temp,0x07
mov temp2,sum
andi temp2,0x38
clc
rol temp2
add temp2,temp
out portc,temp2

jmp start

ret
