org 100h

.data
msg1 db "Give numbers: $" 
msg2 db "Sum = $"
msg3 db "Dif = $"
msg4 db "-$" 
msg5 db "************************$"
space db " $"
first_number db 0
second_number db 0
sum db 0
dif db 0
dekades db 0
monades db 0

.code 

start:    

mov ah,00h 
mov al,00h
mov dekades,00h
mov monades,00h
mov sum,00h
mov dif,00h
mov first_number,00h
mov second_number,00h
 
mov dx, offset msg1
mov ah, 09h
int 21h 

mov ah,01h
int 21h 
cmp al,'K'
jz end
cmp al,3Ah
jb skip
sub al,37h
mov first_number,al
jmp end_skip
skip:
sub al,30h
mov first_number,al
end_skip:

mov ah,01h
int 21h 
cmp al,3Ah
jb skip2
sub al,37h
mov second_number,al
jmp end_skip2
skip2:
sub al,30h
mov second_number,al
end_skip2:  
;------newline----------
mov ah, 0Eh       
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h

mov dx, offset msg2
mov ah, 09h
int 21h 

mov al,first_number
add al,second_number
mov sum,al

mov al,sum
convert_sumhex_to_decimal:
cmp al,0Ah
jge afairesh
jmp over
afairesh:
sub al,0Ah
inc dekades
jmp convert_sumhex_to_decimal
over:
mov monades,al

mov al,dekades
add al,30h
mov dekades,al

mov al,monades
add al,30h
mov monades,al
;print sum_dekadiko
mov dl,dekades
mov ah,02h
int 21h

mov dl,monades
mov ah,02h
int 21h  

mov dx, offset space
mov ah, 09h
int 21h  

mov dx, offset msg3
mov ah, 09h
int 21h

mov dekades,00h
mov monades,00h 

mov al,second_number
neg al             
;and al,00001111b
add al,first_number
mov dif,al

mov al,first_number
sub al,second_number
rol al,1
jnc no_fix_needed
fix_negative: 
mov al,second_number
neg al             
and al,00001111b
add al,first_number
mov dif,al   
;----print '-'-----
mov dx, offset msg4
mov ah, 09h
int 21h
no_fix_needed:
mov al,dif   

convert_difhex_to_decimal:
 cmp al,0Ah
jge afairesh2
jmp over2
afairesh2:
sub al,0Ah
inc dekades
jmp convert_difhex_to_decimal
over2:
mov monades,al

mov al,dekades
add al,30h
mov dekades,al

mov dl,dekades
mov ah,02h
int 21h

mov al,monades
add al,30h
mov monades,al

mov dl,monades
mov ah,02h
int 21h 

mov ah, 0Eh       
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h

mov dx, offset msg5
mov ah, 09h
int 21h

mov ah, 0Eh       
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h

jmp start

end:
ret