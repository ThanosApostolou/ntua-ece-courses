/*
 * project.asm
 *
 *  Created: 22/9/2015 12:24:41 ??
 *   Author: George
 */ 
.include "m16def.inc"
.org 0x00
.def reg= r16
.def temp= r17
.def x0 =r18
.def x1 =r19
.def x2 = r20
.def result =r21
    ldi reg,LOW(RAMEND)
	out SPL,reg
	ldi reg,HIGH(RAMEND)
	out SPH,reg
	clr reg
	out DDRA,reg
	ser reg
	out DDRB,reg

start:
	in reg,PINA
	andi reg,0b00000111
	mov temp,reg
	andi temp,(1<<PA0)
	mov x0,temp
	mov temp,reg
	andi temp,(1<<PA1)
	lsr temp
	mov x1,temp
	mov temp,reg
	andi temp,(1<<PA2)
	lsr temp
	lsr temp
	mov x2,temp
y0:
	ldi result,0
	or result,x0
	eor result,x1
	eor result,x2
y1: 
	mov temp,reg
	andi temp,0b00000110
	cpi temp,0
	breq NO
	mov temp,reg
	andi temp,0b00000011
	cpi temp,0
	breq NO
	mov temp,reg
	andi temp,0b00000101
	cpi temp,0
	breq NO
	ori result,(1<<PA1)
NO:
	out PORTC,result
	rjmp start







	








	

	

	


