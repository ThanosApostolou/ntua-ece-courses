include emu8086.inc
org 100h

main:
  
           
                 
        push ax
        push bx
        push cx
        push dx
        pusha
        mov cx,2
        mov dx,0          
start:  mov ax,8   ;because we type the number from left
        mul dx      ;to right we need to multiply by 10 in
        mov dx,ax   ;order to create the value of the number
ignore: mov ah,8
        int 21h     ;waiting an input
        cmp al,30h
        jl ignore
        cmp al,37h
        jg ignore   ;accept only digits 0-9
        mov ah,0eh
        int 10h   
        sub al,30h  ;change ASCII value to real value
        mov ah,00h
        add dx,ax   ;dx will have the number that we 
        loop start  ;insert from keyborad in hex form
        mov bx,dx   
        
waiting1:
        mov ah,8
        int 21h     ;waiting an input
        cmp al,'+'
        jne waiting1: 
        mov ah,0eh
        int 10h
        pop ax          
                         
        mov cx,2
        mov dx,0          
start1: mov ax,8   ;because we type the number from left
        mul dx      ;to right we need to multiply by 10 in
        mov dx,ax   ;order to create the value of the number
ignore1:mov ah,8
        int 21h     ;waiting an input
        cmp al,30h
        jl ignore1
        cmp al,38h
        jg ignore   ;accept only digits 0-9
        mov ah,0eh
        int 10h   
        sub al,30h  ;change ASCII value to real value
        mov ah,00h
        add dx,ax   ;dx will have the number that we 
        loop start1  ;insert from keyborad in dec form
        
        add bx,dx
        push bx         
        
        push ax
waiting:
        mov ah,8
        int 21h     ;waiting an input
        cmp al,13
        jne waiting:    
        pop ax
        
        
        push ax
        push dx
        MOV AH,9            ;Change line.
        MOV DX, offset msg3
        INT 21H
        pop dx
        pop ax
        
        PRINT "SUM="  
        
            
        push ax
        push dx
        MOV CX,0 
        MOV AX,BX           ;!!!! BX had the number
BCD:                    ;Start making the number decimal from hex.
        MOV DX,0
        DIV sixteen             ;Divide by 10,until you cannot do it anymore.
        PUSH DX             
        INC CX              ;Hold the number of digits.
        CMP AX,0
        JNE BCD
        MOV DX,0
BCD_DIG:
        POP AX              ;Get in the reverse order the digits.
        MOV DL,AL
        cmp al,9
        jg letter
        ADD DL,30H
        jmp print
letter: add dl,55
        
print:          ;Transform them into correct ASCII codes.
        MOV AH,2
        INT 21H             ;Print them one by one digit.
        LOOP BCD_DIG  
        pop dx
        pop ax 
        
        mov bx,ax  
        push ax
        mov al,' '
        mov ah,0eh
        int 10h      
        mov ax,0
        pop ax 
        
        pop bx  
        
         
        push ax
        push dx 
        MOV CX,0 
        MOV AX,BX           ;!!!! BX had the number
BCD1:                    ;Start making the number decimal from hex.
        MOV DX,0
        DIV ten            ;Divide by 10,until you cannot do it anymore.
        PUSH DX             
        INC CX              ;Hold the number of digits.
        CMP AX,0
        JNE BCD1
        MOV DX,0
BCD_DIG1:
        POP AX              ;Get in the reverse order the digits.
        MOV DL,AL
        ADD DL,30H          ;Transform them into correct ASCII codes.
        MOV AH,2
        INT 21H             ;Print them one by one digit.
        LOOP BCD_DIG1
        
        pop dx
        pop ax  
        
        push ax
        push dx
        MOV AH,9            ;Change line.
        MOV DX, offset msg3
        INT 21H
        pop dx
        pop ax   

        
        popa
        pop dx
        pop cx
        pop bx
        pop ax
        jmp main
        
        
        
                    
        
        
        
        
         


ret

 
msg3 db 0ah,0dh,"$" 
sixteen dw 0010H            ;Constant sixteen = 16.
ten dw 000AH                ;Constant ten = 10.


