/*
 * project.asm
 *
 *  Created: 22/9/2015 12:24:41 ??
 *   Author: George
 */ 
.include "m16def.inc"
.org 0x00
.def reg= r16
.def temp= r17
.def compare= r18
.def counter = r19
    ldi reg,LOW(RAMEND)
	out SPL,reg
	ldi reg,HIGH(RAMEND)
	out SPH,reg
	clr reg
	out DDRB,reg
	clr reg
	out DDRA,reg
	ser reg
	out DDRC,reg

start:
	ldi counter,0x00
	ldi compare,0x01
	in reg,PINA
next1:
	mov temp,reg
	and temp,compare
	cpi temp,0x00
	brne no_zero1
	inc counter
no_zero1:
	lsl compare
	cpi compare,0x00
	brne next1

	ldi compare,0x01
	in reg,PINB
next2:
	mov temp,reg
	and temp,compare
	cpi temp,0x00
	brne no_zero2
	inc counter
no_zero2:
	lsl compare
	cpi compare,0x00
	brne next2

	clr reg
	clr compare
	ldi temp,10
dek:
	cp counter,temp
	brlo mon
	inc reg
	subi counter,10
	rjmp dek
mon:
	mov compare,counter
	
	lsl reg
	lsl reg
	lsl reg
	lsl reg
	or reg,compare
	out PORTC,reg


	rjmp start
	

	

	


