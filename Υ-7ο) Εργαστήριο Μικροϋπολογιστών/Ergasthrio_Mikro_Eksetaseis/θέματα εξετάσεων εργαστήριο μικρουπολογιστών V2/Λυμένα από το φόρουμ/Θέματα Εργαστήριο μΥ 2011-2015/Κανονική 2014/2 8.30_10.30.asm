                  include emu8086.inc
org 100h

main:
PRINT "DWSE ARITHMOUS:"  
           
                 
        push ax
        push bx
        push cx
        push dx
        pusha
        mov cx,2
        mov dx,0          
start:  mov ax,16   ;because we type the number from left
        mul dx      ;to right we need to multiply by 10 in
        mov dx,ax   ;order to create the value of the number
ignore: mov ah,8
        int 21h     ;waiting an input 
        
        cmp al,30h
        jl ignore
        cmp cx,2
        jne second
        cmp al,34h
        jle cont
        jmp skip
second:
        cmp al,39h
        jle cont
skip:               ;accept only digits 0-9, a-f ,A-F 
        cmp al,41h
        jl ignore
        cmp al,46h
        jle cont
        cmp al,61h
        jl ignore
        cmp al,66h
        jle cont
        jmp ignore
cont: 
        mov ah,0eh
        int 10h
        cmp al,39h
        jle number
        cmp al,46h
        jle letter_big
        jmp letter_small
number:           
        sub al,48  ;change ASCII value to real value
        jmp next
letter_big: 
        sub al,55
        jmp next
letter_small:
        sub al,87             
next:
        mov ah,00h
        add dx,ax   ;dx will have the number that we 
        loop start  ;insert from keyborad in hex form
        mov bx,dx   
        
        mov al,' '
        mov ah,0eh
        int 10h      
        mov ax,0          
                         
       mov cx,2
        mov dx,0          
start1:  mov ax,16   ;because we type the number from left
        mul dx      ;to right we need to multiply by 10 in
        mov dx,ax   ;order to create the value of the number
ignore1: mov ah,8
        int 21h     ;waiting an input 
        
        cmp al,30h
        jl ignore1
        cmp cx,2
        jne second1
        cmp al,34h
        jle cont1
        jmp skip1
second1:
        cmp al,39h
        jle cont1
skip1:               ;accept only digits 0-9, a-f ,A-F 
        cmp al,41h
        jl ignore1
        cmp al,46h
        jle cont1
        cmp al,61h
        jl ignore1
        cmp al,66h
        jle cont1
        jmp ignore1
cont1: 
        mov ah,0eh
        int 10h
        cmp al,39h
        jle number1
        cmp al,46h
        jle letter_big1
        jmp letter_small1
number1:           
        sub al,48  ;change ASCII value to real value
        jmp next1
letter_big1: 
        sub al,55
        jmp next1
letter_small1:
        sub al,87             
next1:
        mov ah,00h
        add dx,ax   ;dx will have the number that we 
        loop start1  ;insert from keyborad in hex form
        
        
        cmp bx,dx
        ja normal
        sub dx,bx
        mov bx,dx
        jmp output
normal:
        sub bx,dx 
output:              
        
        push ax
waiting:
        mov ah,8
        int 21h     ;waiting an input
        cmp al,13
        jne waiting:    
        pop ax
        
        
        push ax
        push dx
        MOV AH,9            ;Change line.
        MOV DX, offset msg3
        INT 21H
        pop dx
        pop ax 
        
        PRINT "APOLYTH=" 
        
            
        push ax
        push dx
        MOV CX,0 
        MOV AX,BX           ;!!!! BX had the number
BCD:                    ;Start making the number decimal from hex.
        MOV DX,0
        DIV sixteen             ;Divide by 10,until you cannot do it anymore.
        PUSH DX             
        INC CX              ;Hold the number of digits.
        CMP AX,0
        JNE BCD
        MOV DX,0
BCD_DIG:
        POP AX              ;Get in the reverse order the digits.
        MOV DL,AL
        cmp al,9
        jg letter
        ADD DL,30H
        jmp print
letter: add dl,55
        
print:          ;Transform them into correct ASCII codes.
        MOV AH,2
        INT 21H             ;Print them one by one digit.
        LOOP BCD_DIG  
        pop dx
        pop ax 
          
        
        push ax
        push dx
        MOV AH,9            ;Change line.
        MOV DX, offset msg3
        INT 21H
        pop dx
        pop ax   

        
        popa
        pop dx
        pop cx
        pop bx
        pop ax
        jmp main
        
        
        
                    
        
        
        
        
         


ret

 
msg3 db 0ah,0dh,"$" 
sixteen dw 0010H            ;Constant sixteen = 16.
ten dw 000AH                ;Constant ten = 10.


