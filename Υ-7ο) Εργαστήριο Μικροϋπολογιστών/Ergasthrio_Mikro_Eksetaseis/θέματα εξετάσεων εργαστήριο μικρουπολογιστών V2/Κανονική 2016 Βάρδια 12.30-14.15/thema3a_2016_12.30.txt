.include "m16def.inc"
.def led=r16
.def _x=r17
.def _y=r18
.def temp=r19
.def output_D=r20

; fix ret for subroutines
reset:
ldi r24, low(RAMEND)
out SPL, r24
ldi r24, high(RAMEND)
out SPH, r24

start:
ldi output_D, 0x00
clr temp
out ddra, temp
ser temp
out ddrd, temp

in _x, pina
andi _x, 0x0f

mov temp, _x
subi _x, 0x09
cpi _x, 0x00
brmi lower

mov _y, _x

count_loop:
sec
ror output_D
dec _y
cpi _y, 0x00
brne count_loop

out portd, output_D
jmp start

lower:
mov _y, temp

count_loop2:
sec
ror output_D
dec _y
cpi _y, 0x00
brne count_loop2

out portd, output_D
jmp start