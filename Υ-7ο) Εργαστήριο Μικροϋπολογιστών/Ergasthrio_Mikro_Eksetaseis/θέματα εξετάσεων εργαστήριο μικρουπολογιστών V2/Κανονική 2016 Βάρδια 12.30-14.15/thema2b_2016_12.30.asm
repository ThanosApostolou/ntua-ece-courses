
org 100h

.data

msg1 db  "DOSE THN EISODO: $" 
first db 0
second db 0 
sum db 0
diff db 0
is_number db 0
space db " $"
   
.code 

; print msg1
mov dx, offset msg1
mov ah, 09h
int 21h                           

;************FIRST****************
; read first hex digit
read_first:
mov ah,08h
int 21h
;   check if valid
cmp al, 'P'
je terminate
cmp al, 31h
jbe read_first
cmp al, 47h
jae read_first
cmp al, 3Ah
jae check1
jmp skip_check1 ; digit is between 0 and 9
check1:         ; check if it's between char : and @
cmp al, 40h
jbe read_first 
jmp its_valid
skip_check1:
mov is_number, 01h
; digit is okay 
its_valid:  ; so print it
mov dl,al
mov ah,02h
int 21h  
; save it
cmp is_number, 00h
je is_char
sub al, 30h
mov first, al 
mov is_number, 00h
jmp skip_is_char
is_char:
sub al, 37h
mov first, al
skip_is_char:
; print a space
mov dx, offset space
mov ah, 09h
int 21h      

;**********SECOND*******************
; read second hex digit 
read_second:
mov ah,08h
int 21h
;   check if valid
cmp al, 'P'
je terminate
cmp al, 31h
jbe read_second
cmp al, 47h
jae read_second
cmp al, 3Ah
jae check2
jmp skip_check2
check2:
cmp al, 40h
jbe read_second  
mov is_number, 00h
jmp its_valid2
skip_check2: 
mov is_number, 01h
; digit is okay
its_valid2: ; so print it  
mov dl,al
mov ah,02h
int 21h 
; save it
mov second, al
cmp is_number, 00h
je is_char2
sub second, 30h
mov is_number, 00h
jmp skip_is_char2 
is_char2:
sub second, 37h
skip_is_char2:

; find sum 
mov ah, 00h
mov al, 00h
mov al, first
mov sum, al
mov al, second
add sum, al

; find diff
mov al, first
mov diff, al
mov al, second
sub diff, al



terminate:   
ret