	.include "m16def.inc"
reset:
	ldi r17 , low(RAMEND) ;initialize stack pointer
	out SPL , r17
	ldi r17 , high(RAMEND)
	out SPH , r17
	clr r16
	out DDRB,r16 ;B-INPUT.
	;Arxikopoihsh gia output se LCD othoni
	;PD7-PD2 arxikopoiountai os eksodos
	ldi r24, 0xFC ;8eloume 11111100 sthn eksodo -> FC se hex
	out DDRD, r24
	rcall lcd_init
	clr r17
READ:
	in r16,PINB
	cp r16,r17
	;breq READ
	mov r17,r16
	clr r20
	ldi r24 ,0x01
	;wait for new input.
	;save new valuercall lcd_command
	ldi r24,low(1530)
	ldi r25,high(1530)
	rcall wait_usec
	ldi r19,0x07
WRITE_BIN:
	mov r18,r19
	mov r16,r17
	cpi r18,0x00
	breq CONT
shift:
	lsr r16
	dec r18
	cpi r18,0x01
	brge shift
	andi r16,0x01
	mov r24,r16
	adiw r24,48
	rcall lcd_data
	;lsr r16
	dec r19
	rjmp WRITE_BIN
	;katharismos othonhs
	;aparaithth kathusterhsh
	;8 digits-> 7 shifts max.
	;grafei ton duadiko arithmo
	;arxizontas apo ta MSB
	;shift oses theseis kai h thesh tou Bit
	;Grapse sthn othonh
CONT:
	andi r16,0x01 ;grapse LSB
	mov r24,r16
	adiw r24,48
	rcall lcd_data
	ldi r24,'='
	rcall lcd_data
	mov r16,r17
	clr r24
	cpi r16, 1 ; thetikos?
	brge positive
	cpi r16,0 ;mhden?
	breq MON_1
	ldi r24,'-' ;arnhtikos
	rcall lcd_data
	neg r16
	rjmp hundred
POSITIVE:
	ldi r24,'+'
	rcall lcd_data
HUNDRED:
	clr r24
	cpi r16,100
	brlo DECADE
	ldi r24,'1'
	ori r20,0x01 ;Ston r20 shmeiwnoume oti grafthkan
	;dekades h monades
	lsl r20 ;se periptwsh pou dwthei monopshfios
	;dekadikos
	rcall lcd_data ;na mhn tupwthoun mhdenika
	subi r16,100
	clr r24
DECADE:
	cpi r16,10
	brlo WRITEDEC ;r24 = digit
	ori r20,0x01
	inc r24
	subi r16,10
	brpl DECADE
WRITEDEC:
	cpi r20,0x00
	breq MON_1
	adiw r24,48
	rcall lcd_data
MON_1:
	clr r24
MON_2:
	cpi r16,1
	brlo WRITE_MON
	inc r24
	dec r16
	brpl MON_2
WRITE_MON:
	adiw r24,48
	clr r20
	rcall lcd_data
	rjmp reset
	;rjmp READ
wait_usec:
	sbiw r24 ,1 ; 2 cycles (0.250 ?sec)
	nop ; 1 cycle (0.125 ?sec)
	nop ; 1 cycle (0.125 ?sec)
	nop ; 1 cycle (0.125 ?sec)
	nop ; 1 cycle (0.125 ?sec)
	brne wait_usec ; 1 ? 2 cycles (0.125 ? 0.250 ?sec)
	ret ; 4 cycles (0.500?sec)
wait_msec:
	push r24 ; 2 cycles (0.250 ?sec)
	push r25 ; 2 cycles
	ldi r24 , low(998) ;Fortwse ton kataxvrhth r25:r24 me 998
	;(1 cycle - 0.125 ?sec)
	ldi r25 , high(998) ; 1 cycle (0.125?sec)
	rcall wait_usec ; 3 cycle (0.375 ?sec),prokalei synolika
	;ka8usterhsh 998. 375?sec
	pop r25 ; 2 cycles (0.250 ?sec)
	pop r24 ; 2 cycles
	sbiw r24 ,1 ; 2 cycles
	brne wait_msec ; 1 ? 2 cycles (0.125 ? 0.250 ?sec)
	ret ; 4cycles (0.500 ?sec)
	;Routines gia othoni
write_2_nibbles:
	;Gia metafora 2 tmhmatwn twn 4 bit th fora ston elegkth ths o8onhw
	;lcd etsi oste na dosoume stin othoni olokliro byte
	push r24 ;stelenei ta 4 MSB
	in r25 ,PIND ;diavazontas ta 4 LSB kai ta
	;ksanastelnoume
	andi r25 ,0x0f ;gia na mhn xalasoume thn opoia
	;prohgoumenh katastash
	andi r24 ,0xf0 ;apomonwnontai ta 4 MSB kai
	add r24 ,r25 ;syndiazontai me ta prouparxonta 4
	;LSB
	out PORTD ,r24 ;kai dinontai sthn eksodo
	sbi PORTD ,PD3 ; dhmiourgeite palmos ?nable ston
	;akrodekth PD3
	cbi PORTD ,PD3 ; PD3=1 kai meta PD3=0
	pop r24 ; stelnei ta 4 LSB. Anaktatai to byte.
	swap r24 ; enallasontai ta 4 MSB me ta 4 LSB
	andi r24 ,0xf0 ;pou me thn seira tous apostellontai
	add r24 ,r25
	out PORTD ,r24
	sbi PORTD ,PD3 ;Neos palmos ?nable
	cbi PORTD ,PD3
	ret
lcd_data:
	;Gia apostolh enos byte dedomenwn ston elegkth thw o8onhs lcd
	;Eisodos: o r24 me to byte tou dedomenou blepe pinaka data
	;Eksodos: - (emfanizei stin othoni an to display einai on stis rithmiseis
	;to data pou steilame. an steiloyme allo, auto tha emfanistei apo piso)
	sbi PORTD ,PD2 ; epilogh tou kataxwrhth dedomenwn
	;(PD2=1)
	rcall write_2_nibbles ; apostolh toy byte
	ldi r24 ,43 ;anamonh 43?sec mexri na
	;oloklhrwthei h lhpsh
	ldi r25 ,0 ;twn dedomenwn apo ton elegkth ths
	;lcd
	rcall wait_usec
	ret
lcd_command:
	;Gia apostolh mias entolhs ston elegkth ths o8onhs lcd
	;Eisodos: o r24 me to byte tis entolis blepe pinaka entolwn
	;Eksodos: - (kanei oti pei i entoli pou dosame ston r24)
	cbi PORTD ,PD2 ; epilogh tou kataxwrhth dedomenwn
	;(PD2=1)
	rcall write_2_nibbles ;apostolh ths entolhs kai anamonh
	;39?sec
	ldi r24 ,39 ;gia thn oloklhrwsh ths ekteleshs ths
	;apo ton elegkth ths lcd.
	ldi r25 ,0 ;SHM:uparxoun dyo entoles, oi clear
	;display kai return home,
	rcall wait_usec ;pou apaitoun shmantika megalutero
	;xroniko diasthma
	ret
lcd_init:
	;arxikopoihsh kai ruumiseis o8onhs
	;Rythmiseis othonis:
	;DL=0 4 bit mode
	;N=1 2lines
	;F=0 5x8 dots
	;D=1 display on
	;C=0 cursor off
	;B=0 blinking off
	;I/D = 1 DDRAM adress auto increment
	;SH=0 shift of entire display off
	;No input no output
	ldi r24 ,40 ; Otan o elegkths ths lcd trofodoteitai me
	ldi r25 ,0 ;reuma ektelei thn dikia tou arxikopoihsh
	rcall wait_msec ;Anamonh 40 msec mexri auth na oloklhrwthei.
	ldi r24 ,0x30 ;entolh metavashs se 8 bit mode
	out PORTD ,r24 ;epeidh den mporoume na eimaste vevaioi
	sbi PORTD ,PD3 ;gia thn diamorfwsh eisodou tou elegkth
	cbi PORTD ,PD3 ;ths othonhs, h entolh apostelletai duo
	;fores
	ldi r24 ,39
	ldi r25 ,0 ;ean o elegkths ths othonhs vrisketai se
	;8-bit mode
	rcall wait_usec ;den tha sumbei tipota , alla an o elegkths
	;exei diamorfwsh
	;eisodou 4 bit tha metavei se diamorfwsh 8 bit
	ldi r24 ,0x30
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x20 ;allagh se 4-bit mode
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x28 ;epilogh xarakthrwn megethous 5x8
	;koukidwn
	rcall lcd_command ;kai emfanish duo grammwn sthn othonh
	ldi r24 ,0x0c ;energopoihsh ths othonhs ,apokrupsh tou
	;kersora
	rcall lcd_command
	ldi r24 ,0x01 ;katharismos ths othonhs
	rcall lcd_command
	ldi r24 ,low(1530)
	ldi r25 ,high(1530)
	rcall wait_usec
	ldi r24 ,0x06 ;energoihsh automaths aukshshs kata 1 ths
	;dieuthunshs
	rcall lcd_command ;pou einai apothikeumenh ston metrhth
	;dieuthunsewn kai apergopoihsh ths olisthishs oloklhrhs ths othonhs
	ret



