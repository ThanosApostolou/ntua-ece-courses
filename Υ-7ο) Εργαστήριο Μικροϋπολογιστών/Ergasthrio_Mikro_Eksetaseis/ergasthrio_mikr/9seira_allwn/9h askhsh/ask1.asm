
.include "m16def.inc"
.dseg
_tmp_: .byte 2
.cseg
reset:
	ldi r17 , low(RAMEND) ;initialize stack pointer
	out SPL , r17
	ldi r17 , high(RAMEND)
	out SPH , r17
	;Arxikopoihsh PORTC - Keypad 4x4
	ldi r24 ,(1 << PC7) | (1 << PC6) | (1 << PC5) | (1 << PC4) ;Thetei os
	;eksodous ta 4MSB tis thiras PORTC
	out DDRC, r24
	;Arxikopoihsh gia output se LCD othoni
	;PD7-PD2 arxikopoiountai os eksodos
	ldi r24, 0xFC ;8eloume 11111100 sthn eksodo -> FC
	;se hex
	out DDRD, r24
	clr r24 ;clean output
	out PORTC,r24
	out PORTD,r24
	;Arxikopoihsh othonis se katastasi oste na mporei na
	;xrisimopoiithei
	rcall lcd_init
	;[upper|lower] [MSB|LSB]
	;K: lower: HLHH (1011) upper: LHLL (0100) =4?
	;E: lower: LHLH (0101) upper: LHLL (0100) = 45
	;N: lower: HHHL(1110) upper: LHLL (0100) = 4?
	;O: lower: HHHH (1111) upper: LHLL (0100) = 4F
NONE:
	;Stelnoume to byte stin othoni, 1 gramma kathe fora me tin
	;lcd_data
	ldi r24, 0x4E
	rcall lcd_data
	ldi r24, 0x4F
	rcall lcd_data
	ldi r24, 0x4E
	rcall lcd_data
	ldi r24, 0x45
	rcall lcd_data
input_keypad:
	ldi r24, 15 ;15msec gia spinthirismo
	rcall scan_keypad_rising_edge ;Elegxoume olo to pliktrologio
	rcall keypad_to_ascii ;Metatrepoume se ascii tin
	;eisodo (ara o r24 exei ton binary code tou grammatos pou theloume)
	cpi r24,'/' ;An exoume �/� simainei oti den
	;diabastike tipota
	breq input_keypad ;Opote synexizei na emfanizetai
	;NONE kai ksanadiabazo
	;Katharismos othonis me entoli, gia na fygei to NONE
	;Clear display : 000 000 01
	push r24 ;Gia na min xasoume to ascii apo
	;to read
	ldi r24, 0x01 ;Command gia Display
	rcall lcd_command
	ldi r24,low(1530) ;aparaititi kathisterisi
	ldi r25,high(1530)
	rcall wait_usec
	pop r24
	rcall lcd_data ;Kai to stelnoume stin othoni
	rjmp input_keypad
	;Routines
scan_row:
	;Eisodos: Arithmos grammis
	;Eksodos: 4LSB tou r24 i katastasi kathe diakopti (gia ta 4 psifia tis
	;grammis)
	ldi r25 ,0x08
back_:
	lsl r25
	dec r24
	brne back_
	out PORTC ,r25
	nop
	nop
	in r24 ,PINC
	andi r24 ,0x0f
	ret
	; arxikopoihsh me �0000 1000�
	; aristerh olisthisi tou �1� toses theseis
	; osos einai o arithmos ths grammhs
	; h antistoixh grammh tithetai sto logiko �1�
	; kathusterhsh gia na prolabei na ginei h allagh katastashs
	; epistrefoun oi theseis (sthles)twn diakoptwn poy einai piesmenoi
	; apomonwnontai ta 4 LSB opoy ta �1� deixnoun pou einai pathmenoi
	; oi diakoptes
scan_keypad:
	;Elegxos oloklirou pliktrologiou
	;Eisodos: kamia
	;Eksodos: stous r25:r24 i katastasi kai twn 16 diakoptwn
	ldi r24 ,0x01
	rcall scan_row
	swap r24 ;Sta high tou r27 ta psifia tis grammis 1
	mov r27 ,r24
	ldi r24 ,0x02
	rcall scan_row
	add r27 ,r24 ;Sta low tou r27 ta psifia tis grammis 2
	ldi r24 ,0x03
	rcall scan_row
	swap r24
	mov r26 ,r24 ;Sta high tou r26 ta psifia tis grammis 3
	ldi r24 ,0x04
	rcall scan_row
	add r26 ,r24 ;Sta low tou r26 ta psifia tis grammis 4
	movw r24 ,r26 ;R24=R26 KAI R25=R27
	ret
	;msb r24 exei ta psifia tis grammis 3 [4,5,6,B]
	;lsb r24 exei ta psifia tis grammis 4 [1,2,3,A]
	;msb r25 exei ta psifia tis grammis 1 [*,0,#,D]
	;lsb r25 exei ta psifia tis grammis 2 [7,8,9,C]
	; elegkse thn prwth grammh tou plhktrologiou
	; appothikeuse to apotelesma
	; sta 4 msb tou r27
	; elegkse thn deuterh grammh toy plhktrologiou
	; apothikeuse to apotelesma sta 4 lsb tou r27
	; elegkse thn trith grammh toy plhktrologiou
	; appothikeuse to apotelesma
	; sta 4 msb tou r26
	; elegkse thn tetarth grammh toy plhktrologiou
	; apothikeuse to apotelesma sta 4 lsb tou r26
	; metefere to apotelesma stous kataxwrhtes r25:r24
scan_keypad_rising_edge:
	;Elegxos oloklirou pliktrologiou me spinthirismo
	;Eisodos: Anamenomenos xronos spinthirismou diakoptwn se msec,
	;ston r24 (10-20msec)
	;Eksodos: stous r25:r24 i katastasi kai twn 16 diakoptwn
	mov r22 ,r24
	rcall scan_keypad
	push r24
	push r25
	mov r24 ,r22
	ldi r25 ,0
	rcall wait_msec
	rcall scan_keypad
	pop r23
	pop r22
	and r24 ,r22
	and r25 ,r23
	ldi r26 ,low(_tmp_)
	ldi r27 ,high(_tmp_)
	ld r23 ,X+
	ld r22 ,X
	st X ,r24
	st -X ,r25
	com r23
	com r22
	and r24 ,r22
	and r25 ,r23
	ret
	; apothikeuse to xrono spin8irismou ston r22
	; elegkse to plhktologio gia piesmenous diakoptes
	;kai apothikeuse to apotelesma
	;kathusterise r22 ms (tupikes times 10-20 msec pou
	;kathorizontai apo ton
	; katasteuasth tou plhktrologiou� xronodiarkeia spin8hrismwn)
	; elegkse to plhktologio ksana kai
	; aporipse osa plhktra emfanizoun
	; spin8irismo
	;fortwse thn katastash twn diakoptwn sthn
	; prohfoumenh klhsh ths routinas stous r27:r26
	;apothhkeuse sth RAM th nea katastash
	; twn diakoptwn
	;vres tous diakptes pou exoun �molis� path8ei
wait_usec:
	sbiw r24 ,1 ; 2 cycles (0.250 ?sec)
	nop ; 1 cycle (0.125 ?sec)
	nop ; 1 cycle (0.125 ?sec)
	nop ; 1 cycle (0.125 ?sec)
	nop ; 1 cycle (0.125 ?sec)
	brne wait_usec ; 1 ? 2 cycles (0.125 ? 0.250 ?sec)
	ret ; 4 cycles (0.500?sec)
wait_msec:
	push r24 ; 2 cycles (0.250 ?sec)
	push r25 ; 2 cycles
	ldi r24 , low(998) ;Fortwse ton kataxvrhth r25:r24 me 998
	;(1 cycle - 0.125 ?sec)
	ldi r25 , high(998) ; 1 cycle (0.125?sec)
	rcall wait_usec ; 3 cycle (0.375 ?sec),prokalei synolika
	;ka8usterhsh 998. 375?sec
	pop r25 ; 2 cycles (0.250 ?sec)
	pop r24 ; 2 cycles
	sbiw r24 ,1 ; 2 cycles
	brne wait_msec ; 1 ? 2 cycles (0.125 ? 0.250 ?sec)
	ret ; 4cycles (0.500 ?sec)
keypad_to_ascii:
	;Antistoixisi diakoptwn se ascii
	;Eisodos: 16 bit stous r25:r24 (oti pirame diladi apo tin
	;scan_keypad_rising_edge)
	;Eksodos: O kodikos ascii pou antistoixei ston proto patimeno diakopti
	;pou entopistike, apothikeuetai ston r24, i / an den patithike kapoios
	movw r26 ,r24
	ldi r24 ,'*'
	sbrc r26 ,0
	ret
	ldi r24 ,'0'
	sbrc r26 ,1
	ret
	ldi r24 ,'#'
	sbrc r26 ,2
	ret
	;logiko �1� stis 8eseis tou kataxwrhth r26 dhlwnoun
	;ta parakatw sumbola kai ari8mous
	; r26 C 9 8 7 D # 0 *
	ldi r24 ,'D'
	sbrc r26 ,3
	ret
	ldi r24 ,'7'
	sbrc r26 ,4
	ret
	ldi r24 ,'8'
	sbrc r26 ,5
	ret
	ldi r24 ,'9'
	sbrc r26 ,6
	ret
	ldi r24 ,'C'
	sbrc r26 ,7
	ret
	ldi r24 ,'4'
	sbrc r27 ,0
	ret
	ldi r24 ,'5'
	sbrc r27 ,1
	ret
	ldi r24 ,'6'
	sbrc r27 ,2
	ret
	ldi r24 ,'B'
	sbrc r27 ,3
	ret
	ldi r24 ,'1'
	sbrc r27 ,4
	ret
	ldi r24 ,'2'
	sbrc r27 ,5
	ret
	ldi r24 ,'3'
	sbrc r27 ,6
	ret
	ldi r24 ,'A'
	sbrc r27 ,7
	ret
	ldi r24,'/'
	ret
	;Routines gia othoni
write_2_nibbles:
	;Gia metafora 2 tmhmatwn twn 4 bit th fora ston elegkth ths o8onhw
	;lcd etsi oste na dosoume stin othoni olokliro byte
	push r24 ;stelenei ta 4 MSB
	in r25 ,PIND ;diavazontas ta 4 LSB kai ta
	;ksanastelnoume
	andi r25 ,0x0f ;gia na mhn xalasoume thn opoia
	;prohgoumenh katastash
	andi r24 ,0xf0 ;apomonwnontai ta 4 MSB kai
	add r24 ,r25 ;syndiazontai me ta prouparxonta 4
	;LSB
	out PORTD ,r24 ;kai dinontai sthn eksodo
	sbi PORTD ,PD3 ; dhmiourgeite palmos ?nable ston
	;akrodekth PD3
	cbi PORTD ,PD3 ; PD3=1 kai meta PD3=0
	pop r24 ; stelnei ta 4 LSB. Anaktatai to byte.
	swap r24 ; enallasontai ta 4 MSB me ta 4 LSB
	andi r24 ,0xf0 ;pou me thn seira tous apostellontai
	add r24 ,r25
	out PORTD ,r24
	sbi PORTD ,PD3 ;Neos palmos ?nable
	cbi PORTD ,PD3
	ret
lcd_data:
	;Gia apostolh enos byte dedomenwn ston elegkth thw o8onhs lcd
	;Eisodos: o r24 me to byte tou dedomenou blepe pinaka data
	;Eksodos: - (emfanizei stin othoni an to display einai on stis rithmiseis
	;to data pou steilame. an steiloyme allo, auto tha emfanistei apo piso)
	sbi PORTD ,PD2 ; epilogh tou kataxwrhth dedomenwn
	;(PD2=1)
	rcall write_2_nibbles ; apostolh toy byte
	ldi r24 ,43 ;anamonh 43?sec mexri na
	;oloklhrwthei h lhpsh
	ldi r25 ,0 ;twn dedomenwn apo ton elegkth ths lcd
	
	rcall wait_usec
	ret
lcd_command:
	;Gia apostolh mias entolhs ston elegkth ths o8onhs lcd
	;Eisodos: o r24 me to byte tis entolis blepe pinaka entolwn
	;Eksodos: - (kanei oti pei i entoli pou dosame ston r24)
	cbi PORTD ,PD2 ; epilogh tou kataxwrhth dedomenwn
	;(PD2=1)
	rcall write_2_nibbles ;apostolh ths entolhs kai anamonh
	;39?sec
	ldi r24 ,39 ;gia thn oloklhrwsh ths ekteleshs ths
	;apo ton elegkth ths lcd.
	ldi r25 ,0 ;SHM:uparxoun dyo entoles, oi clear
	;display kai return home,
	rcall wait_usec ;pou apaitoun shmantika megalutero
	;xroniko diasthma
	ret
lcd_init:
	;arxikopoihsh kai ruumiseis o8onhs
	;Rythmiseis othonis:
	;DL=0 4 bit mode
	;N=1 2lines
	;F=0 5x8 dots
	;D=1 display on
	;C=0 cursor off
	;B=0 blinking off
	;I/D = 1 DDRAM adress auto increment
	;SH=0 shift of entire display off
	;No input no output
	ldi r24 ,40 ; Otan o elegkths ths lcd trofodoteitai me
	ldi r25 ,0 ;reuma ektelei thn dikia tou arxikopoihsh
	rcall wait_msec ;Anamonh 40 msec mexri auth na oloklhrwthei.
	ldi r24 ,0x30 ;entolh metavashs se 8 bit mode
	out PORTD ,r24 ;epeidh den mporoume na eimaste vevaioi
	sbi PORTD ,PD3 ;gia thn diamorfwsh eisodou tou elegkth
	cbi PORTD ,PD3 ;ths othonhs, h entolh apostelletai duo
	;fores
	ldi r24 ,39
	ldi r25 ,0 ;ean o elegkths ths othonhs vrisketai se
	;8-bit mode
	rcall wait_usec ;den tha sumbei tipota , alla an o elegkths
	;exei diamorfwsh
	;eisodou 4 bit tha metavei se diamorfwsh 8 bit
	ldi r24 ,0x30
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x20 ;allagh se 4-bit mode
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x28 ;epilogh xarakthrwn megethous 5x8
	;koukidwn
	rcall lcd_command ;kai emfanish duo grammwn sthn othonh
	ldi r24 ,0x0c ;energopoihsh ths othonhs ,apokrupsh tou
	;kersora
	rcall lcd_command
	ldi r24 ,0x01 ;katharismos ths othonhs
	rcall lcd_command
	ldi r24 ,low(1530)
	ldi r25 ,high(1530)
	rcall wait_usec
	ldi r24 ,0x06 ;energoihsh automaths aukshshs kata 1 ths
	;dieuthunshs
	rcall lcd_command ;pou einai apothikeumenh ston metrhth
	;dieuthunsewn kai apergopoihsh ths olisthishs oloklhrhs ths othonhs
	ret


