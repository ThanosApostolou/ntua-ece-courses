.include "m16def.inc"
.def result=r26
.def A=r16
.def B=r17
.def C=r18
.def D=r19
.def E=r20
.def temp1=r21
.def temp2=r22
.def F0=r23

reset: 
ldi r24,low(RAMEND)
out SPL,r24
ldi r24,high(RAMEND)
out SPH,r24
ldi r24,255
out DDRA,r24
ldi r24,224
out DDRC,r24
clr A
clr B
clr C
clr D
clr E
clr temp1
clr temp2
clr F0
clr result


main:
in temp1,PINC

mov A,temp1
ldi temp2,1
and A,temp2

mov B,temp1
ldi temp2,2
and B,temp2
ror B

mov C,temp1
ldi temp2,4
and C,temp2
ror C
ror C

mov D,temp1
ldi temp2,8
and D,temp2
ror D
ror D
ror D

mov E,temp1
ldi temp2,16
and E,temp2
ror E
ror E
ror E
ror E

;calc_F0
mov temp1,A
and temp1,B
mov temp2,temp1

mov temp1,B
and temp1,C
or temp2,temp1

mov temp1,C
and temp1,D
or temp2,temp1

mov temp1,D
and temp1,E
or temp2,temp1

com temp2
ldi temp1,1
and temp2,temp1
mov F0,temp2
mov result,temp2

;calc_F0
mov temp2,D
or temp2,E
com temp2

mov temp1,A
and temp1,B
and temp1,C
and temp1,D

or temp1,temp2
ldi temp2,1
and temp1,temp2
clc
rol temp1
or result,temp1

; calc_F2
clc
ror temp1
mov temp2,F0
or temp1,temp2

ldi temp2,1
and temp1,temp2
clc
rol temp1
clc
rol temp1
or result,temp1


out PORTA,result

jmp main
