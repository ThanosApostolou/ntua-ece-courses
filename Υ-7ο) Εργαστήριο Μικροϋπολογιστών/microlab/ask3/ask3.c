#include<avr/io.h>
#include<stdbool.h> 

int main()
{
    DDRD = 0x00;        //configure portB as input
    DDRB = 0xFF;        //configure portC as output
     
    unsigned char input=0, result;
    bool SW0, SW1, SW2, SW3;
 	
	result=1;
	PORTB=result;
    while(1)
    {	
        input = PIND & 15;
		SW0 = input & 1;
        SW1 = input & 2;
        SW2 = input & 4;
        SW3 = input & 8;		

        if (SW3) {
			while(SW3) {
				input = PIND & 15;
				SW0 = input & 1;
        		SW1 = input & 2;
        		SW2 = input & 4;
        		SW3 = input & 8;
			}
			result = 1;
			PORTB = result;
		} else if (SW2) {
			while(SW2 && !SW3) {
				input = PIND & 15;
				SW0 = input & 1;
        		SW1 = input & 2;
        		SW2 = input & 4;
        		SW3 = input & 8;
			}
			if (!SW3) {
				result = 128;
				PORTB = result;
			}
		} else if (SW1) {
		    while(SW1 && !SW2 && !SW3) {
				input = PIND & 15;
				SW0 = input & 1;
        		SW1 = input & 2;
        		SW2 = input & 4;
        		SW3 = input & 8;
			}
			if (!SW2 && !SW3) {
				result <<= 1;
				if (result == 0) {
					result = 1;
				}
				PORTB = result;
			}
		} else if (SW0) {
			while(SW0 && !SW1 && !SW2 && !SW3) {
				input = PIND & 15;
				SW0 = input & 1;
        		SW1 = input & 2;
        		SW2 = input & 4;
        		SW3 = input & 8;
			}
			if (!SW1 && !SW2 && !SW3) {
				result >>= 1;
				if (result < 1) {
					result = 128;
				}
				PORTB = result;
			}
		}
        
    }
    return 0;
}
