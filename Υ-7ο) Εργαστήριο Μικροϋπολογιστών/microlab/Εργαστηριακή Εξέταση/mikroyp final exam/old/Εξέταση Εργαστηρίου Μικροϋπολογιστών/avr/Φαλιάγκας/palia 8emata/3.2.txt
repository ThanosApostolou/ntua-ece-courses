

DATA_SEG SEGMENT
MSG1 DB 00AH,00DH,'GIVE A DECIMAL NUMBER: $'
MSG2 DB 00AH,00DH,'HEX: $' 
false_msg DB 00AH,00DH,'PRESS ENTER: $'
DATA_SEG ENDS

CODE_SEG SEGMENT
    ASSUME CS:CODE_SEG, DS:DATA_SEG  
    
    PRINT_UNSAFE macro CHAR
        mov DL, CHAR ; Place char byte in DL.
        mov AH, 0x02 ; Load DOS operation.
        int 0x21 ; Call DOS.
    endm
    
    PRINT_STR MACRO STRING 
        PUSH DX
        PUSH AX
        MOV DX,OFFSET STRING
        MOV AH,9
        INT 21H
        POP AX
        POP DX
    ENDM
    
    READ MACRO 
        PUSH DX
        MOV DH,AH
        MOV AH, 08
        INT 21H
        MOV AH,DH
        POP DX
    ENDM   
    
    PRINT MACRO CHAR 
        PUSH DX
        PUSH AX
        MOV DL, CHAR
        MOV AH,02
        MOV AL,02
        INT 21H
        POP AX
        POP DX  
    ENDM    
    
    EXIT macro
        ; Service select: AH <- 0x4C
        ; Returned value: AL <- 0x00
        mov AX, 0x4C00
        int 0x21 ; Invoke DOS software interrupt.
    endm
        
    MAIN PROC FAR
        MOV AX,DATA_SEG
        MOV DS,AX
      START: 
        PRINT_STR MSG1
        mov CX,4
        mov DX,0
        mov AX,0
      R_DEC:      
        call in_dec
        mov AH,0
        cmp AL,'Q'
        je QUIT
        ;rol AL,4
        push AX
        mov AX,DX
        mov BX,10d
        mul BX
        ;rol AX,4  
        mov DX,AX
        pop AX
        add AX,DX
        mov DX,AX
        loop R_DEC
        ;jmp START
      
      SECOND_LINE:
        READ ;diavazoume eisodo ap t plhktrologio
        cmp AL,13 ;an den ine enter
        je CONT ;3anadiavase
        PRINT_STR false_msg
        jmp SECOND_LINE     
      
      CONT: 
        PRINT_STR MSG2  
        mov AX,DX
        call out_hex_word
        jmp START
        
     QUIT:
        EXIT 
     MAIN ENDP     
      
                    
                    
    in_dec proc NEAR
    _DIGNORE:
        READ ; Read a char from keyboard.
        cmp AL, 'Q' ; If user entered 'Q', terminate program.
        je _DQUIT
        cmp AL, '0' ; chr(AL) < chr(0)?.
        jl _DIGNORE ; yes: Ignore and request new char.
        cmp AL, '9' ; chr(AL) > chr(9)?
        jg _DIGNORE ; yes: Ignore and request new char.
        PRINT AL ; Char is in 0-9 range. Print it to screen.
        sub AL, '0' ; Get numeric value.
    _DQUIT:
        ret ; Terminate routine.
    endp               
           
    out_hex_word proc NEAR
        mov BX, AX ; Save AX in BX.
        xchg AH, AL ; Exchange AL with AH,
        ; Now AL contains the two most significant hex digits.
        call out_hex_byte ; Print AL as 2 hex digits.
        mov AX, BX ; Restore AX from BX.
        ; Now AL contains the two least significant hex digits.
        call out_hex_byte ; Print AL as 2 hex digits.
        ret ; Terminate routine.
    endp  
    
    out_hex_byte proc NEAR
        mov CH, AL ; Save AL in CH.
        mov CL, 4 ; Set rotation counter.
        shr AL, CL ; Swap high & low nibble of AL, to print MSH first.
        and AL, 0x0f ; Mask out high nibble (low nibble is single hex digit).
        mov DL, AL ; Copy AL to DL.
        call out_hex ; ... and print as hex.
        mov AL, CH ; Recover unswapped AL from CH.
        and AL, 0x0f ; Mask out high nibble (already printed).
        mov DL, AL ; Copy AL to DL.
        call out_hex ; ... and print as hex.
        ret ; Terminate routine.
    endp                     
    
    out_hex proc NEAR
        cmp DL, 9 ; DL <= 9?
        jle _DEC ; yes: jump to appropriate fixing code.
        add DL, 0x37 ; no : Prepare DL by adding chr(A) - 10 = 0x37.
        jmp _HEX_OUT ; ... and go to output stage.
        _DEC:
        add DL, '0' ; Prepare DL by adding chr(0) = 0x30.
        _HEX_OUT:
        PRINT_UNSAFE DL ; Print char to screen.
        ret ; Terminate routine.
    endp    
    
                   
    CODE_SEG ENDS
END MAIN      

    
