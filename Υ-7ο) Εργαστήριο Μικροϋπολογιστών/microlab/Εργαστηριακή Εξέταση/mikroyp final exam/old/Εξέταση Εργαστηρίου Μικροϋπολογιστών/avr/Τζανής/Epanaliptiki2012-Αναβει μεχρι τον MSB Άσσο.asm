/***************INTRO BREAKER:����� ��� ������ �� 8 ���������� BIT-BIT*****************/
.include "m16def.inc" 

.def temp=r31
.def x0=r16
.def x1=r17
.def x2=r18
.def x3=r19
.def x4=r20
.def x5=r21
.def x6=r22
.def x7=r23

.def in1=r26


rjmp reset 

reset: 
ldi temp,high(RAMEND) 
out SPH, temp 
ldi temp,low(RAMEND) 
out SPL, temp
 
clr temp		; temp = 0x00
out DDRA,temp	; PORTA for input
ser temp		; temp = 0xFF
out DDRC,temp	; PORTC for output

start:
in in1,PINA
mov x0,in1
mov x1,in1
mov x2,in1
mov x3,in1
mov x4,in1
mov x5,in1
mov x6,in1
mov x7,in1

andi x0, $01

andi x1, $02
lsr x1

andi x2, $04
lsr x2
lsr x2

andi x3, $08
lsr x3
lsr x3
lsr x3

andi x4, $10
lsr x4
lsr x4
lsr x4
lsr x4

andi x5, $20
lsr x5
lsr x5
lsr x5
lsr x5
lsr x5

andi x6, $40
lsr x6
lsr x6
lsr x6
lsr x6
lsr x6
lsr x6

andi x7, $80
lsr x7
lsr x7
lsr x7
lsr x7
lsr x7
lsr x7
lsr x7	

cpi x7, 0x01
breq led0

cpi x6, 0x01
breq led1

cpi x5, 0x01
breq led2

cpi x4, 0x01
breq led3

cpi x3, 0x01
breq led4

cpi x2, 0x01
breq led5

cpi x1, 0x01
breq led6

cpi x0, 0x01
breq led7

jmp led8

led0:
clr temp
out PORTC,temp
jmp start

led1:
ldi temp, $80
out PORTC,temp
jmp start

led2:
ldi temp,0b11000000
out PORTC,temp
jmp start

led3:
ldi temp,0b11100000
out PORTC,temp
jmp start

led4:
ldi temp,0b11110000
out PORTC,temp
jmp start

led5:
ldi temp,0b11111000
out PORTC,temp
jmp start

led6:
ldi temp,0b11111100
out PORTC,temp
jmp start

led7:
ldi temp,0b11111110
out PORTC,temp
jmp start

led8:
ldi temp,0b11111111
out PORTC,temp
jmp start