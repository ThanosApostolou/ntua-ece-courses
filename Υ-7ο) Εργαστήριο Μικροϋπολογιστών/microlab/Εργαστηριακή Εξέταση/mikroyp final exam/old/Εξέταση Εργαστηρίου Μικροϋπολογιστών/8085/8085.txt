*************************************************
************** 8085 SOS! ************************
JC an einai mikroteros
JNC an den einai mikroteros
JZ an einai isos
JNZ an den einai isos
;VASIKA:
LDA 2000H	;DIAVASMA DIAKOPTWN STON A
STA 3000H	;E3ODOS STA LEDS
CALL BEEP	;KLHSH ROUTINAS GIA PARAGWGH HXOY
CMA		;ANTISTROFH LOGIKH

MVI B,03H
MVI C,E8H
CALL DELB	;XRONOKA8YSTERHSH 1SEC

CALL KIND	;ANAGNWSH XARAKTHRA APO PLHKTROLOGIO STON A

MOV A,B		;� ��������� ����� �� ����� ������������� �� ���� 2
DCR A		;����� ��������� 1 ��� �� �
CMA		;��� ����������� �������� ������


***************** EKTYPWSH 7SEGMENT ***************
LXI H,0B00H	;������������ ��� ���� 0�0O�
MOV M,A		;A ���� ����������
LXI D,0B00H	;�������� ������ ��� �� 7segment display 
CALL STDM	;��� 0�00� ��� 0B90H
CALL DCD	;����������� ��� ������� ��� ������������

** ALLIWS **
STA 0B05H	 ;�� ���: ��� 05 � � ��� ��� 04 � �
MOV A,B
STA 0B04H



*********** ROUTINA EKTYPWSHS *********************
PRINT:	PUSH PSW
	PUSH B
	PUSH D
	PUSH H
	LXI D,0B00H
	CALL STDM
	CALL DCD
	POP H
	POP D
	POP B
	POP PSW
	RET

******* ROUTINA ARXIKOPOIHSHS EKTYPWSHS **********
INIT:
	PUSH PSW
	MVI A,10H
	STA 0B00H
	STA 0B01H
	STA 0B02H
	STA 0B03H
	STA 0B04H
	STA 0B05H
	POP PSW
	CALL PRINT
	RET

*********** OTAN 8ELOYME KA8E YHFIO TOY A *************
LDA 2000H
MOV B,A
BIT0:			; D = BIT0
	ANI 01H
	MOV D,A
	MOV A,B
	RRC
BIT1:			; E = BIT1
	ANI 01H
	MOV E,A
	MOV A,B
	RRC
	RRC

BIT2:			; H = BIT2
	ANI 01H
	MOV H,A
	MOV A,B
	RRC
	RRC
	RRC

BIT3:			; L = BIT3
	ANI 01H
	MOV L,A
	MOV A,B
	RRC
	RRC
	RRC
	RRC

;PROSOXH EDW, 3ANAXRHSIMOPOIW TOYS D-E-H-L

BIT4:			; D = BIT4
	ANI 01H
	MOV D,A
	MOV A,B
	RRC
	RRC
	RRC
	RRC
	RRC

BIT5:			; E = BIT5
	ANI 01H
	MOV E,A
	MOV A,B
	RRC
	RRC
	RRC
	RRC
	RRC
	RRC

BIT6:			; H = BIT6
	ANI 01H
	MOV H,A
	MOV A,B
	RRC
	RRC
	RRC
	RRC
	RRC
	RRC
	RRC

BIT7:			; L = BIT7
	ANI 01H
	MOV L,A

************ A8ROIZW ONES + ZEROS *************
LDA 2000H
MOV B,A

BIT0:			; D = BIT0
	ANI 01H
	MOV D,A
	MOV A,B
	RRC

BIT1:			; E = BIT1
	ANI 01H
	MOV E,A
	MOV A,B
	RRC
	RRC

BIT2:			; H = BIT2
	ANI 01H
	MOV H,A
	MOV A,B
	RRC
	RRC
	RRC

BIT3:			; L = BIT3
	ANI 01H
	MOV L,A

COUNTING1:
	MVI A,00H
	ADD D
	ADD E
	ADD H
	ADD L	
	MOV C,A			

BIT4-7:		; BITS 4-7
	MOV A,B
	RRC
	RRC
	RRC
	RRC

BIT4:			; D = BIT4
	ANI 01H
	MOV D,A
	MOV A,B
	RRC
	RRC
	RRC
	RRC
	RRC

BIT5:			; E = BIT5
	ANI 01H
	MOV E,A
	MOV A,B
	RRC
	RRC
	RRC
	RRC
	RRC
	RRC

BIT6:			; H = BIT6
	ANI 01H
	MOV H,A
	MOV A,B
	RRC
	RRC
	RRC
	RRC
	RRC
	RRC
	RRC

BIT7:			; L = BIT7
	ANI 01H
	MOV L,A
		
COUNTING2:
	MVI A,00H
	ADD D
	ADD E
	ADD H
	ADD L	
	ADD C		;A<-������ �����

	MOV C,A	;��� �������� �����
	MVI A,08H
	SUB C		;���!

*************** DYADIKOS A --> DEKADIKOS CBA *******************
BIN-TO-DEC:		 ;����� �� ������� ��� ���������� �
				 ;�� 10-������ �� ����������� �������� ��� PRINT-SEG 
				 ;A->������� B->������� C->�����������
	MOV C,A	
	ANI 80H  ;������� �� ���� �� ���������� ��� �������� - ��������� ��� MSB
	CPI 80H 
	JZ MINUS ;END
	MOV A,C
		 ;������ ������� ���
	JMP CON1

	MINUS:
	MVI A,1CH 	 ;������� ���� � ��� ������ ��� �����
	STA 0B03H
	MOV A,C
	CMA       	 ;������������� ��� � ��� ����������� 1
	ADI 01H
	
	CON1:
	MVI B,00H 	 ;������������ 10���
	MVI C,00H	 ;������������ 100���
	
	LO:
	SUI 64H
	JC LO1 		 ;�� �������� "EXTRA" 100���
	INR C 		 ;100������ ++
	JMP LO	

	LO1:
	ADI 64H

	LO2:
	SUI 0AH
	JC LO3 		 ;�� �������� "EXTRA" 10���
	INR B
	JMP LO2
	
	LO3:
	ADI 0AH

**************** DYADIKOS APO KIND SE HEX STA 7SEGMENTS *****************
���-HEX:		 ;�������� ��� ������������ ������ (�������) ��� ����� �� 2 �������� 4-4
	CALL KIND	 ;��� ��������� ��� SEG'S �� ����������� ����� ���� ������� �� ��� ������
	MOV C,A		 ;���� ���� ����� 4 ��� (16-0F),�� SEG'S ���������� �� 16-���� ����������
	ANI 0FH		  
	MOV B,A	;4LSB->B  
	MOV A,C		  
	ANI F0H
	RRC
	RRC
	RRC
	RRC	;4MSB->A


*************** POLLAPLASIASMOS B := C x B **********************
	MOV A,C
MUL:	ADD C
	DCR B
	JNZ MUL
	SUB C
	MOV B,A	;Keep result on B

***************** INTRPT! **********************
;STO KYRIWS PROGRAMMA EPITREPOUME DIAKOPES:
...
SIM
...

INTR_ROUTINE:
	DI
	...	;KWDIKAS GIA TO TI 8ELOUME NA KANEI STH DIAKOPH
	EI
	RET

