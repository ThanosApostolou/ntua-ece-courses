/*
 * project.asm
 *
 *  Created: 22/9/2015 12:24:41 ??
 *   Author: George
 */ 
.include "m16def.inc"
.org 0x00
.def reg= r16
.def temp= r17
.def digits =r18
.def x1 =r19
.def x2 = r20
.def result =r21
    ldi reg,LOW(RAMEND)
	out SPL,reg
	ldi reg,HIGH(RAMEND)
	out SPH,reg
	clr reg
	out DDRA,reg
	clr reg
	out DDRB,reg
	ser reg
	out DDRC,reg

start:
	ldi temp,0
	ldi digits,8
	in reg,PINA
next:
	clc
	ror reg
	brcs no_add
	inc temp
no_add:
	dec digits
	brne next

	in reg,PINB
	ldi digits,8
next1:
	clc
	ror reg
	brcs no_add1
	inc temp
no_add1:
	dec digits
	brne next1

	mov reg,temp
	clr temp
dek:
	cpi reg,10
	brlo mon
	inc temp
	subi reg,10
	rjmp dek
mon:
	lsl temp
	lsl temp
	lsl temp
	lsl temp
	or reg,temp

	out PORTC,reg
	rjmp start







	








	

	

	


