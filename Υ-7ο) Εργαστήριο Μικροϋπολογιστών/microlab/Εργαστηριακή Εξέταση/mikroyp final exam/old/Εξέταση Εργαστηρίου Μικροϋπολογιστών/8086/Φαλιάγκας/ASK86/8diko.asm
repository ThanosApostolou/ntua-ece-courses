; multi-segment executable file template.
   
   
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt


                         ;EXODOS
EXIT    MACRO

        MOV AX,4C00H
        INT 21H

ENDM


PRINT_STR MACRO STRING        ;EKTIPOSI STRING
PUSH DX
PUSH AX
    MOV DX,OFFSET STRING
    MOV AH,9
    INT 21H
POP AX
POP DX
ENDM


PRINT MACRO CHAR            ;EKTYPOSI HARAKTIRA
PUSH DX
PUSH AX
    MOV DL,CHAR
    MOV AH,2
    INT 21H
POP AX
POP DX
ENDM     


READ MACRO                  ;ANAGNOSI APO PLIKTROLOGIO
    MOV AH,8
    INT 21H
ENDM    
           

PRINT_ENTER MACRO
	PUSH DX
	PUSH AX
	PRINT 0AH
	PRINT 0DH
	POP AX
	POP DX
ENDM		   
           
           
READ_E MACRO                  ;ANAGNOSI APO PLIKTROLOGIO
    MOV AH,1
    INT 21H
ENDM    

;BAZEI STOIXIO STON PINAKA + AUKS TOU COUNTER
INS_T MACRO OFS VAL			;BAZW MESA TO ONOMA TOU PINAKA P XO DHLOSEI!!
	PUSH BX
	MOV BX,OFS
	
	MOV [<ONOMA_PINAKA>+BX], VAL
	INC OFS
	POP BX
ENDM

;EPISTREFEI TIMH TREXONTOS STOIXIOU STON VAL K AUKSANEI KATA 1 TOU COUNT
GET_T MACRO OFS VAL
	PUSH BX
	MOV BX,OFS
	
	MOV VAL, [<ONOMA_PINAKA>+BX] ;ANTIKA8ISTO ONOMA PINAKA
	INC OFS
	POP BX
ENDM

   
   
data segment
    ; add your data here!
    pkey db "press any key...$"
ends

stack segment
    dw   128  dup(0)
ends

code segment
start:
; set segment registers:
    mov ax, data
    mov ds, ax
    mov es, ax

    
    CALL READ_OCT4
    PRINT_ENTER
    MOV CX,DX
    CALL PRINT_DEC 
    PRINT_ENTER  
    MOV DX,CX
    CALL PRINT_OCT
    PRINT_ENTER 
    MOV BL,CH
    CALL PRINT_BIN
    MOV BL,CL
    CALL PRINT_BIN
     PRINT_ENTER  
    
    
    lea dx, pkey
    mov ah, 9
    int 21h        ; output string at ds:dx
    
    ; wait for any key....    
    mov ah, 1
    int 21h
    
    mov ax, 4c00h ; exit to operating system.
    int 21h    
ends  

PRINT_BIN PROC NEAR
    PUSH CX 
    PUSH DX
    PUSH AX
    MOV CX,8
    
 PBADR1: 
 
    RCL bl,1
    JC PBADR2
    PRINT '0'
    LOOP PBADR1
    JMP PBEND
    
    
 PBADR2:
    PRINT '1'
    LOOP PBADR1
 PBEND:
    
    POP AX
    POP DX
    POP CX
    
    RET
PRINT_BIN ENDP  

      
      PRINT_OCT	PROC	NEAR
	PUSH	AX
	PUSH	BX
	PUSH	CX
	
	ROL	DX,1	; t?p??? p??ta t? MSB
	MOV	BX,0
	ADC	BX,30H
	PUSH	DX
	PRINT	BL
	POP	DX
	; a???????? s??e???? �e ta ?p????pa 15-bits
	; p?????ta? ta ?? 3-ade?
	MOV	CX,5
ADDR10:
	ROL	DX,1
	ROL	DX,1
	ROL	DX,1
	MOV	BX,DX
	AND	BX,0007H	; ap?�????? ta 3 ????te?? s?�a?t??? bits
	ADD	BX,30H		; �etat??p? t?? a???�? st?? a?t?st???? ??d???
	PUSH	DX
	PRINT	BL		; ASCII ?a? t?? t?p???
	POP	DX
	LOOP	ADDR10

	POP	CX
	POP	BX
	POP	AX
	RET
PRINT_OCT	ENDP


;???t??a ??a t?? e?t?p?s? 16-bit ap??s?�?? a???�?? se de?ad??? �??f?
;??s?d??| DX: 16-bit a???�??
PRINT_DEC	PROC	NEAR
	PUSH	AX
	PUSH	BX
	PUSH	CX
	PUSH	DX
	MOV	AX,DX
	MOV	CX,0
ADDR8:
	MOV	DX,0
	MOV	BX,10	; d?a??? �e t? 10 t?? a???�?
	DIV	BX	
	INC	CX	; a????? t? �et??t? t?? ??f???
	PUSH	DX	; t? ?p????p? t? ap????e?? st? st???a
	CMP	AX,0	; e????? a? t? p????? e??a? 0
	JNE	ADDR8	; a? e??a? �?d?? t??e??sa
ADDR9:
	POP	DX
	ADD	DX,30H	; �etat??p? t?? a???�?? st?? a?t?st????
			; ASCII
	PRINT	DL
	LOOP	ADDR9	; epa???a?e t? d?ad??as?a �???? ?a 
			; te?e??s??? ?? a???�??
	POP	DX
	POP	CX
	POP	BX
	POP	AX
	RET
PRINT_DEC	ENDP
      
       
   OCT_KEYB PROC NEAR
	PUSH DX 
IGNORE_OCT_KEYB:	
	READ
	CMP AL, 'Q'
	JE OCTKEND
	CMP AL, 30H
	JL IGNORE_OCT_KEYB
	CMP AL, 37H
	JG IGNORE_OCT_KEYB
	PUSH AX
	PRINT AL
	POP AX
	SUB AL, 30H
OCTKEND: POP DX
	RET
OCT_KEYB ENDP


;???t??a ??a t?? e?s?d? de?ae?ad???? a???�?? 4-??f??? ap? t? p???t???????
;???d??| DX: de?ae?ad???? a???�?? 4-??f???
READ_OCT4	PROC	NEAR
	PUSH	CX
	MOV	DX,0
	MOV	CX,4
ROCADDR6:
	CALL	OCT_KEYB
	CMP	AL,'Q'
	JE	QUIT_READ_OCT4
	SHL	DX,1
	SHL	DX,1
	SHL	DX,1
	ADD	DL,AL
	LOOP	ROCADDR6
QUIT_READ_OCT4:
	POP	CX
	RET
READ_OCT4	ENDP
    
       
       
end start ; set entry point and stop the assembler.

