; multi-segment executable file template.


; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt


                         ;EXODOS
EXIT    MACRO

        MOV AX,4C00H
        INT 21H

ENDM


PRINT_STR MACRO STRING        ;EKTIPOSI STRING
PUSH DX
PUSH AX
    MOV DX,OFFSET STRING
    MOV AH,9
    INT 21H
POP AX
POP DX
ENDM


PRINT MACRO CHAR            ;EKTYPOSI HARAKTIRA
PUSH DX
PUSH AX
    MOV DL,CHAR
    MOV AH,2
    INT 21H
POP AX
POP DX
ENDM     


READ MACRO                  ;ANAGNOSI APO PLIKTROLOGIO
    MOV AH,8
    INT 21H
ENDM    
           

PRINT_ENTER MACRO
	PUSH DX
	PUSH AX
	PRINT 0AH
	PRINT 0DH
	POP AX
	POP DX
ENDM		   
           
           
READ_E MACRO                  ;ANAGNOSI APO PLIKTROLOGIO
    MOV AH,1
    INT 21H
ENDM    

;BAZEI STOIXIO STON PINAKA + AUKS TOU COUNTER
INS_T MACRO OFS VAL			;BAZW MESA TO ONOMA TOU PINAKA P XO DHLOSEI!!
	PUSH BX
	MOV BX,OFS
	
	MOV [<ONOMA_PINAKA>+BX], VAL
	INC OFS
	POP BX
ENDM

;EPISTREFEI TIMH TREXONTOS STOIXIOU STON VAL K AUKSANEI KATA 1 TOU COUNT
GET_T MACRO OFS VAL
	PUSH BX
	MOV BX,OFS
	
	MOV VAL, [<ONOMA_PINAKA>+BX] ;ANTIKA8ISTO ONOMA PINAKA
	INC OFS
	POP BX
ENDM



data segment
    ; add your data here! 
    pinak db 20 dup(?)  
    count db (0)
    size db (0)
    temp db (0) 
    vols db (0)
    sum db (0)
    pkey db "press any key...$"
ends

stack segment
    dw   128  dup(0)  
    
ends

code segment
start:
; set segment registers:
    mov ax, data
    mov ds, ax
    mov es, ax
     
     
    mov cx,9
    loopi: 
    call READ_LET
    CMP AL,0DH
    JE OKSO 
    MOV temp,AL
    CALL IS_VOL
    add vols,al
    mov al,temp
    CALL CHAR_VAL
    add sum,al
    mov al,temp
    mov bh,0
    mov bl,count
    mov [pinak+bx],al
    inc count 
    inc size
    loop loopi  
    
    okso:
     
    PRINT_ENTER
    MOV DH,0
    MOV DL,sum
    CALL PRINT_DEC 
    PRINT_ENTER
    MOV DH,0
    MOV DL,vols
    CALL PRINT_DEC
    PRINT_ENTER  
    
    mov ch,0
    mov cl,size
    mov count,0
    
    lop:
    mov bh,0
    mov bl,count
    mov al,[pinak+bx]
    call CAP_TO_LOW
    PRINT AL
    inc count
    loop lop
    
    PRINT_ENTER 
    
    
     mov ch,0
    mov cl,size
    mov count,0
    
    lopi:
    mov bh,0
    mov bl,count
    mov al,[pinak+bx]
    call LOW_TO_CAP
    PRINT AL
    inc count
    loop lopi
    
    PRINT_ENTER
    
    
    
    
    
     
    
    
    
            
 
    lea dx, pkey
    mov ah, 9
    int 21h        ; output string at ds:dx
    
    ; wait for any key....    
    mov ah, 1
    int 21h
    
    mov ax, 4c00h ; exit to operating system.
    int 21h    
ends     

    PRINT_DEC	PROC	NEAR
	PUSH	AX
	PUSH	BX
	PUSH	CX
	PUSH	DX
	MOV	AX,DX
	MOV	CX,0
ADDR8:
	MOV	DX,0
	MOV	BX,10	; d?a??? �e t? 10 t?? a???�?
	DIV	BX	
	INC	CX	; a????? t? �et??t? t?? ??f???
	PUSH	DX	; t? ?p????p? t? ap????e?? st? st???a
	CMP	AX,0	; e????? a? t? p????? e??a? 0
	JNE	ADDR8	; a? e??a? �?d?? t??e??sa
ADDR9:
	POP	DX
	ADD	DX,30H	; �etat??p? t?? a???�?? st?? a?t?st????
			; ASCII
	PRINT	DL
	LOOP	ADDR9	; epa???a?e t? d?ad??as?a �???? ?a 
			; te?e??s??? ?? a???�??
	POP	DX
	POP	CX
	POP	BX
	POP	AX
	RET
PRINT_DEC	ENDP


;gurnaw ston al mono gramma
READ_LET  PROC	NEAR
	push dx
	rlign:
	READ  
	cmp al,0dh
	je rlret
	cmp al,41h
	jl rlign
	cmp al,54h
	jg rlmaybecap
	jmp rlret
rlmaybecap:
	cmp al,61h
	jl rlign
	cmp al,74h
	jg rlign
	
rlret:	push ax
	PRINT al
	pop ax
	pop dx
	ret 

    
	
READ_LET ENDP

;ama o al einai fonien al=1
IS_VOL PROC NEAR
    cmp al,41h
	jz isvolyea
	cmp al,45h
	jz isvolyea
	cmp al,49h
	jz isvolyea
	cmp al,4fh
	jz isvolyea
	cmp al,55h
	jz isvolyea
	cmp al,61h
	jz isvolyea
	cmp al,65h
	jz isvolyea
	cmp al,69h
	jz isvolyea
	cmp al,6fh
	jz isvolyea
	cmp al,75h
	jz isvolyea
	mov al,0
	ret
isvolyea:
	mov al,1
	ret
IS_VOL ENDP

;gurnaei al=char val
CHAR_VAL PROC NEAR
	cmp al,54h
	jg cvlow
	sub al,40h
	ret
cvlow:
	sub al,60h
	ret
CHAR_VAL ENDP

;gurnaei ton antistoixo low ston al
CAP_TO_LOW PROC NEAR
	cmp al,54h
	jg ctlend
	add al,20h
ctlend:
	ret
CAP_TO_LOW ENDP

;gurnaei ton antistoixo cap ston al
LOW_TO_CAP PROC NEAR
    cmp al,61h
	jl ltcend
	sub al,20h
ltcend:
	ret
LOW_TO_CAP ENDP
	
	
	

end start ; set entry point and stop the assembler.
