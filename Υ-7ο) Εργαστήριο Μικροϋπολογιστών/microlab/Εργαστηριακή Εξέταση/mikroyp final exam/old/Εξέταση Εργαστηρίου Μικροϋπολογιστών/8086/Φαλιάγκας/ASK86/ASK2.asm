; multi-segment executable file template.

; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt


                         ;EXODOS
EXIT    MACRO

        MOV AX,4C00H
        INT 21H

ENDM


PRINT_STR MACRO STRING        ;EKTIPOSI STRING
PUSH DX
PUSH AX
    MOV DX,OFFSET STRING
    MOV AH,9
    INT 21H
POP AX
POP DX
ENDM


PRINT MACRO CHAR            ;EKTYPOSI HARAKTIRA
PUSH DX
PUSH AX
    MOV DL,CHAR
    MOV AH,2
    INT 21H
POP AX
POP DX
ENDM     


READ MACRO                  ;ANAGNOSI APO PLIKTROLOGIO
    MOV AH,8
    INT 21H
ENDM    
           

PRINT_ENTER MACRO
	PUSH DX
	PUSH AX
	PRINT 0AH
	PRINT 0DH
	POP AX
	POP DX
ENDM		   
           
           
READ_E MACRO                  ;ANAGNOSI APO PLIKTROLOGIO
    MOV AH,1
    INT 21H
ENDM    

;BAZEI STOIXIO STON PINAKA + AUKS TOU COUNTER
INS_T MACRO OFS VAL			;BAZW MESA TO ONOMA TOU PINAKA P XO DHLOSEI!!
	PUSH BX
	MOV BX,OFS
	
	MOV [<ONOMA_PINAKA>+BX], VAL
	INC OFS
	POP BX
ENDM

;EPISTREFEI TIMH TREXONTOS STOIXIOU STON VAL K AUKSANEI KATA 1 TOU COUNT
GET_T MACRO OFS VAL
	PUSH BX
	MOV BX,OFS
	
	MOV VAL, [<ONOMA_PINAKA>+BX] ;ANTIKA8ISTO ONOMA PINAKA
	INC OFS
	POP BX
ENDM

data segment
    ; add your data here!
    pkey db "press any key...$" 
    A8R DB "SUM=$"
    EV DB "EVEN=$"
    SUM DB (0)
    ART DB (0)
ends

stack segment
    dw   128  dup(0)
ends

code segment
start:
; set segment registers:
    mov ax, data
    mov ds, ax
    mov es, ax

    MOV CX,9
    
    LOOPI:  
    CALL DEC_KEYB
    CMP AL,0DH
    JZ OKSO
    ADD SUM,AL
    CALL IS_EVEN
    ADD ART,AL  
    
    LOOP LOOPI
OKSO:    
    PRINT_ENTER
    PRINT_STR A8R
    MOV DH,0
    MOV DL,SUM
    CALL PRINT_DEC
    PRINT_ENTER  
    PRINT_STR EV
    MOV DH,0
    MOV DL,ART
    CALL PRINT_DEC
    PRINT_ENTER 
    
    
    
    
    
            
    lea dx, pkey
    mov ah, 9
    int 21h        ; output string at ds:dx
    
    ; wait for any key....    
    mov ah, 1
    int 21h
    
    mov ax, 4c00h ; exit to operating system.
    int 21h    
ends  

PRINT_DEC	PROC	NEAR
	PUSH	AX
	PUSH	BX
	PUSH	CX
	PUSH	DX
	MOV	AX,DX
	MOV	CX,0
ADDR8:
	MOV	DX,0
	MOV	BX,10	; d?a??? �e t? 10 t?? a???�?
	DIV	BX	
	INC	CX	; a????? t? �et??t? t?? ??f???
	PUSH	DX	; t? ?p????p? t? ap????e?? st? st???a
	CMP	AX,0	; e????? a? t? p????? e??a? 0
	JNE	ADDR8	; a? e??a? �?d?? t??e??sa
ADDR9:
	POP	DX
	ADD	DX,30H	; �etat??p? t?? a???�?? st?? a?t?st????
			; ASCII
	PRINT	DL
	LOOP	ADDR9	; epa???a?e t? d?ad??as?a �???? ?a 
			; te?e??s??? ?? a???�??
	POP	DX
	POP	CX
	POP	BX
	POP	AX
	RET
PRINT_DEC	ENDP
         
DEC_KEYB PROC NEAR
	PUSH DX 
IGNORE_DEC_KEYB:	
	READ
	CMP AL, 0DH
	JE ADDR16
	CMP AL, 30H
	JL IGNORE_DEC_KEYB
	CMP AL, 39H
	JG IGNORE_DEC_KEYB
	PUSH AX
	PRINT AL
	POP AX
	SUB AL, 30H
ADDR16: POP DX
	RET
DEC_KEYB ENDP
  
         
IS_EVEN PROC NEAR
ROR AL,1
JC ISEEND
MOV AL,1
RET
ISEEND:
MOV AL,0
RET
IS_EVEN ENDP
         
         
end start ; set entry point and stop the assembler.


