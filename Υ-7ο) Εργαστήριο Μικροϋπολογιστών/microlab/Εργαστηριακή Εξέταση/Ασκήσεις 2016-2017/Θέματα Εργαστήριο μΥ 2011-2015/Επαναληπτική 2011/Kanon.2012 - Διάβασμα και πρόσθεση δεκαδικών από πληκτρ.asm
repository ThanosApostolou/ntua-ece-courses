/*
 * Kanon.asm
 *
 *  Created: 21/2/2013 7:38:18 ??
 *   Author: ????? ??????????
 */ 

.include "m16def.inc" 
.def x1=r19
.def y1=r20

.DSEG 
_tmp_:.byte 2
.CSEG

reset: 
	ldi r24,low(RAMEND) 	; initialize stack pointer
	out SPL,r24
	ldi r24,high(RAMEND)
	out SPH,r24

;initialize I/O:
	ldi r18,(1<<PC7)|(1<<PC6)|(1<<PC5)|(1<<PC4)
	out DDRC,r18	; initialize PORTC for keyboard-input

rcall lcd_init


read1:
	ldi r24,15	
	rcall scan_keypad_rising_edge
	rcall keypad_to_ascii
	cpi r24,0	;CHECK IF PRESSED
	breq read1
	cpi r24,'0'
	brlt read1
	cpi r24,'8'
	brge read1
	mov x1,r24

read2:
	ldi r24,15	
	rcall scan_keypad_rising_edge
	rcall keypad_to_ascii
	cpi r24,0	;CHECK IF PRESSED
	breq read2
	cpi r24,'0'
	brlt read2
	cpi r24,'8'
	brge read2
	mov y1,r24

sum:
	subi x1,0x30	;AFAIRW 30H PAIRNW THN TIMH APO TON ASCII
	subi y1,0x30
	add x1,y1

	cpi x1,0x0A
	brge ten
	jmp units

ten:
	ldi r24,'1'
	rcall lcd_data
	subi x1,0x0A

units:
	mov r24,x1
	ldi r30,0x30
	add r24,r30		;PROS8ETW PALI TO 30H GIA NA TO GYRISW SE ASCII GIA PRINT STHN LCD
	rcall lcd_data



;FUNCTIONS USED
scan_row:
ldi r25 ,0x08		; initialize using 00001000
back_: 
lsl r25			    ; shift left r24 times 
dec r24
brne back_
out PORTC ,r25
nop
nop
in r24 ,PINC
andi r24 ,0x0f
ret

scan_keypad:
ldi r24 ,0x01
rcall scan_row        
swap r24
mov r27 ,r24
ldi r24 ,0x02
rcall scan_row
add r27 ,r24
ldi r24 ,0x03
rcall scan_row
swap r24
mov r26 ,r24
ldi r24 ,0x04
rcall scan_row
add r26 ,r24
movw r24 ,r26
ret

scan_keypad_rising_edge:
mov r22 ,r24 
rcall scan_keypad
push r24
push r25
mov r24 ,r22
ldi r25 ,0
rcall wait_msec
rcall scan_keypad
pop r23
pop r22 
and r24 ,r22            
and r25 ,r23
ldi r26 ,low(_tmp_)
ldi r27 ,high(_tmp_)
ld r23 ,X+                
ld r22 ,X
st X ,r24 
st -X ,r25             
com r23                   
com r22                  
and r24 ,r22           
and r25 ,r23
ret

keypad_to_ascii:
movw r26 ,r24 
ldi r24 ,'*'
sbrc r26 ,0
ret
ldi r24 ,'0'
sbrc r26 ,1
ret
ldi r24 ,'#'
sbrc r26 ,2
ret
ldi r24 ,'D' 
sbrc r26 ,3 
ret
ldi r24 ,'7'
sbrc r26 ,4
ret
ldi r24 ,'8'
sbrc r26 ,5
ret
ldi r24 ,'9'
sbrc r26 ,6
ret
ldi r24 ,'C'
sbrc r26 ,7
ret
ldi r24 ,'4'
sbrc r27 ,0 
ret
ldi r24 ,'5'
sbrc r27 ,1
ret
ldi r24 ,'6'
sbrc r27 ,2
ret
ldi r24 ,'B'
sbrc r27 ,3
ret
ldi r24 ,'1'
sbrc r27 ,4
ret
ldi r24 ,'2'
sbrc r27 ,5
ret
ldi r24 ,'3'
sbrc r27 ,6
ret
ldi r24 ,'A'
sbrc r27 ,7
ret
clr r24
ret

wait_usec:   
sbiw r24 ,1  
nop
nop
nop
nop
brne wait_usec
ret

wait_msec:
push r24
push r25
ldi r24 , low(998)     
ldi r25 , high(998)     
rcall wait_usec       
pop r25
pop r24
sbiw r24 , 1
brne wait_msec
ret

lcd_init:
ldi r24 ,40
ldi r25 ,0
rcall wait_msec
ldi r24 ,0x30
out PORTD ,r24
sbi PORTD ,PD3
cbi PORTD ,PD3
ldi r24 ,39
ldi r25 ,0
rcall wait_usec
ldi r24 ,0x30   
out PORTD ,r24
sbi PORTD ,PD3
cbi PORTD ,PD3
ldi r24 ,39
ldi r25 ,0
rcall wait_usec 
ldi r24 ,0x20
out PORTD ,r24
sbi PORTD ,PD3
cbi PORTD ,PD3
ldi r24 ,39
ldi r25 ,0
rcall wait_usec 
ldi r24 ,0x28
rcall lcd_command
ldi r24 ,0x0c
rcall lcd_command 
ldi r24 ,0x01
rcall lcd_command
ldi r24 ,low(1530)
ldi r25 ,high(1530)
rcall wait_usec
ldi r24 ,0x06 
rcall lcd_command  
ret

lcd_data:
sbi PORTD ,PD2
rcall write_2_nibbles
ldi r24 ,43
ldi r25 ,0
rcall wait_usec
ret

lcd_command:
cbi PORTD ,PD2
rcall write_2_nibbles
ldi r24 ,39
ldi r25 ,0
rcall wait_usec
ret

write_2_nibbles:
push r24
in r25 ,PIND
andi r25 ,0x0f
andi r24 ,0xf0
add r24 ,r25
out PORTD ,r24
sbi PORTD ,PD3  
cbi PORTD ,PD3
pop r24
swap r24
andi r24 ,0xf0 
add r24 ,r25
out PORTD ,r24
sbi PORTD ,PD3
cbi PORTD ,PD3
ret