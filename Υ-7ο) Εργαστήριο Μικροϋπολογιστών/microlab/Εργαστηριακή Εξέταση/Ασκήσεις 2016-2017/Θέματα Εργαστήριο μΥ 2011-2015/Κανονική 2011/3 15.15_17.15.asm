/*
 * project.asm
 *
 *  Created: 22/9/2015 12:24:41 ??
 *   Author: George
 */ 
.include "m16def.inc"
.org 0x00
.def reg= r16
.def temp= r17
.def phase= r18
.def counter =r19
    ldi reg,LOW(RAMEND)
	out SPL,reg
	ldi reg,HIGH(RAMEND)
	out SPH,reg
	clr reg
	out DDRA,reg
	ser reg
	out DDRD,reg

	in reg,PINA
	andi reg,0b01000000
	sbrc reg,6
	ldi phase,1
	ldi phase,0
start:
	in reg,PINA
	mov temp,reg
	andi temp,0b10000000
	cpi temp,0
	breq counter1
	ldi counter,1
	out PORTD,counter
	rjmp start
counter1:
	mov temp,reg
	andi temp,0b01000000
	cpi temp,0
	brne adding
	ldi phase,0
	rjmp start
adding:
	cpi counter,0
	breq start
	cpi phase,0
	brne start
	ldi phase,1
	inc counter
	out PORTD,counter
	rjmp start




	

	

	


