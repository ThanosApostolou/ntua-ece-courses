/*
 * project.asm
 *
 *  Created: 22/9/2015 12:24:41 ??
 *   Author: George
 */ 
.include "m16def.inc"
.org 0x00
.def reg= r16
.def temp= r17
.def checker =r18
.def result =r19
    ldi reg,LOW(RAMEND)
	out SPL,reg
	ldi reg,HIGH(RAMEND)
	out SPH,reg
	clr reg
	out DDRA,reg
	ser reg
	out DDRD,reg

start:
	ldi result,0
	ldi checker,0b10000000
	in reg,PINA
next_dig:
	mov temp,reg
	and temp,checker
	cpi temp,0
	brne found_it
	lsr checker
	cpi checker,0
	breq output
	rjmp next_dig

found_it:
	or result,checker
	lsl checker
	cpi checker,0
	brne found_it

output:
	out PORTD,result
	rjmp start







	

	

	


