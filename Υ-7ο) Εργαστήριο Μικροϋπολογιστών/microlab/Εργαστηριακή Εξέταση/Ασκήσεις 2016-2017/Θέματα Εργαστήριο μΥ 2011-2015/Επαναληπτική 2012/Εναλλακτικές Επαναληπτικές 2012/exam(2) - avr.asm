.include "m16def.inc"
.def input=r18
.def temp=r26
.def position=r27
.def led=r28

main:
    ldi temp, low(RAMEND)	; initialize stack pointer
    out SPL, temp
    ldi temp, high(RAMEND)
    out SPH, temp

	ser temp				; ������������ ������
	out DDRC, temp 
	clr temp				; ������������ �������
	out DDRA, temp 

start:
	in input, PINA			; �������
	ldi temp, 0x80
	ldi position, 8			; � ���� ��� ������ 1, �������������� ��� 8
loop:
	cp input, temp			; ������� �� ���� ���� ��������� �� ����� 1
	brsh next
	lsr temp
	dec position			; ������ ��� ����� ��� ������ 1
	cpi temp, 0
	brne loop
next:
	ldi led, 0				; ������������ ������ ��� 0
	ldi temp, 9				; ������� ��� �� ������� loop
	cpi position, 0			; �� � ���� ����� 0, ���� ��� ������� ������ 1
	breq printout
setout:
	dec temp
	cp temp, position		; �� ������� ��� ���� ��� ����� �� 1, ���������
	breq printout
	lsr led
	ori led, 0x80			; �������� ���� ����� 1 ���� �����
	rjmp setout
printout:
	out PORTC, led			; ������
	rjmp start
