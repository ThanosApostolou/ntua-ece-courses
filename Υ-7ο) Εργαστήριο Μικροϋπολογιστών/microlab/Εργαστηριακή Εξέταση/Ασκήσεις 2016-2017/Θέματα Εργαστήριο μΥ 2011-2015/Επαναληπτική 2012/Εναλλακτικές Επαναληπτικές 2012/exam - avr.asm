.include "m16def.inc"
.def temp=r26
.def led=r28
.def input=r18

main:
    ldi temp , low(RAMEND)	; initialize stack pointer
    out SPL , temp
    ldi temp , high(RAMEND)
    out SPH , temp

	ser temp				; ������������ ������
	out DDRC,temp 
	clr temp				; ������������ �������
	out DDRA,temp 

start:
	in input, PINA			; ������� ���� PORTA
	cpi input,0x80			; ������� �� ���� ������� �� 8� bit
	brlo sw7
	rjmp none
sw7:
	cpi input,0x40			; ������� �� ���� ������� �� 7� bit
	brlo sw6
	ldi led,0b10000000
	rjmp printout
sw6:
	cpi input,0x20			; ������� �� ���� ������� �� 6� bit
	brlo sw5
	ldi led,0b11000000
	rjmp printout
sw5:
	cpi input,0x10			; ������� �� ���� ������� �� 5� bit
	brlo sw4
	ldi led,0b11100000
	rjmp printout
sw4:
	cpi input,0x08			; ������� �� ���� ������� �� 4� bit
	brlo sw3
	ldi led,0b11110000
	rjmp printout
sw3:
	cpi input,0x04			; ������� �� ���� ������� �� 3� bit
	brlo sw2
	ldi led,0b11111000
	rjmp printout
sw2:
	cpi input,0x02			; ������� �� ���� ������� �� 2� bit
	brlo sw1
	ldi led,0b11111100
	rjmp printout
sw1:
	cpi input,0x01			; ������� �� ���� ������� �� 1� bit
	brlo none
	ldi led,0b11111110
	rjmp printout
none:
	ldi led,0b00000000
printout:
	out PORTC, led			; ������ ���� PORTC
	rjmp start



