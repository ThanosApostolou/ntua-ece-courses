#include <stdbool.h>
#include <avr/io.h>
 
// provide _delay_ms and _delay_us functions
#define F_CPU 16000000UL
#include <util/delay.h>
 
 
unsigned char temp, sign;
 
// return true if device is detected
bool one_wire_reset () {
    unsigned char temp;
         
    DDRA = DDRA | (1<<PA4);
    PORTA = PORTA & ~(1<<PA4);
    _delay_us(480);
    DDRA = DDRA & ~(1<<PA4);
    PORTA = PORTA & ~(1<<PA4);
    _delay_us(100);
    temp = PINA;
    _delay_us(380);
    if (temp & (1<<PA4)) {
        return false;
    } else {
        return true;
    }
}

unsigned char one_wire_receive_bit () {
	unsigned char a_bit;
	
	DDRA = DDRA | (1<<PA4);
    PORTA = PORTA & ~(1<<PA4);
	_delay_us(2);
	DDRA = DDRA & ~(1<<PA4);
    PORTA = PORTA & ~(1<<PA4);
	_delay_us(10);
	if (!(PINA & (1<<PA4))) {
        a_bit = 0;
    } else {
        a_bit = 1;
    }
	_delay_us(49);
	return a_bit;
}

unsigned char one_wire_receive_byte () {
	unsigned char a_bit, a_byte=0;

	for (int i=0; i<8; i++) {
		a_bit = one_wire_receive_bit();
		a_byte = a_byte >> 1;
		if (a_bit != 0) {
			a_bit = 0x80;
		}
		a_byte = a_byte | a_bit;
	}
	return a_byte;
}

int main () {
    if (one_wire_reset) {
    }
    return 0;
}
