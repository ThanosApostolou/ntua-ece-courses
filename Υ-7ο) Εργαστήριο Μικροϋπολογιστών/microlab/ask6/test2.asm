.include "m16def.inc"


.def temp = r16
.def sign = r17
//.def ones = r18
//.def tens = r19
//.def hundreds = r20
.def flag = r21
.def aux = r22
.def conv = r18
.def fisrt = r19
.def second = r20

.CSEG

.org	0x00
rjmp	reset

reset:

	ldi r24 ,(1 << PC7) | (1 << PC6) | (1 << PC5) | (1 << PC4) 
	out DDRC,r24 
	/* INITIALIZE STACK */
	ldi r24,high(RAMEND)
	out SPH,r24
	ldi r24,low(RAMEND)
	out SPL,r24

	/* SET I/O */
	ldi r24,(1 << PD7) | (1 << PD6) | (1 << PD5) | (1 << PD4) | (1 << PD3) | (1 << PD2)
    out DDRD,r24

	/* INITIALAIZE LCD */
	rcall lcd_init

main:
	
	ldi r24,low(500)
	ldi r25,high(500)
	rcall wait_msec

	/* INITIALIZE VARIABLES */
	clr hundreds
	clr tens
	clr ones
	clr flag
	
	read_1st: ;read 1st hex digit
	ldi r24,0x14
	rcall scan_keypad_rising_edge
	mov temp,r24
	add temp,r25
	cpi temp,0
	breq sensor
	rcall keypad_to_hex
	mov r22,r24
	ldi r24 ,0x01        ;clean lcd screen      
	rcall lcd_command  
	ldi r24 ,low(1530)  
	ldi r25 ,high(1530)  
	rcall wait_usec 
	mov r24,r22
	mov first,r24
	rcall lcd_data
	
	read_2nd: ;read 2nd hex digit
	ldi r24,0x14
	rcall scan_keypad_rising_edge
	mov temp,r24
	add temp,r25
	cpi temp,0
	breq read_2nd
	rcall keypad_to_hex
	mov second,r24
	rcall lcd_data
	
	ldi r24,'='
	rcall lcd_data
	
	subi first,0x30
	cpi first,0x0A
	brlo conv_from_ascii_fisrt
	rjmp fisrt_to_dec

	fisrt_to_dec:
	subi first,0x07

	conv_from_ascii_fisrt: ; convert first digit from ascii to number
	mov conv,first
	lsl conv
	lsl conv
	lsl conv
	lsl conv
	subi second,0x30
	cpi second,0x0A
	brlo conv_from_ascii_sec
	rjmp second_to_dec

	second_to_dec:
	subi second,0x07

	conv_from_ascii_sec: ;convert second digit from ascii to number
	add conv,second
	mov r24,conv
	ldi r23,0x30
	mov temp, conv
	cpi
	cpi temp,128 ;elegxos prosimou
	brlo posative
	rjmp negadive
	
	sensor:
	rcall get_temperature
	mov temp,r24
	mov sign,r25

	rcall clear_display

	cpi sign,0x80
	breq no_device
	
	cpi sign,0x00
	breq positive
	

	negative:
	cpi conv,0xff93
	brlo cont_neg
	rjmp no_device
	cont_neg:
	ldi r24,'-'
	rcall lcd_data
	neg conv ;symplirwma ws pros 2
	ldi temp, 0x00
	rjmp hund
	
	positive:
	cpi conv,0x7e
	brlo cont_pos
	rjmp no_device
	cont_pos:
	ldi r24,'+'
	rcall lcd_data
	ldi temp,0x00
	rjmp hund

	;convert hex number to dec number and display it

	hund:
	cpi conv,100
	brlo deca
	subi conv,100
	inc temp
	jmp hund

	deca:
	cpi temp,0
	breq if_less_hund
	mov r24,temp

	add r24,r23
	rcall lcd_data
	cpi conv, 10
	brlo no_mon
	ldi temp,0x00
	jmp deca

	if_less_hund:
	cpi conv,10
	brlo mon
	subi conv,10
	inc temp
	jmp if_less_hund

	mon:
	cpi temp,0
	breq end
	mov r24,temp
	;out  PORTB,r24
	add r24,r23
	rcall lcd_data
	mov r24,conv
	out PORTA, r24
	add r24,r23
	rcall lcd_data
	rjmp read_1st

	no_mon:
	ldi r24,0x00
	add r24,r23
	rcall lcd_data

	end:
	mov r24,conv
	;out PORTA, r24
	add r24,r23
	rcall lcd_data
	;rjmp read_1st
	
////////////////////////////////////////////
	_hundreds:
		
		/* NO NEED TO LOOP HUNDREDS (+/- 127 MAX) */
		cpi temp,100
		brlo _tens
		inc hundreds
		ldi flag,0x80
		subi temp,100


	_tens: 
		/* COUNT TENS */
		cpi temp,10
		brlo _ones
		inc tens
		inc flag
		subi temp,10
		rjmp _tens

	_ones:
		/* COUNT ONES */
		mov ones,temp

	/* DISPLAY hundreds, tens, ones THROUGH r24 */
	/* BY ADDING 0x30 (ASCII CODE) */
	
	/* DISPLAY + OR - */
		cpi sign,0x00
		brne print_minus

		ldi r24,'+'
		rcall lcd_data

		rjmp continue

print_minus:
		ldi r24,'-'
		rcall lcd_data

	
continue:
	sbrc flag,7
	rjmp print_hundreds
	cpi flag,0x00
	breq print_ones
	rjmp print_tens

	print_hundreds:
		subi hundreds,-0x30    
		mov r24,hundreds
		rcall lcd_data

	print_tens:
		subi tens,-0x30
		mov r24,tens
		rcall lcd_data

	print_ones:
		subi ones,-0x30
		mov r24,ones
		rcall lcd_data
///////////////////////////////////////
	/* Check for .5 */
		sbrs aux,0
		rjmp celsius
		ldi r24,'.'
		rcall lcd_data
		ldi r24,'5'
		rcall lcd_data

	/* Celsius */
	celsius:
		ldi r24,0xB2
		rcall lcd_data
		ldi r24,'C'
		rcall lcd_data

rjmp main	

no_device:
	ldi r24,'N'
	rcall lcd_data
	ldi r24,'o'
	rcall lcd_data
	ldi r24,' '
	rcall lcd_data
	ldi r24,'D'
	rcall lcd_data
	ldi r24,'e'
	rcall lcd_data
	ldi r24,'v'
	rcall lcd_data
	ldi r24,'i'
	rcall lcd_data
	ldi r24,'c'
	rcall lcd_data
	ldi r24,'e'
	rcall lcd_data

rjmp main



/* ROUTINE SEGMENT */

get_temperature:
	rcall one_wire_reset
	/* Check if a sensor is connected */
	cpi r24,0;
	breq nothing_connected
	/* Transmit 0xCC to address all devices on bus */
	ldi r24,0xCC
	rcall one_wire_transmit_byte
	/* Transmit 0x44 to convert a single temp */
	ldi r24,0x44
	rcall one_wire_transmit_byte
	/* Wait until sensor ready state */
	wait_till_ready:
		rcall one_wire_receive_bit
		sbrs r24,0
		rjmp wait_till_ready
	/* Reset and read data from sensor */
	rcall one_wire_reset
	/* Transmit 0xCC to address all devices on bus */
	ldi r24,0xCC
	rcall one_wire_transmit_byte
	/* Transmit 0xBE to read from scratchpad */
	ldi r24,0xBE
	rcall one_wire_transmit_byte
	/* Read temperature */
	rcall one_wire_receive_byte
	mov temp,r24
	/* Read sign */
	rcall one_wire_receive_byte
	mov sign,r24
	/* Check is temp is positive */
	cpi sign,0x00
	breq positive
	/* Convert temp if negative (2's complement)*/
	com temp
	inc temp
positive:
	/* Divide by 2 and complement*/
	lsr temp
	in aux,SREG
	/* Return to r24:r25*/
	mov r24,temp
	mov r25,sign
	/* Print temperature 
	print:
	ser temp
	out DDRA,temp
	out PORTA,r24
	*/
ret
nothing_connected:
	/* Return disconnected code */
	ldi r25,0x80
	/* Print disconnected code
	 *ser r24
	 *out DDRA,r24
	 *out PORTA,r25
	 */
	clr r24
ret

one_wire_reset:
sbi DDRA ,PA4 ; PA4 configured for output
cbi PORTA ,PA4 ; 480 ?sec reset pulse
ldi r24 ,low(480)
ldi r25 ,high(480)
rcall wait_usec
cbi DDRA ,PA4
cbi PORTA ,PA4
ldi r24 ,100 ; wait 100 ?sec for devices
ldi r25 ,0 ; to transmit the presence pulse
rcall wait_usec
in r24 ,PINA ; sample the line
push r24
ldi r24 ,low(380) ; wait for 380 ?sec
ldi r25 ,high(380)
rcall wait_usec
pop r25 ; return 0 if no device was
clr r24 ; detected or 1 else
sbrs r25 ,PA4
ldi r24 ,0x01
ret
one_wire_receive_byte:
ldi r27 ,8
clr r26
loop_:
rcall one_wire_receive_bit
lsr r26
sbrc r24 ,0
ldi r24 ,0x80
or r26 ,r24
dec r27
brne loop_
mov r24 ,r26
ret
one_wire_transmit_byte:
mov r26 ,r24
ldi r27 ,8
_one_more_:
clr r24
sbrc r26 ,0
ldi r24 ,0x01
rcall one_wire_transmit_bit
lsr r26
dec r27
brne _one_more_
ret

one_wire_receive_bit:

sbi DDRA ,PA4
cbi PORTA ,PA4 ; generate time slot
ldi r24 ,0x02
ldi r25 ,0x00
rcall wait_usec
cbi DDRA ,PA4 ; release the line
cbi PORTA ,PA4
ldi r24 ,10 ; wait 10 ?s
ldi r25 ,0
rcall wait_usec
clr r24 ; sample the line
sbic PINA ,PA4
ldi r24 ,1
push r24
ldi r24 ,49 ; delay 49 ?s to meet the standards
ldi r25 ,0 ; for a minimum of 60 ?sec time slot
rcall wait_usec ; and a minimum of 1 ?sec recovery time
pop r24
ret

one_wire_transmit_bit:
push r24 ; save r24
sbi DDRA ,PA4
cbi PORTA ,PA4 ; generate time slot
ldi r24 ,0x02
ldi r25 ,0x00
rcall wait_usec
pop r24 ; output bit
sbrc r24 ,0
sbi PORTA ,PA4
sbrs r24 ,0
cbi PORTA ,PA4
ldi r24 ,58 ; wait 58 ?sec for the
ldi r25 ,0 ; device to sample the line
rcall wait_usec
cbi DDRA ,PA4 ; recovery time
cbi PORTA ,PA4
ldi r24 ,0x01
ldi r25 ,0x00
rcall wait_usec
ret

wait_usec:
	sbiw r24 ,1 
	nop 
	nop 
	nop 
	nop
	brne wait_usec 
ret 

wait_msec:
	push r24
	push r25 
	ldi r24 , low(998) 
	ldi r25 , high(998) 
	rcall wait_usec 
	pop r25 
	pop r24 
	sbiw r24 , 1
	brne wait_msec 
 ret

 scan_row:
 ldi r25,0x08
 back:
 lsl r25
 dec r24
 brne back
 out PORTC,r25
 nop
 nop
 in r24,PINC
 andi r24,0x0f
 ret

 scan_keypad:
 ldi r24,0x01
 rcall scan_row
 swap r24
 mov r27,r24
 ldi r24,0x02
 rcall scan_row
 add r27,r24
 ldi r24,0x03
 rcall scan_row
 swap r24
 mov r26,r24
 ldi r24, 0x04
 rcall scan_row
 add r26,r24
 movw r24,r26
 ret

 scan_keypad_rising_edge:
 mov r22,r24
 rcall scan_keypad
 push r24
 push r25
 mov r24,r22
 ldi r25,0
 rcall wait_msec
 rcall scan_keypad
 pop r23
 pop r22
 and r24,r22
 and r25,r23
 ldi r26,low(_tmp_)
 ldi r27,high(_tmp_)
 ld r23,X+
 ld r22,X
 st X,r24
 st -X,r25
 com r23
 com r22
 and r24,r22
 and r25,r23
 ret

 keypad_to_hex:
 movw r26,r24
 ldi r24,'E'
 sbrc r26,0
 ret
 ldi r24,'0'
 sbrc r26,1
 ret
 ldi r24,'F'
 sbrc r26,2
 ret
 ldi r24,'D'
 sbrc r26,3
 ret
 ldi r24,'7'
 sbrc r26,4
 ret
 ldi r24,'8'
 sbrc r26,5
 ret
 ldi r24,'9'
 sbrc r26,6
 ret
 ldi r24,'C'
 sbrc r26,7
 ret
 ldi r24,'4'
 sbrc r27,0
 ret
 ldi r24,'5'
 sbrc r27,1
 ret
 ldi r24,'6'
 sbrc r27,2
 ret
 ldi r24,'B'
 sbrc r27,3
 ret
 ldi r24,'1'
 sbrc r27,4
 ret
 ldi r24,'2'
 sbrc r27,5
 ret
 ldi r24,'3'
 sbrc r27,6
 ret
 ldi r24,'A'
 sbrc r27,7
 ret 
 clr r24
 ret
 
write_2_nibbles:
	push r24 
	in r25 ,PIND 
	andi r25 ,0x0f 
	andi r24 ,0xf0 
	add r24 ,r25 
	out PORTD ,r24 
	sbi PORTD ,PD3 
	cbi PORTD ,PD3 
	pop r24 
	swap r24 
	andi r24 ,0xf0 
	add r24 ,r25
	out PORTD ,r24
	sbi PORTD ,PD3 
	cbi PORTD ,PD3
ret

lcd_data:
	sbi PORTD ,PD2 
	rcall write_2_nibbles 
	ldi r24 ,43 
	ldi r25 ,0 
	rcall wait_usec
ret

lcd_command:
	cbi PORTD ,PD2 
	rcall write_2_nibbles 
	ldi r24 ,39 
	ldi r25 ,0 
	rcall wait_usec 
ret

lcd_init:
	ldi r24 ,40 
	ldi r25 ,0 
	rcall wait_msec 
	ldi r24 ,0x30 
	out PORTD ,r24 
	sbi PORTD ,PD3 
	cbi PORTD ,PD3 
	ldi r24 ,39
	ldi r25 ,0 
	rcall wait_usec 
	ldi r24 ,0x30
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x20 
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x28 
	rcall lcd_command 
	ldi r24 ,0x0c
	rcall lcd_command
	ldi r24 ,0x01 
	rcall lcd_command
	ldi r24 ,low(1530)
	ldi r25 ,high(1530)
	rcall wait_usec
	ldi r24 ,0x06 
	rcall lcd_command 
		
ret

clear_display:
	ldi r24,0x01
	rcall lcd_command
	ldi r24,low(1530)
	ldi r25,high(1530)
	rcall wait_usec
ret

