#include <avr/io.h>
  
// provide _delay_ms and _delay_us functions
#define F_CPU 8000000UL
#include <util/delay.h>
  
// return 1 if device is detected else 0
unsigned char one_wire_reset () {
    unsigned char temp;
          
    DDRA = DDRA | (1<<PA4);    // sbi DDRA,PA4
    PORTA = PORTA & ~(1<<PA4); // cbi PORTA,PA4
    _delay_us(480);
    DDRA = DDRA & ~(1<<PA4);   // cbi DDRA,PA4
    PORTA = PORTA & ~(1<<PA4); // cbi PORTA,PA4
    _delay_us(100);
    temp = PINA;
    _delay_us(380);
    if (temp & (1<<PA4)) {
        return 0;
    } else {
        return 1;
    }
}

// return 1 if PA4 is 1 else 0
unsigned char one_wire_receive_bit () {
    unsigned char a_bit;
     
    DDRA = DDRA | (1<<PA4);    // sbi DDRA,PA4
    PORTA = PORTA & ~(1<<PA4); // cbi PORTA,PA4
    _delay_us(2);
    DDRA = DDRA & ~(1<<PA4);   // cbi DDRA,PA4
    PORTA = PORTA & ~(1<<PA4); // cbi PORTA,PA4
    _delay_us(10);
    if (PINA & (1<<PA4)) {
        a_bit = 1;
    } else {
        a_bit = 0;
    }
    _delay_us(49);
    return a_bit;
}

// read 8 bits and return them as byte
unsigned char one_wire_receive_byte () {
    unsigned char a_bit, a_byte=0;
 
    for (int i=0; i<8; i++) {
        a_bit = one_wire_receive_bit();
        a_byte = a_byte >> 1;
        if (a_bit != 0) {
            a_bit = 0x80;
        }
        a_byte = a_byte | a_bit;
    }
    return a_byte;
}

// transmit a bit by setting PA4 to 1 if the sent bit is 1, else to 0
void one_wire_transmit_bit (unsigned char a_bit) {
	DDRA = DDRA | (1<<PA4);    // sbi DDRA,PA4
    PORTA = PORTA & ~(1<<PA4); // cbi PORTA,PA4
	_delay_us (2);
	if (a_bit != 0) {
		PORTA = PORTA | (1<<PA4);  // sbi PORTA,PA4
	} else {
		PORTA = PORTA & ~(1<<PA4); // cbi PORTA,PA4
	}
	_delay_us(58);
	DDRA = DDRA & ~(1<<PA4);      // cbi DDRA,PA4
    PORTA = PORTA & ~(1<<PA4);	  // cbi PORTA,PA4
	_delay_us(1);
}

// transmit a byte by transmitting 8 bits
void one_wire_transmit_byte (unsigned char a_byte) {	
	unsigned char a_bit;

	for (int i=0; i<8; i++) {
		if (a_byte & (1<<PA0)) {
			a_bit = 1;
		} else {
			a_bit = 0;
		}
		one_wire_transmit_bit (a_bit);
		a_byte = a_byte >> 1;
	}
}

// this function  reads the temperature
// and prints it at PORTA
void get_temperature() {
	unsigned char temp, sign;
	if (one_wire_reset() == 1) { // device detected
		one_wire_transmit_byte (0xCC);  // send command 0xCC
		one_wire_transmit_byte (0x44);  // send command 0x44
		while (one_wire_receive_bit() == 0) {} // wait until it receives 1
		one_wire_reset();               // reset one-wire
		one_wire_transmit_byte (0xCC);  // send command 0xCC
		one_wire_transmit_byte (0xBE);  // send command 0xBE
		temp = one_wire_receive_byte(); // receive temperature
		sign = one_wire_receive_byte(); // read sign 
		if (sign != 0) {
			temp = (~temp) + 1; // do a 2's complement if sign is 1 
		}
		temp = temp >> 1;       // devide by 2, due to 0.5 sensor's accuracy
		temp = ~temp;           // show the 1's complement
        
        // show the temperature at leds
		DDRA = 0xFF;
		PORTA = temp;	
	} else {  // device not detected
        // light only the most significant led
		DDRA = 0xFF;
		PORTA = 0x80;
	}
}

int main () {
    while (1) {  // run forever
		get_temperature();
	}
    return 0;
}
