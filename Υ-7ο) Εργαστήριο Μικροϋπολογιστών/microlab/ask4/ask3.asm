.include "m16def.inc"

.org 0x0
rjmp reset1
.org 0x2
rjmp ISR0            ; int0 interrupt
.org 0x10
rjmp ISR_TIMER1_OVF  ; timer overflow interrupt
;----------
.def on=r21
.def counter=r23
reset1:

ldi r16,high(ramend) ; initialize stack pointer
out sph,r16
ldi r16,low(ramend)
out spl,r16

reset:
ldi r24 ,( 1 << ISC01) | ( 1 << ISC00)
out MCUCR , r24
ldi r24 ,( 1 << INT0)
out GICR , r24
sei
;----------
ldi r24 ,(1<<TOIE1)
out TIMSK ,r24
;----------
ldi r24 ,(1<<CS12) | (0<<CS11) | (1<<CS10) ; CK/1024
out TCCR1B ,r24
;----------

;----------
clr r24
clr r21;on
ldi r16,0x00
out ddrb,r16      ; eisodos/e3odos
ldi r16,0xff
out ddra,r16

;-----------------ELEGXOS GIA TIMH PB0
check:
sei            
in r17,pinb      ;elegxos gia pb0
andi r17,0x01
cpi r17,0x01
breq led_on1
jmp check

;------------------------

led_on1:
;===============XRONOKA8YSTERHSH GIA ANTIMETVPISH TOU SPIN8HRISMOU
cpi on,0x01
breq led_on
trap:
in r17,pinb      ;elegxos gia pb0
andi r17,0x01
cpi r17,0x00
brne trap

led_on:
cpi on,0x00
breq first_time   ; ELEGXOS AN PROKEITE GIA APLO ANAMMA H ANANEWSH MESW TOU REG ON (<- H TIMH TOU KA8ORIZETAI ME VASH AN TO PA7 EINAI ON H OFF GIA ON=1 GIA OFF=0)
ldi r22,0x01
ldi r18,0xff

;.............................0,5 SECONDS OLA TA LED TOU PORT A ANAMMENA 

out porta,r18
ldi on,0x01
ldi r24,0xf0
out TCNT1H,r24
ldi r24,0xbe
out TCNT1L,r24
jmp check

;.......................2,5 SECONDS EPIPLEON GIA TO PA7

refresh:
ldi r18,0x80
out porta,r18
ldi on,0x01
ldi r24,0xb3
out TCNT1H,r24
ldi r24,0xb5
out TCNT1L,r24
jmp check

;.........................APLO ANAMMA(XWRIS ANANEWSH)
first_time:
ldi r18,0x80
out porta,r18
ldi on,0x01
ldi r24,0xA4
out TCNT1H ,r24
ldi r24 ,0x73
out TCNT1L ,r24
jmp check

;---------------------------- ROUTINA GIA E3YPHRETHSH DIAKOPWN
ISR0:
push on
push r22
push r26
rcall led_on
pop r26
pop r22
pop on
reti

;---------------------------- ROUTINA GIA E3YPHRETHSH YPERXEILHSHS XRONISTH
ISR_TIMER1_OVF:
ldi r24,0x00
cpi r22,0x01
breq rr
ldi r18,0x00
out porta,r18
ldi on,0x00
jmp check
rr:
ldi r22,0x00
jmp refresh