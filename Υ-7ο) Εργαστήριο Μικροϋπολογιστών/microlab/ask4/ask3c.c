#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>

unsigned char input=0, result=0;
bool CLOCK=false;

// check() is called from B0 input, 
// from int0 interrupt and from timer overflow
void check () {
	TCCR1B=0x05;
	if (result == 0) {
		// if called when all leds are off
		result=128;
		TCNT1H=0xA4;
		TCNT1L=0x73;
	} else if ((result == 0xFF) && CLOCK) {
		// if called from timer overflow when all leds were on (0.5s passed)
		result=128;	
		TCNT1H= 0xB3;
		TCNT1L= 0xB5;
		CLOCK=false;
	} else if (!CLOCK) {
		// if called from interrupt or B0 input
		result= 0xFF;
		TCNT1H= 0xF0;
		TCNT1L= 0xBE;
	} else if ((result == 128) && CLOCK) {
		// if called from timer overflow when 1 led was on (3s passed)
		result=0;
		TCCR1B= 0x00;
		CLOCK=false;
    }
}

// timer overflow routine
ISR(TIMER1_OVF_vect) {
		CLOCK=true; // set clock as true when timer overflows
		check();	
}

// interrupt int0 routine
ISR(INT0_vect){
	check();
}
	

int main () {  
	DDRB= 0x00;
	DDRA= 0xFF;
	MCUCR=0x03;
	TCNT1H=0x00;
	TCNT1L=0x00;
	GICR=0x40;
	TIMSK=0x04;
	TCCR1B= 0x00;
	asm("sei"); // enable interrupts
	result=0;
	while (1) {
		input = PINB & 1;
		if (input == 1) { // if B0 was pressed
			check ();
			while (input == 1) {
				// treat pressed B0 button as 1 hit
				input = PINB & 1;
				PORTA=result;
			}
		}
		PORTA=result;
	}
	return 0;
}