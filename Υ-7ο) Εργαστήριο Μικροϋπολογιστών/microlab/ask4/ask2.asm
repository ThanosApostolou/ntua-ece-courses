.include "m16def.inc"

;----------
.org 0x0
rjmp reset
.org 0x4
rjmp ISR1

reset:
ldi r24 , low(RAMEND)    ; initialize stack pointer 
out SPL , r24  
ldi r24 , high(RAMEND)  
out SPH , r24
ldi r24,(1<<ISC11)|(1<<ISC10)
out MCUCR,r24
ldi r24,(1<<INT1)
out GICR,r24
sei
ser r26
out DDRA,r26
clr r26

loop1:
out PORTA,r26
ldi r24,low(200)
ldi r25,high(200)
rcall wait_msec
inc r26
rjmp loop1

;-----------------
; wait routine
wait_msec:     
push r24      
push r25  
ldi r24 , low(998)       
ldi r25 , high(998) 
rcall wait_usec         
pop r25       
pop r24              
sbiw r24 , 1         
brne wait_msec      
ret   

wait_usec:     
sbiw r24 ,1      
nop           
nop          
nop           
nop           
brne wait_usec
ret              

;------------------
; interrupt INT1
ISR1:
push r24
push r25

;check for spinthirismo
spin:
ldi r24,(1<<INTF1)
out GIFR,r24
ldi r24,low(5)
ldi r25,high(5)
rcall wait_msec
in r24,GIFR
rol r24
rol r24
brcs spin

clr r16
out DDRB,r16
in r16,PINB
clc
clr r17
clr r27

loop2:
rol r16
brcc no_carry
inc r27
no_carry:
inc r17
cpi r17,8
brne loop2

ldi r16, 0b00001111
out DDRC,r16
out PORTC,r27

pop r25
pop r24
reti
