.include "m16def.inc"
.def reg=r19
.def c1=r20
.def counter=r18
.def ncounter=r21 ; NORMAL COUNTER-->PORTA

;----------------------------
; interrupt int0(mask all the others)
.org 0x0
rjmp reset
.org 0x2
rjmp isr0

reset:
ldi r24,( 1<<isc01) |( 1<<isc00)
out mcucr, r24
ldi r24, (1<<int0)
out gicr, r24
sei
;----------------------------
; fix ret for subroutines
reset1: 

ldi r24 , low(RAMEND) ; initialize stack pointer
out SPL , r24
ldi r24 , high(RAMEND)
out SPH , r24
;----------------------------
; INITIALIZE PORTS
ser r24 ; initialize PORTA for output
out DDRA , r24
ser r24 ; initialize PORTB for output
out DDRB , r24
clr r24 
;----------------------------
; kyriws programma-metrhths
main:
clr ncounter
clr reg

loop:
out porta,ncounter
ldi r24 , low(200);load r25:r24 with 200
ldi r25 , high(200);delay 2/10 second
rcall wait_msec
inc ncounter
rjmp loop

;---------------------------
; kathisterisi 2/10 sec
wait_usec:
sbiw r24 ,1 
nop 
nop 
nop 
nop 
brne wait_usec
ret 

wait_msec:
push r24 
push r25 
ldi r24 , low(998)  ;load r25:r24 with 98
ldi r25 , high(998) ; delay 2/10 second
rcall wait_usec 
pop r25 
pop r24 
sbiw r24 , 1 
brne wait_msec
ret
;---------------------------
; interrupt subroutine
isr0:
push r24
push r25

; check for spinthirismo
spin:
ldi r24,(1<<intf0)
out gifr,r24
ldi r24, low(5)
ldi r25, high(5)
rcall wait_msec
in r18,gifr
andi r18,0b01000000
cpi r18,0b01000000
breq spin

; check if PD0 is enabled
in r23,pind
andi r23,0x01
cpi r23,0x00
breq skip
inc reg
skip:
out portb, reg

pop r25
pop r24
reti

