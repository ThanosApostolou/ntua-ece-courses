/*
 * project.asm
 *
 *  Created: 22/9/2015 12:24:41 ??
 *   Author: George
 */ 
.include "m16def.inc"
.org 0x00
.def reg= r16
.def temp= r17
.def result= r18
    ldi reg,LOW(RAMEND)
	out SPL,reg
	ldi reg,HIGH(RAMEND)
	out SPH,reg
	clr reg
	out DDRB,reg
	ldi reg,0b00000011
	out DDRA,reg

start:
	ldi result,0x00
	in reg,PINB
	mov temp,reg
	andi temp,0b00001111
	cpi temp,0b00001100
	breq Y0_on
	cpi temp,0b00000011
	breq Y0_on
	rjmp Y1
Y0_on:
	ori result,0x01

Y1:
	mov temp,reg
	andi temp,0b11110000
	cpi temp,0b00110000
	breq Y1_on
	cpi temp,0b11000000
	breq Y1_on
	rjmp output
Y1_on:
	ori result,0b00000010

output:
	out PORTA,result
	rjmp start
	


