/*
 * avr4_1.asm
 *
 *  Created: 9/1/2015 6:57:08 ??
 *   Author: KOSTAS
 */ 

.include "m16def.inc"
.org 0x00
 rjmp reset
.org 0x04
 rjmp ISR0
.def temp = r16
.def delay1 = r17
.def delay2 = r18
.def reg =r19

reset:
ldi temp, low(RAMEND)
out SPL, temp
ldi temp, high(RAMEND)
out SPH, temp
ldi temp,(1<<ISC11)|(1<<ISC10)
out MCUCR,temp
ldi temp,(1<<INT1)
out GICR,temp
sei
	ser temp
	out DDRC,temp
	clr temp
	out DDRD,temp

main:
	ldi reg,0x0f
	out PORTC,reg
	rjmp main



ISR0:
	cli
	push temp
    in temp,SREG
	push temp                ; always save SREG
waiting:						;Wait for the sparkle effect.
	clr temp
	ldi temp,(1<<INTF1)      ;  ldi temp,(1<<INTF0) for INT0 
	out GIFR,temp
	ldi r24,low(5)
	ldi r25,high(5)
	rcall wait_msec
	in temp,GIFR
	sbrc temp,7              ;  sbrc temp,6 for INT0
	rjmp waiting 

	ldi reg,0xf0
	out PORTC,reg
	ldi r24,low(2000)
	ldi r25,high(2000)
	rcall wait_msec
	pop temp
	out SREG,temp
	pop temp
	sei
	reti



wait_usec:            /* waiting routines */
	sbiw r24,1
	nop
	nop
	nop
	nop
	brne wait_usec
	ret

wait_msec:
	push r24
	push r25
	ldi r24,low(998)
	ldi r25,high(998)
	rcall wait_usec
	pop r25
	pop r24
	sbiw r24,1
	brne wait_msec
	ret

