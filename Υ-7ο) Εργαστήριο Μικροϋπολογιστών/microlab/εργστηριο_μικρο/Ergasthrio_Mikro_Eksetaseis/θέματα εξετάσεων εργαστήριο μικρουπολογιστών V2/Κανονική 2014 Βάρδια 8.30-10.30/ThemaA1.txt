org 0800h

jmp start

off:
    mvi a,00h
    cma
    sta 3000h

start:
mvi b,00h ; counter
lda 2000h
mvi c,08h

loop1:
      dcr c
      jz off
      inr b
      rrc
      jnc loop1

mvi a,08h
sub b
mov b,a

inr b
mvi a,00h

loop2:
      stc
      rar
      dcr b
      jnz loop2

cma 
sta 3000h

jmp start
end