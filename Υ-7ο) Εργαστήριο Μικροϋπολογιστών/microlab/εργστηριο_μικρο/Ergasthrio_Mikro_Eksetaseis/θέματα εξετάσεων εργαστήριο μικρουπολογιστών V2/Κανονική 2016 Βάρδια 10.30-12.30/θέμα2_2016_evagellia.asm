
org 100h

.data
msg1 db "Give numbers: $" 
msg2 db "Even: $"
msg3 db "Odd : $"
space db " $"
temp db 0 
ceven db 0
codd db 0
.code 

start:
mov bx,00h 
mov temp,00h
mov ceven,00h
mov codd,00h
mov dx, offset msg1
mov ah, 09h
int 21h 

read:             
mov ah,08h
int 21h 
cmp al,'Q'        ; IF '*'
jz end
cmp al,0dh
jz moveon  
cmp al,'1'
jb read
cmp al,'9'
ja read
mov dl,al
mov ah,02
int 21h 

ror al,1
jc odd
inc ceven 
jmp skip
odd:
inc codd
skip:

inc bx
cmp bx,6         ; IF INPUT=16
jnz read 
moveon:
;//////////newline//////////
mov ah, 0Eh       ; PRINT NEW LINE SEQUENCE
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h
;//////////////////////////
mov dx, offset msg2
mov ah, 09h
int 21h 

mov ah,00h
mov al,00h
add al,ceven
add al,30h 
mov dl,al
mov ah,02
int 21h 

mov dx, offset space
mov ah, 09h
int 21h
  
mov dx, offset msg3
mov ah, 09h
int 21h 

mov ah,00h
mov al,00h
add al,codd
add al,30h  
mov dl,al
mov ah,02
int 21h


;//////////newline//////////
mov ah, 0Eh       ; PRINT NEW LINE SEQUENCE
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h
;//////////////////////////
jmp start
end:  
ret




