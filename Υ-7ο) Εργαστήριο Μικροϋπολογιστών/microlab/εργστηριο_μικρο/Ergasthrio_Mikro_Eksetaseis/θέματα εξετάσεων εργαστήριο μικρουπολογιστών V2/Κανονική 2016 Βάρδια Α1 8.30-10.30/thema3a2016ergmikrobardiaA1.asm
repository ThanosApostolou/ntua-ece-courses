.include "m16def.inc"
.def count_porta=r19
.def count_portb=r20
.def count_loop=r21
.def sum=r22
.def temp=r23
.def led=r16

; fix ret for subroutines
reset:
ldi r24, low(RAMEND)
out SPL, r24
ldi r24, high(RAMEND)
out SPH, r24

start:

clr r16
out ddra,r16
out ddrb,r16
ser r16
out ddrc,r16

in r17,pina
in r18,pinb

ldi count_loop,0x08
ldi sum,0x00
ldi led,0x01

loop1:
ror r17
brcc count_zeros_porta
return1:
dec count_loop
cpi count_loop,0x00
brne loop1

ldi count_loop,0x08

loop2:
ror r18
brcc count_zeros_portb
return2:
dec count_loop
cpi count_loop,0x00
brne loop2

add sum,count_porta
add sum,count_portb

mov temp,sum
subi sum,0x09
cpi sum,0x00
brmi lower

;dec sum          auto mallon einai lathos
rotate1:
clc
rol led
dec sum
cpi sum,0x00
brne rotate1
end_of_rotate1:
out portc,led
jmp start

lower:
;dec temp       episis
rotate2:
clc
rol led
dec temp
cpi temp,0x00
brne rotate2
end_of_rotate2:
out portc,led
jmp start

;***************Routines******************
count_zeros_porta:
inc count_porta
jmp return1

count_zeros_portb:
inc count_portb
jmp return2





