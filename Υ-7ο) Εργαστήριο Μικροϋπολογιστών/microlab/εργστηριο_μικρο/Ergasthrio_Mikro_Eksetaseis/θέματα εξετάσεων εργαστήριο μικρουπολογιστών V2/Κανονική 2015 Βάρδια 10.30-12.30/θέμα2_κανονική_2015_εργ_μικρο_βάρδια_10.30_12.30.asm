
org 100h

.data 
msg1 db "Give numbers: $"
msg3 db "MIN$"
msg2 db "MAX$"
space db " $"
anok db ":$"
what db "*****************$"
first_n db 0
second_n db 0
third_n db 0
fourth_n db 0
min db 0
max db 0

.code

start:

mov first_n,00h
mov second_n,00h
mov third_n,00h
mov fourth_n,00h
mov min,00h
mov max,00h

mov dx, offset msg1
mov ah, 09h
int 21h 

mov ah,01h
int 21h
mov first_n,al
mov max,al
mov min,al

mov ah,01h
int 21h 
mov second_n,al
        
mov ah,01h
int 21h
mov third_n,al

mov ah,01h
int 21h
mov fourth_n,al

mov al,max
cmp al,second_n
jae skip1
mov al,second_n
mov max,al
skip1:
mov al,max
cmp al,third_n
jae skip2 
mov al,third_n
mov max,al
skip2:    
mov al,max
cmp al,fourth_n 
jae skip3     
mov al,fourth_n
mov max,al
skip3:

mov al,min
cmp al,second_n
jb skip4
mov al,second_n
mov min,al
skip4:
cmp al,third_n
jb skip5
mov al,third_n
mov min,al
skip5:
cmp al,fourth_n 
jb skip6
mov al,fourth_n
mov min,al
skip6:

mov ah, 0Eh       
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h

mov dx, offset msg3
mov ah, 09h
int 21h            

mov dx, offset space
mov ah, 09h
int 21h 

mov dx, offset msg2
mov ah, 09h
int 21h 

mov dx, offset anok
mov ah, 09h
int 21h

mov dl,min
mov ah,02h
int 21h

mov dx, offset space
mov ah, 09h
int 21h  

mov dl,max
mov ah,02h
int 21h

mov ah, 0Eh       
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h

mov dx, offset what
mov ah, 09h
int 21h            

mov ah, 0Eh       
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h 

jmp start
end:
ret




