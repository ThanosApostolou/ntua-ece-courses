org 100h

.data
array db 'P','e','r','i','c','l','e','s'  
temp1 db ?
temp2 db ?
counter db ? 
counter2 db 0 
msg1 db 0dh,0Ah, "The array is: $"
msg2 db 0Dh,0Ah, "Give a number: $"        
msg3 db 0dh,0Ah, "The shifted array is: $"


.code 

mov dx,offset msg1
mov ah,09h
int 21h

;*********************
; PRINT ARRAY
mov bh,0h
mov bl,0h
print1:   
mov bh,0h
mov dl,array[bx]
mov ah,02h
int 21h
inc bx
cmp bx,08h
jnz print1
;********************* 

;*********************
; NEW LINE
mov ah, 0Eh          ; PRINT NEW LINE SEQUENCE
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h
;********************

;********************
; HOW MANY TIMES?
mov dx, offset msg2 ; DISPLAY THE MESSAGE
mov ah,09h
int 21h

mov ah,01h          ; READ THE INTEGER
int 21h

sub al,30h          ; MAKE IT A NUMBER
mov counter,al      ; SAVE IT INTO VARIABLE COUNTER
;*********************

;*********************
; SHIFT THE ARRAY BY 1 POSITION
SHIFT:
mov al,array[0]      ; SAVE FIRST ELEMENT IN VARIABLE temp1
mov temp1,al

mov al,array[7]      ; FIRST ELEMENT IS array[0]
mov array[0],al      ; MOVE LAST ELEMENT IN FIRST PLACE

mov bl,6h            ; FOR ALL ELSE, MOVE THEM BY ONE ARRAY CELL  
again:
mov bh,0h        
mov al,array[bx]
mov array[bx+1],al
dec bx
cmp bx,0
jnz again
mov al,temp1
mov array[1],al
;*********************

;********************* 
; SHIFT COUNTER TIMES
inc counter2        ; HOW MANY TIMES TO REPEAT THE SHIFT?
mov al,counter2
cmp al,counter
jnz SHIFT
;*********************  

;*********************
; NEW LINE
mov ah, 0Eh          ; PRINT NEW LINE SEQUENCE
mov al, 0Dh
int 10h
mov al, 0Ah
int 10h
;********************
  
;*********************
; PRINT SHIFTED ARRAY

mov dx,offset msg3
mov ah,09h
int 21h
mov bh,0h
mov bl,0h
print2:   
mov bh,0h
mov dl,array[bx]
mov ah,02h
int 21h
inc bx
cmp bx,08h
jnz print2
;********************

ret




