.include "m16def.inc"
.def flag=r17
.org 0x0
rjmp reset
.org 0x10
rjmp ISR_TIMER1_OVF

reset:
ldi r24 , low(RAMEND) ; initialize stack pointer
out SPL , r24
ldi r24 , high(RAMEND)
out SPH , r24

ldi r24 ,(1 << PC7) | (1 << PC6) | (1 << PC5) | (1 << PC4)  
out DDRC ,r24  

ldi r24 ,(1<<TOIE1) ;
out TIMSK ,r24 

ldi r24 ,(1<<CS12) | (0<<CS11) | (1<<CS10) ; CK/1024
out TCCR1B ,r24

.org 0x100
ser r16
out ddrb,r16
;rcall lcd_init

main:

ldi r24,0x1
rcall lcd_command
ldi r24,5
rcall wait_msec
;jmp main

get_a:
in r16,pina
cpi r16,0x0
breq get_a

ldi r24,0x85
out TCNT1H ,r24 
ldi r24 ,0xEE
out TCNT1L ,r24

ldi r24,0b00001110 ; display without cursor
rcall lcd_command

check_if:
rcall scan_keypad
rcall keypad_to_ascii
cpi r24,0x0
breq check_if
;rcall scan_keypad
;rcall keypad_to_ascii
cpi r24,0x42
brne alarm_on
ldi r24, 'B'
rcall lcd_data
ldi r24,5
rcall wait_msec
again:
rcall scan_keypad
rcall keypad_to_ascii
cpi r24,0x0
brne again
check_if2:
rcall scan_keypad
rcall keypad_to_ascii
cpi r24,0x0
breq check_if2
cpi r24,0x31
brne alarm_on
ldi r24, '1'
rcall lcd_data
ldi r24,5
rcall wait_msec
again1:
rcall scan_keypad
rcall keypad_to_ascii
cpi r24,0x0
brne again1
check_if3:
rcall scan_keypad
rcall keypad_to_ascii
cpi r24,0x0
breq check_if3
cpi r24,0x33
brne alarm_on
ldi flag,0x1
;jmp alarm_off
ldi r24, '3'
rcall lcd_data
ldi r24,5
rcall wait_msec
jmp alarm_off

alarm_on:

ldi r24,0x1
rcall lcd_command
ldi r24,5
rcall wait_msec


ldi flag,0x0
ldi r24,'A'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24,'L'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24,'A'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24,'R'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, 'M'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, ' '
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, 'O'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, 'N'
rcall lcd_data
ldi r24,5
rcall wait_msec

led:
ser r16
out portb,r16
ldi r24,200
rcall wait_msec
clr r16
out portb,r16
rcall wait_msec
jmp led

alarm_off:

ldi r24,0x1
rcall lcd_command
ldi r24,5
rcall wait_msec


ldi r24,'A'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24,'L'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24,'A'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24,'R'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, 'M'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, ' '
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, 'O'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, 'F'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, 'F'
rcall lcd_data
ldi r24,5
rcall wait_msec

ldi r24,0b00001100 ; display without cursor
rcall lcd_command
end:
jmp end

ISR_TIMER1_OVF:
repeat:
cpi flag,0x1
brne wrong
jmp repeat
wrong:
ldi r24,'A'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24,'L'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24,'A'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24,'R'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, 'M'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, ' '
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, 'O'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, 'F'
rcall lcd_data
ldi r24,5
rcall wait_msec
ldi r24, 'F'
rcall lcd_data
ldi r24,5
rcall wait_msec

ldi r24,0b00001100 ; display without cursor
rcall lcd_command

led1:
ser r16
out portb,r16
ldi r24,200
rcall wait_msec
clr r16
out portb,r16
rcall wait_msec
jmp led1

;***********************************************************************************

write_2_nibbles: 
push r24         
in r25 ,PIND       
andi r25 ,0x0f  
andi r24 ,0xf0        
add r24 ,r25     
out PORTD ,r24            
sbi PORTD ,PD3   
cbi PORTD ,PD3  
pop r24                  
swap r24  
andi r24 ,0xf0   
add r24 ,r25 
out PORTD ,r24              
sbi PORTD ,PD3  
cbi PORTD ,PD3 
ret

;===============================================================================================
lcd_data:                
sbi PORTD ,PD2       
rcall write_2_nibbles     
ldi r24 ,43                
ldi r25 ,0                   
rcall wait_usec  
ret
;=================================================================================================
lcd_command: 
cbi PORTD ,PD2        
rcall write_2_nibbles    
ldi r24 ,39                   
ldi r25 ,0                    
rcall wait_usec          
ret   

;====================================================================================================
lcd_init:      
ldi r24 ,40            
ldi r25 ,0           
rcall wait_msec      
ldi r24 ,0x30          
out PORTD ,r24                  
sbi PORTD ,PD3        
cbi PORTD ,PD3         
ldi r24 ,39  
ldi r25 ,0                      
rcall wait_usec                                                  
                         
ldi r24 ,0x30         
out PORTD ,r24       
sbi PORTD ,PD3  
cbi PORTD ,PD3  
ldi r24 ,39  
ldi r25 ,0  
rcall wait_usec   
ldi r24 ,0x20                   
out PORTD ,r24          
sbi PORTD ,PD3  
cbi PORTD ,PD3  
ldi r24 ,39  
ldi r25 ,0  
rcall wait_usec       
ldi r24 ,0x28         
rcall lcd_command      
ldi r24 ,0x0c             
rcall lcd_command       
ldi r24 ,0x01              
rcall lcd_command  
ldi r24 ,low(1530)  
ldi r25 ,high(1530)  
rcall wait_usec      
ldi r24 ,0x06             
rcall lcd_command                                                                                       
ret  

;===================================================================================
scan_row:
ldi r25 ,0x08  
back_: 
lsl r25 
dec r24
brne back_
out PORTC ,r25 
nop
nop 
in r24 ,PINC 
andi r24 ,0x0f 
ret 
;================================================================================
scan_keypad:
ldi r24 ,0x01
rcall scan_row
swap r24 
mov r27 ,r24 
ldi r24 ,0x02 
rcall scan_row
add r27 ,r24 
ldi r24 ,0x03
rcall scan_row
swap r24 
mov r26 ,r24 
ldi r24 ,0x04 
rcall scan_row
add r26 ,r24 
movw r24 ,r26 
ret

;=====================================================================================
keypad_to_ascii: 
movw r26 ,r24 
ldi r24 ,'*'
sbrc r26 ,0
ret
ldi r24 ,'0'
sbrc r26 ,1
ret
ldi r24 ,'#'
sbrc r26 ,2
ret
ldi r24 ,'D'
sbrc r26 ,3 
ret 
ldi r24 ,'7'
sbrc r26 ,4
ret
ldi r24 ,'8'
sbrc r26 ,5
ret
ldi r24 ,'9'
sbrc r26 ,6
ret
ldi r24 ,'C'
sbrc r26 ,7
ret
ldi r24 ,'4' 
sbrc r27 ,0 
ret
ldi r24 ,'5'
sbrc r27 ,1
ret
ldi r24 ,'6'
sbrc r27 ,2
ret
ldi r24 ,'B'
sbrc r27 ,3
ret
ldi r24 ,'1'
sbrc r27 ,4
ret
ldi r24 ,'2'
sbrc r27 ,5
ret
ldi r24 ,'3'
sbrc r27 ,6
ret
ldi r24 ,'A'
sbrc r27 ,7
ret
clr r24
ret
;===================================================================================
wait_msec:
push r24
push r25
ldi r24,low(998)
ldi r25,high(998)
rcall wait_usec
pop r25
pop r24
sbiw r24,1
brne wait_msec
ret

wait_usec:
sbiw r24,1
nop
nop
nop
nop
brne wait_usec
ret
