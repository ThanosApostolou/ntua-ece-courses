.include "m16def.inc"
.dseg
_tmp_: .byte 2
.cseg
reset:
	ldi r17 , low(RAMEND) ;initialize stack pointer
	out SPL , r17
	ldi r17 , high(RAMEND)
	out SPH , r17
;Arxikopoihsh gia output se LCD othoni
;PD7-PD2 arxikopoiountai os eksodos
	ldi r24, 0xFC
	out DDRD, r24
	clr r24
	out DDRA, r24 ;Port A eisodos
;clean output
	out PORTA,r24
	out PORTD,r24
;Arxikopoihsh othonis se katastasi oste na mporei na xrisimopoiithei
	rcall lcd_init
;[upper|lower] [MSB|LSB]
;Arxika i endeiksi 00:00
	ldi r24, '0'
	rcall lcd_data
	ldi r24, '0'
	rcall lcd_data
	ldi r24, 'M'
	rcall lcd_data
	ldi r24, 'I'
	rcall lcd_data
	ldi r24, 'N'
	rcall lcd_data
	ldi r24, ':'
	rcall lcd_data
	ldi r24,'0'
	rcall lcd_data	
	ldi r24,'0'	
	rcall lcd_data
	ldi r24, 'S'
	rcall lcd_data
	ldi r24, 'E'
	rcall lcd_data
	ldi r24, 'C'
	rcall lcd_data
	ldi r17,0x00
	ldi r18,0x00

begin:
	in r19, PINA
	andi r19 ,0x80 ;apomonwsh tvn triwn bit pou mas noiazei
	cpi r19 , 0x80 ;mexri na mhn to pataei pia
	breq reset ; an den to xei afhsei epistrofh sth leave alliws 3ekina to xronometro
leave:
	in r19, PINA
	andi r19 ,0x01 ;apomonwsh tvn triwn bit pou mas noiazei
	cpi r19 ,0x01 ;perimene pathma tou PB0
	brne begin ; an den einai pathmeno 3ana sto begin alliws phgaine sto leave kai perimene na to afhsei 
	rjmp start_timer
start:
	ldi r17, 0x00 ;Metritis gia deuterolepta
	ldi r18, 0x00 ;Metritis gia lepta
;Isxyei oti arithmos + 48 = ascii code arithmou
start_timer:
	ldi r24 ,low(1000) ;load r24:r25 with 1000
	ldi r25 ,high(1000) ;delay 1 second
	rcall wait_msec
	inc r17 ;aukshsh twn seconds
	cpi r17 , 60 ;des an eftase to 60"=1'
	brne stop_timer ;kai an nai stop_timer
	clr r17 ;mhdenise ta seconds
	inc r18 ;aukshse ta minutes	
	cpi r18 , 60 ; des an eftase thn wra
	breq start ; an nai mhdenise metrhtes kai ksekina pali apo thn arxh
stop_timer:
	ldi r24 ,0x01
	rcall lcd_command ;katharismos othonhs
	ldi r24,low(1530) ;aparaithth kathusterish
	ldi r25,high(1530)
	rcall wait_usec
	mov r24 , r18
	rcall bin_to_dec
	ldi r24, 'M'
	rcall lcd_data
	ldi r24, 'I'
	rcall lcd_data
	ldi r24, 'N'
	rcall lcd_data
	ldi r24, ':'
	rcall lcd_data	
	mov r24 , r17
	rcall bin_to_dec
	ldi r24, 'S'
	rcall lcd_data
	ldi r24, 'E'
	rcall lcd_data
	ldi r24, 'C'
	rcall lcd_data
	rjmp begin
;Metatroph binary se decimal (mono dekades kai monades)
bin_to_dec:
	ldi r20 , 48
	clr r21 ;metrhths decadwn
	mov r16 , r24
DECADES:
	cpi r16 , 10
	brlo WDECADES
	inc r21
	subi r16 , 10
	rjmp DECADES
WDECADES: ;afou tis metrhsame tis ektupwnoume
	add r21 , r20
	mov r24 , r21
	rcall lcd_data
MONADES: ;mas exoun meinei mono monades ston r16
	add r16 , r20
	mov r24 , r16
	rcall lcd_data
	ret
wait_usec:
	sbiw r24 ,1 ; 2 cycles (0.250 �sec)
	nop ; 1 cycle (0.125 �sec)
	nop ; 1 cycle (0.125 �sec)
	nop ; 1 cycle (0.125 �sec)
	nop ; 1 cycle (0.125 �sec)
	brne wait_usec ; 1 � 2 cycles (0.125 � 0.250 �sec)
	ret ; 4 cycles (0.500�sec)
wait_msec:
	push r24 ; 2 cycles (0.250 �sec)
	push r25 ; 2 cycles
	ldi r24 , low(998) ;Fortwse ton kataxvrhth r25:r24 me 998 (1 cycle - 0.125 �sec)
	ldi r25 , high(998) ; 1 cycle (0.125�sec)
	rcall wait_usec ; 3 cycle (0.375 �sec),prokalei synolika ka8usterhsh 998. 375�sec
	pop r25 ; 2 cycles (0.250 �sec)
	pop r24 ; 2 cycles
	sbiw r24 ,1 ; 2 cycles
	brne wait_msec ; 1 � 2 cycles (0.125 � 0.250 �sec)
	ret ; 4cycles (0.500 �sec)
;Routines gia othoni
write_2_nibbles:
;Gia metafora 2 tmhmatwn twn 4 bit th fora ston elegkth ths o8onhw lcd etsi oste na dosoume stin othoni olokliro byte
	push r24 ;stelenei ta 4 MSB
	in r25 ,PIND ;diavazontas ta 4 LSB kai ta ksanastelnoume
	andi r25 ,0x0f ;gia na mhn xalasoume thn opoia prohgoumenh katastash
	andi r24 ,0xf0 ;apomonwnontai ta 4 MSB kai
	add r24 ,r25 ;syndiazontai me ta prouparxonta 4 LSB
	out PORTD ,r24 ;kai dinontai sthn eksodo
	sbi PORTD ,PD3 ; dhmiourgeite palmos �nable ston akrodekth PD3
	cbi PORTD ,PD3 ; PD3=1 kai meta PD3=0
	pop r24 ; stelnei ta 4 LSB. Anaktatai to byte.
	swap r24 ; enallasontai ta 4 MSB me ta 4 LSB
	andi r24 ,0xf0 ;pou me thn seira tous apostellontai
	add r24 ,r25
	out PORTD ,r24
	sbi PORTD ,PD3 ;Neos palmos �nable
	cbi PORTD ,PD3
	ret
lcd_data:
;Gia apostolh enos byte dedomenwn ston elegkth thw o8onhs lcd
;Eisodos: o r24 me to byte tou dedomenou blepe pinaka data
;Eksodos: - (emfanizei stin othoni an to display einai on stis rithmiseis to data pou steilame. an steiloyme allo, auto tha emfanistei apo piso)
	sbi PORTD ,PD2 ; epilogh tou kataxwrhth dedomenwn (PD2=1)
	rcall write_2_nibbles ; apostolh toy byte
	ldi r24 ,43 ;anamonh 43�sec mexri na oloklhrwthei h lhpsh
	ldi r25 ,0 ;twn dedomenwn apo ton elegkth ths lcd
	rcall wait_usec
	ret
lcd_command:
;Gia apostolh mias entolhs ston elegkth ths o8onhs lcd
;Eisodos: o r24 me to byte tis entolis blepe pinaka entolwn
;Eksodos: - (kanei oti pei i entoli pou dosame ston r24)
	cbi PORTD ,PD2 ; epilogh tou kataxwrhth dedomenwn (PD2=1)
	rcall write_2_nibbles ;apostolh ths entolhs kai anamonh 39�sec
	ldi r24 ,39 ;gia thn oloklhrwsh ths ekteleshs ths apo ton elegkth ths lcd.
	ldi r25 ,0 ;SHM:uparxoun dyo entoles, oi clear display kai return home,
	rcall wait_usec ;pou apaitoun shmantika megalutero xroniko diasthma
	ret
lcd_init:
;arxikopoihsh kai ruumiseis o8onhs
;Rythmiseis othonis:
;DL=0 4 bit mode
;N=1 2lines
;F=0 5x8 dots
;D=1 display on
;C=0 cursor off
;B=0 blinking off
;I/D = 1 DDRAM adress auto increment
;SH=0 shift of entire display off
;No input no output
	ldi r24 ,40 ; Otan o elegkths ths lcd trofodoteitai me
	ldi r25 ,0 ;reuma ektelei thn dikia tou arxikopoihsh
	rcall wait_msec ;Anamonh 40 msec mexri auth na oloklhrwthei.
	ldi r24 ,0x30 ;entolh metavashs se 8 bit mode
	out PORTD ,r24 ;epeidh den mporoume na eimaste vevaioi
	sbi PORTD ,PD3 ;gia thn diamorfwsh eisodou tou elegkth
	cbi PORTD ,PD3 ;ths othonhs, h entolh apostelletai duo fores
	ldi r24 ,39
	ldi r25 ,0 ;ean o elegkths ths othonhs vrisketai se 8-bit mode
	rcall wait_usec ;den tha sumbei tipota , alla an o elegkths exei diamorfwsh
;eisodou 4 bit tha metavei se diamorfwsh 8 bit
	ldi r24 ,0x30
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x20 ;allagh se 4-bit mode
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x28 ;epilogh xarakthrwn megethous 5x8 koukidwn
	rcall lcd_command ;kai emfanish duo grammwn sthn othonh
	ldi r24 ,0x0c ;energopoihsh ths othonhs ,apokrupsh tou kersora
	rcall lcd_command
	ldi r24 ,0x01 ;katharismos ths othonhs
	rcall lcd_command
	ldi r24 ,low(1530)
	ldi r25 ,high(1530)
	rcall wait_usec
	ldi r24 ,0x06 ;energoihsh automaths aukshshs kata 1 ths dieuthunshs
	rcall lcd_command ;pou einai apothikeumenh ston metrhth dieuthunsewn kai apergopoihsh ths olisthishs oloklhrhs ths othonhs
	ret


