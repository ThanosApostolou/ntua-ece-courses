
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

.data 
msg1 db 0Dh,0Ah, " Enter a string of at most 5 decimal numbers: $" 
pointer1 dw ?
pointer2 dw 6
array1 db 6h dup (0)          ; first number  
temp1 db ?
var1 db ?
.code

mov dx, offset msg1
mov ah,09h
int 21h

mov bx,00h

read1:
mov ah,08h
int 21h 
cmp al,0dh
jz operation
cmp al,30h
jb read1
cmp al,3Ah
jae read1
mov dl,al
mov ah,02h
int 21h
sub al,30h
mov array1[bx],al
inc bx
cmp bx,6h 
jz operation
jmp read1

operation:

mov temp1,bl   ; save number of string1
mov dl,temp1  
mov al,temp1 
mov ah,0h
mov pointer1,ax 
;sub pointer1,1h
inc temp1

std
LEA SI, array1
add SI, pointer1
LEA DI, array1
add DI, pointer2
MOV Cl, temp1
REP MOVSB


mov temp1,dl
mov al, temp1
mov var1, al
dec temp1
;dec temp1
loop: 
mov bh,0h
mov bl,temp1
mov al,0h
mov array1[bx],al
dec temp1
dec var1
jnz loop


RET








