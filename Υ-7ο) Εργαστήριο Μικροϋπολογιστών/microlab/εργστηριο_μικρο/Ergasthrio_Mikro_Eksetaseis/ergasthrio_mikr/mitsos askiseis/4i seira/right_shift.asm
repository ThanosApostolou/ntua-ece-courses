
name "right shift"
org 100h

.data 
msg1 db 0Dh,0Ah, " Enter a string of at most 5 decimal numbers: $" 
array1 db 6h dup (0)          ; first number  
array2 db 6h dup (0)
temp1 db ?
var db ?
.code

mov dx, offset msg1
mov ah,09h
int 21h

mov bx,00h

;****************************************  
;NUMBER INPUT   

read1:          
mov ah,08h                        
int 21h 
cmp al,0dh                      ; array[temp1]  temp1: cell index
jz operation
cmp al,30h                     
jb read1
cmp al,3Ah
jae read1
mov dl,al
mov ah,02h
int 21h
sub al,30h
mov array1[bx],al
inc bx
cmp bx,5h 
jz operation
jmp read1
                
operation:   
mov ah,08h
int 21h
cmp al,0dh
jnz operation

dec bx
mov temp1,bl   ; save number of string1 




;******************************************
;RIGHT_SHIFT

mov cx,5
mov var,bl
inc var

right_shift:        
mov bl,temp1
mov al,array1[bx] 
mov dx,bx
mov bx,cx
mov array2[bx],al 
mov cx,bx
mov bx,dx
dec cx       
dec temp1
dec var 
ja right_shift
;********************************************
ret




