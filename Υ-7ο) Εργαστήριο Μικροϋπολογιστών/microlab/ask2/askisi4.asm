ORG 100H

.DATA
A DB 30H, 30H, 30H, 30H         ; array with the 4 decimal numbers of 1st number
B DB 30H, 30H, 30H, 30H         ; array with the 4 decimal numbers of 2nd number
C DB 4 DUP (?)
operator DB 1 2BH
A_hex DW 1 0000H
B_hex DW 1 0000H
result_hex DW 1 0000H
negative DB 1 2BH

.CODE
START:
; initialize A,B,C with 0
MOV A[00H],30H
MOV A[01H],30H
MOV A[02H],30H
MOV A[03H],30H
MOV B[00H],30H
MOV B[01H],30H
MOV B[02H],30H
MOV B[03H],30H
MOV C[00H],30H
MOV C[01H],30H
MOV C[02H],30H
MOV C[03H],30H
; read 1st number
MOV AH,08H            ; INT 21H reads into AL

READ_FIRST_A:
INT 21H       
CMP AL,41H
JE  A_GIVEN           ; if 'A' is given jump to A_GIVEN
CMP AL,30H
JL  READ_FIRST_A
CMP AL,39H
JG  READ_FIRST_A      ; if digit is given (0 to 9) then continue     
MOV A[03H],AL

MOV DI,03H            ; index

READ_REST_A:
INT 21H       
CMP AL,41H            
JE  A_GIVEN           ; if 'A' is given jump to A_GIVEN
CMP AL,2BH              
JE  PLUS_GIVEN        ; if + is given jump to PLUS_GIVEN
CMP AL,2DH            
JE  MINUS_GIVEN       ; if - is given jump to MINUS_GIVEN
CMP AL,30H            
JL  READ_REST_A
CMP AL,39H
JG  READ_REST_A       ; if digit is given (0 to 9) then continue
CMP DI,00H
JL  READ_REST_A       ; if A is full, loop until +,- or 'A'
MOV CL,A[01H]          ; A[DI-1] <- A[DI] <- AL
MOV A[00H],CL
MOV CL,A[02H]
MOV A[01H],CL
MOV CL,A[03H]
MOV A[02H],CL
MOV A[03H],AL
DEC DI 
JMP READ_REST_A     

PLUS_GIVEN:
MOV offset operator, 2BH
JMP READ_FIRST_B

MINUS_GIVEN:
MOV offset operator, 2DH

READ_FIRST_B:
INT 21H       
CMP AL,41H
JE  A_GIVEN           ; if 'A' is given jump to A_GIVEN
CMP AL,30H
JL  READ_FIRST_B
CMP AL,39H
JG  READ_FIRST_B      ; if digit is given (0 to 9) then continue     
MOV B[03H],AL

MOV DI,02H            ; index

READ_REST_B:
INT 21H       
CMP AL,41H            
JE  A_GIVEN           ; if 'A' is given jump to A_GIVEN
CMP AL,3DH              
JE  RESULT             ; if = is given jump to PRINT
CMP AL,30H            
JL  READ_REST_B
CMP AL,39H
JG  READ_REST_B       ; if digit is given (0 to 9) then continue
CMP DI,00H
JL  READ_REST_B       ; if A is full, loop until = or 'A'
MOV CL,B[01H]          ; A[DI-1] <- A[DI] <- AL
MOV B[00H],CL
MOV CL,B[02H]
MOV B[01H],CL
MOV CL,B[03H]
MOV B[02H],CL
MOV B[03H],AL
DEC DI 
JMP READ_REST_B

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
RESULT:

;;;;;; PRINT INPUT
MOV AH,02H            ; INT 21H prints DL

MOV DI,00H
PRINT_A_NZ:
MOV DL,A[DI]
CMP DI,03H
JE  PRINT_A
INC DI
CMP DL,30H
JE  PRINT_A_NZ


DEC DI
PRINT_A:            
MOV DL,A[DI]         
INT 21H
INC DI
CMP DI,03H
JLE PRINT_A

MOV DL,operator
INT 21H


MOV DI,00H
PRINT_B_NZ:
MOV DL,B[DI]
CMP DI,03H
JE  PRINT_B
INC DI
CMP DL,30H
JE  PRINT_B_NZ


DEC DI

PRINT_B:            
MOV DL,B[DI]         
INT 21H
INC DI
CMP DI,03H
JLE PRINT_B

MOV DL,'='
INT 21H

;;; CALCULATION
;;; CX = A to hex


MOV AX,0000H
MOV AL,A[00H]
SUB AL,30H
MOV BX,3E8H
MUL BX
MOV CX,AX
   
MOV AX,0000H
MOV AL,A[01H] 
SUB AL,30H
MOV BX,64H
MUL BX   
ADD CX,AX

MOV AX,0000H
MOV AL,A[02H]    
SUB AL,30H
MOV BX,0AH
MUL BX
ADD CX,AX

MOV AX,0000H
MOV AL,A[03H]  
SUB AL,30H
ADD CX,AX

MOV A_hex,CX 

MOV AX,0000H
MOV AL,B[00H] 
SUB AL,30H
MOV BX,3E8H
MUL BX
MOV CX,AX
   
MOV AX,0000H
MOV AL,B[01H]  
SUB AL,30H
MOV BX,64H
MUL BX   
ADD CX,AX

MOV AX,0000H
MOV AL,B[02H]     
SUB AL,30H
MOV BX,0AH
MUL BX
ADD CX,AX

MOV AX,0000H
MOV AL,B[03H]     
SUB AL,30H
ADD CX,AX

MOV B_hex,CX


MOV AL,B[00h]

MOV B_hex,CX


CMP operator,'-'
JE SUB_NUMB
JMP ADD_NUMB

ADD_NUMB:
MOV AX,A_hex
ADD AX,B_hex
MOV result_hex,AX 
JMP PRINT_RESULT

SUB_NUMB:
MOV AX,A_hex
CMP AX,B_hex
JL GREATER
JMP LESS

GREATER:
MOV AX,B_hex
MOV BX,A_hex
SUB AX,BX
MOV result_hex,AX 
JMP PRINT_RESULT_N
         
LESS:
MOV BX,B_hex
SUB AX,BX
MOV result_hex,AX 
JMP PRINT_RESULT

; print ascii from hex

PRINT_RESULT_N:
MOV AH,02H
MOV DL,'-'
INT 21H
MOV negative,'-'

PRINT_RESULT:
MOV CX,result_hex
MOV AH,02H

MOV DL,CH             ; A[0] = 1st hex digit
AND DL,0F0H
ROR DL,04H
CMP DL,09H
JLE ADD_30H_1         ; if hex digit is greater than 9 then add 07H for ascii correction
ADD DL,07H
ADD_30H_1:
ADD DL,30H
MOV C[00H],DL

MOV DL,CH             ; A[1] = 2nd hex digit
AND DL,0FH
CMP DL,09H
JLE ADD_30H_2         
ADD DL,07H
ADD_30H_2:
ADD DL,30H
MOV C[01H],DL

MOV DL,CL             ; A[2] =  3rd hex digit
AND DL,0F0H
ROR DL,04H
CMP DL,09H
JLE ADD_30H_3
ADD DL,07H
ADD_30H_3:
ADD DL,30H
MOV C[02H],DL

MOV DL,CL             ; A[3] =  4th hex digit
AND DL,0FH
CMP DL,09H
JLE ADD_30H_4
ADD DL,07H
ADD_30H_4:
ADD DL,30H
MOV C[03H],DL


MOV DI,00H            ; print C without zeros at start
PRINT_C_HEX_NZ:
MOV DL,C[DI]
CMP DI,03H
JE  PRINT_C_HEX
INC DI
CMP DL,30H
JE  PRINT_C_HEX_NZ


DEC DI
PRINT_C_HEX:            
MOV DL,C[DI]         
INT 21H
INC DI
CMP DI,03H
JLE PRINT_C_HEX


; fill C with zeros
MOV C[00H],00H
MOV C[01H],00H
MOV C[02H],00H
MOV C[03H],00H
; print ascii from dec
MOV AH,02H
MOV DL,'='
INT 21H

MOV DL,negative
CMP DL,'-'
JE  PRINT_MINUS
JMP PRINT_DEC

PRINT_MINUS:
MOV AH,02H
MOV DL,'-'
INT 21H 
 
PRINT_DEC:
MOV AX,result_hex 
MOV DX,00H
MOV CX,03E8H
DIV CX
ADD AL,30H
MOV C[00H],AL
MOV AX,DX
MOV DX,00H
MOV CX,0064H
DIV CX
ADD AL,30H
MOV C[01H],AL
MOV AX,DX
MOV DX,00H
MOV CX,000AH
DIV CX
ADD AL,30H
MOV C[02H],AL
ADD DL,30H
MOV C[03H],DL

MOV AH,02H
MOV DX,0000H
MOV DI,00H

MOV DI,00H
PRINT_C_NZ:
MOV DL,C[DI]
CMP DI,03H
JE  PRINT_C
INC DI
CMP DL,30H
JE  PRINT_C_NZ


DEC DI
PRINT_C:            
MOV DL,C[DI]         
INT 21H
INC DI
CMP DI,03H
JLE PRINT_C

; new line
MOV DL,0AH
INT 21H  
MOV DL,0DH
INT 21H

JMP START

A_GIVEN:

RET
