

scan_row:
ldi r25,0x08
back:
lsl r25
dec r24
brne back
out PORTC,r25
nop
nop
in r24,PINC
andi r24.0x0f
ret

scan_keypad:
ldi r24,0x01
rcall scan_row
swap r24
mov r27,r24
ldi r24,0x02
rcall scan_row
add r27,r24
ldi r24,0x03
rcall scan_row
swap r24
mov r26,r24
ldi r24, 0x04
rcall scan_row
add r26,r24
movw r24,r26
ret

scan_keypad_rising_edge:
mov r22,r24
rcall scan_keypad
push r24
push r25
mov r24,r22
ldi r25,0
rcall wait_msec
rcall scan_keypad
pop r23
pop r22
and r24,r22
and r25,r23
ldi r26,low(_tmp_)
ldi r27,high(_tmp_)
ld r23,X+
ld r22,X
st X,r24
st -X,r25
com r23
com r22
and r24,r22
and r25,r23
ret

keypad_to_ascii:
movw r26,r24
ldi r24,'*'
sbrc r26,0
ret
ldi r24,'0'
sbrc r26,1
ret
ldi r24,'#'
sbrc r26,2
ret
ldi r24,'D'
sbrc r26,3
ret
ldi r24,'7'
sbrc r26,4
ret
ldi r24,'8'
sbrc r26,5
ret
ldi r24,'9'
sbrc r26,6
ret
ldi r24,'C'
sbrc r26,7
ret
ldi r24,'4'
sbrc r27,0
ret
ldi r24,'5'
sbrc r27,1
ret
ldi r24,'6'
sbrc r27,2
ret
ldi r24,'B'
sbrc r27,3
ret
ldi r24,'1'
sbrc r27,4
ret
ldi r24,'2'
sbrc r27,5
ret
ldi r24,'3'
sbrc r27,6
ret
ldi r24,'A'
sbrc r27,7
ret 
clr r24
ret

