.include "m16def.inc"

.DSEG
_tmp_:.byte 2
.def temp =r16
.def first=r17
.def second=r18
.def conv=r19
.CSEG
.org 0x0


reset:
ldi r24 ,(1 << PC7) | (1 << PC6) | (1 << PC5) | (1 << PC4) 
out DDRC,r24 
ldi r24, low(RAMEND)
out SPL,r24
ldi r24, high(RAMEND)
out SPH,r24
ser r24
;ldi r24,(1 << PD7) | (1 << PD6) | (1 << PD5) | (1 << PD4) | (1 << PD3) | (1 << PD2) 
out DDRD,r24
ser r24
out DDRA,r24
ser r24
out DDRB,r24
rcall lcd_init

read_1st: ;read 1st hex digit
ldi r24,0x14
rcall scan_keypad_rising_edge
mov temp,r24
add temp,r25
cpi temp,0
breq read_1st
rcall keypad_to_hex
mov r22,r24
ldi r24 ,0x01        ;clean lcd screen      
rcall lcd_command  
ldi r24 ,low(1530)  
ldi r25 ,high(1530)  
rcall wait_usec 
mov r24,r22
mov first,r24
rcall lcd_data

read_2nd: ;read 2nd hex digit
ldi r24,0x14
rcall scan_keypad_rising_edge
mov temp,r24
add temp,r25
cpi temp,0
breq read_2nd
rcall keypad_to_hex
mov second,r24
rcall lcd_data


ldi r24,'='
rcall lcd_data

subi first,0x30
cpi first,0x0A
brlo conv_from_ascii_fisrt
rjmp fisrt_to_dec

fisrt_to_dec:
subi first,0x07

conv_from_ascii_fisrt: ; convert first digit from ascii to number
mov conv,first
lsl conv
lsl conv
lsl conv
lsl conv
subi second,0x30
cpi second,0x0A
brlo conv_from_ascii_sec
rjmp second_to_dec

second_to_dec:
subi second,0x07

conv_from_ascii_sec: ;convert second digit from ascii to number
add conv,second
mov r24,conv
ldi r20,0x30
mov temp, conv
cpi temp,128 ;elegxos prosimou
brlo positive


negative:
ldi r24,'-'
rcall lcd_data
neg conv ;symplirwma ws pros 2
ldi temp, 0x00
rjmp hund

positive:
ldi r24,'+'
rcall lcd_data
ldi temp,0x00
rjmp hund

;convert hex number to dec number and display it

hund:
cpi conv,100
brlo deca
subi conv,100
inc temp
jmp hund

deca:
cpi temp,0
breq if_less_hund
mov r24,temp

add r24,r20
rcall lcd_data
cpi conv, 10
brlo no_mon
ldi temp,0x00
jmp deca

if_less_hund:
cpi conv,10
brlo mon
subi conv,10
inc temp
jmp if_less_hund

mon:
cpi temp,0
breq end
mov r24,temp
;out  PORTB,r24
add r24,r20
rcall lcd_data
mov r24,conv
out PORTA, r24
add r24,r20
rcall lcd_data
rjmp read_1st

no_mon:
ldi r24,0x00
add r24,r20
rcall lcd_data

end:
mov r24,conv
;out PORTA, r24
add r24,r20
rcall lcd_data
rjmp read_1st



;;;;;;;;;;;;;;;;
;routines
;;;;;;;;;;;;;;;;

scan_row:
ldi r25,0x08
back:
lsl r25
dec r24
brne back
out PORTC,r25
nop
nop
in r24,PINC
andi r24,0x0f
ret

scan_keypad:
ldi r24,0x01
rcall scan_row
swap r24
mov r27,r24
ldi r24,0x02
rcall scan_row
add r27,r24
ldi r24,0x03
rcall scan_row
swap r24
mov r26,r24
ldi r24, 0x04
rcall scan_row
add r26,r24
movw r24,r26
ret

scan_keypad_rising_edge:
mov r22,r24
rcall scan_keypad
push r24
push r25
mov r24,r22
ldi r25,0
rcall wait_msec
rcall scan_keypad
pop r23
pop r22
and r24,r22
and r25,r23
ldi r26,low(_tmp_)
ldi r27,high(_tmp_)
ld r23,X+
ld r22,X
st X,r24
st -X,r25
com r23
com r22
and r24,r22
and r25,r23
ret

keypad_to_hex:
movw r26,r24
ldi r24,'E'
sbrc r26,0
ret
ldi r24,'0'
sbrc r26,1
ret
ldi r24,'F'
sbrc r26,2
ret
ldi r24,'D'
sbrc r26,3
ret
ldi r24,'7'
sbrc r26,4
ret
ldi r24,'8'
sbrc r26,5
ret
ldi r24,'9'
sbrc r26,6
ret
ldi r24,'C'
sbrc r26,7
ret
ldi r24,'4'
sbrc r27,0
ret
ldi r24,'5'
sbrc r27,1
ret
ldi r24,'6'
sbrc r27,2
ret
ldi r24,'B'
sbrc r27,3
ret
ldi r24,'1'
sbrc r27,4
ret
ldi r24,'2'
sbrc r27,5
ret
ldi r24,'3'
sbrc r27,6
ret
ldi r24,'A'
sbrc r27,7
ret 
clr r24
ret

write_2_nibbles: 
push r24         
in r25 ,PIND       
andi r25 ,0x0f  
andi r24 ,0xf0        
add r24 ,r25     
out PORTD ,r24            
sbi PORTD ,PD3   
cbi PORTD ,PD3  
pop r24                  
swap r24  
andi r24 ,0xf0   
add r24 ,r25 
out PORTD ,r24              
sbi PORTD ,PD3  
cbi PORTD ,PD3 
ret
;-------------
lcd_data:                
sbi PORTD ,PD2       
rcall write_2_nibbles     
ldi r24 ,43                
ldi r25 ,0                   
rcall wait_usec  
ret
;-------------
lcd_command: 
cbi PORTD ,PD2        
rcall write_2_nibbles    
ldi r24 ,39                   
ldi r25 ,0                    
rcall wait_usec          
ret   
;------------
lcd_init:      
ldi r24 ,40            
ldi r25 ,0           
rcall wait_msec      
ldi r24 ,0x30          
out PORTD ,r24                  
sbi PORTD ,PD3        
cbi PORTD ,PD3         
ldi r24 ,39  
ldi r25 ,0                      
rcall wait_usec                                                  
                         
ldi r24 ,0x30         
out PORTD ,r24       
sbi PORTD ,PD3  
cbi PORTD ,PD3  
ldi r24 ,39  
ldi r25 ,0  
rcall wait_usec   
ldi r24 ,0x20                   
out PORTD ,r24          
sbi PORTD ,PD3  
cbi PORTD ,PD3  
ldi r24 ,39  
ldi r25 ,0  
rcall wait_usec       
ldi r24 ,0x28         
rcall lcd_command      
ldi r24 ,0x0c             
rcall lcd_command       
ldi r24 ,0x01              
rcall lcd_command  
ldi r24 ,low(1530)  
ldi r25 ,high(1530)  
rcall wait_usec      
ldi r24 ,0x06             
rcall lcd_command                                                                                       
ret  


wait_usec:
sbiw r24 ,1 
nop 
nop 
nop 
nop 
brne wait_usec
ret 

wait_msec:
push r24 
push r25 
ldi r24 , low(998)  
ldi r25 , high(998) 
rcall wait_usec 
pop r25 
pop r24 
sbiw r24 , 1 
brne wait_msec
ret


