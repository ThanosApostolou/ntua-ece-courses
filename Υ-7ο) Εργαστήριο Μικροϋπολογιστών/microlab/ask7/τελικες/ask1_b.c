#include <avr/io.h>

void TransmitUSART (uint8_t data) // transmit a byte to UDR
{
    while (!(UCSRA & (1<<UDRE))); // wait until usart is ready to transmit
    UDR=data;
}

uint8_t  ReceiveUSART()  // return a byte from UDR
{
    while (!(UCSRA & (1<<RXC))); // wait until usart is ready to receive
    return UDR;
}

// transmit a string by transmiting 1 character at a time
void transmit_text (const char *astring) {
    while ((*astring)) {
        TransmitUSART(*astring++);
    }
}

int main(void) {
   uint8_t  temp;
    // (USART initialization)
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART Receiver: On,  Transmitter: On
    // USART Mode: Asynchronous,  Baud rate: 9600  (Fck=4 MHz)
    // Initialize similar to the assembly code
    UCSRA=0x00;
    UCSRB=(1<<RXEN) | (1<<TXEN);
    UCSRC=(1<<URSEL) | (3<<UCSZ0);
    UBRRH=0x00;
    UBRRL=0x33;
    while (1){
        temp=ReceiveUSART(); // read a byte and keep i in temp
        if (temp < '0' || temp > '8') {
            // if it's not between '0' and '8'
            transmit_text ("INVALIDNUMBER");
            TransmitUSART ('\n');
        } else {
            // if it is between '0' and '8'
            transmit_text ("Read");
            TransmitUSART (temp);
            TransmitUSART ('\n');
            temp = temp - '0'; // get the arithmetic value from ascii
            DDRC = 0xFF; // configure PORTC for output
            // light up the needed led
            if (temp == 0) {
                PORTC = 0;
            } else {
                PORTC = 0 | (1<<(temp-1));
            }
        }
    }
    return 0;
}
