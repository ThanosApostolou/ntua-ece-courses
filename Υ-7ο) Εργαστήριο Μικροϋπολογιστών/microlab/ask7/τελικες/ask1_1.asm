.include "m16def.inc"

.DSEG

.CSEG
.org 0x00
astring: .db "tziova kourepsou",'\0'

rjmp main

main:
 ldi r24,high(RAMEND) ;initialize stack pointer
 out SPH,r24
 ldi r24,low(RAMEND)
 out SPL,r24
 rcall usart_init ;initialize usart
again:
 rcall transmit_astring
rjmp again



;-------------- our routines

transmit_astring: ; read and write every char from the string to usart_transmit
 push ZL
 push ZH
 push r24
 ldi ZL,low(astring)
 ldi ZH,high(astring)
 transmit_loop:
  lpm r24,Z+
  cpi r24,'\0'
  rcall usart_transmit
  brne transmit_loop
 ldi r24,'\n'
 rcall usart_transmit
 pop r24
 pop ZH
 pop ZL
 ret

;-------------- given routines

usart_init:
 clr r24             ; initialize UCSRA to zero
 out UCSRA ,r24
 ldi r24 ,(1<<RXEN) | (1<<TXEN)      ; activate transmitter/receiver
 out UCSRB ,r24
 ldi r24 ,0 ; baud rate = 9600
 out UBRRH ,r24
 ldi r24 ,51
 out UBRRL ,r24
 ldi r24 ,(1 << URSEL) | (3 << UCSZ0) ; 8-bit character size,
 out UCSRC ,r24 ; 1 stop bit
 ret
 

usart_transmit:
 sbis UCSRA ,UDRE ; check if usart is ready to transmit
 rjmp usart_transmit ; if no check again, else transmit
 out UDR ,r24        ; content of r24
 ret
 
usart_receive:
 sbis UCSRA ,RXC ; check if usart received byte
 rjmp usart_receive  ; if no check again, else read
 in r24 ,UDR         ; receive byte and place it in
 ret   ;r24
