#include <avr/io.h>
 
void TransmitUSART (uint8_t data) // ��������� ��� �������� ��������� 
{
    while (!(UCSRA & (1<<UDRE)));
    UDR=data;
}
 
uint8_t  ReceiveUSART()  // ��������� ��� ���� ���������
{
    while (!(UCSRA & (1<<RXC)));
    return UDR;
}
 
void transmit_text (const char *astring) {
 
    while ((*astring)) {
        TransmitUSART(*astring++);
        //*astring++;
    }
}
 
int main(void) {
   uint8_t  temp;
    // ������������ ��������� (USART initialization)
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART Receiver: On,  Transmitter: On
    // USART Mode: Asynchronous,  Baud rate: 9600  (Fck=4 MHz)
    UCSRA=0x00;  
    UCSRB=(1<<RXEN) | (1<<TXEN); // 0001 1000 ������ RXEN=�XEN=1
    UCSRC=(1<<URSEL) | (3<<UCSZ0);   // 1000 0110 ������ URSEL =1, USBS=0, UCSZ1= UCSZ0=1
    UBRRH=0x00;   // 4000000/(9600*16)-1=25 ��� 4M�z�� ������=0,2%
    UBRRL=0x33;  // UBRR=25
    while (1){
        temp=ReceiveUSART();// ���������� ���������
        if (temp < '0' || temp > '8') {
            transmit_text ("INVALIDNUMBER");
            TransmitUSART ('\n');
        } else {
            transmit_text ("Read");
            TransmitUSART (temp);
            TransmitUSART ('\n');
            temp = temp - '0';
            DDRC = 0xFF;
            if (temp == 0) {
                PORTC = 0;
            } else {
                PORTC = 0 | (1<<(temp-1));
            }
        }
    }
    //return 0;
}
