 ; Routine: usart_init
 ; Description:
 ; This routine initializes the
 ; usart as shown below.
 ; ------- INITIALIZATIONS -------
  ; Baud rate: 9600 (Fck= 8MHz)
 ; Asynchronous mode
 ; Transmitter on
 ; Reciever on
 ; Communication parameters: 8 Data ,1 Stop , no Parity
 ; --------------------------------
 ; parameters: None.
 ; return value: None.
 ; registers affected: r24
 ; routines called: None
 
 usart_init:
 clr r24             ; initialize UCSRA to zero
 out UCSRA ,r24
 ldi r24 ,(1<<RXEN) | (1<<TXEN)      ; activate transmitter/receiver
 out UCSRB ,r24
 ldi r24 ,0 ; baud rate = 9600
 out UBRRH ,r24
 ldi r24 ,51
 out UBRRL ,r24
 ldi r24 ,(1 << URSEL) | (3 << UCSZ0) ; 8-bit character size,
 out UCSRC ,r24 ; 1 stop bit
 ret
 
 ; Routine: usart_transmit
 ; Description:
 ; This routine sends a byte of data
 ; using usart.
 ; parameters:
 ; r24: the byte to be transmitted
 ; must be stored here.
 ; return value: None.
 ; registers affected: r24
 ; routines called: None.
 
 usart_transmit:
 sbis UCSRA ,UDRE ; check if usart is ready to transmit
 rjmp usart_transmit ; if no check again, else transmit
 out UDR ,r24        ; content of r24
 ret
 
 ; Routine: usart_receive
 ; Description:
 ; This routine receives a byte of data
 ; from usart.
 ; parameters: None.
 ; return value: the received byte is
 ; returned in r24.
 ; registers affected: r24
 ; routines called: None.
 
 usart_receive:
 sbis UCSRA ,RXC ; check if usart received byte
 rjmp usart_receive  ; if no check again, else read
 in r24 ,UDR         ; receive byte and place it in
 ret   ;r24
 
 
  ; Routine: usart_init
  ; Description:
  ; This routine initializes the
  ; ADC as shown below.
  ; ------- INITIALIZATIONS -------
  ; 
  ;Vref: Vcc (5V for easyAVR6)
  ; Selected pin is A0
  ; ADC Interrupts are Enabled
  ; Prescaler is set as CK/128 = 62.5kHz
  ; --------------------------------
  ; parameters: None.
  ; return value: None.
  ; registers affected: r24
  ; routines called: None
  ADC_init:
  Ldi r24,(1<<REFS0) ; Vref: Vcc
  Out ADMUX,r24 ;MUX4:0= 00000 forA0.
  ;ADC is Enabled (ADEN=1)
  ;ADC Interrupts are Enabled (ADIE=1)
  ;SetPrescaler CK/128 = 62.5Khz (ADPS2:0=111)
  ldi r24,(1<<ADEN)|(1<<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)
  out ADCSRA,r24
  ret