//input C0-C5 (A-E)
//output A0-A3 (F0-F2)
//FO = (AB+BC+CD+DE)'
//F1 = ABCD+D'E'
//F2 = F0+F1

#include<avr/io.h>
#include<stdbool.h> 
int main()
{
	DDRC = 0x00;		//configure portB as input
	DDRA = 0xFF;		//configure portC as output
    
	unsigned char input, result;
	bool A,B,C,D,E,F0,F1,F2;

	while(1)
	{
		input = PINC;
		
		A = input & 1;
		B = input & 2;
		C = input & 4;
		D = input & 8;
		E = input & 16;
				
		F0 = !((A && B) || (B && C) || (C && D) || (D && E));
		F1 = ((A && B && C && D) || (!(D || E)));
		F2 = F0 || F1;
		
		result = 0;
		if (F0) {
			result++;
		}
		if (F1) {
			result += 2;
		}
		if (F2) {
			result += 4;
		}

		PORTA = result;

	}
	return 0;
}

