ORG 100H

.DATA
msg1 db 0Dh,0Ah, "GIVE 3 DIGITS: $"
msg2 db 0Dh,0Ah, "HEX= $"
A DB 3 DUP(?)         ; array with the 3 decimal numbers

.CODE
START:
MOV DX, offset msg1   ; print msg1
MOV AH, 09H
INT 21H
MOV DI,00H
MOV AH,08H            ; INT 21H reads into AL

READ_FIRST_3_DEC:
INT 21H       
CMP AL,51H
JE  Q_GIVEN           ; if Q is given jump to Q_GIVEN
CMP AL,30H
JL  READ_FIRST_3_DEC
CMP AL,39H
JG  READ_FIRST_3_DEC  ; if digit is given (0 to 9) then continue     
MOV A[DI],AL
INC DI
CMP DI,03H             
JGE READ_NEXT_DEC     ; if more than 3 numbers are given
JMP READ_FIRST_3_DEC  ; loop until Q is given or read 3 numbers

READ_NEXT_DEC:
INT 21H
CMP AL,51H
JE  Q_GIVEN           ; if Q is given jump to Q_GIVEN
CMP AL,0DH
JE  ENTER_GIVEN       ; if ENTER is given jump to ENTER_GIVEN
CMP AL,30H
JL  READ_NEXT_DEC
CMP AL,39H
JG  READ_NEXT_DEC     ; if digit is given (0 to 9) then continue     
MOV DH,A[01H]
MOV A[00H],DH
MOV DH,A[02H]
MOV A[01H],DH
MOV A[02H],AL         ; A[0] <- A[1] <- A[2] <- DL
JMP READ_NEXT_DEC     ; loop until Q or ENTER is given
  

ENTER_GIVEN:
MOV AH,02H            ; INT 21H prints DL            
MOV DL,A[00H]         ; print the 3 given decimal digits
INT 21H
MOV DL,A[01H]          
INT 21H
MOV DL,A[02H]          
INT 21H          
MOV AH, 09H           ; print msg2 
MOV DX, offset msg2
INT 21H

; calculate hex number from decimal digits
MOV AL,A[00H]         ; get 1st digit
SUB AL,30H            ; substract 30H==40 to get decimal value from ascii
MOV BL,64H            ; multiply with 64H==100 to get hundreds
MUL BL
MOV CX,AX             ; store it to CX

MOV AL,A[01H]         ; get 2nd digit
SUB AL,30H            ; substract 30H==40 to get decimal value from ascii
MOV BL,0AH            ; multiply with 0AH==10 to get tens
MUL BL                                                   
ADD CX,AX             ; add it to CX

MOV AL,A[02H]         ; get 3rd digit
SUB AL,30H            ; substract 30H==40 to get decimal value from ascii
MOV AH,00H                                                   
ADD CX,AX             ; add it to CX

; CX now has the hexadecimal value
; turn each hex digit to ascii and print it
MOV AH,02H

MOV DL,CH             ; print 1st hex digit
AND DL,0FH
CMP DL,09H
JLE ADD_30H_1         ; if hex digit is greater than 9 then add 07H for ascii correction
ADD DL,07H
ADD_30H_1:
ADD DL,30H
INT 21H

MOV DL,CL             ; print 2nd hex digit
AND DL,0F0H
ROR DL,04H
CMP DL,09H
JLE ADD_30H_2
ADD DL,07H
ADD_30H_2:
ADD DL,30H
INT 21H

MOV DL,CL             ; print 3rd hex digit
AND DL,0FH
CMP DL,09H
JLE ADD_30H_3
ADD DL,07H
ADD_30H_3:
ADD DL,30H
INT 21H
                      
JMP START

Q_GIVEN:

RET
