ORG 100H

.DATA
msg1 db 0Dh,0Ah, "GIVE 4 OCTAL DIGITS: $"
msg2 db 0Dh,0Ah, "Decimal: $"
;msg3 db 0Dh,0Ah, ".$"
A DB 4 DUP(?)         ; array with the 4 numbers


.CODE
START:
CALL PRINT_MSG1
MOV DI,00H
MOV AH,08H 

READ_4_OCT:
INT 21H       
CMP AL,43H
JE  C_GIVEN           ; if C is given jump to Q_GIVEN
CMP AL,30H
JL  READ_4_OCT
CMP AL,37H
JG  READ_4_OCT  ; if digit is given (0 to 9) then continue     
MOV A[DI],AL
INC DI
CMP DI,04H             
JE ENTER_GIVEN
JMP READ_4_OCT

ENTER_GIVEN:
MOV AH,02H            ; INT 21H prints DL            
MOV DL,A[00H]         ; print the 3 given decimal digits
INT 21H
MOV DL,A[01H]          
INT 21H
MOV DL,A[02H]          
INT 21H               
MOV DL,2EH
INT 21H               ; print dot
MOV DL,A[03H]
INT 21H        
CALL PRINT_MSG2

MOV AH,00H
MOV AL,A[00H]
SUB AL,30H
MOV BL,40H
MUL BL
MOV CX,AX

MOV AH,00H
MOV AL,A[01H]
SUB AL,30H
MOV BL,08H
MUL BL
ADD CX,AX

MOV AL,A[02H]
SUB AL,30H
ADD CX,AX

MOV AH,00H
MOV AL,A[03H]
SUB AL,30H
MOV BL,7DH
MUL BL
MOV BX,AX

MOV AX,CX
MOV CL,64H
DIV CL
MOV A[00H],AL
MOV AL,AH
MOV AH,00H
MOV CL,0AH
DIV CL
MOV A[01H],AL
MOV A[02H],AH

MOV DI,00H
MOV AH,02H

PRINT1:
MOV DL,A[DI]
ADD DL,30H
INT 21H
INC DI
CMP DI,03H
JL PRINT1

; print dot
MOV DL,2EH
INT 21H

MOV AX,BX
MOV CL,64H
DIV CL
MOV A[00H],AL
MOV AL,AH
MOV AH,00H
MOV CL,0AH
DIV CL
MOV A[01H],AL
MOV A[02H],AH


MOV DI,00H
MOV AH,02H

PRINT2:
MOV DL,A[DI]
ADD DL,30H
INT 21H
INC DI
CMP DI,03H
JL PRINT2

JMP START

C_GIVEN:
HLT 
 
;;;; common routines ;;;;
 
PRINT_MSG1:
PUSH DX
PUSH AX
MOV DX, offset msg1
MOV AH, 09H
INT 21H
POP AX
POP DX
RET

PRINT_MSG2:
PUSH DX
PUSH AX
MOV DX, offset msg2
MOV AH, 09H
INT 21H
POP AX
POP DX
RET
;;;;;;;;;;;;;;;;

END
