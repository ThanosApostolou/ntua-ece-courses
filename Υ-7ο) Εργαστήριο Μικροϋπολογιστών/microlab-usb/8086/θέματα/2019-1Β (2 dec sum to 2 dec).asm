; Read 2 decimals
; calculate sum=x+y (hex)
; if sum>=16 (dec) prin "OVF" else print "SUM="sum

ORG 100H

.DATA
msg1 db 0Dh,0Ah, "DOSE 2 ARITHMOUS: $"
msg2 db 0Dh,0Ah, "SUM: $"
msg3 db 0Dh,0Ah, "OVF$"
A DB 4 DUP(?)         ; array with the 4 numbers

.CODE
START:
CALL PRINT_MSG1 

MOV DI,02h
CALL READ_DECS

MOV DI,02h
CALL PRINT_CHARS    ; print 2 chars

MOV AL,A[00H]
SUB AL,30H          ; AL = x

MOV AH,A[01H]
SUB AH,30H          ; AH = y

ADD AL,AH           ; AL = x+y

CMP AL,0x10h
JAE GREATER_15
CALL PRINT_MSG2


; Turn AL hex to decimal in BH-BL-AL
CALL HEX_TO_3DEC

MOV A[00h],BL
MOV A[01h],AL
MOV DI,02h
CALL PRINT_NUMBERS
JMP START

GREATER_15:
CALL PRINT_MSG3

JMP START 
 





;;;; common routines ;;;;

PRINT_CHAR:  ;Print char from DL
PUSH AX
MOV AH,02h
INT 21H
POP AX
RET

PRINT_DIGIT:  ;Print digit from DL
PUSH AX
PUSH DX
MOV AH,02h
ADD DL,'0'
INT 21H
POP DX
POP AX
RET

PRINT_HEX:    ;Print hex from DL
PUSH AX
PUSH DX
ADD DL,'0'
CMP DL,'9'
JBE PRINT_HEX_FINAL      
ADD DL,07h  ; if greater than '9' fix ascii
PRINT_HEX_FINAL:
MOV AH,02h
INT 21H
POP DX
POP AX
RET

ASCII_TO_HEX:   ; ascii DL to hex DL
SUB DL,'0'
CMP DL,09h
JBE ASCII_TO_HEX_FINISH  
SUB DL,07h
ASCII_TO_HEX_FINISH:
RET

ASCII_TO_DIGIT:   ; ascii DL to hex DL
SUB DL,'0'
RET

; Turn AL hex to decimal in BH-BL-AL
HEX_TO_3DEC:
MOV BX,0000h
HUND:       ; hundreads to BH
CMP AL,64h
JB DEC
SUB AL,64h
INC BH
JMP HUND
DEC:        ; decades to BL
CMP AL,0Ah
JB MON
SUB AL,0AH
INC BL
JMP DEC
MON:        ; monades are left to AL
RET

READ_DECS:    ; Read DI numbers to A[] array
PUSH AX
PUSH BX
PUSH DI
MOV BX,DI
MOV DI,00H
MOV AH,08H
READ_DECS_LOOP:
INT 21H       
CMP AL,'T'
JE  END           ; if T is given jump to END
CMP AL,'0'
JL  READ_DECS_LOOP
CMP AL,'9'
JG  READ_DECS_LOOP ; if digit is given (0 to 9) then continue     
MOV A[DI],AL
INC DI
CMP DI,BX             
JE  READ_DECS_FINISH
JMP READ_DECS_LOOP
READ_DECS_FINISH:   
POP DI
POP BX
POP AX
RET

READ_OCTALS:     ; Read DI numbers to A[] array
PUSH AX
PUSH BX
PUSH DI
MOV BX,DI
MOV DI,00H
MOV AH,08H
READ_OCTALS_LOOP:
INT 21H       
CMP AL,'T'
JE  END           ; if T is given jump to END
CMP AL,'0'
JL  READ_OCTALS_LOOP
CMP AL,'7'
JG  READ_OCTALS_LOOP ; if digit is given (0 to 9) then continue     
MOV A[DI],AL
INC DI
CMP DI,BX             
JE  READ_OCTALS_FINISH
JMP READ_OCTALS_LOOP
READ_OCTALS_FINISH:   
POP DI
POP BX
POP AX
RET

READ_HEXALS:      ; Read DI numbers to A[] array
PUSH AX
PUSH BX
PUSH DI
MOV BX,DI
MOV DI,00H
MOV AH,08H
READ_HEXALS_LOOP:
INT 21H       
CMP AL,'T'
JE  END           ; if T is given jump to END
CMP AL,'0'
JL  READ_HEXALS_LOOP
CMP AL,'9'
JG  NOT_DIGIT: ; if digit is given (0 to 9) then continue
JMP READ_HEXALS_CONTINUE
NOT_DIGIT:
CMP AL,'A'
JL  READ_HEXALS_LOOP    
CMP AL,'F'
JG  READ_HEXALS_LOOP: ; if char is ('A' to 'F') then continue
READ_HEXALS_CONTINUE:
MOV A[DI],AL
INC DI
CMP DI,BX             
JE  READ_HEXALS_FINISH
JMP READ_HEXALS_LOOP
READ_HEXALS_FINISH:   
POP DI
POP BX
POP AX
RET

;;; PRINT DI chars from A[]
PRINT_CHARS:
PUSH AX
PUSH BX
PUSH DX
PUSH DI
MOV BX,DI
MOV DI,00h
MOV AH,02h              ;INT 21H behave as print
PRINT_CHARS_LOOP:
MOV DL,A[DI]
INT 21H
INC DI
CMP DI,BX
JL PRINT_CHARS_LOOP
POP DI
POP DX
POP BX
POP AX
RET

; Print DI numbers from A[]
PRINT_NUMBERS:
PUSH AX
PUSH BX
PUSH DX
PUSH DI
MOV BX,DI
MOV DI,00h
MOV AH,02h              ;INT 21H behave as print
PRINT_NUMBERS_LOOP:
MOV DL,A[DI]
ADD DL,'0'              ;turn number to ascii
INT 21H
INC DI
CMP DI,BX
JL PRINT_NUMBERS_LOOP
POP DI
POP DX
POP BX
POP AX
RET
 
PRINT_MSG1:
PUSH DX
PUSH AX
MOV DX, offset msg1
MOV AH, 09H
INT 21H
POP AX
POP DX
RET

PRINT_MSG2:
PUSH DX
PUSH AX
MOV DX, offset msg2
MOV AH, 09H
INT 21H
POP AX
POP DX
RET

PRINT_MSG3:
PUSH DX
PUSH AX
MOV DX, offset msg3
MOV AH, 09H
INT 21H
POP AX
POP DX
RET
;;;;;;;;;;;;;;;;

END:
END
