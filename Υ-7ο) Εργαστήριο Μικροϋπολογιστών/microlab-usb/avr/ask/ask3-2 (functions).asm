;input C0-C5 (A-E)
;output A0-A3 (F0-F2)
;FO = (AB+BC+CD+DE)'
;F1 = ABCD+D'E'
;F2 = F0+F1

.include "m16def.inc"
.def A=r16
.def B=r17
.def C=r18
.def D=r19
.def E=r20
.def input=r21
.def F0=r22
.def F1=r23
.def temp1=r24
.def temp2=r25
.def result=r26

start: 
ldi temp1,low(RAMEND)   ; init stack
out SPL,temp1
ldi temp1,high(RAMEND)
out SPH,temp1

ser temp1
out DDRA,temp1          ; port A is output
ldi temp1,224
out DDRC,temp1         ; port C0,C1,C2,C3, is input

clr A
clr B
clr C
clr D
clr E
clr input
clr temp1
clr temp2
clr F0
clr F1
clr result

main:


in input,PINC

mov A,input
andi A,1

mov B,input
andi B,2
ror B

mov C,input
andi C,4
ror C
ror C

mov D,input
andi D,8
ror D
ror D
ror D

mov E,input
andi E,16
ror E
ror E
ror E
ror E

;calc_F0
mov temp1,A
and temp1,B
mov F0,temp1  ; AB

mov temp1,B
and temp1,C
or F0,temp1   ; AB+BC

mov temp1,C
and temp1,D
or F0,temp1   ; AB+BC+CD

mov temp1,D
and temp1,E
or F0,temp1   ; AB+BC+CD+DE

com F0
andi F0,1     ; (AB+BC+CD+DE)'
mov result,F0

;calc_F1
mov F1,D
or F1,E
com F1         ; (D+E)' == D'E'

mov temp1,A
and temp1,B
and temp1,C
and temp1,D    ; ABCD

or F1,temp1
andi F1,1      ; ABCD+D'E'
clc
rol F1
or result,F1   ; 

; calc_F2
mov temp1,F1
clc
ror temp1
or temp1,F0    ; F0+F1

andi temp1,1
clc
rol temp1
clc
rol temp1
or result,temp1 ; add F2 to 3rd led


out PORTA,result

jmp main
