; normal counter at PORTA
; if INT1 (PD3) then show led count of PORTB at PORTC

.include "m16def.inc"
.def temp=r24
.def counter=r16
.def input=r17
.def switches=r18
.org 0x0
rjmp start
.org 0x4
rjmp ISR1

start:
ldi temp , low(RAMEND)    ; initialize stack pointer 
out SPL , temp  
ldi temp , high(RAMEND)  
out SPH , temp

ser temp
out DDRA,temp         ;output
ldi temp, 0b00001111  ;output at 4 LSB
out DDRC,temp
clr temp
out DDRB,temp         ;input

rcall ENABLE_INT1

main:
clr counter
loop1:
out PORTA,counter
rcall wait
inc counter
rjmp loop1          

;------------------
; interrupt INT1
ISR1:
rcall FIX_INT1_SPIN
in input,PINB
clc            ; clear carry
clr switches
clr temp
loop_8:        ; rol 8 times and get number of carries (switches on)
rol input
brcc NO_CARRY
inc switches
NO_CARRY:
inc temp
cpi temp,8
brne loop_8
out PORTC,switches
reti

;;;; common routines ;;;;
; Enable INT0
ENABLE_INT0:
push r24
ldi r24,(1<<ISC01)|(1<<ISC00)
out MCUCR, r24
ldi r24, (1<<INT0)
out GICR, r24
pop r24
sei
ret

; Enable INT1
ENABLE_INT1:
push r24
ldi r24,(1<<ISC11)|(1<<ISC10)
out MCUCR, r24
ldi r24, (1<<INT1)
out GICR, r24
pop r24
sei
ret

; fix spinthirismo for interrupt INTF0
FIX_INT0_SPIN:
push r24
push r25
ldi r24,(1<<INTF0)
out GIFR,r24
ldi r24, low(5)
ldi r25, high(5)
rcall wait_msec
in r24,GIFR
andi r24,0b01000000
cpi r24,0b01000000
breq FIX_INT0_SPIN
pop r25
pop r24
ret

; fix spinthirismo for interrupt INTF1
FIX_INT1_SPIN:
push r24
push r25
ldi r24,(1<<INTF1)
out GIFR,r24
ldi r24, low(5)
ldi r25, high(5)
rcall wait_msec
in r24,GIFR
andi r24,0b01000000
cpi r24,0b01000000
breq FIX_INT1_SPIN
pop r25
pop r24
ret

; wait for 200 ms
wait:
push r24
push r25
ldi r24,low(200)
ldi r25,high(200)
rcall wait_msec
pop r25
pop r24
ret

wait_usec:
sbiw r24,1
nop
nop
nop
nop
brne wait_usec
ret

wait_msec:
push r24
push r25
ldi r24,low(998)
ldi r25,high(998)
rcall wait_usec
pop r25
pop r24
sbiw r24,1
brne wait_msec
ret 
;;;;;;;;;;;;;;;;
