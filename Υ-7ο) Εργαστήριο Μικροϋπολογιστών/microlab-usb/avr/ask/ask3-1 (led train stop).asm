; 1 led train at PORTA move every 0.5 sec
; button b0 stops it

.include "m16def.inc"
.def counter=r16
.def input=r28
.def temp=r24

start: 
ldi temp,low(RAMEND)   ; init stack
out SPL,temp
ldi temp,high(RAMEND)
out SPH,temp


ser temp
out DDRA,temp           ; Port A output
ldi temp,254
out DDRB,temp           ; Port B0 input

clr counter             ; counter = 0
clr r18
main:
ldi counter,1
out PORTA,counter

upwards:
rcall read_input
out PORTA,counter
rcall wait
rol counter
cpi counter,128
breq downwards
rjmp upwards

downwards:
rcall read_input
out PORTA,counter
rcall wait
ror counter
cpi counter,1
breq upwards
jmp downwards

;;;; common routines ;;;;


; freeze while B = 1, else return input
read_input:
clr input
in input,PINB
cpi input,1
breq read_input    ; freeze while B = 1
ret

; wait for 500 ms
wait:
push r24
push r25
ldi r24,low(500)
ldi r25,high(500)
rcall wait_msec
pop r25
pop r24
ret

wait_usec:
sbiw r24,1
nop
nop
nop
nop
brne wait_usec
ret

wait_msec:
push r24
push r25
ldi r24,low(998)
ldi r25,high(998)
rcall wait_usec
pop r25
pop r24
sbiw r24,1
brne wait_msec
ret 
