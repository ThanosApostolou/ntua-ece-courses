; INT0 (PD2) or PB0 light PA7 for 3 sec
; Press again when already on, lights all PA for 0.5 sec
; and renews PA7 time

.include "m16def.inc"
.def input=r16
.def on=r21
.def counter=r23
.def temp=r24

.org 0x0
rjmp start
.org 0x2
rjmp ISR0            ; int0 interrupt
.org 0x10
rjmp ISR_TIMER1_OVF  ; timer overflow interrupt
;----------

start:

ldi temp,high(ramend) ; initialize stack pointer
out SPH,temp
ldi temp,low(ramend)
out SPL,temp

clr temp
out DDRB,temp      ; eisodos/e3odos
ser temp
out DDRA,temp

clr on
rcall INIT_TCNT1
rcall ENABLE_INT0

;-----------------ELEGXOS GIA TIMH PB0
check:
sei            
in r17,pinb      ;elegxos gia pb0
andi r17,0x01
cpi r17,0x01
breq led_on1
jmp check

;------------------------

led_on1:
;===============XRONOKA8YSTERHSH GIA ANTIMETVPISH TOU SPIN8HRISMOU
cpi on,0x01
breq led_on
trap:
in r17,pinb      ;elegxos gia pb0
andi r17,0x01
cpi r17,0x00
brne trap

led_on:
cpi on,0x00
breq first_time   ; ELEGXOS AN PROKEITE GIA APLO ANAMMA H ANANEWSH MESW TOU REG ON (<- H TIMH TOU KA8ORIZETAI ME VASH AN TO PA7 EINAI ON H OFF GIA ON=1 GIA OFF=0)
ldi r22,0x01
ldi r18,0xff

;.............................0,5 SECONDS OLA TA LED TOU PORT A ANAMMENA 

out porta,r18
ldi on,0x01
rcall SET_TCNT1_05
jmp check

;.......................2,5 SECONDS EPIPLEON GIA TO PA7

refresh:
ldi r18,0x80
out porta,r18
ldi on,0x01
rcall SET_TCNT1_25
jmp check

;APLO ANAMMA(XWRIS ANANEWSH)
first_time:
ldi r18,0x80
out porta,r18
ldi on,0x01
rcall SET_TCNT1_3
jmp check

;ROUTINA GIA E3YPHRETHSH DIAKOPWN
ISR0:
rcall FIX_INT0_SPIN
push on
push r22
push r26
rcall led_on
pop r26
pop r22
pop on
reti

; ROUTINA GIA E3YPHRETHSH YPERXEILHSHS XRONISTH
ISR_TIMER1_OVF:
ldi r24,0x00
cpi r22,0x01
breq rr
ldi r18,0x00
out porta,r18
ldi on,0x00
jmp check
rr:
ldi r22,0x00
jmp refresh


;;;;;;;; COMMON ROUTINES ;;;;;;;;

; Set TCNNT1 for 0.5 sec
SET_TCNT1_05:
push r24
ldi r24,0xf0
out TCNT1H,r24
ldi r24,0xbe
out TCNT1L,r24
pop r24
ret
; Set TCNNT1 for 2.5 sec
SET_TCNT1_25:
push r24
ldi r24,0xb3
out TCNT1H,r24
ldi r24,0xb5
out TCNT1L,r24
pop r24
ret
; Set TCNNT1 for 3 sec
SET_TCNT1_3:
push r24
ldi r24,0xA4
out TCNT1H ,r24
ldi r24 ,0x73
out TCNT1L ,r24
pop r24
ret


; Init Timer TCNT1
INIT_TCNT1:
push r24
ldi r24 ,(1<<TOIE1)
out TIMSK ,r24
ldi r24 ,(1<<CS12) | (0<<CS11) | (1<<CS10) ; CK/1024
out TCCR1B ,r24
pop r24
ret

; Enable INT0
ENABLE_INT0:
push r24
ldi r24,(1<<ISC01)|(1<<ISC00)
out MCUCR, r24
ldi r24, (1<<INT0)
out GICR, r24
pop r24
sei
ret

; Enable INT1
ENABLE_INT1:
push r24
ldi r24,(1<<ISC11)|(1<<ISC10)
out MCUCR, r24
ldi r24, (1<<INT1)
out GICR, r24
pop r24
sei
ret

; fix spinthirismo for interrupt INTF0
FIX_INT0_SPIN:
push r24
push r25
ldi r24,(1<<INTF0)
out GIFR,r24
ldi r24, low(5)
ldi r25, high(5)
rcall wait_msec
in r24,GIFR
andi r24,0b01000000
cpi r24,0b01000000
breq FIX_INT0_SPIN
pop r25
pop r24
ret

; fix spinthirismo for interrupt INTF1
FIX_INT1_SPIN:
push r24
push r25
ldi r24,(1<<INTF1)
out GIFR,r24
ldi r24, low(5)
ldi r25, high(5)
rcall wait_msec
in r24,GIFR
andi r24,0b01000000
cpi r24,0b01000000
breq FIX_INT1_SPIN
pop r25
pop r24
ret

; freeze while B = 1, else return input
read_input:
clr input
in input,PINB
cpi input,1
breq read_input    ; freeze while B = 1
ret

; wait for 500 ms
wait:
push r24
push r25
ldi r24,low(500)
ldi r25,high(500)
rcall wait_msec
pop r25
pop r24
ret

wait_usec:
sbiw r24,1
nop
nop
nop
nop
brne wait_usec
ret

wait_msec:
push r24
push r25
ldi r24,low(998)
ldi r25,high(998)
rcall wait_usec
pop r25
pop r24
sbiw r24,1
brne wait_msec
ret 
;;;;;;;;;;;;;;;;

