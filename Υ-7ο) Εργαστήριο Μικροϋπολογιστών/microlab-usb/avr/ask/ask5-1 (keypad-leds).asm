; read 2 numbers from keypad
;

.include "m16def.inc"

.DSEG
_tmp_: .byte 2						;

.CSEG

.def flag = r16						; Flag to see if the input is right or wrong
.def leds = r18						; Var to light on or off leds

reset:
	ldi r24 , low(RAMEND)			; Initialize stack pointer
	out SPL , r24					;
	ldi r24 , high(RAMEND)			;
	out SPH , r24					;

	clr r24							; Clear R24
	ldi r24 ,(1 << PC7) | (1 << PC6) | (1 << PC5) | (1 << PC4) ; Set as outputs the 4 MSB's
	out DDRC ,r24					; of PortC
	ser r24							;
	out DDRA, r24					; PortA as output

start:
clr flag						;

rcall READ_KEYPAD               ; read first key into r25:r24

cpi r24,0x02					; If it is equal to number 0 (=02 on LCD display), second read
brne second						; Branch if not equal
inc flag						; 

second:

rcall READ_KEYPAD
	cpi r25,0x04					; if is equal to number 6
	brne check						;
	inc flag						;

check:
	cpi flag, 0x02					;
	brne tag						; If flag != 2 jump to tag

	rcall right						; Else. right input
	rjmp start						;

tag:
	rcall wrong						; flag !=2 means wrong input
	rjmp start						;

wrong:
	ldi r17,8						; 8 anapse-svise

repeat:
ser leds						;
out PORTA, leds					; Light all leds PA0-7
ldi r24, low(250)				;
ldi r25, high(250)				;
rcall wait_msec					;

clr leds						;
out PORTA, leds					; Light off leds PA0-7
ldi r24, low(250)				;
ldi r25, high(250)				;
rcall wait_msec					;

dec r17							;
cpi r17, 0						;
brne repeat						;
clr leds						;
out PORTA, leds					;
ret								;

right:
ser leds						;
out PORTA, leds					;
ldi r24, low(4000)				;
ldi r25, high(4000)				;
rcall wait_msec					;
clr leds						;
out PORTA, leds					;
ret								;








;;;;;;;; COMMON ROUTINES ;;;;;;;;

; read keypad into r25:r24
READ_KEYPAD:
ldi r24, 80				; xronos spinthirimsou
rcall scan_keypad_rising_edge	;
cpi r24,0x00					;
brne READ_KEYPAD_FINISH							;
cpi r25,0x00		      ; if r25:r24 == 0000h then nothing pressed			;
breq READ_KEYPAD
READ_KEYPAD_FINISH:
ret

; Enable INT0
ENABLE_INT0:
push r24
ldi r24,(1<<ISC01)|(1<<ISC00)
out MCUCR, r24
ldi r24, (1<<INT0)
out GICR, r24
pop r24
sei
ret

; Enable INT1
ENABLE_INT1:
push r24
ldi r24,(1<<ISC11)|(1<<ISC10)
out MCUCR, r24
ldi r24, (1<<INT1)
out GICR, r24
pop r24
sei
ret

; fix spinthirismo for interrupt INTF0
FIX_INT0_SPIN:
push r24
push r25
ldi r24,(1<<INTF0)
out GIFR,r24
ldi r24, low(5)
ldi r25, high(5)
rcall wait_msec
in r24,GIFR
andi r24,0b01000000
cpi r24,0b01000000
breq FIX_INT0_SPIN
pop r25
pop r24
ret

; fix spinthirismo for interrupt INTF1
FIX_INT1_SPIN:
push r24
push r25
ldi r24,(1<<INTF1)
out GIFR,r24
ldi r24, low(5)
ldi r25, high(5)
rcall wait_msec
in r24,GIFR
andi r24,0b01000000
cpi r24,0b01000000
breq FIX_INT1_SPIN
pop r25
pop r24
ret

; wait for 500 ms
wait:
push r24
push r25
ldi r24,low(500)
ldi r25,high(500)
rcall wait_msec
pop r25
pop r24
ret

; Set TCNNT1 for 0.5 sec
SET_TCNT1_05:
push r24
ldi r24,0xf0
out TCNT1H,r24
ldi r24,0xbe
out TCNT1L,r24
pop r24
ret
; Set TCNNT1 for 2.5 sec
SET_TCNT1_25:
push r24
ldi r24,0xb3
out TCNT1H,r24
ldi r24,0xb5
out TCNT1L,r24
pop r24
ret
; Set TCNNT1 for 3 sec
SET_TCNT1_3:
push r24
ldi r24,0xA4
out TCNT1H ,r24
ldi r24 ,0x73
out TCNT1L ,r24
pop r24
ret

; Init Timer TCNT1
INIT_TCNT1:
push r24
ldi r24 ,(1<<TOIE1)
out TIMSK ,r24
ldi r24 ,(1<<CS12) | (0<<CS11) | (1<<CS10) ; CK/1024
out TCCR1B ,r24
pop r24
ret

scan_row:
ldi r25,0x08
back:
lsl r25
dec r24
brne back
out PORTC,r25
nop
nop
in r24,PINC
andi r24,0x0f
ret
scan_keypad:
ldi r24,0x01
rcall scan_row
swap r24
mov r27,r24
ldi r24,0x02
rcall scan_row
add r27,r24
ldi r24,0x03
rcall scan_row
swap r24
mov r26,r24
ldi r24, 0x04
rcall scan_row
add r26,r24
movw r24,r26
ret
scan_keypad_rising_edge:
mov r22,r24
rcall scan_keypad
push r24
push r25
mov r24,r22
ldi r25,0
rcall wait_msec
rcall scan_keypad
pop r23
pop r22
and r24,r22
and r25,r23
ldi r26,low(_tmp_)
ldi r27,high(_tmp_)
ld r23,X+
ld r22,X
st X,r24
st -X,r25
com r23
com r22
and r24,r22
and r25,r23
ret
wait_usec:
sbiw r24,1
nop
nop
nop
nop
brne wait_usec
ret
wait_msec:
push r24
push r25
ldi r24,low(998)
ldi r25,high(998)
rcall wait_usec
pop r25
pop r24
sbiw r24,1
brne wait_msec
ret
;;;;;;;;;;;;;;;;



.exit
