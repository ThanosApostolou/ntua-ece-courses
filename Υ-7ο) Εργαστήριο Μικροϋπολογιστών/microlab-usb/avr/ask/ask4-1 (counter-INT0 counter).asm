; simple counter every 0.2 sec at PORTA
; count interrupts (pd2) if pd0 and show them at PORTB

.include "m16def.inc"
.def reg=r19
.def c1=r20
.def counter=r18
.def ncounter=r21 ; NORMAL COUNTER-->PORTA
.def input=r23
.def temp=r24

; interrupt int0(mask all the others)
.org 0x0
rjmp start
.org 0x2
rjmp isr0

start:
ldi temp,low(RAMEND)   ; init stack
out SPL,temp
ldi temp,high(RAMEND)
out SPH,temp

rcall ENABLE_INT0

ser temp ; initialize PORTA for output
out DDRA , temp
ser temp ; initialize PORTB for output
out DDRB , temp
clr temp 

; kyriws programma-metrhths
main:
clr ncounter
clr reg

loop:
out PORTA,ncounter
rcall wait
inc ncounter
rjmp loop

; interrupt subroutine
ISR0:
rcall FIX_INT0_SPIN
; check if PD0 is enabled
in input,PIND
andi input,0x01
cpi input,0x00
breq skip
inc reg
skip:
out PORTB, reg
reti

;;;; common routines ;;;;
; Enable INT0
ENABLE_INT0:
push r24
ldi r24,(1<<ISC01)|(1<<ISC00)
out MCUCR, r24
ldi r24, (1<<INT0)
out GICR, r24
pop r24
sei
ret

; fix spinthirismo for interrupt INTF0
FIX_INT0_SPIN:
push r24
push r25
ldi r24,(1<<INTF0)
out GIFR,r24
ldi r24, low(5)
ldi r25, high(5)
rcall wait_msec
in r24,GIFR
andi r24,0b01000000
cpi r24,0b01000000
breq FIX_INT0_SPIN
pop r25
pop r24
ret

; wait for 200 ms
wait:
push r24
push r25
ldi r24,low(200)
ldi r25,high(200)
rcall wait_msec
pop r25
pop r24
ret

wait_usec:
sbiw r24,1
nop
nop
nop
nop
brne wait_usec
ret

wait_msec:
push r24
push r25
ldi r24,low(998)
ldi r25,high(998)
rcall wait_usec
pop r25
pop r24
sbiw r24,1
brne wait_msec
ret 
;;;;;;;;;;;;;;;;
