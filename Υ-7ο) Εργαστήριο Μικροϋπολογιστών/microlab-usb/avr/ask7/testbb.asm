

.include "m16def.inc"
.def counter=r22
.def temp=r16

.org 0x00
jmp reset

.org 0x1c
jmp ADC_interrupt
reti

reset:
ldi r24 , low(RAMEND) ; initialize stack pointer
out SPL , r24
ldi r24 , high(RAMEND)
out SPH , r24
rcall usart_init
rcall ADC_init
ser counter
out DDRB,counter
ser counter
out DDRC,counter
ser counter
out DDRD,counter
clr counter
out PORTB,counter

ldi temp,0xC3
out ADMUX,temp
ldi temp,0xCE
out ADCSRA,temp
ldi r20,5

sei
;clt
sbi ADCSRA,ADSC
loop:
 ;in temp,ADCSRA
 ;sbrc temp,ADSC
 ;jmp  loop          ; wait until ADSC is 0
 ;ldi ZL, ADCL
 ;ldi ZH, ADCH
 ;lpm r24,Z
 ;rcall usart_transmit
 ;in temp,ADCSRA
 ;sbrc temp,ADSC
 ;jmp loop             ; wait until ADSC is 0
 ;in temp, ADCL
 ;out PORTC,r24
 ;in temp, ADCH
 ;out PORTD,temp
 out PORTB,counter           
 ldi r24 , low(1000)   ; load r25:r24 with 200
 ldi r25 , high(1000)  ; delay 2/10 second
 rcall wait_msec      ; 
 clt
 sbi ADCSRA,ADSC      ; begin another meassurement
 rjmp loop

ADC_interrupt:
  push ZL
  push ZH
  push temp
  in temp,ADCSRA
  sbrc temp,ADSC
  jmp  ADC_interrupt          ; wait until ADSC is 0
  ldi ZL,ADCL
  ldi ZH,ADCH
  lpm r24,Z+
   
  
  hundreds:
  cpi r24,100
  brlo deca
  inc r19
  subi r24,100
  rjmp hundreds  

  deca:
  cpi r24,10
  brlo mon
  inc r17
  subi r24,10
  rjmp deca
  
  mon:
  subi r19,-0x30
  subi r17, -0x30
  mov r18,r24
  subi r18,-0x30
  mov r24, r19
  rcall usart_transmit
  mov r24,r17
  rcall usart_transmit
  mov r24,r18
  rcall usart_transmit
  pop temp
  pop ZH
  pop ZL
  set
 
 inc counter
 reti


;-------------------------------------------------------------------------------------------------------
 ; Routine: usart_init
 ; Description:
 ; This routine initializes the
 ; usart as shown below.
 ; ------- INITIALIZATIONS -------
  ; Baud rate: 9600 (Fck= 8MHz)
 ; Asynchronous mode
 ; Transmitter on
 ; Reciever on
 ; Communication parameters: 8 Data ,1 Stop , no Parity
 ; --------------------------------
 ; parameters: None.
 ; return value: None.
 ; registers affected: r24
 ; routines called: None
 
 usart_init:
 clr r24             ; initialize UCSRA to zero
 out UCSRA ,r24
 ldi r24 ,(1<<RXEN) | (1<<TXEN)      ; activate transmitter/receiver
 out UCSRB ,r24
 ldi r24 ,0 ; baud rate = 9600
 out UBRRH ,r24
 ldi r24 ,51
 out UBRRL ,r24
 ldi r24 ,(1 << URSEL) | (3 << UCSZ0) ; 8-bit character size,
 out UCSRC ,r24 ; 1 stop bit
 ret
 
 ; Routine: usart_transmit
 ; Description:
 ; This routine sends a byte of data
 ; using usart.
 ; parameters:
 ; r24: the byte to be transmitted
 ; must be stored here.
 ; return value: None.
 ; registers affected: r24
 ; routines called: None.
 
 usart_transmit:
 sbis UCSRA ,UDRE ; check if usart is ready to transmit
 rjmp usart_transmit ; if no check again, else transmit
 out UDR ,r24        ; content of r24
 ret
 
 ; Routine: usart_receive
 ; Description:
 ; This routine receives a byte of data
 ; from usart.
 ; parameters: None.
 ; return value: the received byte is
 ; returned in r24.
 ; registers affected: r24
 ; routines called: None.
 
 usart_receive:
 sbis UCSRA ,RXC ; check if usart received byte
 rjmp usart_receive  ; if no check again, else read
 in r24 ,UDR         ; receive byte and place it in
 ret   ;r24
 
 
  ; Routine: usart_init
  ; Description:
  ; This routine initializes the
  ; ADC as shown below.
  ; ------- INITIALIZATIONS -------
  ; 
  ;Vref: Vcc (5V for easyAVR6)
  ; Selected pin is A0
  ; ADC Interrupts are Enabled
  ; Prescaler is set as CK/128 = 62.5kHz
  ; --------------------------------
  ; parameters: None.
  ; return value: None.
  ; registers affected: r24
  ; routines called: None
  ADC_init:
  Ldi r24,(1<<REFS0) ; Vref: Vcc
  Out ADMUX,r24 ;MUX4:0= 00000 forA0.
  ;ADC is Enabled (ADEN=1)
  ;ADC Interrupts are Enabled (ADIE=1)
  ;SetPrescaler CK/128 = 62.5Khz (ADPS2:0=111)
  ldi r24,(1<<ADEN)|(1<<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)
  out ADCSRA,r24
  ret

;---------------------------
; kathisterisi 2/10 sec
wait_usec:
sbiw r24 ,1 
nop 
nop 
nop 
nop 
brne wait_usec
ret 

wait_msec:
push r24 
push r25 
ldi r24 , low(998)  ;load r25:r24 with 98
ldi r25 , high(998) ; delay 2/10 second
rcall wait_usec 
pop r25 
pop r24 
sbiw r24 , 1 
brne wait_msec
ret

