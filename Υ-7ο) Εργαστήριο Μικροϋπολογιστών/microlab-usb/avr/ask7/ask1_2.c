#include <avr/io.h>

void TransmitUSART (char data) // ��������� ��� �������� ��������� 
{
	while (!(UCSRA & (1<<UDRE)));
	UDR=data;
}

unsigned char ReceiveUSART()  // ��������� ��� ���� ���������
{
	while (!(UCSRA & (1<<RXC)));
	return UDR;
}

void transmit_text (char* astring) {

	while (*(astring) != '\0') {
		TransmitUSART(*astring);
		astring++;
	}
}

int main(void) {
	unsigned char temp;
	// ������������ ��������� (USART initialization)
	// Communication Parameters: 8 Data, 1 Stop, No Parity
	// USART Receiver: On,  Transmitter: On
	// USART Mode: Asynchronous,  Baud rate: 9600  (Fck=4 MHz)
	UCSRA=0x00;  
	UCSRB=0x18; // 0001 1000 ������ RXEN=�XEN=1
	UCSRC=0x86;   // 1000 0110 ������ URSEL =1, USBS=0, UCSZ1= UCSZ0=1
	UBRRH=0x00;   // 4000000/(9600*16)-1=25 ��� 4M�z�� ������=0,2%
	UBRRL=0x19;  // UBRR=25
	while (1){
		temp=ReceiveUSART();// ���������� ���������
		if (temp < '0' || temp > '8') {
			transmit_text ("InvalidNumber");
			TransmitUSART ('\n');
		} else {
			transmit_text ("Read");
			TransmitUSART (temp);
			TransmitUSART ('\n');
			temp = temp - '0';
			DDRC = 0xFF;
			if (temp == 0) {
				PORTC = 0;
			} else {
				PORTC = 0 | (1<<(temp-1));
			}
		}
	}
	return 0;
}
