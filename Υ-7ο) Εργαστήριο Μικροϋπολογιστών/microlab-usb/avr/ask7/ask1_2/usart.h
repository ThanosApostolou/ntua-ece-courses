void usart_init() {
	UCSRA = 0;	// Initialize UCSRA to zero
	UCSRB = (1 << RXEN) | (1 << TXEN);	// Activate transmitter, receiver
	UBRRH = 0;			// Baud rate = 9600
	UBRRL = 51;
	UCSRC = (1 << URSEL) | (3 << UCSZ0);
}

void usart_transmit(int8_t byte) {
	int8_t mask = 1 << UDRE;
	while (!(UCSRA & mask));	// Loop till bit is set
	UDR = byte;
}

uint8_t usart_receive() {
	int8_t mask = 1 << RXC;
	while (!(UCSRA & mask));	// Loop till bit is set
	return UDR;
}

void usart_transmit_string(const char *s)
{
	while (*s) {
		usart_transmit(*s++);
	}
}