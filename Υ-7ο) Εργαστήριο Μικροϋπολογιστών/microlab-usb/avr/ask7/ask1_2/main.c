#include <avr/io.h>
#define F_CPU 8000000UL
#include <util/delay.h>
#include <avr/interrupt.h>
#include "mylibs/usart.h"

const char *read_prompt ="Read";
const char *invalid_prompt = "Invalid\n";

int main(void)
{
	usart_init();
	uint8_t byte;
	while (1) {
		byte = usart_receive();
		if (byte < '0' || byte > '8') {
			usart_transmit_string(invalid_prompt);
		} else {
			usart_transmit_string(read_prompt);
			usart_transmit(' ');
			usart_transmit(byte);
			usart_transmit('\n');
			DDRC = 0xFF;
			int position = byte - '0' - 1; 
			if (position < 0) {
				PORTC = 0;
			} else {
				PORTC = 1 << position;
			}
		}
	}
}

