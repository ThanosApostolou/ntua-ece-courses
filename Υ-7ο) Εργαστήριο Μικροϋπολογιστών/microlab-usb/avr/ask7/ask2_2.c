#include <avr/io.h>
#include <avr/interrupt.h>

void TransmitUSART (uint8_t data) // ��������� ��� �������� ��������� 
{
    while (!(UCSRA & (1<<UDRE)));
    UDR=data;
}

void ADC_init () {
	ADMUX = (1<<REFS0);
	ADCSRA = (1<<ADEN)|(1<<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
	return;
}

ISR(ADC_vect) {
	uint8_t low, high;
	uint16_t lowhigh;
	;DDRC=0xFF;
	;PORTC=80;

	while (ADCSRA & (1<<ADSC)) ;
	low=ADCL;
	high=ADCH;
	lowhigh = (high << 8) + low;
	lowhigh = lowhigh * 50;
	lowhigh = lowhigh >> 10;
	high = lowhigh/10;
	low = lowhigh%10;
	TransmitUSART(high+0x30);
	TransmitUSART('.');
	TransmitUSART(low+0x30);
	TransmitUSART('\n');
	return;
}

int main(void) {
    // ������������ ��������� (USART initialization)
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART Receiver: On,  Transmitter: On
    // USART Mode: Asynchronous,  Baud rate: 9600  (Fck=4 MHz)
    UCSRA=0x00;  
    UCSRB=(1<<RXEN) | (1<<TXEN); // 0001 1000 ������ RXEN=�XEN=1
    UCSRC=(1<<URSEL) | (3<<UCSZ0);   // 1000 0110 ������ URSEL =1, USBS=0, UCSZ1= UCSZ0=1
    UBRRH=0x00;   // 4000000/(9600*16)-1=25 ��� 4M�z�� ������=0,2%
    UBRRL=0x33;  // UBRR=25
	ADC_init();
	
	asm ("sei");  // enable interrupts
    while (1){
		ADCSRA = ADCSRA | (1<<ADSC);
    }
    return 0;
}
