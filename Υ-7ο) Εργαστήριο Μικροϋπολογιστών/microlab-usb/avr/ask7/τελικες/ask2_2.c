#include <avr/io.h>
#include <avr/interrupt.h>

void TransmitUSART (uint8_t data) // transmit 1 byte
{
    while (!(UCSRA & (1<<UDRE)));
    UDR=data;
}

void ADC_init () { // initialize ADC
	ADMUX = (1<<REFS0);
	ADCSRA = (1<<ADEN)|(1<<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
	return;
}

// this function is called with each interrupt from ADC
// most of the work is done here
ISR(ADC_vect) {
	uint8_t low, high;
	uint16_t lowhigh;

	while (ADCSRA & (1<<ADSC)) ; // wait until ADSC is 0 (meassurement finished)
	low=ADCL;
	high=ADCH;
	lowhigh = (high << 8) + low; // store ADC into a 16bit variable
	lowhigh = lowhigh * 50;      // multiply by 50 instead of Vfr=5
	lowhigh = lowhigh >> 10;     // and divide by 10 in order to get 1 decimal precision
	high = lowhigh/10;
	low = lowhigh%10;
	TransmitUSART(high+0x30);    // transmit Voltage in ascii
	TransmitUSART('.');
	TransmitUSART(low+0x30);
	TransmitUSART('\n');
	return;
}

int main(void) {
    // (USART initialization)
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART Receiver: On,  Transmitter: On
    // USART Mode: Asynchronous,  Baud rate: 9600  (Fck=4 MHz)
    UCSRA=0x00;
    UCSRB=(1<<RXEN) | (1<<TXEN);
    UCSRC=(1<<URSEL) | (3<<UCSZ0);
    UBRRH=0x00;
    UBRRL=0x33;
	ADC_init();

	asm ("sei");  // enable interrupts
    while (1){
		ADCSRA = ADCSRA | (1<<ADSC); // start another ADC meassurement
    }
    return 0;
}
