.include "m16def.inc"

.DSEG
_tmp_:.byte 2
.CSEG

.org 0x0
rjmp reset
.org 0x10
rjmp ISR_TIMER1_OVF

reset:

ldi r24 ,(1 << PC7) | (1 << PC6) | (1 << PC5) | (1 << PC4) 
out DDRC,r24 
ldi r24, low(RAMEND)
out SPL,r24
ldi r24, high(RAMEND)
out SPH,r24
ldi r24 ,(1<<TOIE1)
out TIMSK ,r24

;----------
ldi r24 ,(1<<CS12) | (0<<CS11) | (1<<CS10) ; CK/1024
out TCCR1B ,r24
ser r24
out DDRA,r24
clr r24
ldi r20,1

main:
sei
read_1_numb:
ldi r24,0x14
rcall scan_keypad_rising_edge
mov r23,r24
add r24,r25
cpi r24,0
breq read_1_numb
mov r17,r23
mov r18,r25

read_2_numb:
ldi r24,0x14
rcall scan_keypad_rising_edge
mov r23,r24
add r24,r25
cpi r24,0
breq read_2_numb
cpi r17,2
brne false
cpi r25,64
brne false

correct:
ldi r20,0
ldi r16,0xff
out PORTA,r16
ldi r24,0x85
out TCNT1H ,r24
ldi r24 ,0xee
out TCNT1L ,r24
ldi r24 , low(4000)
ldi r25 , high(4000)
rcall wait_msec
rjmp main

false:
ldi r20,1
ldi r16,0xff
loop:
sei
out PORTA,r16
ldi r24,0xf8
out TCNT1H ,r24
ldi r24 ,0x5f
out TCNT1L ,r24



scan_row:
ldi r25,0x08
back:
lsl r25
dec r24
brne back
out PORTC,r25
nop
nop
in r24,PINC
andi r24,0x0f
ret

scan_keypad:
ldi r24,0x01
rcall scan_row
swap r24
mov r27,r24
ldi r24,0x02
rcall scan_row
add r27,r24
ldi r24,0x03
rcall scan_row
swap r24
mov r26,r24
ldi r24, 0x04
rcall scan_row
add r26,r24
movw r24,r26
ret

scan_keypad_rising_edge:
mov r22,r24
rcall scan_keypad
push r24
push r25
mov r24,r22
ldi r25,0
rcall wait_msec
rcall scan_keypad
pop r23
pop r22
and r24,r22
and r25,r23
ldi r26,low(_tmp_)
ldi r27,high(_tmp_)
ld r23,X+
ld r22,X
st X,r24
st -X,r25
com r23
com r22
and r24,r22
and r25,r23
ret

wait_usec:
sbiw r24 ,1 
nop 
nop 
nop 
nop 
brne wait_usec
ret 

wait_msec:
push r24 
push r25 
ldi r24 , low(998)  
ldi r25 , high(998) 
rcall wait_usec 
pop r25 
pop r24 
sbiw r24 , 1 
brne wait_msec
ret

ISR_TIMER1_OVF:
cli
cpi r20,0
breq cor
cpi r20,16
brge cor
inc r20
com r16
rjmp loop
cor:
clr r20
clr r24
clr r16
clr r17
clr r18
clr r23
out PORTA,r16 

rjmp main


