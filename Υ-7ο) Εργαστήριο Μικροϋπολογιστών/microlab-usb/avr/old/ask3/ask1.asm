.include "m16def.inc"
.def counter=r16

reset: 
ldi r24,low(RAMEND)
out SPL,r24
ldi r24,high(RAMEND)
out SPH,r24
ser r24
out DDRA,r24
ldi r24,254
out DDRB,r24
clr counter
clr r18
main:
ldi counter,1
out PORTA,counter

upwards:
clr r18
in r18,PINB
cpi r18,1
brlo continue_u
rcall freeze
continue_u:
out PORTA,counter
ldi r24,low(500)
ldi r25,high(500)
rcall wait_msec
rol counter
cpi counter,128
breq downwards
rjmp upwards

downwards:
clr r18
in r18,PINB
cpi r18,1
brlo continue_d
rcall freeze
continue_d:
out PORTA,counter
ldi r24,low(500)
ldi r25,high(500)
rcall wait_msec
ror counter
cpi counter,1
breq upwards
jmp downwards

freeze:
clr r18
in r18,PINB
cpi r18,1
breq freeze
ret

wait_usec:
sbiw r24,1
nop
nop
nop
nop
brne wait_usec
ret

wait_msec:
push r24
push r25
ldi r24,low(998)
ldi r25,high(998)
rcall wait_usec
pop r25
pop r24
sbiw r24,1
brne wait_msec
ret 
