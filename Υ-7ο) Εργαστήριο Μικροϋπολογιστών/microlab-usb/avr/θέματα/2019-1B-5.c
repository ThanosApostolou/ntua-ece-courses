// 1<=x<=7 from 3 MSB of PINB
// light PORTC led of number

#include<avr/io.h>
#include<stdbool.h> 

int main()
{
    DDRB = 0x00;        //configure portA as input
    DDRC = 0xFF;        //configure portB as output
     
    unsigned char input, result=0;

    while(1)
    {	
        input = PINB & 0b11100000; // input is 3 MSB of PINB
		input = input >> 5;
		if (input <= 1) {       // x must be >= 1
			result = 1;
		} else {
			result = (1 << (input-1));
		}
		PORTC = result;
    }
    return 0;
}
