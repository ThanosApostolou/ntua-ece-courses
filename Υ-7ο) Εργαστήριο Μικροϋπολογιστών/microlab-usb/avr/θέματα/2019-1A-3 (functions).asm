;input B0-B4 (x0-3)
;output A7-A6 (F7-F6)
;F7 = x0 XOR x1 XOR x2 XOR x2
;F6 = x0x1+x2x3

.include "m16def.inc"
.def x0=r16
.def x1=r17
.def x2=r18
.def x3=r19
.def input=r21
.def F7=r22
.def F6=r23
.def temp1=r24
.def result=r25

start: 
ldi temp1,low(RAMEND)   ; init stack
out SPL,temp1
ldi temp1,high(RAMEND)
out SPH,temp1

ser temp1
out DDRA,temp1          ; port A is output
clr temp1
out DDRB,temp1          ; port B is input

clr x1
clr x0
clr x2
clr x3
clr input
clr temp1
clr F6
clr F7
clr result

main:


in input,PINB

mov x0,input
andi x0,1

mov x1,input
andi x1,2
ror x1

mov x2,input
andi x2,4
ror x2
ror x2

mov x3,input
andi x3,8
ror x3
ror x3
ror x3

;calc pa7
mov F7,x0
eor F7,x1
eor F7,x2
eor F7,x3    ; F7 = x0 XOR x1 XOR x2 XOR x2
lsl F7
lsl F7
lsl F7
lsl F7
lsl F7
lsl F7
lsl F7       ; move F7 to MSB

mov F6,x0
and F6,x1    ; F6 = x0x1
mov temp1,x2
and temp1,x3
or F6,temp1  ; F6 = x0x1+x2x3
lsl F6       ; move F6 to 2nd MSB
lsl F6
lsl F6
lsl F6
lsl F6
lsl F6

mov result,F7
or result,F6

out PORTA,result

jmp main
