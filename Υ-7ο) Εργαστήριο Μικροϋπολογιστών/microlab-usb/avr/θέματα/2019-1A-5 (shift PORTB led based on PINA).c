// 0<=x<=6 from 3 LSB of PINA
// light PORTB led of number

#include<avr/io.h>
#include<stdbool.h> 

int main()
{
    DDRA = 0x00;        //configure portA as input
    DDRB = 0xFF;        //configure portB as output
     
    unsigned char input, result=0;

    while(1)
    {	
        input = PINA & 0b00000111; // input is 3 LSB of PINA		
		if (input == 0) {
			result = 0;
		} else if (input > 6) {  // maximum allowed value
 			result = 1 << 5;
		} else {
			result = (1 << (input-1));
		}
		PORTB = result;
    }
    return 0;
}
