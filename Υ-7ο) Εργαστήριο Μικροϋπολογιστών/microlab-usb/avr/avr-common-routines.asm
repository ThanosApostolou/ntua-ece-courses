;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;; COMMON ROUTINES ;;;;;;;;;;;;;;;;

;;;;;;;; General ;;;;;;;;

; from hex temp to ascii temp
ASCII_TO_HEX:
subi temp,0x30
cpi temp,0x0A
brlo HEX_TO_ASCII_FINISH
subi temp,0x07
HEX_TO_ASCII_FINISH:
ret

PRINT_HEX_TO_3DEC:
push temp
push r24
ldi temp,0
hund:
cpi number,100
brlo found_hund
subi number,100
inc temp
jmp hund
found_hund:
cpi temp,0
breq if_less_hund    ; don't print hundreads if 0
mov r24,temp
subi r24,-0x30
rcall lcd_data
if_less_hund:
ldi temp,0
deca:
cpi number,10
brlo found_deca
subi number,10
inc temp
jmp deca
found_deca:
cpi temp,0
breq if_less_deca    ; don't print decades if 0
mov r24,temp
subi r24,-0x30
rcall lcd_data
if_less_deca:
ldi temp,0
mon:
mov r24,number
subi r24,-0x30
rcall lcd_data
pop r24
pop temp
ret

;;;;;;;;;;;;;;;;



;;;;;;;; Interrupts and Timer ;;;;;;;;
; Enable INT0
ENABLE_INT0:
push r24
ldi r24,(1<<ISC01)|(1<<ISC00)
out MCUCR, r24
ldi r24, (1<<INT0)
out GICR, r24
pop r24
sei
ret

; Enable INT1
ENABLE_INT1:
push r24
ldi r24,(1<<ISC11)|(1<<ISC10)
out MCUCR, r24
ldi r24, (1<<INT1)
out GICR, r24
pop r24
sei
ret

; fix spinthirismo for interrupt INTF0
FIX_INT0_SPIN:
push r24
push r25
ldi r24,(1<<INTF0)
out GIFR,r24
ldi r24, low(5)
ldi r25, high(5)
rcall wait_msec
in r24,GIFR
andi r24,0b01000000
cpi r24,0b01000000
breq FIX_INT0_SPIN
pop r25
pop r24
ret

; fix spinthirismo for interrupt INTF1
FIX_INT1_SPIN:
push r24
push r25
ldi r24,(1<<INTF1)
out GIFR,r24
ldi r24, low(5)
ldi r25, high(5)
rcall wait_msec
in r24,GIFR
andi r24,0b01000000
cpi r24,0b01000000
breq FIX_INT1_SPIN
pop r25
pop r24
ret

; Set TCNNT1 for 0.5 sec
SET_TCNT1_05:
push r24
ldi r24,0xf0
out TCNT1H,r24
ldi r24,0xbe
out TCNT1L,r24
pop r24
ret
; Set TCNNT1 for 2.5 sec
SET_TCNT1_25:
push r24
ldi r24,0xb3
out TCNT1H,r24
ldi r24,0xb5
out TCNT1L,r24
pop r24
ret
; Set TCNNT1 for 3 sec
SET_TCNT1_3:
push r24
ldi r24,0xA4
out TCNT1H ,r24
ldi r24 ,0x73
out TCNT1L ,r24
pop r24
ret

; Init Timer TCNT1
INIT_TCNT1:
push r24
ldi r24 ,(1<<TOIE1)
out TIMSK ,r24
ldi r24 ,(1<<CS12) | (0<<CS11) | (1<<CS10) ; CK/1024
out TCCR1B ,r24
pop r24
ret
;;;;;;;;;;;;;;;;

;;;;;;;; KEYPAD ;;;;;;;;
; read keypad into r25:r24
READ_KEYPAD:
ldi r24, 80				; xronos spinthirimsou
rcall scan_keypad_rising_edge	;
cpi r24,0x00					;
brne READ_KEYPAD_FINISH							;
cpi r25,0x00		      ; if r25:r24 == 0000h then nothing pressed			;
breq READ_KEYPAD
READ_KEYPAD_FINISH:
ret

keypad_to_hex:
movw r26,r24
ldi r24,'E'
sbrc r26,0
ret
ldi r24,'0'
sbrc r26,1
ret
ldi r24,'F'
sbrc r26,2
ret
ldi r24,'D'
sbrc r26,3
ret
ldi r24,'7'
sbrc r26,4
ret
ldi r24,'8'
sbrc r26,5
ret
ldi r24,'9'
sbrc r26,6
ret
ldi r24,'C'
sbrc r26,7
ret
ldi r24,'4'
sbrc r27,0
ret
ldi r24,'5'
sbrc r27,1
ret
ldi r24,'6'
sbrc r27,2
ret
ldi r24,'B'
sbrc r27,3
ret
ldi r24,'1'
sbrc r27,4
ret
ldi r24,'2'
sbrc r27,5
ret
ldi r24,'3'
sbrc r27,6
ret
ldi r24,'A'
sbrc r27,7
ret 
clr r24
ret

scan_row:
ldi r25,0x08
back:
lsl r25
dec r24
brne back
out PORTC,r25
nop
nop
in r24,PINC
andi r24,0x0f
ret
scan_keypad:
ldi r24,0x01
rcall scan_row
swap r24
mov r27,r24
ldi r24,0x02
rcall scan_row
add r27,r24
ldi r24,0x03
rcall scan_row
swap r24
mov r26,r24
ldi r24, 0x04
rcall scan_row
add r26,r24
movw r24,r26
ret
scan_keypad_rising_edge:
mov r22,r24
rcall scan_keypad
push r24
push r25
mov r24,r22
ldi r25,0
rcall wait_msec
rcall scan_keypad
pop r23
pop r22
and r24,r22
and r25,r23
ldi r26,low(_tmp_)
ldi r27,high(_tmp_)
ld r23,X+
ld r22,X
st X,r24
st -X,r25
com r23
com r22
and r24,r22
and r25,r23
ret
;;;;;;;;;;;;;;;;

;;;;;;;; LCD ;;;;;;;;
; write r24 to LCD
lcd_data:                
sbi PORTD ,PD2       
rcall write_2_nibbles     
ldi r24 ,43                
ldi r25 ,0                   
rcall wait_usec  
ret

LCD_CLEAN:
ldi r24 ,0x01        ;clean lcd screen      
rcall lcd_command
ldi r24 ,low(1530)  
ldi r25 ,high(1530)
rcall wait_usec
ret

lcd_command: 
cbi PORTD ,PD2        
rcall write_2_nibbles    
ldi r24 ,39                   
ldi r25 ,0                    
rcall wait_usec          
ret   

lcd_init:      
ldi r24 ,40            
ldi r25 ,0           
rcall wait_msec      
ldi r24 ,0x30          
out PORTD ,r24                  
sbi PORTD ,PD3        
cbi PORTD ,PD3         
ldi r24 ,39  
ldi r25 ,0                      
rcall wait_usec                                                                        
ldi r24 ,0x30         
out PORTD ,r24       
sbi PORTD ,PD3  
cbi PORTD ,PD3  
ldi r24 ,39  
ldi r25 ,0  
rcall wait_usec   
ldi r24 ,0x20                   
out PORTD ,r24          
sbi PORTD ,PD3  
cbi PORTD ,PD3  
ldi r24 ,39  
ldi r25 ,0  
rcall wait_usec       
ldi r24 ,0x28         
rcall lcd_command      
ldi r24 ,0x0c             
rcall lcd_command       
ldi r24 ,0x01              
rcall lcd_command  
ldi r24 ,low(1530)  
ldi r25 ,high(1530)  
rcall wait_usec      
ldi r24 ,0x06             
rcall lcd_command                                                                                       
ret

write_2_nibbles: 
push r24         
in r25 ,PIND       
andi r25 ,0x0f  
andi r24 ,0xf0        
add r24 ,r25     
out PORTD ,r24            
sbi PORTD ,PD3   
cbi PORTD ,PD3  
pop r24                  
swap r24  
andi r24 ,0xf0   
add r24 ,r25 
out PORTD ,r24              
sbi PORTD ,PD3  
cbi PORTD ,PD3 
ret
;;;;;;;;;;;;;;;;

;;;;;;;; WAIT ;;;;;;;;
; wait for 500 ms
wait:
push r24
push r25
ldi r24,low(500)
ldi r25,high(500)
rcall wait_msec
pop r25
pop r24
ret

wait_usec:
sbiw r24,1
nop
nop
nop
nop
brne wait_usec
ret
wait_msec:
push r24
push r25
ldi r24,low(998)
ldi r25,high(998)
rcall wait_usec
pop r25
pop r24
sbiw r24,1
brne wait_msec
ret
;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;