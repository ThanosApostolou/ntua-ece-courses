.include "m16def.inc"

.def temp = r16
.def sign = r17

.CSEG

.org	0x00
rjmp	reset

reset:

	/* INITIALIZE STACK */
	ldi r24,high(RAMEND)
	out SPH,r24
	ldi r24,low(RAMEND)
	out SPL,r24

main:
	rcall get_temperature
	ldi r24,low(200)
	ldi r25,high(200)
	rcall wait_msec
	rjmp main



get_temperature:
	rcall one_wire_reset
	/* Check if a sensor is connected */
	cpi r24,0;
	breq nothing_connected
	/* Transmit 0xCC to address all devices on bus */
	ldi r24,0xCC
	rcall one_wire_transmit_byte
	/* Transmit 0x44 to convert a single temp */
	ldi r24,0x44
	rcall one_wire_transmit_byte
	/* Wait until sensor ready state */
	wait_till_ready:
		rcall one_wire_receive_bit
		sbrs r24,0
		rjmp wait_till_ready
	/* Reset and read data from sensor */
	rcall one_wire_reset
	/* Transmit 0xCC to address all devices on bus */
	ldi r24,0xCC
	rcall one_wire_transmit_byte
	/* Transmit 0xBE to read from scratchpad */
	ldi r24,0xBE
	rcall one_wire_transmit_byte
	/* Read temperature */
	rcall one_wire_receive_byte
	mov temp,r24
	/* Read sign */
	rcall one_wire_receive_byte
	mov sign,r24
	/* Check is temp is positive */
	cpi sign,0x00
	breq positive
	/* Convert temp if negative (2's complement)*/
	com temp
	inc temp
positive:
	/* Divide by 2 and complement*/
	lsr temp
	com temp
	/* Return to r24:r25*/
	mov r24,temp
	mov r25,sign
	/* Print temperature */
	print:
	ser temp
	out DDRA,temp
	out PORTA,r24
ret
nothing_connected:
	/* Return disconnected code */
	ldi r25,0x80
	/* Print disconnected code */
	ser r24
	out DDRA,r24
	out PORTA,r25
	
	clr r24
ret

/* ROUTINE SEGMENT */

one_wire_reset:
sbi DDRA ,PA4 ; PA4 configured for output
cbi PORTA ,PA4 ; 480 ?sec reset pulse
ldi r24 ,low(480)
ldi r25 ,high(480)
rcall wait_usec
cbi DDRA ,PA4
cbi PORTA ,PA4
ldi r24 ,100 ; wait 100 ?sec for devices
ldi r25 ,0 ; to transmit the presence pulse
rcall wait_usec
in r24 ,PINA ; sample the line
push r24
ldi r24 ,low(380) ; wait for 380 ?sec
ldi r25 ,high(380)
rcall wait_usec
pop r25 ; return 0 if no device was
clr r24 ; detected or 1 else
sbrs r25 ,PA4
ldi r24 ,0x01
ret

one_wire_receive_byte:
ldi r27 ,8
clr r26
loop_:
rcall one_wire_receive_bit
lsr r26
sbrc r24 ,0
ldi r24 ,0x80
or r26 ,r24
dec r27
brne loop_
mov r24 ,r26
ret

one_wire_transmit_byte:
mov r26 ,r24
ldi r27 ,8
_one_more_:
clr r24
sbrc r26 ,0
ldi r24 ,0x01
rcall one_wire_transmit_bit
lsr r26
dec r27
brne _one_more_
ret

one_wire_receive_bit:

sbi DDRA ,PA4
cbi PORTA ,PA4 ; generate time slot
ldi r24 ,0x02
ldi r25 ,0x00
rcall wait_usec
cbi DDRA ,PA4 ; release the line
cbi PORTA ,PA4
ldi r24 ,10 ; wait 10 ?s
ldi r25 ,0
rcall wait_usec
clr r24 ; sample the line
sbic PINA ,PA4
ldi r24 ,1
push r24
ldi r24 ,49 ; delay 49 ?s to meet the standards
ldi r25 ,0 ; for a minimum of 60 ?sec time slot
rcall wait_usec ; and a minimum of 1 ?sec recovery time
pop r24
ret

one_wire_transmit_bit:
push r24 ; save r24
sbi DDRA ,PA4
cbi PORTA ,PA4 ; generate time slot
ldi r24 ,0x02
ldi r25 ,0x00
rcall wait_usec
pop r24 ; output bit
sbrc r24 ,0
sbi PORTA ,PA4
sbrs r24 ,0
cbi PORTA ,PA4
ldi r24 ,58 ; wait 58 ?sec for the
ldi r25 ,0 ; device to sample the line
rcall wait_usec
cbi DDRA ,PA4 ; recovery time
cbi PORTA ,PA4
ldi r24 ,0x01
ldi r25 ,0x00
rcall wait_usec
ret





wait_usec:
	sbiw r24 ,1 
	nop 
	nop 
	nop 
	nop
	brne wait_usec 
ret 

wait_msec:
	push r24
	push r25 
	ldi r24 , low(998) 
	ldi r25 , high(998) 
	rcall wait_usec 
	pop r25 
	pop r24 
	sbiw r24 , 1
	brne wait_msec 
 ret
