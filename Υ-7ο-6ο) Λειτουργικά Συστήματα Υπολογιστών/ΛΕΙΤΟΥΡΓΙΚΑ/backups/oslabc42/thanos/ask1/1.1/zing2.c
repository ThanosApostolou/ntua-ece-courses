#include <unistd.h>
#include <stdio.h>

void zing(void) {
	char *name=NULL;
	name=getlogin();
	if (name == NULL) {
		printf ("Cannot read user's name\n");
	} else {
		printf ("Have a nice day, my friend %s\n", name);
	}
	return;
}
