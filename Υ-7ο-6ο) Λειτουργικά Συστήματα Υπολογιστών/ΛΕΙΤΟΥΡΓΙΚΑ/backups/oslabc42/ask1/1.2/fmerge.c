#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include "write.h"

void fmerge (char *infilename1, char *infilename2, char *outfilename) {
	int fd, oflags, mode;
	oflags = O_CREAT | O_WRONLY | O_TRUNC;
	mode = S_IRUSR | S_IWUSR;
	fd = open(outfilename, oflags, mode);
	if (fd==-1) {
		perror("open");
		exit(1);
	}
	write_file(fd, infilename1);
	close (fd);
	oflags = O_CREAT | O_WRONLY | O_APPEND;
	fd = open(outfilename, oflags, mode);
	if (fd==-1) {
		perror("open");
		exit(1);
	}
	write_file(fd, infilename2);
	close(fd);
	return;
}
