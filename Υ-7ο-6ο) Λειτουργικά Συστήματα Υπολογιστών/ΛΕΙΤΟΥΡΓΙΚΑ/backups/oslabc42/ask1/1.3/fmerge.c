#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include "write.h"

void fcreate (char *infilename, char *outfilename) {
	int fd, oflags, mode;
	oflags = O_CREAT | O_WRONLY | O_TRUNC;
	mode = S_IRUSR | S_IWUSR;
	fd = open(outfilename, oflags, mode);
	if (fd==-1) {
		perror("open");
		exit(1);
	}
	write_file(fd, infilename);
	close (fd);
	return;
}

void fappend (char *infilename, char *outfilename) {
	int fd, oflags, mode;
	mode = S_IRUSR | S_IWUSR;
	oflags = O_CREAT | O_WRONLY | O_APPEND;
	fd = open(outfilename, oflags, mode);
	if (fd==-1) {
		perror("open");
		exit(1);
	}
	write_file(fd, infilename);
	close(fd);
	return;
}
