#include "fmerge.h"
#include <stdio.h>

int main (int argc, char **argv) {
	if (argc < 3) {
		printf ("Usage: ./fconc infile1 infile2 [outfile (default:fconc.out)]\n");
	}
	if (argc == 3) {
		fcreate (argv[1], "fconc.out");
		fappend (argv[2], "fconc.out");
	}
	else {
		fcreate (argv[1], argv[argc - 1]);
		int i;
		for (i=2; i < argc-1; i++) {
			fappend (argv[i], argv[argc -1]);
		}
	}
	return 0;
}
