#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <assert.h>

#include <sys/wait.h>
#include <sys/types.h>

#include "proc-common.h"
#include "request.h"

/* Compile-time parameters. */
#define SCHED_TQ_SEC 2                /* time quantum */
#define TASK_NAME_SZ 60               /* maximum size for a task's name */

/* Processes queue implemented with a linked list structure
 */
typedef struct proc_node {
	pid_t pid;
	struct proc_node *next;
} proc_node_t;

proc_node_t *head=NULL;
proc_node_t *active=NULL;

/* Find and start next process in list
 */
void start_next_proc(pid_t pid) {
	proc_node_t *current;
	current=head;
	while (current!=NULL) {
		if (current->pid == pid) break;
		current=current->next;
	}
	if (current == NULL) {
		fprintf(stderr, "pid not in processes list\n");
		exit(1);
	}
	active=current->next;
	if (active == NULL) {
		active = head;
	}
	kill(active->pid, SIGCONT);
}

/* Remove finished process from list and start next one.
 * Finish program if it was the last process.
 */
void remove_current_start_next_proc(pid_t pid) {
	proc_node_t *current, *previous;
	previous=head;
	current=head;
	while (current!=NULL) {
		if (current->pid == pid) break;
		previous=current;
		current=current->next;
	}
	if (current == NULL) {
		fprintf(stderr, "pid not in processes list\n");
		exit(1);
	}
	if (current == head) {
		head=current->next;
		previous=head;
	} else {
		previous->next = current->next;
	}
	free(current);
	if (head == NULL) {
		fprintf (stdout, "all processes terminated successfully\n");
		exit(0);
	} else {
		active=previous->next;
		if (active == NULL) {
			active = head;
		}
		kill(active->pid, SIGCONT);
	}
}

/*
 * SIGALRM handler
 */
static void sigalrm_handler(int signum) {
	kill (active->pid, SIGSTOP);
}

/*
 * SIGCHLD handler
 */
static void sigchld_handler(int signum) {
	int status;
	pid_t p;
	for (;;) {
		p = waitpid(-1, &status, WUNTRACED | WNOHANG);
		if (p < 0) {
			perror("waitpid");
			exit(1);
		}
		if (p == 0)
			break;
		explain_wait_status(p, status);
		if (WIFEXITED(status) || WIFSIGNALED(status)) {
			/* A child has died */
			alarm(SCHED_TQ_SEC);
			remove_current_start_next_proc(p);
		}
		if (WIFSTOPPED(status)) {
			/* A child has stopped due to SIGSTOP/SIGTSTP, etc */
			alarm(SCHED_TQ_SEC);
			start_next_proc(p);
		}
	}
}

/* Install two signal handlers.
 * One for SIGCHLD, one for SIGALRM.
 * Make sure both signals are masked when one of them is running.
 */
static void install_signal_handlers(void) {
	sigset_t sigset;
	struct sigaction sa;

	sa.sa_handler = sigchld_handler;
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGCHLD);
	sigaddset(&sigset, SIGALRM);
	sa.sa_mask = sigset;
	if (sigaction(SIGCHLD, &sa, NULL) < 0) {
		perror("sigaction: sigchld");
		exit(1);
	}

	sa.sa_handler = sigalrm_handler;
	if (sigaction(SIGALRM, &sa, NULL) < 0) {
		perror("sigaction: sigalrm");
		exit(1);
	}

	/*
	 * Ignore SIGPIPE, so that write()s to pipes
	 * with no reader do not result in us being killed,
	 * and write() returns EPIPE instead.
	 */
	if (signal(SIGPIPE, SIG_IGN) < 0) {
		perror("signal: sigpipe");
		exit(1);
	}
}

// add process node at the end
void append_proc (pid_t pid) {
	if (head == NULL) {
		head=malloc(sizeof(proc_node_t));
		head->pid = pid;
		head->next=NULL;
	} else {
		proc_node_t *current = head;
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = malloc(sizeof(proc_node_t));
		current->next->pid = pid;
		current->next->next = NULL;
	}
}

/* Ceate process and raise SIGSTOP
 */
void create_process (char executable[]) {
	char *newargv[] = { executable, NULL, NULL, NULL };
	char *newenviron[] = { NULL };

	//stop process when it's first created
	raise(SIGSTOP);
	execve(executable, newargv, newenviron);

	/* execve() only returns on error */
	perror("execve");
	exit(1);
}

int main(int argc, char *argv[])
{
	pid_t pid;
	int nproc;
	/*
	 * For each of argv[1] to argv[argc - 1],
	 * create a new child process, add it to the process list.
	 */
	int i;
	for (i=1; i < argc;  i++) {
		pid=fork();
        if (pid < 0) {
    		perror("main: fork");
    		exit(1);
    	}
        if (pid == 0) { //child
    		create_process(argv[i]);
    		exit(1);
    	}
		// father
		append_proc (pid);
	}
	nproc = argc-1; /* number of proccesses goes here */

	/* Wait for all children to raise SIGSTOP before exec()ing. */
	wait_for_ready_children(nproc);

	/* Install SIGALRM and SIGCHLD handlers. */
	install_signal_handlers();

	if (nproc == 0) {
		fprintf(stderr, "Scheduler: No tasks. Exiting...\n");
		exit(1);
	}
	// run first process
	active=head;
	alarm(SCHED_TQ_SEC);
	kill (active->pid, SIGCONT);

	/* loop forever  until we exit from inside a signal handler. */
	while (pause())
		;
	/* Unreachable */
	fprintf(stderr, "Internal error: Reached unreachable point\n");
	return 1;
}
