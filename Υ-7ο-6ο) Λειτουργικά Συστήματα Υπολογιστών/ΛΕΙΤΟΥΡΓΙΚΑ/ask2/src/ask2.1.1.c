#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "proc-common.h"

#define SLEEP_PROC_SEC  10
#define SLEEP_TREE_SEC  3

//create D process
void D_process(void)
{
	change_pname("D");
	printf("PID = %ld, name D, Sleeping...\n", (long)getpid());
	sleep(SLEEP_PROC_SEC);
	printf("PID = %ld, name D, Exiting...\n", (long)getpid());
	exit(13);

}

//create C process
void C_process(void)
{
	change_pname("C");
	printf("PID = %ld, name C, Sleeping...\n", (long)getpid());
	sleep(SLEEP_PROC_SEC);
	printf("PID = %ld, name C, Exiting...\n", (long)getpid());
	exit(17);
}

//create B process
void B_process(void)
{
	change_pname("B");
	pid_t pid;
	int status;
	
	pid = fork();
	if (pid < 0) {
		perror("B:fork");
		exit(1);
	}	
	if (pid == 0) {
		D_process();
		exit(1);
	}
	printf("PID = %ld, name B, Waiting...\n", (long)getpid());
	pid = wait(&status);
	explain_wait_status(pid, status);
	printf("PID = %ld, name B, Exiting...\n", (long)getpid());
	exit(19);	
}

//create A process
void A_process(void)
{
	change_pname("A");
	pid_t pid;
	int status;
	//create first child
	pid = fork();
	if (pid < 0) {
		perror("A:fork");
		exit(1);
	}
	if (pid == 0) {
		B_process();
		exit(1);
	}
	//create second child
	pid = fork();
	if (pid < 0) {
     	perror("A:fork");
    	exit(1);
    }
    if (pid == 0) {
        C_process();
        exit(1);
    }
	printf("PID = %ld, name A, Waiting...\n", (long)getpid());
	pid = wait(&status);
    explain_wait_status(pid, status);
	pid = wait(&status);
	explain_wait_status(pid, status);
	printf("PID = %ld, name A, Exiting...\n", (long)getpid());
	exit(16);
}

int main(void)
{
	pid_t pid;
	int status;
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		A_process();
		exit(1);
	}
	sleep(SLEEP_TREE_SEC);
	show_pstree(pid);
	pid = wait(&status);
	explain_wait_status(pid, status);
	return 0;
}
