#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "tree.h"
#include "proc-common.h"

void fork_procs(struct tree_node *root)
{
        int status,i;
        pid_t pid, p, a[root->nr_children];
        printf("PID = %ld, name %s starting...\n", (long)getpid(), root->name);
        change_pname(root->name);		
        for(i=0; i<root->nr_children; i++){
                pid=fork();			
                a[i]=pid;			
					
                if(pid<0){
                        perror("fork:");
                        exit(1);
                }
                if(pid==0){
                        fork_procs(root->children+i);
                        exit(1);
                }
        }
        wait_for_ready_children(root->nr_children);	 
			
        printf("PID = %ld, name %s stopping...\n", (long)getpid(), root->name);

        raise(SIGSTOP);		
				 
        printf("PID = %ld, name %s is awake\n",(long)getpid(), root->name);
        for(i=0; i<root->nr_children; i++){
                        kill(a[i],SIGCONT);	
                        p=wait(&status);	
                        explain_wait_status(p,status);
                }
        printf("PID = %ld, name %s exiting...\n", (long)getpid(), root->name);
        exit(0);
}
int main(int argc, char *argv[])
{
        pid_t pid;
        int status;
        struct tree_node *root;

        if (argc < 2){		 
				
                fprintf(stderr, "Usage: %s <tree_file>\n", argv[0]);
                exit(1);
        }

        root = get_tree_from_file(argv[1]);
        print_tree(root);	
        pid = fork();		
        if (pid < 0) {
                perror("main: fork");
                exit(1);
        }
        if (pid == 0) {
                fork_procs(root);
                exit(1);
        }
        wait_for_ready_children(1);	
        show_pstree(pid);		
        kill(pid, SIGCONT);
        pid=wait(&status);		
        explain_wait_status(pid, status);

        return 0;
}
