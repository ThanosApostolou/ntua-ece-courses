#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#define SLEEP_TREE_SEC 2

void explain_wait_status(pid_t pid, int status)
{
	if (WIFEXITED(status))
		fprintf(stderr, "My PID = %ld: Child PID = %ld terminated normally, exit status = %d\n",
			(long)getpid(), (long)pid, WEXITSTATUS(status));
	else if (WIFSIGNALED(status))
		fprintf(stderr, "My PID = %ld: Child PID = %ld was terminated by a signal, signo = %d\n",
			(long)getpid(), (long)pid, WTERMSIG(status));
	else if (WIFSTOPPED(status))
		fprintf(stderr, "My PID = %ld: Child PID = %ld has been stopped by a signal, signo = %d\n",
			(long)getpid(), (long)pid, WSTOPSIG(status));
	else {
		fprintf(stderr, "%s: Internal error: Unhandled case, PID = %ld, status = %d\n",
			__func__, (long)pid, status);
		exit(1);
	}
	fflush(stderr);
}

void show_pstree(pid_t p) {
	int ret;
	char cmd[1024];

	snprintf(cmd, sizeof(cmd), "echo; echo; pstree -G -c -p %ld; echo; echo",
		(long)p);
	cmd[sizeof(cmd)-1] = '\0';
	ret = system(cmd);
	if (ret < 0) {
		perror("system");
		exit(104);
	}
}


int fork_procs2 (int pfd[]) {
	int value=0;
	fprintf(stdout, "hello world\n");
	value=5;
	if (write(pfd[1], &value, sizeof(value)) != sizeof(value)) {
		perror("child: read from pipe");
		exit(1);
	}
}


void fork_procs (int pfd[]) {
	pid_t pid1, pid2;
	int value=0, x=0, y=0, pfd_new[2];
	fprintf(stdout, "hello world\n");
	value=100;
	printf("PID = %ld, creating pipe...\n", (long)getpid());
	if (pipe(pfd_new) < 0) {
		perror("pipe");
		exit(1);
	}
	pid1=fork();
	if (pid1<0) {
		perror("fork");
		exit(1);
	}
	if (pid1 == 0) {
		fork_procs2(pfd_new);
		exit(1);
	}
	if (read(pfd_new[0], &x, sizeof(x)) != sizeof(x)) {
		perror("child: read from pipe");
		exit(1);
	}
	pid2=fork();
	if (pid2<0) {
		perror("fork");
		exit(1);
	}
	if (pid2 == 0) {
		fork_procs2(pfd_new);
		exit(1);
	}
	if (read(pfd_new[0], &y, sizeof(y)) != sizeof(y)) {
		perror("child: read from pipe");
		exit(1);
	}
	value=x+y;
	if (write(pfd[1], &value, sizeof(value)) != sizeof(value)) {
		perror("child: read from pipe");
		exit(1);
	}
	exit(1);
}

int main(int argc, char *argv[]) {
    pid_t pid;
	int status, pfd[2], value=0;
	printf("PID = %ld, creating pipe...\n", (long)getpid());
	if (pipe(pfd) < 0) {
		perror("pipe");
		exit(1);
	}
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		fork_procs(pfd);
		exit(1);
	}
	show_pstree(pid);
	if (read(pfd[0], &value, sizeof(value)) != sizeof(value)) {
		perror("child: read from pipe");
		exit(1);
	}
	printf("\nThe expression result is: %d\n", value);
	return 0;
}
