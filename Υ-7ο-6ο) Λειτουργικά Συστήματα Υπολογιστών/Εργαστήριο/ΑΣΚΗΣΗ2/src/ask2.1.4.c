#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "proc-common.h"
#include "tree.h"

int get_int_from_name (char *name) {
	int x;
	sscanf(name, "%d", &x);
	return x;
}

int calculate (int x, int y, char* operator) {
	if (operator[0] == '*') {
		return x*y;
	}
	else if (operator[0] == '+') {
		return x+y;
	}
	else {
		fprintf(stderr, "Supported operations *,+\n");
		exit(1);
	}
}

int fork_process(struct tree_node *root, int pfd[])
{
	int value=0;
	
	change_pname(root->name);
	printf("PID = %ld, name %s, starting...\n", (long)getpid(), root->name);
	if (root->nr_children==0) {
		value=get_int_from_name(root->name);
	}
	else if (root->nr_children==2) {
		int pfd_new[2];
		int status, x=0, y=0;
		pid_t pid1, pid2;
		printf("PID = %ld, name %s, creating new pipe\n", (long)getpid(), root->name);
		if (pipe(pfd_new) < 0) {
			perror("pipe");
			exit(1);
		}
		pid1=fork();
		if (pid1<0) {
			perror("fork");
			exit(1);
		}
		if (pid1==0) {
			fork_process(root->children, pfd_new);
			exit(1);
		}
		if (read(pfd_new[0], &x, sizeof(x)) != sizeof(x)) {
			perror("child: read from pipe");
			exit(1);
		}
		pid2=fork();
		if (pid2<0) {
			perror("fork");
			exit(1);
		}
		if (pid2==0) {
			fork_process(root->children+1, pfd_new);
			exit(1);
		}
		if (read(pfd_new[0], &y, sizeof(y)) != sizeof(y)) {
			perror("child: read from pipe");
			exit(1);
		}
		value = calculate (x, y, root->name);
		pid1=wait(&status);
		explain_wait_status(pid1, status);
		pid2=wait(&status);
		explain_wait_status(pid2, status);
	}
	else {
		fprintf(stderr, "Error: node should have 0 or 2 childern\n");
		exit(1);
	}
	if (write(pfd[1], &value, sizeof(value)) != sizeof(value)) {
		perror("child: read from pipe");
		exit(1);
	}
	printf("PID = %ld, name %s, exiting...\n", (long)getpid(), root->name);
	exit(1);
}	



int main(int argc, char *argv[])
{
    pid_t pid;
	int status, pfd[2], value=0;
	
	struct tree_node *root;
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <input_tree_file>\n\n", argv[0]);
		exit(1);
	}
	root = get_tree_from_file(argv[1]);
	print_tree(root);
	printf("PID = %ld, name %s, creating pipe...\n", (long)getpid(), root->name);
	if (pipe(pfd) < 0) {
		perror("pipe");
		exit(1);
	}
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		fork_process(root, pfd);
		exit(1);
	}
	if (read(pfd[0], &value, sizeof(value)) != sizeof(value)) {
		perror("child: read from pipe");
		exit(1);
	}
	sleep (10);
	show_pstree(pid);
	pid=wait(&status);
	explain_wait_status(pid, status);
	printf("\nThe expression result is: %d\n", value);
	printf("PID = %ld, name %s, exiting...\n", (long)getpid(), root->name);
	return 0;
}
