#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "proc-common.h"

#define SLEEP_PROC_SEC  10
#define SLEEP_TREE_SEC  3

//create D process
void D_process(void)
{
	change_pname("D");
	printf("D: Sleeping...\n");
	sleep(SLEEP_PROC_SEC);
	printf("D: Exiting...\n");
	exit(13);

}

//create C process
void C_process(void)
{
	change_pname("C");
	printf("C:Sleeping...\n");
	sleep(SLEEP_PROC_SEC);
	printf("C:Exiting...\n");
	exit(17);
}

//create B process
void B_process(void)
{
	change_pname("B");
	pid_t pid;
	int status;
	
	pid = fork();
	if (pid < 0) {
		perror("B:fork");
		exit(1);
	}	
	if (pid == 0) {
		D_process();
		exit(1);
	}
	printf("B:Waiting...\n");
	pid = wait(&status);
	explain_wait_status(pid, status);
	printf("B:Exiting...\n");
	exit(19);	
}

//create A process
void A_process(void)
{
	change_pname("A");
	pid_t pid;
	int status;
	//create first child
	pid = fork();
	if (pid < 0) {
		perror("A:fork");
		exit(1);
	}
	if (pid == 0) {
		B_process();
		exit(1);
	}
	//create second child
	pid = fork();
	if (pid < 0) {
     	perror("A:fork");
    	exit(1);
    }
    if (pid == 0) {
        C_process();
        exit(1);
    }
	printf("A:Waiting...\n");
	pid = wait(&status);			//wait both children
        explain_wait_status(pid, status);
	pid = wait(&status);
        explain_wait_status(pid, status);
	printf("A:Exiting...\n");
	exit(16);
}

int main(void)
{
	pid_t pid;
	int status;

	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		A_process();
		exit(1);
	}

	/*
	 * Father
	 */
	/* for ask2-signals */
	/* wait_for_ready_children(1); */

	/* for ask2-{fork, tree} */
	sleep(SLEEP_TREE_SEC);

	/* Print the process tree root at pid */
	show_pstree(pid);

	/* for ask2-signals */
	/* kill(pid, SIGCONT); */

	/* Wait for the root of the process tree to terminate */
	pid = wait(&status);
	explain_wait_status(pid, status);

	return 0;
}
