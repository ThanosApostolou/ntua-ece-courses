#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <assert.h>

#include <sys/wait.h>
#include <sys/types.h>

#include "proc-common.h"
#include "request.h"

/* Compile-time parameters. */
#define SCHED_TQ_SEC 2                /* time quantum */
#define TASK_NAME_SZ 60               /* maximum size for a task's name */
#define SHELL_EXECUTABLE_NAME "shell" /* executable for shell */

/* priority enumeration
 */
typedef enum priority {
	LOW,
	HIGH,
} priority_t;

/* Processes queue implemented with a linked list structure
 */
typedef struct proc_node {
	pid_t pid;
	int id;
	priority_t p;
	struct proc_node *next;
} proc_node_t;

proc_node_t *head=NULL;
proc_node_t *active=NULL;
//proc_node_t *new_active=NULL;

const char* PriorityString[2] = {"LOW", "HIGH"};

// add process node at the end
void append_proc (pid_t pid) {
	if (head == NULL) {
		head=malloc(sizeof(proc_node_t));
		head->pid = pid;
		head->id=0;
		head->p=LOW;
		head->next=NULL;
	} else {
		proc_node_t *current = head;
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = malloc(sizeof(proc_node_t));
		current->next->pid = pid;
		current->next->id = current->id + 1;
		current->next->p=LOW;
		current->next->next = NULL;
	}
}

/* Find and start next process in list
 */
void start_next_proc(pid_t pid) {
	proc_node_t *current;
	int exists_high=0; // boolean variable to set true if there is even 1 high process
	current=head;
	while (current!=NULL) {
		if (current->p == HIGH) exists_high=1;
		if (current->pid == pid) break;
		current=current->next;
	}
	if (current == NULL) {
		fprintf(stderr, "the pid not in processes list\n");
		exit(1);
	}
	active=current->next;
	if (active == NULL || (exists_high && active->p == LOW)) {
		active = head;
	}
	kill(active->pid, SIGCONT);
}

/* Remove finished process from list and start next one.
 * Finish program if it was the last process.
 */
void remove_current_start_next_proc(pid_t pid) {
	proc_node_t *current=head, *previous=head;
	int exists_high=0; // boolean variable to set true if there is even 1 high process

	while (current!=NULL) {
		if (current->p == HIGH) exists_high=1;
		if (current->pid == pid) break;
		previous=current;
		current=current->next;
	}
	if (current == NULL) {
		fprintf(stderr, "this pid not in processes list\n");
		exit(1);
	}
	if (current == head) {
		head=current->next;
		previous=head;
	} else {
		previous->next = current->next;
	}
	free(current);
	if (head == NULL) {
		fprintf (stdout, "all processes terminated successfully\n");
		exit(0);
	} else {
		active=previous->next;
		if (active == NULL || (exists_high && active->p == LOW)) {
			active = head;
		}
		kill(active->pid, SIGCONT);
	}
}


static int sched_change_priority (int id, priority_t p) {
	proc_node_t *current=head,*previous=head;

	// find process node with the specific id
	while (current != NULL) {
		if (current->id == id) break;
		previous=current;
		current=current->next;
	}
	if (current == NULL) {
		fprintf(stderr, "there's no process with this id\n");
		return -ENOSYS;
	} else {
		if (current != head && (current->p == LOW && p == HIGH)) {
			previous->next = current->next;
			current->next = head;
			head=current;
		} else if (current->p == HIGH && p == LOW) {
			proc_node_t *last_high=current;
			while (last_high->next != NULL && last_high->next->p != LOW) {
				last_high=last_high->next;
			}
			if (current != last_high) {
				if (current == head) {
					head = current->next;
				} else {
					previous->next = current->next;
				}
				current->next = last_high->next;
				last_high->next = current;
			}
		}
		current->p = p;
		return 0;
	}
}

/* Print a list of all tasks currently being scheduled.  */
static void sched_print_tasks(void) {
	proc_node_t *current;
	current=head;
	while (current != NULL) {
		fprintf(stdout, "PID: %d \t ID: %d \t Priority: %s", current->pid, current->id, PriorityString[current->p]);
		if (current == active) {
			fprintf(stdout, "\tActive Process\n");
		} else {
			fprintf(stdout, "\n");
		}
		current=current->next;
	}
}

/* Send SIGKILL to a task determined by the value of its
 * scheduler-specific id.
 */
static int sched_kill_task_by_id(int id) {
	proc_node_t *current;

	// find process node with the specific id
	current=head;
	while (current != NULL) {
		if (current->id == id) break;
		current=current->next;
	}
	if (current == NULL) {
		fprintf(stderr, "there's no process with this id\n");
		return -ENOSYS;
	} else {
		remove_current_start_next_proc(current->pid);
		return 0;
	}
}


/* Create a new task.  */
static void sched_create_task(char *executable) {
	pid_t pid;
	pid=fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) { //child
		char *newargv[] = { executable, NULL, NULL, NULL };
		char *newenviron[] = { NULL };

		//stop process when it's first created
		raise(SIGSTOP);
		execve(executable, newargv, newenviron);

		/* execve() only returns on error */
		perror("execve");
		exit(1);
	}
	// father
	append_proc (pid);
}

/* Process requests by the shell.  */
static int process_request(struct request_struct *rq) {
	switch (rq->request_no) {
		case REQ_PRINT_TASKS:
			sched_print_tasks();
			return 0;

		case REQ_KILL_TASK:
			return sched_kill_task_by_id(rq->task_arg);

		case REQ_EXEC_TASK:
			sched_create_task(rq->exec_task_arg);
			return 0;

		case REQ_HIGH_TASK:
			return sched_change_priority(rq->task_arg, HIGH);

		case REQ_LOW_TASK:
			return sched_change_priority(rq->task_arg, LOW);

		default:
			return -ENOSYS;
	}
}

/*
 * SIGALRM handler
 */
static void sigalrm_handler(int signum) {
	kill (active->pid, SIGSTOP);
}

/*
 * SIGCHLD handler
 */
static void sigchld_handler(int signum) {
	int status;
	pid_t p;
	for (;;) {
		p = waitpid(-1, &status, WUNTRACED | WNOHANG);
		if (p < 0) {
			perror("waitpid");
			exit(1);
		}
		if (p == 0)
			break;
		explain_wait_status(p, status);
		if (WIFEXITED(status) || WIFSIGNALED(status)) {
			/* A child has died */
			alarm(SCHED_TQ_SEC);
			remove_current_start_next_proc(p);
		}
		if (WIFSTOPPED(status)) {
			/* A child has stopped due to SIGSTOP/SIGTSTP, etc */
			alarm(SCHED_TQ_SEC);
			start_next_proc(p);
		}
	}
}

/* Disable delivery of SIGALRM and SIGCHLD. */
static void signals_disable(void) {
	sigset_t sigset;

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGALRM);
	sigaddset(&sigset, SIGCHLD);
	if (sigprocmask(SIG_BLOCK, &sigset, NULL) < 0) {
		perror("signals_disable: sigprocmask");
		exit(1);
	}
}

/* Enable delivery of SIGALRM and SIGCHLD.  */
static void signals_enable(void) {
	sigset_t sigset;

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGALRM);
	sigaddset(&sigset, SIGCHLD);
	if (sigprocmask(SIG_UNBLOCK, &sigset, NULL) < 0) {
		perror("signals_enable: sigprocmask");
		exit(1);
	}
}


/* Install two signal handlers.
 * One for SIGCHLD, one for SIGALRM.
 * Make sure both signals are masked when one of them is running.
 */
static void install_signal_handlers(void) {
	sigset_t sigset;
	struct sigaction sa;

	sa.sa_handler = sigchld_handler;
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGCHLD);
	sigaddset(&sigset, SIGALRM);
	sa.sa_mask = sigset;
	if (sigaction(SIGCHLD, &sa, NULL) < 0) {
		perror("sigaction: sigchld");
		exit(1);
	}

	sa.sa_handler = sigalrm_handler;
	if (sigaction(SIGALRM, &sa, NULL) < 0) {
		perror("sigaction: sigalrm");
		exit(1);
	}

	/*
	 * Ignore SIGPIPE, so that write()s to pipes
	 * with no reader do not result in us being killed,
	 * and write() returns EPIPE instead.
	 */
	if (signal(SIGPIPE, SIG_IGN) < 0) {
		perror("signal: sigpipe");
		exit(1);
	}
}

static void do_shell(char *executable, int wfd, int rfd) {
	char arg1[10], arg2[10];
	char *newargv[] = { executable, NULL, NULL, NULL };
	char *newenviron[] = { NULL };

	sprintf(arg1, "%05d", wfd);
	sprintf(arg2, "%05d", rfd);
	newargv[1] = arg1;
	newargv[2] = arg2;

	raise(SIGSTOP);
	execve(executable, newargv, newenviron);

	/* execve() only returns on error */
	perror("scheduler: child: execve");
	exit(1);
}

/* Create a new shell task.
 *
 * The shell gets special treatment:
 * two pipes are created for communication and passed
 * as command-line arguments to the executable.
 */
static void sched_create_shell(char *executable, int *request_fd, int *return_fd) {
	pid_t p;
	int pfds_rq[2], pfds_ret[2];

	if (pipe(pfds_rq) < 0 || pipe(pfds_ret) < 0) {
		perror("pipe");
		exit(1);
	}

	p = fork();
	if (p < 0) {
		perror("scheduler: fork");
		exit(1);
	}

	if (p == 0) {
		/* Child */
		close(pfds_rq[0]);
		close(pfds_ret[1]);
		do_shell(executable, pfds_rq[1], pfds_ret[0]);
		assert(0);
	}
	/* Parent */
	close(pfds_rq[1]);
	close(pfds_ret[0]);
	*request_fd = pfds_rq[0];
	*return_fd = pfds_ret[1];

	// add shell procedure to list
	append_proc(p);
}

static void shell_request_loop(int request_fd, int return_fd) {
	int ret;
	struct request_struct rq;

	/*
	 * Keep receiving requests from the shell.
	 */
	for (;;) {
		if (read(request_fd, &rq, sizeof(rq)) != sizeof(rq)) {
			perror("scheduler: read from shell");
			fprintf(stderr, "Scheduler: giving up on shell request processing.\n");
			break;
		}

		signals_disable();
		ret = process_request(&rq);
		signals_enable();

		if (write(return_fd, &ret, sizeof(ret)) != sizeof(ret)) {
			perror("scheduler: write to shell");
			fprintf(stderr, "Scheduler: giving up on shell request processing.\n");
			break;
		}
	}
}

int main(int argc, char *argv[])
{
	int nproc;
	/* Two file descriptors for communication with the shell */
	static int request_fd, return_fd;

	/* Create the shell. */
	sched_create_shell(SHELL_EXECUTABLE_NAME, &request_fd, &return_fd);
	/* shell gets added in scheduler's tasks by sched_create_shell function*/

	/*
	 * For each of argv[1] to argv[argc - 1],
	 * create a new child process, add it to the process list.
	 */
	int i;
 	for (i=1; i < argc;  i++) {
		sched_create_task(argv[i]);
	}
	nproc = argc; /* number of proccesses goes here */

	/* Wait for all children to raise SIGSTOP before exec()ing. */
	wait_for_ready_children(nproc);

	/* Install SIGALRM and SIGCHLD handlers. */
	install_signal_handlers();

	if (nproc == 0) {
		fprintf(stderr, "Scheduler: No tasks. Exiting...\n");
		exit(1);
	}
	// run first process
	active=head;
	alarm(SCHED_TQ_SEC);
	kill (active->pid, SIGCONT);

	shell_request_loop(request_fd, return_fd);

	/* Now that the shell is gone, just loop forever
	 * until we exit from inside a signal handler.
	 */
	while (pause())
		;

	/* Unreachable */
	fprintf(stderr, "Internal error: Reached unreachable point\n");
	return 1;
}
