#include "fconc.h"
#include "write.h"
#include <stdio.h>

int main (int argc, char **argv) {
	if (argc == 3) {
		fconc (argv[1], argv[2], "fconc.out");
	}
	else if (argc == 4) {
		fconc (argv[1], argv[2], argv[3]);
	}
	else {
		printf ("Usage: ./fconc infile1 infile2 [outfile (default:fconc.out)]\n");
	}
	return 0;
}
