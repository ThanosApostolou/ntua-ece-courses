#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

size_t doWrite(int fd, const char *buff, int len) {
	size_t idx=0;
	ssize_t wcnt;
	do {
		wcnt = write(fd, buff + idx, len - idx);
		if (wcnt == -1) { //error
			perror ("write");
			exit(1);
		}
		idx += wcnt;
	} while (idx < len);	
	return idx;
}

void write_file(int fd, const char *infile) {
	int fd_read;
	size_t len;
	fd_read = open(infile, O_RDONLY);
	if (fd_read == -1) {
		perror ("open");
		exit(1);
	}
	char buff[4];
	ssize_t rcnt;
	for (;;){
		rcnt = read(fd_read,buff,sizeof(buff)-1);
		if (rcnt == 0) /* end-of-file */
			break;
		if (rcnt == -1) { /* error */
			perror("read");
			exit(1);
		}
		buff[rcnt] = '\0';
		size_t idx=0;
		for (;;) {
			if (buff[idx] == '\0')
				break;
			len=strlen(buff);
			idx = doWrite(fd, buff, len);
		}
	}
	close(fd_read);
	return;
}

int main (int argc, char **argv) {
	int fd, oflags, mode;
	oflags = O_CREAT | O_WRONLY | O_TRUNC;
	mode = S_IRUSR | S_IWUSR;
	fd = open("output.txt", oflags, mode);
	if (fd==-1) {
		perror("open");
		exit(1);
	}
	write_file(fd, "test.txt");
	close (fd);
	oflags = O_CREAT | O_WRONLY | O_APPEND;
	fd = open("output.txt", oflags, mode);
	if (fd==-1) {
		perror("open");
		exit(1);
	}
	write_file(fd, "test2.txt");
	return 0;
}
