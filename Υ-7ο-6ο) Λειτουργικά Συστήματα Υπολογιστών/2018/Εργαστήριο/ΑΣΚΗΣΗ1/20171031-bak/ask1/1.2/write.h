#ifndef WRITE_H
#define WRITE_H

size_t doWrite (int fd, const char *buff, int len);
void write_file (int fd, const char *infile);

#endif
