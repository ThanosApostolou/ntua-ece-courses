#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "proc-common.h"
#include "tree.h"

void fork_process(struct tree_node *root)
{
	int status, i;
	pid_t pid;
	change_pname(root->name);
	printf("%s has started\n",root->name);
	if(root->nr_children==0){
		printf("%s:Sleeping...\n",root->name);
		sleep(10);
	}
	//if node has children, fork all of them
	else{
		for(i=0; i<root->nr_children; i++){
			pid =fork();
			if(pid<0){
				perror("fork");
				exit(1);
			}
			if(pid==0){
				printf("%s running\n",root->name);
				fork_process(root->children+i);
				exit(1);
			}
		}
	}
	//wait for all children to finish		
	for(i=0; i<root->nr_children; i++){
		pid=wait(&status);
		explain_wait_status(pid, status);
	}
	printf("%s:Exiting...\n",root->name);
	exit(0);
}	



int main(int argc, char *argv[])
{
    pid_t pid;
	int status;
	struct tree_node *root;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <input_tree_file>\n\n", argv[0]);
		exit(1);
	}
	root = get_tree_from_file(argv[1]);
	print_tree(root);

	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		fork_process(root);
		exit(1);
	}
	sleep(5);
	show_pstree(pid);
	pid=wait(&status);
	explain_wait_status(pid, status);
	return 0;
}
