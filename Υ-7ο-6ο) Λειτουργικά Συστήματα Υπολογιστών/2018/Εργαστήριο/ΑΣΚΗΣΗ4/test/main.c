#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>

void prog (char prog[]) {
	char *newargv[] = { prog, NULL, NULL, NULL };
	char *newenviron[] = { NULL };

	printf("I am %s, PID = %ld\n", prog, (long)getpid());
	printf("About to replace myself with the executable %s...\n", prog);
	sleep(2);

	execve(prog, newargv, newenviron);

	/* execve() only returns on error */
	perror("execve");
	exit(1);
}

int main (int argc, char *argv[]) {
    printf ("skata\n");
    pid_t pid;
    int status;

    for (int i=1; i < argc; i++) {
        pid=fork();
        if (pid < 0) {
    		perror("main: fork");
    		exit(1);
    	}
        if (pid == 0) {
    		prog(argv[i]);
    		exit(1);
    	}
    }
    pid = wait(&status);
    return 0;
}
