#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
//#include <ctype.h>
#include <signal.h>

void child (int counter, int n, int k, int read_pd[], int write_pd[]) {
	long long next, current;
	close(read_pd[1]);
	close(write_pd[0]);

	// stop when counter is greater than k
	while (counter <= k) {
		read(read_pd[0], &current, sizeof(current));
		printf("Child with id %d received %lld with counter %d\n", getpid(), current, counter);
		next = current * counter;
		printf("Child with id %d sending int:%lld\n", getpid(), next);
		write(write_pd[1], &next, sizeof(next));

		// increase counter n times
		counter+=n;
	}
}

int main(int argc, char* argv[]) {
	int status;
	int n=0, k=0, fact=1,c;
	int (*PIPES)[2];
	pid_t p;

	// check command line arguments
	if (argc != 3) {
		printf( "Invalid number of arguments\n");
		exit(1);
	} else { // convert string arguments to integers
		n = atoi(argv[1]);
		k = atoi(argv[2]);
		if (n < 1 || n > 10) {
			printf("Rule: 1 <= n <= 10\n");
			exit(1);
		}
		if (n==1) {
			for(c=1; c<=k; c++)
				fact=fact*c;
			printf("Factorial is %d\n",fact);
			exit(1);
		}

	}

	// create n pipes
	PIPES = calloc (n, sizeof(int[2]));
	for (int i=0; i<n; i++) {
		pipe(PIPES[i]);
	}

	// create n children
	for (int i=0; i<n; i++) {
		p = fork();
		if (p < 0) {
			perror("fork");
			exit(1);
		}
		if (p == 0) {
			//child
			if (i != n-1) {
				child(i+1, n, k, PIPES[i], PIPES[i+1]);
			} else {
				child(i+1, n, k, PIPES[i], PIPES[0]);
			}
			exit(0);
		} else {
			// parent
			//printf("I am the father with PID %d and i have a child with pid %d\n", getpid(), p);

		}
	}
	// send initial value 1 at 1st children
	long long initial=1;
	close(PIPES[0][0]);
	write(PIPES[0][1], &initial, sizeof(long long));
	for (int i=0; i<n; i++) {
		wait(&status);
	}
	return(0);
}
