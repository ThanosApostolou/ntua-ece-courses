#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
//#include <ctype.h>
#include <signal.h>

void child (int counter, int n, int k, int read_pd[], int write_pd[]) {
	long long next, current;	//~21!
	close(read_pd[1]);		//close the output side of the pipe we want to read from
	close(write_pd[0]);		//close the input side of the pipe to write to
					//the EOF will never be returned if the unnecessary ends of the pipe are not closed
	// stop when counter is greater than k
	while (counter <= k) {
		if (read(read_pd[0], &current, sizeof(current)) <= 0)
		{	printf("An error occurred in read\n");
			exit(-1);
		}			//read up to sizeof(current) bytes from read_pd[0] into the buffer starting at current
		next = current * counter;
		// debug output
		printf("Child with id %d received %lld with counter %d\n", getpid(), current, counter);
		printf("Child with id %d sending int:%lld\n", getpid(), next);

		// increase counter n times
		counter+=n;
		if (current < 0) {
			long long temp = -1;
			if (write(write_pd[1], &temp, sizeof(temp)) <= 0)
			{	printf("An error occurred in write\n");
				exit(-1);
			}
			break;
		} else if (counter > k ) {
			long long temp = -1;
			if (write(write_pd[1], &temp, sizeof(temp)) <= 0)
			{	printf("An error occurred in write\n");
				exit(-1);
			}
			printf("Child with id %d says that Factorial of %d is %lld\n", getpid(), k, next);
			break;
		} else {
			if (write(write_pd[1], &next, sizeof(next)) <= 0)
			{	printf("An error occurred in write\n");
				exit(-1);
			}
		}
	}
}

int main(int argc, char* argv[]) {
	int status;
	int n=0, k=0;
	long long int fact=1, c;
	int (*PIPES)[2];	//[]->pipe number, [2]-> 0:stdin, 1:stdout, 2:stderr
	pid_t p;

	// check command line arguments
	if (argc != 3) {
		printf( "Invalid number of arguments\n");
		exit(1);
	} else { // convert string arguments to integers
		n = atoi(argv[1]);
		k = atoi(argv[2]);
		if (n < 1 || n > 10) {
			printf("Rule: 1 <= n <= 10\n");
			exit(1);
		}
		if (k < 0) {
			printf("Negative input for factorial\n");
			exit(1);
		}
		if (k==0) {
			printf("Factorial of 0! is 1\n");
			exit(1);
		}
	}

	// create n pipes
	PIPES = calloc (n, sizeof(int[2]));	//allocate requested memor and return a pointer to it
	for (int i=0; i<n; i++) {		//calloc sets allocated memory to zero
		pipe(PIPES[i]);			//fd=PIPES[i]
	}

	// create n children
	if (n == 1) {
		p = fork();
		if (p < 0) {
			perror("fork");
			exit(1);
		}
		if (p==0) {
		for(c=1; c<=k; c++)
			fact=fact*c;
		printf("Factorial is %lld\n",fact);
		exit(1);
		}
	} else {
		for (int i=0; i<n; i++) {
			p = fork();
			if (p < 0) {
				perror("fork");
				exit(1);
			}
			if (p == 0) {
				//child
				if (i != n-1) {
					child(i+1, n, k, PIPES[i], PIPES[i+1]); //read from pipes[i] and write to pipes[i+1]
				} else {
					child(i+1, n, k, PIPES[i], PIPES[0]);   //pipe from child(n)->child(0)
				}
				exit(0);
			} else {
				// parent
				//printf("I am the father with PID %d and i have a child with pid %d\n", getpid(), p);
				if (i==0)
				{
					long long initial=1;
					close(PIPES[0][0]);
					write(PIPES[0][1], &initial, sizeof(long long));
				}
			}
		}
	}
	// send initial value 1 at 1st children
	//	long long initial=1;
	//	close(PIPES[0][0]);
	//	write(PIPES[0][1], &initial, sizeof(long long));
	for (int i=0; i<n; i++) {
		wait(&status);		//wait until all children are finished
	}
	return 0;
}
