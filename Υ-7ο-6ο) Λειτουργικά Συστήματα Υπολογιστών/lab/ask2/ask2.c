#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int counter = 0;

void sighandler (int signum) {
	counter++;
	if (kill(getpid(), SIGCONT) == -1) {
		printf("sigcont error\n");
	}
}

void child () {
	struct sigaction sa;
    sa.sa_handler = sighandler;
    sigemptyset(&(sa.sa_mask));
    sigaddset(&(sa.sa_mask), SIGINT);
    sigaction(SIGINT, &sa, NULL);
	pause();
	while (1) {
		printf("Childi %d is executed (%d)\n", getpid(), counter);
		sleep(1);
	}
}

int main(int argc, char* argv[]) {
	//int status;
	pid_t p;
	pid_t CHILD_PIDS[5];

	for (int i=0; i<5; i++) {
		p = fork();
		if (p < 0) {
			fprintf(stderr,"fork error!\n");
			exit(1);
		}
		if (p == 0) {
			// child
			child();
			exit(0);
		} else {
			// parent
			CHILD_PIDS[i] = p;
		}
	}
	for (int j=0; j<4; j++) {
		for (int i=0; i<5; i++) {
			printf("continue process %d\n", CHILD_PIDS[i]);
			if (kill(CHILD_PIDS[i], SIGINT) == -1){
				printf("sigint error\n");
			}
			sleep(1);
			if (kill(CHILD_PIDS[i], SIGSTOP) == -1) {
				printf("sigstop error\n");
			}
		}
	}
	return(0);
} 
