#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
//#include <ctype.h>
#include <signal.h>


int n = 0;
int child_number = 0;
int enabled_loop;

// stop child's loop
void sigusr2_handler (int signum) {
	enabled_loop = 0;
}

// start child's loop
void sigusr1_handler (int signum) {
	enabled_loop=1;
//	signal(SIGUSR2, sigusr2_handler);
	while (enabled_loop) {
		n++;
		printf("Child%d %d is executed (%d)\n", child_number, getpid(), n);
		sleep(1);
	}
}

void child () {
	signal(SIGUSR1, sigusr1_handler);
	signal(SIGUSR2, sigusr2_handler);
	while (1) {
		pause();
	}
}

int main(int argc, char* argv[]) {
	//int status;
	pid_t p;
	pid_t CHILD_PIDS[5];
	int Queue[5];
	int Order[5] = {1,3,4,2,5};
	// check command line arguments
	if (argc != 6) {
		printf( "Invalid number of arguments\n");
		exit(1);
	} else { // convert string arguments to integers
		for (int i=0; i<5; i++) {
			Queue[i] = atoi(argv[i+1]);
			if (Queue[i]==0) {
				printf("Invalid input type\n");
				exit(1);
			}
		}
		for (int i=0; i<5; i++) {
			for (int j = i + 1; j<5; j++){
			if (Queue[i]==Queue[j] || Queue[i] > 5 || Queue[j] > 5 || Queue[i]<0 || Queue[j]<0) {
				printf("Invalid input\n");
				exit(1);
				}

			}
		}

	}
	

	// create 5 children
	for (int i=0; i<5; i++) {
		child_number = Order[i];
		p = fork();
		if (p < 0) {
			fprintf(stderr,"fork error!\n");
			exit(1);
		}
		if (p == 0) {
			//child
			child();
		
		} else {
			// parent
			//printf("I am the father with PID %d and i have a child with pid %d\n", getpid(), p);
			CHILD_PIDS[child_number - 1] = p;
		}

	}
	sleep(1);
	// continue children 4 times for 3 secs based on given queue
	for (int j=1; j<5; j++) {
		for (int i=0; i<5; i++) {
						
			if (kill(CHILD_PIDS[Queue[i]-1], SIGUSR1) == -1) {
				printf("SIGUSR1 error\n");
			}
			sleep(3);
			if (kill(CHILD_PIDS[Queue[i]-1], SIGUSR2) == -1) {
				printf("SIGUSR2 error\n");
			}
		}
	}
	// terminate children
	for (int i=0; i<5; i++) {
		if (kill(CHILD_PIDS[Queue[i]-1], SIGTERM) == -1) {
			printf("SIGTERM error\n");
		}
	}
	return(0);
} 
