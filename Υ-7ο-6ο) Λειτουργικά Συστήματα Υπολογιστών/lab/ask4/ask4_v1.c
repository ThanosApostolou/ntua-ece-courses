#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
//#include <ctype.h>
#include <signal.h>
#include <fcntl.h>
#include <semaphore.h>

void print_critical (int child_number) {
	// open shared semaphore
	sem_t *semaphore = sem_open("/my_semaphore", O_RDWR);

	// wait until critical area is available
	sem_wait(semaphore);

	for (int i=0; i<5; i++) {
		printf ("Child%d %d executes a critical section\n", child_number, getpid());
		sleep(1);
	}
	// increase semaphore
	sem_post(semaphore);
}

void print_non_critical (int child_number) {
	for (int i=0; i<7; i++) {
		printf ("Child%d %d executes a non critical section\n", child_number, getpid());
		sleep(1);
	}
}


void child1 () {
	int child_number = 1;
	print_critical(child_number);
	print_non_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);

}

void child2 () {
	int child_number=2;
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
}

void child3 () {
	int child_number=3;
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
}

int main(int argc, char* argv[]) {
	int status;
	pid_t p;

	// silently unlink semaphore in case of previous failed execution
	sem_unlink("/my_semaphore");
	// create shared semaphore between processes
	sem_t *semaphore = sem_open("/my_semaphore", O_CREAT | O_EXCL, (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP), 1);
	if (semaphore == SEM_FAILED) {
        perror("sem_open(3) failed");
        exit(EXIT_FAILURE);
    }

	// create 3 children
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child1();
		exit(0);
	}
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child2();
		exit(0);
	}
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child3();
		exit(0);
	}

	// wait for processes to finish
	for (int i=0; i<3; i++) {
		wait(&status);		//wait until all children are finished
	}

	// unlink semaphore
	if (sem_unlink("/my_semaphore") < 0) {
        perror("sem_unlink failed");
	}

	return 0;
}
