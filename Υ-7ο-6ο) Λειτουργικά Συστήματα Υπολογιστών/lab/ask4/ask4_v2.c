#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
//#include <ctype.h>
#include <signal.h>
#include <fcntl.h>
#include <semaphore.h>

sem_t *semaphore;
int read_pipe[2], write_pipe[2];

void print_critical (int child_number) {
	// semaphore wait
	sem_t *semaphore;
	semaphore =
	sem_wait(semaphore);

	for (int i=0; i<5; i++) {
		printf ("Child%d %d executes a critical section\n", child_number, getpid());
		sleep(1);
	}
	// increase semaphore
	sem_post(semaphore);
}

void print_non_critical (int child_number) {
	for (int i=0; i<7; i++) {
		printf ("Child%d %d executes a non critical section\n", child_number, getpid());
		sleep(1);
	}
}


void child1 (int read_pd[], int write_pd[]) {
	int child_number = 1;
	print_critical(child_number);
	print_non_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);

}
/*
void child2 (int read_pd[], int write_pd[]) {
	int child_number=2;
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
}

void child3 (int read_pd[], int write_pd[]) {
	int child_number=3;
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
}*/

int main(int argc, char* argv[]) {
	int status;
	pid_t p;

	// create semaphore
	ftruncate(fd, sizeof(sem_t));
	pipe(read_pipe);
	pipe(write_pipe);
	sem_init(semaphore, 0, 1);

	// create 3 children
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child1();
		exit(0);
	}
	/*
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child2();
		exit(0);
	}
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child3();
		exit(0);
	}
	*/
	// wait for processes to finish
	for (int i=0; i<3; i++) {
		wait(&status);		//wait until all children are finished
	}

	// unlink semaphore
	if (sem_unlink("/my_semaphore") < 0) {
        perror("sem_unlink failed");
	}

	return 0;
}
