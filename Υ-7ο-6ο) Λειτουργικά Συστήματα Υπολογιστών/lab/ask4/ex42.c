#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void semaInit (int *S){
	if (pipe(S) == -1) {
		printf("Pipe Error\n");
		exit(-1);
	}
	return;
}

void semaSignal (int *S){
	char  ch = 'X';
	if(write(S[1], &ch, sizeof(char)) != 1){
		printf("Write Error\n");
		exit(-1);
	}
	return;
}


void semaWait(int *S){
	char ch;
	if(read(S[0], &ch, sizeof(char)) !=1){
		printf("Read Error\n");
		exit(-1);
	}
	return;
}




int main(int argc, char **argv){
	pid_t p1,p2,p3;
	int status;
	int i;
	int S1[2], S2[2];
	semaInit(S1);
	semaInit(S2);
	p1 = fork();
	if(p1 < 0){
			perror("fork");
			exit(-1);
	}
	p2 = fork();
	if(p2 < 0){
			perror("fork");
			exit(-1);
	}
	p3 = fork();
	if(p3 < 0){
			perror("fork");
			exit(-1);
	}
	if(p1 == 0){
		for(i=0; i<1; i++){
		    printf("Child 1 %ld executes a critical section\n", (long)getpid());
		}
		semaSignal(S1);
		for(i=0; i<2; i++){
		    printf("Child 1 %ld executes a non critical section\n", (long)getpid());
		}
		for(i=0; i<2; i++){
		    printf("Child 1 %ld executes a non critical section\n", (long)getpid());
		}
		semaWait(S2);
		for(i=0; i<1; i++){
		    printf("Child 1 %ld executes a critical section\n", (long)getpid());
		}
		semaSignal(S1);
		for(i=0; i<2; i++){
		    printf("Child 1 %ld executes a non critical section\n", (long)getpid());
		}
		semaWait(S1);
		for(i=0; i<1; i++){
		    printf("Child 1 %ld executes a critical section\n", (long)getpid());
		}
		semaSignal(S2);

	}

	if(p2 == 0){

		for(i=0; i<2; i++){
		    printf("Child 2 %ld executes a non critical section\n", (long)getpid());
		}
		semaWait(S2);
		for(i=0; i<1; i++){
		    printf("Child 2 %ld executes a critical section\n", (long)getpid());
		}
		semaSignal(S1);
		for(i=0; i<2; i++){
		    printf("Child 2 %ld executes a non critical section\n", (long)getpid());
		}
		semaWait(S1);
		for(i=0; i<1; i++){
		    printf("Child 2 %ld executes a critical section\n", (long)getpid());
		}
		semaSignal(S2);
		for(i=0; i<2; i++){
		    printf("Child 2 %ld executes a non critical section\n", (long)getpid());
		}
		semaWait(S2);
		for(i=0; i<1; i++){
		    printf("Child 2 %ld executes a critical section\n", (long)getpid());
		}

	}

	if(p3 == 0){
		semaWait(S1);
		for(i=0; i<1; i++){
		    printf("Child 3 %ld executes a critical section\n", (long)getpid());
		}
		semaSignal(S2);
		for(i=0; i<2; i++){
		    printf("Child 3 %ld executes a non critical section\n", (long)getpid());
		}
		semaWait(S1);
		for(i=0; i<1; i++){
		    printf("Child 3 %ld executes a critical section\n", (long)getpid());
		}
		semaSignal(S2);
		for(i=0; i<1; i++){
		    printf("Child 3 %ld executes a non critical section\n", (long)getpid());
		}
		semaWait(S2);
		for(i=0; i<1; i++){
		    printf("Child 3 %ld executes a critical section\n", (long)getpid());
		}
		semaSignal(S1);
		for(i=0; i<2; i++){
		    printf("Child 3 %ld executes a non critical section\n", (long)getpid());
		}



	}


	return 0;

}
