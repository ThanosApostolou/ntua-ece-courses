#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
//#include <ctype.h>
#include <signal.h>
#include <fcntl.h>
#include <semaphore.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_RESET   "\x1b[0m"

void print_critical (int child_number, sem_t *semaphore) {
	// wait until critical area is available
	sem_wait(semaphore);	//decrements (locks) the sem pointed to by semaphore. If sem's value>0, the decrement proceeds and the
				//function returns imm. If sem=0, the call blocks until either it becomes possible to perform the decr
				//or a signal handler interrupts the call
	for (int i=0; i<5; i++) {
		printf (ANSI_COLOR_RED "Child%d %d executes a critical section (%d)" ANSI_COLOR_RESET "\n", child_number, getpid(), i+1);
		sleep(1);
	}

	// increase semaphore
	sem_post(semaphore);	//increments (unlocks) the sem pointed to by semaphore. If sem>0 then another process or thread blocked
				//in a sem_wait call will be woken up and proceed to lock the semaphore. Returns 0 on success
}

void print_non_critical (int child_number) {
	for (int i=0; i<7; i++) {
		printf (ANSI_COLOR_GREEN "Child%d %d executes a non critical section (%d)" ANSI_COLOR_RESET "\n", child_number, getpid(), i+1);
		sleep(1);
	}
}


void child1 () {
	int child_number = 1;
	// open shared semaphore
	sem_t *semaphore = sem_open("/my_semaphore", O_RDWR);	//initialize and open a named semaphore
								//returns the address of the new semaphore
	print_critical(child_number, semaphore);
	print_non_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number, semaphore);
	print_non_critical(child_number);
	print_critical(child_number, semaphore);
}

void child2 () {
	int child_number=2;
	// open shared semaphore
	sem_t *semaphore = sem_open("/my_semaphore", O_RDWR);	//initialize and open a named semaphore
								//returns the address of the new semaphore
	print_non_critical(child_number);
	print_critical(child_number, semaphore);
	print_non_critical(child_number);
	print_critical(child_number, semaphore);
	print_non_critical(child_number);
	print_critical(child_number, semaphore);
}

void child3 () {
	int child_number=3;
	// open shared semaphore
	sem_t *semaphore = sem_open("/my_semaphore", O_RDWR);	//initialize and open a named semaphore
								//returns the address of the new semaphore
	print_critical(child_number, semaphore);
	print_non_critical(child_number);
	print_critical(child_number, semaphore);
	print_non_critical(child_number);
	print_critical(child_number, semaphore);
	print_non_critical(child_number);
}

int main(int argc, char* argv[]) {
	int status;
	pid_t p;

	// silently unlink semaphore in case of previous failed execution
	sem_unlink("/my_semaphore");	//removes the named semaphore my_sem, the semaphore name is removed immediately
					//the sem is destroyed once all other processes that have the semaphore open close it
	// create shared semaphore between processes
	sem_t *semaphore = sem_open("/my_semaphore", O_CREAT | O_EXCL, (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP), 1);
					//error is returned if a sem with the given name exists
	if (semaphore == SEM_FAILED) {
        perror("sem_open(3) failed");
        exit(EXIT_FAILURE);
    }

	// create 3 children
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child1();
		exit(0);
	}
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child2();
		exit(0);
	}
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child3();
		exit(0);
	}

	// wait for processes to finish
	for (int i=0; i<3; i++) {
		wait(&status);		//wait until all children are finished
	}

	// unlink semaphore
	if (sem_unlink("/my_semaphore") < 0) {
        perror("sem_unlink failed");
	}

	return 0;
}
