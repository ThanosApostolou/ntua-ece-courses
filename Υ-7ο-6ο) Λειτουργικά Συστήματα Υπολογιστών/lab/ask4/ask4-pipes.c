#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
//#include <ctype.h>
#include <signal.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// pipes as global variables so that we don't pass them all the time
int wait_pipe[2], signal_pipe[2];

void print_critical (int child_number) {
	int semaphore=1;

	// wait until wait_pipe has data
	read(wait_pipe[0], &semaphore, sizeof(int));

	for (int i=0; i<5; i++) {
		printf (ANSI_COLOR_RED "Child%d %d executes a critical section (%d)" ANSI_COLOR_RESET "\n", child_number, getpid(), i+1);
		sleep(0);
	}

	// write to signal_pipe in order to unlock
	write(signal_pipe[1], &semaphore, sizeof(int));
}

void print_non_critical (int child_number) {
	for (int i=0; i<7; i++) {
		printf (ANSI_COLOR_GREEN "Child%d %d executes a non critical section (%d)" ANSI_COLOR_RESET "\n", child_number, getpid(), i+1);
		sleep(0);
	}
}


void child1 () {
	int child_number = 1;
	close(wait_pipe[1]);		//close the output side of the pipe we want to read from
	close(signal_pipe[0]);		//close the input side of the pipe to write to

	print_critical(child_number);
	print_non_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);

}

void child2 () {
	int child_number=2;
	close(wait_pipe[1]);		//close the output side of the pipe we want to read from
	close(signal_pipe[0]);		//close the input side of the pipe to write to

	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
}

void child3 () {
	int child_number=3;
	close(wait_pipe[1]);		//close the output side of the pipe we want to read from
	close(signal_pipe[0]);		//close the input side of the pipe to write to

	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
	print_critical(child_number);
	print_non_critical(child_number);
}

int main(int argc, char* argv[]) {
	int status;
	pid_t p;

	if (pipe(wait_pipe) == -1) {
		perror("pipe");
		exit(1);
	}
	if (pipe(signal_pipe) == -1) {
		perror("pipe");
		exit(1);
	}

	// create 3 children
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child1();
		exit(0);
	}
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child2();
		exit(0);
	}
	p = fork();
	if (p < 0) {
		perror("fork");
		exit(1);
	}
	if (p == 0) {
		//child
		child3();
		exit(0);
	}

	int semaphore = 1;
	// close the oposite pipes than the children
	close(wait_pipe[0]);
	close(signal_pipe[1]);

	// keep writing in order to inform processes about unlocked semaphore
	// keep reading for an unlocked semaphore until to read
	while (write(wait_pipe[1], &semaphore, sizeof(int))) {
		read(signal_pipe[0], &semaphore, sizeof(int));
	}

	// wait for processes to finish
	for (int i=0; i<3; i++) {
		wait(&status);		//wait until all children are finished
	}

	return 0;
}
