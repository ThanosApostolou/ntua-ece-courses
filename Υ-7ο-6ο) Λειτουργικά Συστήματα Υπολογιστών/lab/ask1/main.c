#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

void print_message () {
	for (int i=0; i<10; i++) {
		printf("Process %d is executed, my father is %d\n", getpid(), getppid());
		sleep(1);
	}
	return;
}

void c1 () {
	int status;
	pid_t child3;
	child3 = fork();
	if (child3 < 0) {
		fprintf(stderr,"fork error!\n");
		exit(1);
	}
	if (child3 == 0) {
		// C3
		print_message();
		exit(0);
	} else {
		// C1
		pid_t child4;
		child4 = fork();
		if (child4 < 0) {
			fprintf(stderr,"fork error!\n");
			exit(1);
		}
		if (child4 == 0) {
			// C4
			print_message();
			exit(0);
		} else {
			// C1
			print_message();
			for (int i=0; i<2; i++) {
				wait(&status);
			}
			exit(0);
		}
	}
	return;
}

void c2 () {
	int status;
	pid_t child5;

	child5 = fork();
	if (child5 < 0) {
		fprintf(stderr,"fork error!\n");
		exit(1);
	}
	if (child5 == 0) {
		// C5
		print_message();
		exit(0);
	} else {
		// C2
		print_message();
		wait(&status);
		exit(0);
	}
	return;
}

int main() {
	int status;
	pid_t child1;
	child1 = fork();
	if (child1 < 0) {
		fprintf(stderr,"fork error!\n");
		exit(1);
	}
	if (child1 == 0) {
		// C1
		c1();
		exit(0);
	} else {
		// F
		pid_t child2;
		child2 = fork();
		if (child2 < 0) {
			fprintf(stderr,"fork error!\n");
			exit(1);
		}
		if (child2 == 0) {
			// C2
			c2();
			exit(0);
		} else {
			// F
			for (int i=0; i<2; i++) {
				wait(&status);
			}
			exit(0);
		}
	}
	return(0);
} 
