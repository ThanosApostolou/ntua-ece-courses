#include <stdio.h>
#include <stdlib.h>

struct node
{
	int  data;
	struct node *next;
	struct node *previous;
}*list_head;
 
struct node *find_place(int ar);
void add_node_to_list(int ar);
void display_all(); 

int main(void)
{
	int a[100],i;
	list_head=NULL;
	for(i=0;i<100;i++) a[i]=rand();
	for(i=0;i<100;i++) add_node_to_list(a[i]);
	display_all();
	system("pause");
	return 0;
}

struct node *find_place(int ar)
{
	struct node *p,*tp;
	p=list_head;
	tp=NULL;
	while(p!=NULL)
	{
		if(ar>=p->data) 
          tp=p;
        else 
          break;    
		p=p->next;
	}
	return tp;
}

void add_node_to_list(int ar)
{
	struct node *neos,*temp_next,*thesi;
	thesi=find_place(ar);
	neos = malloc(sizeof(struct node));
	neos->data=ar;
	if(thesi==NULL)
	{
	 	if(list_head!=NULL) 
		{
			list_head->previous=neos;
			neos->next=list_head;
			neos->previous=NULL;
			list_head=neos;
		}
		else
		{
			neos->next=NULL;
			neos->previous=NULL;
			list_head=neos;
		}
	}
	else
	{
		temp_next=thesi->next;
		thesi->next=neos;
		neos->previous=thesi;
		neos->next=temp_next;
	}
}

void display_all()
{
	struct node *p;
	p=list_head;
	while(p!=NULL)
	{
		printf("%d\n",p->data);
		p=p->next;
	}
}
