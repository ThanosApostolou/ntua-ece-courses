#include <stdio.h>
#include <stdlib.h>

#define PRINT_INT_ARRAY(NAME,SIZE) {int i; for (i=0;i<SIZE;i++) \
printf("%d\n",NAME[i]);}

int main(void)
{
  int pin[10]={5,6,8,2,45,6,7,9,21,32};
  PRINT_INT_ARRAY(pin,10);  
  system("pause");
  return 0;
}
