#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <malloc.h>

struct node 
{
	char onoma[30];
	int ilikia;
	float varos;
	float ypsos;
	struct node *left;
	struct node *right;
};

struct node *root;

struct node *newnode(struct node data);
struct node *insert(struct node data);
void display(struct node *ptr);

int main(void)
{
	struct node temp;
	int i;
	root=NULL;
	for (i=1;i<=10;i++)
	{
		printf("Dose onoma:");
		gets(temp.onoma);
		printf("Dose ilikia:");
		scanf("%d",&temp.ilikia);
		printf("Dose varos:");
		scanf("%f",&temp.varos);
		printf("Dose ypsos:");
		scanf("%f",&temp.ypsos);
		getchar();
		insert(temp);
	}
	display(root);
	system("pause");
	return 0;
}

struct node *newnode(struct node data) 
{ 
 	struct node *new;
	new=malloc(sizeof(struct node)); 
	if(new==NULL)
	{
		puts("No memory");
		return NULL;
 	} 
 	*new=data; 
 	new->left = NULL; 
 	new->right = NULL; 
 	return(new); 
} 
  
struct node *insert(struct node data)
{ 
	struct node *next,*current,*ptr;  
	int isleft;   
	next=current=root;
	ptr=newnode(data); 
	if (root == NULL) 
	{ 
		root=ptr;
		return ptr;
	}
	while(1) 
	{
		if (data.ilikia<current->ilikia) 
		{
			next = current->left;
			isleft=1;
		} 
		else 
		{ 
			next = current->right;
			isleft=0;
		} 
		if(next == NULL)
		{
			if(isleft)
				current->left=ptr;
			else
				current->right=ptr; 
			return ptr; 
		} 
		current=next;
	}
}  

void display(struct node *ptr) 
{ 
	if (ptr == NULL) return; 
	display(ptr->left); 
	printf("Onoma  : %s\n", ptr->onoma);
    printf("Hlikia : %d\n", ptr->ilikia);
    printf("Varos  : %5.1f\n", ptr->varos);
    printf("Ypsos  : %5.1f\n", ptr->ypsos);
	display(ptr->right); 
} 
