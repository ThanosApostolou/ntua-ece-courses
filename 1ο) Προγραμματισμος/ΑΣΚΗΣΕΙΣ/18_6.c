#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <malloc.h>

struct node 
{
	char data[30];
	struct node *left;
	struct node *right;
};

struct node *root;

struct node *newnode(char lex[]);
struct node *insert(char lex[]);
void display(struct node *ptr);

int main(void)
{
	char onoma[30];
	root=NULL;
	while(1)
	{
		printf("Dose onoma:");
		gets(onoma);
		if(strcmp(onoma,"")==0) break;
		insert(onoma);
	}
	display(root);
	system("pause");
	return 0;
}

struct node *newnode(char lex[]) 
{ 
 	struct node *new;
	new=malloc(sizeof(struct node)); 
	if(new==NULL)
	{
		puts("No memory");
		return NULL;
 	} 
 	strcpy(new->data,lex); 
 	new->left = NULL; 
 	new->right = NULL; 
 	return(new); 
} 
  
struct node *insert(char lex[])
{ 
	struct node *next,*current,*ptr;  
	int isleft;   
	next=current=root;
	ptr=newnode(lex); 
	if (root == NULL) 
	{ 
		root=ptr;
		return ptr;
	}
	while(1) 
	{
		if(strcmp(lex,current->data)==-1) 
		{
			next = current->left;
			isleft=1;
		} 
		else 
		{ 
			next = current->right;
			isleft=0;
		} 
		if(next == NULL)
		{
			if(isleft)
				current->left=ptr;
			else
				current->right=ptr; 
			return ptr; 
		} 
		current=next;
	}
}  

void display(struct node *ptr) 
{ 
	if (ptr == NULL) return; 
	display(ptr->left); 
	printf("%s\n", ptr->data); 
	display(ptr->right); 
} 
