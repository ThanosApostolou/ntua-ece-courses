#include <stdio.h>
#include <stdlib.h>

#define SWAP(T,A,B) {T temp; temp=A; A=B; B=temp;}

int main(void)
{
  int a=5,b=10;
  double k=6.7,l=19.8;
  SWAP(int,a,b);
  printf("a=%d b=%d\n",a,b);
  SWAP(float,k,l);
  printf("k=%f l=%f\n",k,l);   
  system("pause");
  return 0;
}
