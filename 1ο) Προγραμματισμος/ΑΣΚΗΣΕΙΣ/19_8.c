/* ������� ��� �� ����� �� ������ ��� assert �� ������ �� ��������� �� ���������� ������ 19_8.exe
��� �� ������ ������� */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void test_assert(int p[], int a, int e)
{
	int i; 
	assert(e<10);
	assert(a>=0);
	for (i=a;i<e;i++)
		printf("%d\n",p[i]);	
	printf("=====\n");
}

int main(void) 
{
	int i,ar[10];
	for (i=0;i<=9;i++)
		ar[i]=rand()%100;
	test_assert(ar,4,9);
	test_assert(ar,0,2);
	getchar();               // ��������� �� ������� enter ��� �� ���������
	test_assert(ar,5,10);    // ���� � ����� �� ������������� ��� ����� assert 
	system("pause");
 	return 0;
}

