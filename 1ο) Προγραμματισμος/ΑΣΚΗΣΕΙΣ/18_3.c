#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <malloc.h>

struct node 
{
	int data;
	struct node *left;
	struct node *right;
};

struct node *root;

struct node *newnode(int num);
struct node *insert(int num);
void display(struct node *ptr);

int main(void)
{
	int a[100],i;

	for(i=0;i<100;i++) a[i]=rand();	
	root=NULL;
	for(i=0;i<100;i++) insert(a[i]);
	display(root);
	system("pause");
	return 0;
}

struct node *newnode(int num) 
{ 
 	struct node *new;
	new=malloc(sizeof(struct node)); 
	if(new==NULL)
	{
		puts("No memory");
		return NULL;
	} 
	new->data = num; 
	new->left = NULL; 
	new->right = NULL; 
	return(new); 
} 
  
struct node *insert(int num) 
{ 
	struct node *next,*current,*ptr;  
	int isleft;   
	next=current=root;
	ptr=newnode(num); 
	if (root == NULL) 
	{ 
		root=ptr;
		return ptr;
	}
	while(1) 
	{
		if(num < current->data)         
		{
			next = current->left;
			isleft=1;
		} 
		else 
		{ 
			next = current->right;
			isleft=0;
		}   
		if(next == NULL)
		{
			if(isleft)
				current->left=ptr;
			else
				current->right=ptr;            
			return ptr;                 
		}  
		current=next;
	}
}  

void display(struct node *ptr) 
{ 
	if (ptr == NULL) return; 
	display(ptr->left); 
	printf("%d ", ptr->data); 
	display(ptr->right); 
} 
