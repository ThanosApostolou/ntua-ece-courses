#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

struct node
{
	char  data[30];
	struct node *next;
}*list_head,*neos;
 
struct node *find_place(char lex[]);
void add_node_to_list(char lex[]);
void display_all();
void read_all();

int main(void)
{
	list_head=NULL;
	read_all();
	display_all();
	system("pause");
	return 0;
}

struct node *find_place(char lex[])
{
	struct node *p,*tp;
	p=list_head;
	tp=NULL;
	while(p!=NULL)
	{
		if(strcmp(lex,p->data)==1) tp=p;
	    p=p->next;
	}
	return tp;
}

void add_node_to_list(char lex[])
{
	struct node *temp_next,*thesi;
	thesi=find_place(lex);
	neos = malloc(sizeof(struct node));
	strcpy(neos->data,lex);
	if(thesi==NULL)
	{
	 	if(list_head!=NULL)
		{
			neos->next=list_head;
			list_head=neos;
		}
		else
		{
			neos->next=NULL;
			list_head=neos;
		}
	}
	else
	{
		temp_next=thesi->next;
		thesi->next=neos;
		neos->next=temp_next;
	}
}

void display_all()
{
	struct node *p;
	p=list_head;
	while(p!=NULL)
	{
		printf("%s\n",p->data);
		p=p->next;
	}
}

void read_all()
{
	FILE *fp;
	char lex[30];
	fp=fopen("onomata.txt","r");
	while(!feof(fp))
	{
		fscanf(fp,"%s",lex);
		add_node_to_list(lex);
	}
}
