#include <stdio.h>
#include <stdlib.h>

void func1(double x, double y)
{
	printf("func1 : %7.3f\n",  x*y);
}
void func2(double x, double y)
{
	printf("func2 : %7.3f\n",  x+y);
}
void func3(double x, double y)
{
	printf("func3 : %7.3f\n",  x-y);
}
void func4(double x, double y)
{
	printf("func4 : %7.3f\n",  x/y);
}
int main(void)
{
	void (*ptr[4]) (double x, double y);
	int i;
	ptr[0]=&func1;
	ptr[1]=&func2; 
	ptr[2]=&func3; 
	ptr[3]=&func4; 
	for (i=0;i<4;i++) ptr[i](10,23);
	for (i=0;i<4;i++) ptr[i](5,10);
    system("pause");
    return 0;
}

