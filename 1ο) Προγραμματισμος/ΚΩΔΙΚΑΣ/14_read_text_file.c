#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    FILE *fp;
    char ch;
    fp=fopen("text.txt","r");
    if (fp==NULL) exit(2);
    while(ch!=EOF)
    {
	    ch=fgetc(fp);
	    putch(ch);
    }
    fclose(fp);
    system("pause");
    return 0;
}


