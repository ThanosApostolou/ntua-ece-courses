#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// ���������� ���������
void bubblesort(int x[],int n)
{
	int i,k,temp;
	for(i=1;i<n;i++)
	{
		for(k=n-1;k>=i;k--)
		{
			if(x[k]<x[k-1])
			{
				temp=x[k];
				x[k]=x[k-1];
				x[k-1]=temp;
			}
		}
	}
}

// ���������� ��������� - ���������� ������
void bubblesort2(int x[],int n)
{
	int i,k,temp,found;
	for(i=1;i<n;i++)
	{
		found=0;
		for(k=n-1;k>=i;k--)
		{
			if(x[k]<x[k-1])
			{
				temp=x[k];
				x[k]=x[k-1];
				x[k-1]=temp;
				found=1;
			}
		}
		if(found==0) break;
	}
}

// ���������� ��������
void selectsort(int x[],int n)
{
	int i,k,temp;
	int max,maxpos;
	for(i=n-1;i>0;i--)
	{
		max=x[0];
		maxpos=0;
		for(k=0;k<=i;k++)
		{
			if(x[k]>max)
			{
				max=x[k];
				maxpos=k;
			}
		}
		temp=x[i];
		x[i]=x[maxpos];
		x[maxpos]=temp;
	}
}

// ���������� quicksort
void q_sort(int a[], int apo, int eos )
{
	int min, max, diax_pos,temp;
	int diax_timi;
	if (eos <= apo) return;     // �� ���������� ���������
	diax_timi = a[apo];
	min = apo;
	max = eos;
	while ( min < max ) 
	{
		while( a[min] <= diax_timi ) min++;
		while( a[max] > diax_timi ) max--;
		if ( min < max ) 
		{
			temp=a[min];
			a[min]=a[max];
			a[max]=temp;
		}
	}
	a[apo] = a[max];
	a[max] = diax_timi;
	diax_pos=max;
	if (apo<diax_pos)
		q_sort( a, apo, diax_pos-1 );     // ���������� ����� ��� ���������� ��� �� ����� �����
	if (eos>diax_pos) 
		q_sort( a, diax_pos+1, eos );     // ���������� ����� ��� ���������� ��� �� ������� �����
 }

// ������� ������ n ������ �� �������� ��������
void fill_it(a,n)
int a[],n;
{
    int i;
    for (i=0;i<n;i++)
        a[i]=rand();
}

// �������� ������������ ������ n ������ �� ������� ��� 10 ������
void print_it(a,n)
int a[],n;
{
    int i;
    for (i=0;i<n;i++)
    {
        printf("%6d ",a[i]);
        if ((i+1)%10==0) putch('\n');
    }
}

// �������� ��������� �� ������ ����� int. ���������� �� ���� ��� ����������� � �������
// � -1 �� ��� ����������
int find_it(int a[],int n,int ar)
{
	int i,found=-1;
	for(i=0;i<n;i++)
	{
		if(a[i]==ar)
		{
			found=i;
			break;
		}
	}
	return found;
}

// ������� ��������� �� ������ ����� int. ���������� �� ���� ��� ����������� � �������
// � -1 �� ��� ����������
int binary(int a[], int n, int ar)
{
	int apo,eos,meson;
	apo=0;
	eos=n-1;
	while(apo<=eos)
	{
		meson=(apo+eos)/2;
		if(ar<a[meson]) 
			eos=meson-1;
		else if(ar>a[meson]) 
			apo=meson+1;
		else
			return meson;
	}
	return -1;
}

// ������� ��������� �� ���������� ���������
int rbinary(int a[],int apo,int eos,int ar) 
{
 	int meson;	
 	if (apo <= eos) 
	{
 	 	meson=(apo+eos)/2;
		if(ar<a[meson]) 
			return rbinary(a,apo,meson-1,ar);
		else if(ar>a[meson]) 
			return rbinary(a,meson+1,eos,ar);
		else
			return meson;	
 	}
 	else
		return -(apo + 1); 
}

int main(void)
{
    int p[30],num,thesi=0;
    fill_it(p,30);
    print_it(p,30);
    printf("\n---------------------------------------------------------------------\n");
    printf("Dose arithmo gia seiriaki anazitisi:");
    scanf("%d",&num);
    thesi=find_it(p,30,num);
    if (thesi>=0)
       printf("O arithmos brethike sth thesi %d\n\n",thesi);
    else
       printf("O arithmos den brethike ston pinaka\n\n");
    print_it(p,30);
    printf("\n---------------------------------------------------------------------\n");   
    bubblesort(p,30); 
    // ���� ��� bubblesort ������� �� ��������������� ��� �� ����� ����������� �����������:
    // selectsort(p,30);
    // q_sort(p,0,29);
    print_it(p,30);
    printf("Dose arithmo gia diadiki anazitisi:");
    scanf("%d",&num);
    thesi=binary(p,30,num);
    if (thesi>=0)
       printf("O arithmos brethike sth thesi %d\n\n",thesi);
    else
       printf("O arithmos den brethike ston pinaka\n\n");
    system("pause");
    return 0;
}
