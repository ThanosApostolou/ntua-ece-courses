#include <stdio.h>
#include <stdlib.h>

void bubblesort(int x[],int n);

int main(void)
{
	int a[20][10];
	int i,j;
	for (i=0;i<20;i++)
		for (j=0;j<10;j++)
			a[i][j]=rand()%100;
	for (i=0;i<20;i++)
	{
		for (j=0;j<10;j++)
		{
			printf("%4d",a[i][j]);
		}				
		putchar('\n');
	}
	for (i=0;i<20;i++)
	{
		bubblesort(a[i],10);
	}
	printf("=======================================\n");
	for (i=0;i<20;i++)
	{
		for (j=0;j<10;j++)
		{
			printf("%4d",a[i][j]);
		}				
		putchar('\n');
	}
	system("pause");
    return 0;
}

void bubblesort(int x[],int n)
{
	int i,k,temp;
	for (i=1;i<n;i++)
	{
		for (k=n-1;k>=i;k--)
		{
			if (x[k]<x[k-1])
			{
				temp=x[k];
				x[k]=x[k-1];
				x[k-1]=temp;
			}
		}
	}
}
