#include <stdio.h>
#include <stdlib.h>

#define PRINT(X) printf("%d\n",X)
#define TETR(A) (A) * (A)
#define MAX(A,B) (A>B) ? A : B

int main(void)
{
  int k=5,l=10;  
  PRINT(20);
  PRINT(k+l);
  l=TETR(4);
  PRINT(TETR(k));
  printf("%d\n",MAX(k,l));
  PRINT(MAX(100,30));
  system("pause");
  return 0;
}
