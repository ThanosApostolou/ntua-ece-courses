#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void print_it(int p[][5]);
int sum(int p[][5]);
int mymax(int p[][5]);
int mymin(int p[][5]);
bool find(int p[][5], int ar);
void sum_line(int p[][5]);
void max_col(int p[][5]);

int main(void)
{
	int i,j,a[10][5],timi;
	srand(time(NULL)); //�������������� �� rand() ���� �� ���������� ���������� �������� ��������
	for(i=0;i<10;i++)
		for(j=0;j<5;j++)
			a[i][j]=rand();
    print_it(a);
    printf("\nAthroisma stoixeion pinaka = %d\n",sum(a));
    printf("Megaliteri timi = %d\n",mymax(a));
    printf("Mikroteri timi = %d\n",mymin(a));
    printf("Dose arithmo gia evresi:");
	scanf("%d",&timi);
	if (find(a,timi))
	   printf("Yparxei ston pinaka\n");
	else
       printf("Den yparxei ston pinaka\n");
    sum_line(a);
    max_col(a);
	system("pause");
    return 0;
}

int sum(int p[][5])
{
	int i,j,ss=0;
	for(i=0;i<10;i++)
		for(j=0;j<5;j++)
			ss=ss+p[i][j];
	return ss;
}

int mymax(int p[][5])
{
	int i,j,mx;
	mx=p[0][0];
	for(i=0;i<10;i++)
		for(j=0;j<5;j++)
			if (p[i][j]>mx) mx=p[i][j];
	return mx;
}

int mymin(int p[][5])
{
	int i,j,mn;
	mn=p[0][0];
	for(i=0;i<10;i++)
		for(j=0;j<5;j++)
			if (p[i][j]<mn) mn=p[i][j];
	return mn;
}

bool find(int p[][5], int ar)
{
	int i,j;
	bool found=false;
	for(i=0;i<10;i++)
	{
		for(j=0;j<5;j++)
		{
			if (ar==p[i][j]) found=true;
			if (found) break;
		}
		if (found) break;
	}
	return found;
}

void print_it(int p[][5])
{
	int i,j;
	for(i=0;i<10;i++)
	{
		for(j=0;j<5;j++)
			printf("%6d ",p[i][j]);
		putch('\n');
	}
}

void sum_line(int p[][5])
{
	int i,j,ath;
	for(i=0;i<10;i++)
	{
		ath=0;
		for(j=0;j<5;j++)
			ath=ath+p[i][j];
		printf("Grami %d = %d\n",i,ath);
	}
}

void max_col(int p[][5])
{
	int i,j,mx;
	for(j=0;j<5;j++)
	{
		mx=p[0][j];
		for(i=0;i<10;i++)
			if (p[i][j]>mx) mx=p[i][j];
		printf("Megistos stilis %d = %d\n",j,mx);
	}
}


