#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int a=5,b=10,*p1,**p2;
	p1=&a;
	*p1=100;
	printf("a=%d b=%d\n",a,b);
	p2=&p1;
	**p2=200;
	printf("a=%d b=%d\n",a,b);
    *p2=&b;
    **p2=300;
    printf("a=%d b=%d\n",a,b);
    p1=&a;
    **p2=400;
    printf("a=%d b=%d\n",a,b);
	system("pause");
	return 0;
}



