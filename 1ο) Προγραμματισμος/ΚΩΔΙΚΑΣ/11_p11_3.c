#include <stdio.h>
#include <stdlib.h>

int *max(int a, int b);

int main(void)
{
	int x,y,*ptr;
	x=100;
	y=200;
	printf("x=%d y=%d\n",x,y);
	ptr=max(x,y);
	printf("max =%d\n",*ptr);
	system("pause");
	return 0;
}

int *max(int a, int b)
{
   if (a>b)
      return &a;
   else
      return &b;      
}

