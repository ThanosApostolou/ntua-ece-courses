#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define DMS(A,B) (A)/(B*B)
#define GET_TWO_FLOATS(A,B) scanf("%f %f",&A,&B)
#define PRINT_LINE(C,X) \
{          \
   int i;  \
   for (i=1;i<=X;i++)   \
      putchar(C);       \
}

int main(void)
{
	float b,y,d;
	GET_TWO_FLOATS(b,y);
	assert(y>0);
	#ifdef DMS
		d=DMS(b,y);  
	#else
		d=b/pow(y,2); 
	#endif 
	printf("���=%f\n",d);
	PRINT_LINE('=',20);
	putchar('\n');
	system("pause");
	return 0;
}

