#include <stdio.h>
#include <stdlib.h>

struct test
{
	char x;
	int y;
	double z;
	char lex[25];
};

void func(struct test p)
{
	printf("%c\n",p.x);
	printf("%d\n",p.y);
	printf("%f\n",p.z);
	printf("%s\n",p.lex);
}

int main(void)
{
	struct test first;
	first.x='A';
	first.y=120;
	first.z=3.145678;
	strcpy(first.lex,"H C ypostirizei domes");
	func(first);
	system("pause");
	return 0;
}




