#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct node
{
	int data;
	struct node *next;
};

struct node *stack_top;
void pop();
bool push(int d);
bool isempty();
void displaytop();

int main(void)
{
	char ch;
	int ar;
	stack_top=NULL;
	while (1)
	{
		printf("\n0->Exit 1->Push 2->Pop 3->Display top:\n");
		ch=getch();
		switch(ch)
		{
			case '0':
					exit(0);
			case '1':
					printf("Dose eanan arithmo:");
					scanf("%d",&ar);
					if (push(ar)==false)
						puts("Den yparxei diathesimi mnimi");
					break;
			case '2':
					pop();
					break;
			case '3':
					displaytop();
					break;
			default:
					puts("Lathos pliktro");
					break;
		}
	}
    system("pause");
	return 0;
}
/*� ������� ��������� ��������� ���� ����� ���� ������ ��� ������� */
bool push(int d)
{
	struct node *neos;
	neos = malloc(sizeof(struct node));
	if (neos==NULL) return false;
	neos->data=d;
	neos->next = stack_top;
	stack_top =neos;
	return true;
}
/*� ������� ��������� ����������� ���� ����� ��� ��� ������ ��� ������� */
void pop()
{
	struct node *temp;
	int d;
	if (isempty())
	{
		puts("Keni stoiva");
		return;
	}
	printf("O arithmos %d exixthi apo ti stoiva\n",stack_top->data);
	temp = stack_top->next;
	free(stack_top);
	stack_top = temp;
}
bool isempty()
{
	if (stack_top==NULL) 
		return true;
	else
		return false;
}
/*� ������� ��������� ��������� �� �������� ��� ��������� ��������� ��� ������� */
void displaytop()
{
	if (isempty()) 	
		puts("Keni stoiva");
	else
		printf("Koryfi stoivas:%d\n", stack_top->data);
}
