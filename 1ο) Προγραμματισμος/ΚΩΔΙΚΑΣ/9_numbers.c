#include <stdio.h>
#include <stdlib.h>

void display_numbers(int apo, int eos)
{
	int i;
	for (i=apo;i<=eos;i++) printf("%d\n",i);
}

int main(void)
{
	display_numbers(15,20);
	display_numbers(122,140);
	system("pause");
	return 0;
}


