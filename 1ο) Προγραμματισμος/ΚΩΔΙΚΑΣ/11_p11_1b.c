#include <stdio.h>
#include <stdlib.h>

int count(char *p, char ch);

int main(void)
{
	char *ptr;
	int a,b;
	ptr="ANANAS";
    a=count(ptr,'A');
	printf("H lexi exei %d A\n",a); 
	b=count("PAPATREXAS",'P');
	printf("H lexi exei %d P\n",b); 
	system("pause");
	return 0;
}

int count(char *p, char ch)
{
    int c=0;
    while(*p != '\0')
	{
		if(*p == ch) c++;
		p++;
	}
	return c; 
}

