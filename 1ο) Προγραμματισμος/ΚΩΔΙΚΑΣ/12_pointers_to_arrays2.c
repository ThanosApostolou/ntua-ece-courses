#include <stdio.h> 
#include <stdlib.h> 

void fill_it(int p[][7]);

int main(void)
{
	int i,j,k;
    int a[5][7],b[5][7],c[5][7];
	int (*ptr[3])[5][7];
	fill_it(a);
	fill_it(b);
	fill_it(c);
	ptr[0]=&a;
	ptr[1]=&b;
	ptr[2]=&c;
	for (k=0;k<3;k++)
	{
	  for (i=0;i<5;i++)
	  {
         for (j=0;j<7;j++)
             printf("%4d",(*ptr[k])[i][j]);
         putchar('\n');    
      }  
      printf("============================\n");   
    }
    system("pause");    
	return 0;
}

void fill_it(int p[][7])
{
     int i,j;
     for (i=0;i<5;i++)
         for (j=0;j<7;j++)
             p[i][j]=rand()%100;
}             


  
