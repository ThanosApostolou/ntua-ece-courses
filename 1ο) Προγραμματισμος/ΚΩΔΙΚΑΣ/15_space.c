#include <stdio.h>
#include <stdlib.h>

int main(int argc,char *argv[])
{
	FILE *fin,*fout;
	char ch;
    if (argc!=3)
	{
		printf("Lathos plithos parametron\n");
		exit(2);
	}
	fin=fopen(argv[1],"r");
	fout=fopen(argv[2],"w");
	if (fin==NULL)
	{
        printf("Provlima sto arxeio eisodoy\n");
        exit(2);
    }
    if (fout==NULL)
	{
        printf("Provlima sto arxeio exodou\n");
        exit(2);
    }
    while (!feof(fin))
    {
        ch=fgetc(fin);
        if (ch!=' ') fputc(ch,fout);  
    }
    fclose(fin);
    fclose(fout);
	system("pause");
	return 0;
}


