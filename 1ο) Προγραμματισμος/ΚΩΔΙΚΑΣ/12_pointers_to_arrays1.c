#include <stdio.h> 
#include <stdlib.h> 

void fill_it(int p[][7]);

int main(void)
{
	int i,j;
    int a[5][7];
	int (*ptr)[5][7];
	fill_it(a);
	ptr=&a;
	for (i=0;i<5;i++)
	{
         for (j=0;j<7;j++)
             printf("%4d",(*ptr)[i][j]);
         putchar('\n');    
    }     
    system("pause");    
	return 0;
}

void fill_it(int p[][7])
{
     int i,j;
     for (i=0;i<5;i++)
         for (j=0;j<7;j++)
             p[i][j]=rand()%100;
}             


  
