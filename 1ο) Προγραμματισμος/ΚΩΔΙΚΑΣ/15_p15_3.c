#include <stdio.h>
#include <stdlib.h>

int main(int argc,char *argv[])
{
	FILE *fin,*fout;
	int i;
	char ch;
    if (argc!=4)
	{
		printf("Lathos plithos parametron\n");
		exit(2);
	}
	fin=fopen(argv[1],"r");
	fout=fopen(argv[2],"w");
	if (fin==NULL || fout==NULL)
	{
        printf("Provlima se arxeio\n");
        exit(2);
    }
    for (i=1;i<=atoi(argv[3]);i++) fputc(' ',fout);
    while (!feof(fin))
    {
        ch=fgetc(fin);
        fputc(ch,fout);
        if (ch=='\n' && !feof(fin))
           for (i=1;i<=atoi(argv[3]);i++) fputc(' ',fout);
    }    
    fclose(fin);
    fclose(fout);
    system("pause");
    return 0;
}


