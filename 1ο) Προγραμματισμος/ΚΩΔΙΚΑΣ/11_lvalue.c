#include <stdio.h>
#include <stdlib.h>

int *max(int *a, int *b);

int main(void)
{
	int x,y,*ptr;
	ptr=&x;
	*ptr=100;
	y=200;
	printf("x=%d y=%d\n",x,y);
	*max(&x,&y)=50;
	printf("x=%d y=%d\n",x,y);
	*max(&x,&y)=999;
	printf("x=%d y=%d\n",x,y);
	system("pause");
	return 0;
}

int *max(int *a, int *b)
{
   if (*a>*b)
      return a;
   else
      return b;      
}

