//binary_tree.c

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct node
{
	int data;
	struct node *left;
	struct node *right;
};

struct node *root;
struct node *newnode(int num);
struct node *find(int key);
struct node *insert(int num);
void display(struct node *ptr);
bool rm(int key);
struct node *find_left_most(struct node *rt);
struct node *find_right_most(struct node *rt);

int main(void)
{
	char ch;
	int a;
	struct node *new;
	root=NULL;
	while(1)
	{
		printf("0->Exodos 1->Prosthiki 2->Evresi ");
		printf("3->Diagrafi 4->Emfanisi:\n");

		ch=getch();
		switch(ch)
		{
			case '0':
					exit(0);
			case '1':
					printf("num:");
					scanf("%d",&a);
					new=insert(a);
					if (root==NULL) root=new;
					if(new==NULL)
						puts("Sen yparxei arketi mnimi");
					break;
			case '2':
					printf("num:");
					scanf("%d",&a);
					new=find(a);
					if(new!=NULL)
						printf("Brethike\n");
					else
						printf("Den yparxei\n");
					break;
			case '3':
					printf("num:");
					scanf("%d",&a);
					rm(a);
					break;		
			case '4':
					display(root);
					puts("");
					break;
			default:
					puts("Lathos pliktro");
					break;
		}
	}
	return 0;
}

struct node *newnode(int num)
{
	struct node *neos;
	neos=malloc(sizeof(struct node));
	neos->data = num;
	neos->left = NULL;
	neos->right = NULL;
	return(neos);
}

void display(struct node *ptr)
{
	if (ptr == NULL) return;
	display(ptr->left);
	printf("%d ", ptr->data);
	display(ptr->right);
}

struct node *find(int key)
{
	struct node *current;
	current=root;
	while(current->data != key)
	{
		if(key < current->data)
			current = current->left;
		else
			current = current->right;
		if(current == NULL)
			return NULL;
	}
	return current;
}

struct node *insert(int num)
{
	struct node *next,*current,*ptr;
	bool isleft;
	next=current=root;
	ptr=newnode(num);
	if (root == NULL)
	{
		return ptr;
	}
	while(1)
	{
		if(num < current->data)
		{
			next = current->left;
			isleft=true;
		}
		else
		{
			next = current->right;
			isleft=false;
		}
		if(next == NULL)
		{
			if(isleft)
				current->left=ptr;
			else
				current->right=ptr;
			return ptr;
		}
		current=next;
	}
}
 
//�������� ��� ������ �� ������ key
bool rm(int key)
{
	struct node *current;
	struct node *parent;
	bool isLeftChild = true;
	current=parent=root;
	while(current->data != key)
	{
		parent = current;
		if(key < current->data)
		{
			isLeftChild = true;
			current = current->left;
		}
		else
		{
			isLeftChild = false;
			current = current->right;
		}
		if(current == NULL)
			return false;
	}
    // � ������ �� ������ key �������. � ��������� ��� ����� ��� ������ current ��� � ��������� ��� ������� ��� ��� ������ parent */
    
    //�� � ������ ��� ���� �����������
	if(current->left==NULL && current->right==NULL)
	{
		if(current == root)	       // �� ����� � ����,
			root = NULL; 	       // �� ������ ����� ����
		else if(isLeftChild)
			parent->left = NULL;   // ����������
		else				       // ��� �� ������
			parent->right = NULL;
	}

	//�� ��� ������� ��������� ���������� ������
	else if(current->right==NULL)
		if(current == root)
			root = current->left;
		else if(isLeftChild)
			parent->left = current->left;
		else
			parent->right = current->left;

    //�� ��� ������� ������ ���������� ������
	else if(current->left==NULL)
		if(current == root)
			root = current->right;
		else if(isLeftChild)
			parent->left = current->right;
		else
			parent->right = current->right;
    //�� ������� ��� ��������� ��� ������ ���������� ������
	else
	{
 		struct node *successor,*temp,*old_root;
 		if(current == root)
		{
			temp=root->left;
			successor=find_left_most(root->right);
			root=root->right;
			successor->left=temp;
		}

		else if(isLeftChild)
		{
			successor=find_left_most(current->right);
			successor->left=current->left;
			parent->left = current->right;
		}
 		else
		{
			successor=find_right_most(current->left);
			successor->right=current->right;
			parent->right = current->left;
		}
	}
	free(current);
	return true;
}

//�������� ��� ��������� �������� ����� ��� ����������
struct node *find_left_most(struct node *rt)
{
	if(rt==NULL) return NULL;
	while(rt->left!=NULL)
	{
		rt=rt->left;
	}
	return rt;
}

//�������� ��� ��������� ����� ����� ��� ����������
struct node *find_right_most(struct node *rt)
{
	if(rt==NULL) return NULL;
	while(rt->right!=NULL)
	{
		rt=rt->right;
	}
	return rt;
}

