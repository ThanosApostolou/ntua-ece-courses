#include <stdio.h>
#include <stdlib.h>

int func1(int x, int y)
{
	return x*y;
}

int func2(int x, int y)
{
	return x+y;
}

int main(void)
{
	int (*ptr1)(int x, int y), (*ptr2)(int x, int y);
	ptr1=&func1; //� ptr1 ������� ���� ��������� func1()
	ptr2=&func2; //� ptr2 ������� ���� ��������� func2()
	printf("%d\n",ptr1(10,12));
	printf("%d\n",ptr2(10,12));
	ptr2=ptr1;
	printf("%d\n",ptr2(10,12));
    system("pause");
    return 0;      
}
