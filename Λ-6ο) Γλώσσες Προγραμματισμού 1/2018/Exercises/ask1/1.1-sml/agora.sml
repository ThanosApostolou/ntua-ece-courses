fun gcd (a:Int64.int, b:Int64.int) =
  if b = 0 then a
  else gcd (b, (a mod b));
  
fun lcm (a:Int64.int, b:Int64.int) =
  (a div gcd (a, b)) * b;

fun parse file =
  let
    fun readInt input = 
      Option.valOf (TextIO.scanStream (Int64.scan StringCvt.DEC) input)
    val inStream = TextIO.openIn file
    val n = readInt inStream
    val _ = TextIO.inputLine inStream
	fun readInts 0 acc = acc 
	    | readInts i acc = readInts (i - 1) (readInt inStream :: acc)
    in
	  (n, readInts n [])
    end;

fun lcm_list ([], prev:Int64.int)       =
		nil
  | lcm_list ((h :: t), prev:Int64.int) =
		let
			val temp = lcm (h:Int64.int, prev:Int64.int)
		in
			temp :: lcm_list (t, temp)
		end;

fun find_minimum ([], minimum_days:Int64.int, missing_village:Int64.int, i:Int64.int) =
				(minimum_days, missing_village)	
		  | find_minimum ((h :: t), minimum_days:Int64.int, missing_village:Int64.int, i:Int64.int) =
				if h < minimum_days then find_minimum (t, h, i+1, i+1)
				else find_minimum (t, minimum_days, missing_village, i+1);

fun create_lcm_minus_1_list ([], _) =
	 nil
  | create_lcm_minus_1_list (_, []) =
	 nil
  | create_lcm_minus_1_list ((h1 :: t1), (h2 :: t2)) =
  		lcm (h1, h2) :: create_lcm_minus_1_list (t1, t2);

	
fun solve_agora (n:Int64.int, V) =
	let
		val Bottom_LCM = 1 :: lcm_list (List.rev (V), 1)
		val Upper_LCM = tl (List.rev (lcm_list (V, 1)))
		val lcm_all = List.last (Bottom_LCM)
		val lcm_minus_1_list = create_lcm_minus_1_list (Bottom_LCM, Upper_LCM)
	in
		find_minimum (lcm_minus_1_list, lcm_all, 0, 0)
	end;

fun agora file =
	let
		val (n, V) = parse file
		val (minimum_days, missing_village) = solve_agora (n, V)
	in
		print (Int64.toString(minimum_days) ^ " " ^ Int64.toString(missing_village) ^ "\n")
	end;