fun list_to_string ([], acc) =
		acc ^ "\n"
  | list_to_string ([h], acc) =
		list_to_string ([], acc ^ Int64.toString(h))
  | list_to_string ((h :: t), acc) =
		list_to_string (t, acc ^ Int64.toString(h) ^ " ");

fun print_list list =
	print (list_to_string (list, ""));

fun print_list2 list = 
	let
		fun loop ([]) =
			print ""
		  | loop ((h :: t)) =
		  	( print (Int64.toString(h) ^ "\n");
			  loop (t) )
	in
		loop (list)
	end;