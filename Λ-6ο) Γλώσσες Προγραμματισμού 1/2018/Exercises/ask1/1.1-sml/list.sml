fun addto (l,v) = 
  if null l then
    nil
  else
    hd l + v :: addto (tl l,v);
    
fun map (f,l) =
  if null l then
    nil
  else
    f (hd l) :: map (f,tl l);