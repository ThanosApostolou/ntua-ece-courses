fun double x = x*2;

fun times4 x = x*4;

fun aveI(x,y) = (x+y) div 2;
fun aveR(x,y) = (x+y) / 2.0;

fun clip s = substring(s,0,size s - 1);
fun middle s = substring(clip s,size s div 2,1);
fun dtrunc s = substring(s, 1, size s - 1);