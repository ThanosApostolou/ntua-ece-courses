#include <iostream>
#include <fstream>
#include <vector>

struct index {
	int i;
	int j;
};

void print_Map (std::vector< std::vector<char> >& Map) {
	for (int i=0; i<Map.size(); ++i) {
		for (int j=0; j<Map[i].size(); ++j) {
			std::cout << Map[i][j];
		}
		std::cout << std::endl;
	}
}

void print_Vector (std::vector<index>& Vector) {
	for (auto temp:Vector) {
		std::cout << temp.i << ' ' << temp.j << std::endl;
	}
}

int main (int argc, char *argv[]) {
	std::ifstream myfile;
	std::vector< std::vector<char> > Map;
	std::vector<index> Matter_index;
	std::vector<index> AntiMatter_index;	
	// open file, read villages and days
	myfile.open(argv[1]);
	if (!myfile.is_open()) {
		std::cerr << "Error opening file " << (argc == 1 ? "" : argv[1]) << std::endl;
		return 1;
	}
	std::vector <char> temp_vec;
	std::string line;
	int i=0;
	while (std::getline(myfile, line)) {
		int j=0;
		for (char& c : line) {
			temp_vec.push_back(c);
			index current_index = {i, j};
			if (c == '+') {
				Matter_index.push_back(current_index);
			} else if (c == '-') {
				AntiMatter_index.push_back(current_index);
			}
			j++;
		}
		Map.push_back(temp_vec);
		temp_vec.clear();
		i++;
	}
	std::cout << "Initial Map:\n";
	print_Map(Map);
	std::cout << std::endl;
	
	std::cout << "Initial Matter_index:\n";
	print_Vector(Matter_index);
	std::cout << std::endl;
	
	std::cout << "Initial AntiMatter_index:\n";
	print_Vector(AntiMatter_index);
	std::cout << std::endl;
	
	for (int i=0; i<Map.size(); ++i) {
		for (int j=0; j<Map[i].size(); ++j) {
			std::cout << Map[i][j];
		}
		std::cout << std::endl;
	}
	return 0;
}