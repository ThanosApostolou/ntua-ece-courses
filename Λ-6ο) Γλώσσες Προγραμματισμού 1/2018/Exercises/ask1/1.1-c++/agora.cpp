#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>

// for debugging
/*
void print_villages_info (int villages, std::vector<unsigned int> days) {
	std::cout << "There are " << villages << " villages" << std::endl;
	for (unsigned int i=0; i<days.size(); ++i) {
		std::cout << "Residents of village " << i+1 << " visit market every " << days[i] << " days" << std::endl;
	}
}
void print_upper_lcm (std::vector<unsigned long long int> Upper_LCM) {
	for (unsigned int i=0; i<Upper_LCM.size(); ++i) {
		std::cout << "Upper LCM of " << i << " is " << Upper_LCM[i] << std::endl;
	}
}
*/

// Greatest Common Divisor
unsigned long long int gcd (unsigned long long int a, unsigned long long int b) {
	unsigned long long int c = a % b;
	while (c != 0) {
		a = b;
		b = c;
		c = a % b;
	}
	return b;
}

// Least Common Multiple of 2 numbers
unsigned long long int lcm (unsigned long long int a, unsigned long long int b) {
	return (a/gcd(a,b))*b;
}

int main (int argc, char *argv[]) {
	std::ifstream myfile;
	unsigned int villages=0, temp=0, missing_village=0;
	unsigned long long int minimum_days=0, current_days=0, bottom_lcm=1;
	std::vector<unsigned int> Days;
	
	// open file, read villages and days
	myfile.open(argv[1]);
	if (!myfile.is_open()) {
		std::cerr << "Error opening file " << (argc == 1 ? "" : argv[1]) << std::endl;
		return 1;
	}
	myfile >> villages;
	while (myfile >> temp) {
		Days.push_back(temp);
	}
	std::vector<unsigned long long int> Upper_LCM(villages,1);
	// for debugging
	// print_villages_info(villages, Days);
	
	Upper_LCM[villages-1] = Days[villages-1];
	for (unsigned int i=villages-2; i>0; --i) {
		Upper_LCM[i] = lcm(Days[i],Upper_LCM[i+1]);
	}
	// for debugging
	// print_upper_lcm(Upper_LCM);
	minimum_days = lcm(Days[0],Upper_LCM[1]);
	missing_village = 0;
	for (unsigned int i=0; i<Days.size(); ++i) {
		if (i == 0) {
			bottom_lcm = 1;
		} else {
			bottom_lcm = lcm(bottom_lcm, Days[i-1]);
		}
		if (i == Days.size()-1) {
			current_days = bottom_lcm;
		} else {
			current_days = lcm(bottom_lcm,Upper_LCM[i+1]);
		}
		if (current_days < minimum_days) {
			minimum_days = current_days;
			missing_village = i+1;
		}
	}
	
	std::cout << minimum_days << ' ' << missing_village << std::endl;		
	myfile.close();
	return 0;
}