#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>

// for debugging
/*
void print_villages_info (int villages, std::vector<unsigned int> days) {
	std::cout << "There are " << villages << " villages" << std::endl;
	for (unsigned int i=0; i<days.size(); ++i) {
		std::cout << "Residents of village " << i+1 << " visit market every " << days[i] << " days" << std::endl;
	}
}
*/

int main (int argc, char *argv[]) {
	std::ifstream myfile;
	unsigned int villages=0, temp=0, missing_village=0, new_missing_village=0, minimum_index=0;
	unsigned long long int minimum=0;
	std::vector<unsigned long long int> Meeting_Days;
	std::vector<unsigned int> Frequency;
	bool running=true, equal_n, equal_n_minus_1;
	
	// open file, read villages and days
	myfile.open(argv[1]);
	if (!myfile.is_open()) {
		std::cerr << "Error opening file " << (argc == 1 ? "" : argv[1]) << std::endl;
		return 1;
	}
	myfile >> villages;
	myfile >> temp;
	Frequency.push_back(temp);
	Meeting_Days.push_back(temp);
	minimum=temp;
	while (myfile >> temp) {
		Frequency.push_back(temp);
		Meeting_Days.push_back(temp);
		if (temp<minimum) {
			minimum=temp;
		}
	}
	// for debugging
	// print_villages_info(villages, Days);
	missing_village=0;
	while (running) {
		equal_n=true;
		equal_n_minus_1=true;
		for (unsigned int i=0; i<Meeting_Days.size(); ++i) {
			if (Meeting_Days[i] != minimum) {
				if (equal_n) {
					equal_n=false;
					new_missing_village=i;
				} else {
					equal_n_minus_1=false;
				}
				if (Meeting_Days[i] < minimum) {
					minimum_index=i;
				}
			}
		}
		if (equal_n) {
			missing_village=0;
			break;
		} else if (equal_n_minus_1) {
			missing_village=new_missing_village;
			break;
		}		
		Meeting_Days[minimum_index] += Frequency[minimum_index];
		minimum=Meeting_Days[minimum_index];
	}
	std::cout << minimum << ' ' << missing_village << std::endl;		
	myfile.close();
	return 0;
}