#/bin/bash
if [[ $(time ./agora ../agora-tests/agora.in1)  == $(cat ../agora-tests/agora.out1) ]] && \
   [[ $(time ./agora ../agora-tests/agora.in2)  == $(cat ../agora-tests/agora.out2) ]] && \
   [[ $(time ./agora ../agora-tests/agora.in3)  == $(cat ../agora-tests/agora.out3) ]] && \
   [[ $(time ./agora ../agora-tests/agora.in6)  == $(cat ../agora-tests/agora.out6) ]] && \
   [[ $(time ./agora ../agora-tests/agora.in7)  == $(cat ../agora-tests/agora.out7) ]] && \
   [[ $(time ./agora ../agora-tests/agora.in9)  == $(cat ../agora-tests/agora.out9) ]] && \
   [[ $(time ./agora ../agora-tests/agora.in11) == $(cat ../agora-tests/agora.out11) ]] && \
   [[ $(time ./agora ../agora-tests/agora.in14) == $(cat ../agora-tests/agora.out14) ]] && \
   [[ $(time ./agora ../agora-tests/agora.in17) == $(cat ../agora-tests/agora.out17) ]]
then
	echo "SUCCESS!"
else
	echo "FAIL!"
fi
