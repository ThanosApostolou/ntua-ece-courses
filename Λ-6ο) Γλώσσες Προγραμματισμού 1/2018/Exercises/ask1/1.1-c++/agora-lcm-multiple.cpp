#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>

// for debugging
/*
void print_villages_info (int villages, std::vector<unsigned int> days) {
	std::cout << "There are " << villages << " villages" << std::endl;
	for (unsigned int i=0; i<days.size(); ++i) {
		std::cout << "Residents of village " << i+1 << " visit market every " << days[i] << " days" << std::endl;
	}
}
*/

// Greatest Common Divisor
unsigned long long int gcd (unsigned long long int a, unsigned long long int b) {
	unsigned long long int c = a % b;
	while (c != 0) {
		a = b;
		b = c;
		c = a % b;
	}
	return b;
}

// Least Common Multiple of 2 numbers or of a Vector(passed by reference)
unsigned long long int lcm (unsigned long long int a, unsigned long long int b) {
	return (a/gcd(a,b))*b;
}

unsigned long long int lcm_vector (std::vector<unsigned int> Numbers) {
	unsigned long long int final_lcm=0;
	if (Numbers.size() == 1) {
		return Numbers[1];
	} else {
		final_lcm=lcm(Numbers[0], Numbers[1]);
		for (unsigned int i=2; i<Numbers.size(); ++i) {
			final_lcm=lcm(final_lcm,Numbers[i]);
		}
		return final_lcm;
	}
}

// lcm of a Vector (passed by value) without some number
unsigned long long int lcm_vector_without (std::vector<unsigned int> Numbers, unsigned int without) {
	Numbers.erase(Numbers.begin()+without);
	return lcm_vector(Numbers);
}

int main (int argc, char *argv[]) {
	std::ifstream myfile;
	unsigned int villages=0, temp=0, missing_village=0;
	unsigned long long int new_days=0, minimum_days=0;
	std::vector<unsigned int> Days;
	
	// open file, read villages and days
	myfile.open(argv[1]);
	if (!myfile.is_open()) {
		std::cerr << "Error opening file " << (argc == 1 ? "" : argv[1]) << std::endl;
		return 1;
	}
	myfile >> villages;
	while (myfile >> temp) {
		Days.push_back(temp);
	}
	// for debugging
	// print_villages_info(villages, Days);
	minimum_days=lcm_vector_without(Days,0);
	missing_village=1;
	for (unsigned int i=1; i<Days.size(); ++i) {
		new_days=lcm_vector_without(Days,i);
		if (new_days<minimum_days) {
			minimum_days=new_days;
			missing_village=i+1;
		}
	}
	if (lcm(new_days,Days.back()) == minimum_days) {
		missing_village=0;
	}
	std::cout << minimum_days << ' ' << missing_village << std::endl;		
	myfile.close();
	return 0;
}