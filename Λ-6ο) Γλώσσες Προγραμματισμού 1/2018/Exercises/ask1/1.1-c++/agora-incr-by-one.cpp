#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <numeric>

// for debugging
void print_villages_info (int villages, std::vector<unsigned int> days) {
	std::cout << "There are " << villages << " villages" << std::endl;
	for (unsigned int i=0; i<days.size(); ++i) {
		std::cout << "Residents of village " << i+1 << " visit markter every " << days[i] << " days" << std::endl;
	}
}

int main (int argc, char *argv[]) {
	std::ifstream myfile;
	unsigned int villages=0, temp=0, missing_village=0;
	unsigned long long int meeting_days=0;
	bool missing1=true, missing2=true;
	std::vector<unsigned int> days;
	
	// open file, then read villages and days
	myfile.open(argv[1]);
	if (!myfile.is_open()) {
		std::cerr << "Error opening file " << (argc == 1 ? "" : argv[1]) << std::endl;
		return 1;
	}
	myfile >> villages;
	while (myfile >> temp) {
		days.push_back(temp);
	}
	// for debugging
	print_villages_info(villages, days);
	//std::cout << "lcm of 5 and 6: " << 5*6*7/std::__gcd(5,6,7) << std::endl;
	while(missing2) {
		missing1=false;
		missing2=false;
		missing_village=0;
		meeting_days++;
		if (meeting_days % 50 == 0) {
			std::cout << "Metting_days = " << meeting_days << std::endl;
		}
		for (unsigned int i=0; i<days.size(); ++i) {
			if (meeting_days % days[i] != 0) {
				if (!missing1) {
					missing1=true;
					missing_village=i+1;
				} else {
					missing2=true;
					break;
				}
			}
		}
	}
	std::cout << meeting_days << ' ' << missing_village << std::endl;
		
	myfile.close();
	return 0;
}