#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>

// for debugging

void print_villages_info (int villages, std::vector<unsigned int> days) {
	std::cout << "There are " << villages << " villages" << std::endl;
	for (unsigned int i=0; i<days.size(); ++i) {
		std::cout << "Residents of village " << i+1 << " visit market every " << days[i] << " days" << std::endl;
	}
}
void print_meeting_days (int villages, std::vector<unsigned long long int> days) {
	for (unsigned int i=0; i<days.size(); ++i) {
		std::cout << "Meeting Days without village " << i+1 << ": " << days[i] << " days" << std::endl;
	}
}
// Greatest Common Divisor
unsigned long long int gcd (unsigned long long int a, unsigned long long int b) {
	unsigned long long int c = a % b;
	while (c != 0) {
		a = b;
		b = c;
		c = a % b;
	}
	return b;
}

// Least Common Multiple of 2 numbers
unsigned long long int lcm (unsigned long long int a, unsigned long long int b) {
	return (a/gcd(a,b))*b;
}

int main (int argc, char *argv[]) {
	std::ifstream myfile;
	unsigned int villages=0, temp=0, missing_village=0;
	unsigned long long int minimum_days=0;
	std::vector<unsigned int> Days;
	
	// open file, read villages and days
	myfile.open(argv[1]);
	if (!myfile.is_open()) {
		std::cerr << "Error opening file " << (argc == 1 ? "" : argv[1]) << std::endl;
		return 1;
	}
	myfile >> villages;
	while (myfile >> temp) {
		Days.push_back(temp);
	}
	std::vector<unsigned long long int> Meeting_Days(villages,1);
	// for debugging
	print_villages_info(villages, Days);
	//current=Days[0];
	for (unsigned int i=0; i<Days.size(); ++i) {
		//previous=current;
		//current=lcm(current,Days[i]);
		for (unsigned int j=0; j<Meeting_Days.size(); ++j) {
			if (i != j) {
				Meeting_Days[j]=lcm(Meeting_Days[j], Days[i]);
			}
		}
		print_meeting_days(villages, Meeting_Days);
	}
	minimum_days=Meeting_Days[0];
	missing_village=1;
	for (unsigned int i=1; i<Meeting_Days.size(); ++i) {
		if (Meeting_Days[i] < minimum_days) {
			minimum_days = Meeting_Days[i];
			missing_village = i+1;
		}
	}
	if (lcm(minimum_days,Days[missing_village-1]) == minimum_days) {
		missing_village=0;
	}
	std::cout << minimum_days << ' ' << missing_village << std::endl;		
	myfile.close();
	return 0;
}