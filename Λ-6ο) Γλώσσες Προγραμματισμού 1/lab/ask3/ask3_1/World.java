import java.util.ArrayList;

class World {
    int N=0, M=0;
    char[][] MapArray = new char[0][0];
    int[][] TimeArray = new int[0][0];
    boolean[][] CheckedArray = new boolean[0][0];
    Square cat = new Square();
    ArrayList <Square> pipes = new ArrayList <Square>();
    ArrayList <Square> newpipes = new ArrayList <Square>();
    ArrayList <Square> squares = new ArrayList <Square>();
    ArrayList <Square> newsquares = new ArrayList <Square>();


    void Init () {
        N = MapArray.length;
        if (N > 0) {
            M = MapArray[0].length;
        } else {
            M = 0;
        }

        TimeArray = new int[N][M];
        CheckedArray = new boolean[N][M];
        for (int i=0; i < N; i++) {
            for (int j=0; j < M; j++) {
                CheckedArray[i][j] = false;
                char temp = MapArray[i][j];
                if (temp == 'W') {
                    pipes.add(new Square(i, j, 0));
                    TimeArray[i][j] = 0;
                } else if (temp == 'X') {
                    TimeArray[i][j] = -2;
                } else if  (temp == 'A') {
                    cat.X = i;
                    cat.Y = j;
                    TimeArray[i][j] = -1;
                } else {
                    TimeArray[i][j] = -1;
                }
            }
        }
    }

    void Print () {
        System.out.println("N=" + N + " M=" + M);
        System.out.print("Cat: ");
        cat.Print();
        System.out.println("MapArray:");
        for (char[] i : MapArray) {
            for (char j : i) {
                System.out.print(j);
                System.out.print('\t');
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("TimeArray:");
        for (int[] i : TimeArray) {
            for (int j : i) {
                System.out.print(j);
                System.out.print('\t');
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("CheckedArray:");
        for (boolean[] i : CheckedArray) {
            for (boolean j : i) {
                System.out.print(j);
                System.out.print('\t');
            }
            System.out.println();
        }
        System.out.println();
    }

    void PrintPipes () {
        for (int i=0; i < pipes.size(); i++) {
            System.out.print("Pipe" + i + ": ");
            pipes.get(i).Print();
        }
    }
}