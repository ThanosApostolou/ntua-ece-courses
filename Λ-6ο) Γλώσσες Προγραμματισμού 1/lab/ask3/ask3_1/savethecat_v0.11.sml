(* Print a given array2, for debugging purposes
 * 1 function for char array and 1 function for int array *)
fun print_char_array A =
    let
        val N = Array2.nRows A
        val M = Array2.nCols A
        fun print_elements i j =
            if j = (M-1) andalso i <> (N-1) then (
                print(Char.toString (Array2.sub (A, i, j)));
                print("\n");
                print_elements (i+1) 0
            ) else if j = (M-1) andalso i = (N-1) then (
                print(Char.toString (Array2.sub (A, i, j)));
                print("\n")
            ) else (
                print(Char.toString (Array2.sub (A, i, j)));
                print("\t");
                print_elements i (j+1)
            )
    in
        print_elements 0 0;
        print("\n")
    end

fun print_int_array A =
    let
        val N = Array2.nRows A
        val M = Array2.nCols A
        fun print_elements i j =
            if j = (M-1) andalso i <> (N-1) then (
                print(Int.toString (Array2.sub (A, i, j)));
                print("\n");
                print_elements (i+1) 0
            ) else if j = (M-1) andalso i = (N-1) then (
                print(Int.toString (Array2.sub (A, i, j)));
                print("\n")
            ) else (
                print(Int.toString (Array2.sub (A, i, j)));
                print("\t");
                print_elements i (j+1)
            )
    in
        print_elements 0 0;
        print("\n")
    end
fun print_bool_array A =
    let
        val N = Array2.nRows A
        val M = Array2.nCols A
        fun print_elements i j =
            if j = (M-1) andalso i <> (N-1) then (
                print(Bool.toString (Array2.sub (A, i, j)));
                print("\n");
                print_elements (i+1) 0
            ) else if j = (M-1) andalso i = (N-1) then (
                print(Bool.toString (Array2.sub (A, i, j)));
                print("\n")
            ) else (
                print(Bool.toString (Array2.sub (A, i, j)));
                print("\t");
                print_elements i (j+1)
            )
    in
        print_elements 0 0;
        print("\n")
    end

(* Read the file line by line and return the map as lists inside a list with N and M *)
fun parse file =
    let
        fun next_String input = (TextIO.inputAll input)
        val stream = TextIO.openIn file
        fun readLines acc N =
            case TextIO.inputLine stream of
                SOME line =>
                    let
                        val list_line = explode line
                        val trimed_line = List.take (list_line, (length list_line)-1)
                    in
                        readLines (trimed_line :: acc) (N+1)
                    end
                | NONE      => (rev acc, N, length (hd acc))
    in
        readLines [] 0
    end

fun find_init map_array time_array =
    let
        val N = Array2.nRows map_array
        val M = Array2.nCols map_array
        fun iterate i j (catX, catY) pipes =
            let
                val current = Array2.sub (map_array, i, j)
                val cat_coord = if current = #"A" then (i, j)
                                else (catX, catY)
                val pipes_list = if current = #"W" then (i, j, 0) :: pipes
                                 else pipes
                val _ = if current = #"W" then
                            Array2.update (time_array, i, j, 0)
                        else if current = #"X" then
                            Array2.update (time_array, i, j, ~2)
                        else ()
            in
                if i = (N-1) andalso j = (M-1) then
                    (cat_coord, rev pipes_list)
                else if i <> (N-1) andalso j = (M-1) then
                    iterate (i+1) 0 cat_coord pipes_list
                else
                    iterate i (j+1) cat_coord pipes_list
            end
    in
        iterate 0 0 (~1, ~1) []
    end

fun flood_fill time_array N M pipes =
    let
        (* update this coordinate if it's valid *)
        fun update_this thisX thisY thisTime new_pipes =
            if thisX >= 0 andalso thisX < N andalso thisY >= 0 andalso thisY < M then
                let
                    val current = Array2.sub (time_array, thisX, thisY)
                    val change = if current = ~1 then true
                                 else false
                in
                    if change then
                        let
                            val _ = Array2.update (time_array, thisX, thisY, thisTime)
                        in
                            (thisX, thisY, thisTime) :: new_pipes
                        end
                    else new_pipes
                end
            else new_pipes

        (* update all 4 nearest squares *)
        fun update_surround (X, Y, time) new_pipes =
            let
                val new_pipes = update_this (X-1) Y (time+1) new_pipes
                val new_pipes = update_this (X+1) Y (time+1) new_pipes
                val new_pipes = update_this X (Y-1) (time+1) new_pipes
                val new_pipes = update_this X (Y+1) (time+1) new_pipes
            in
                new_pipes
            end

        (* for each pipe, remove it from list and get new pipes until list is empty *)
        fun fill_from_pipes [] [] =
            ()
          | fill_from_pipes [] new_pipes =
            fill_from_pipes new_pipes []
          | fill_from_pipes pipes new_pipes=
            let
                val (X, Y, time) = hd pipes
                val rest_pipes = (tl pipes)
                val new_pipes = update_surround (X, Y, time) new_pipes
            in
                fill_from_pipes rest_pipes new_pipes
            end
    in
        fill_from_pipes pipes []
    end

fun solve_cat (catX, catY) time_array checked_array N M =
    let
        fun add_square (thisX, thisY, thisTime, thisMoves) new_squares (bestX, bestY, bestTime, bestMoves) =
            if thisX >= 0 andalso thisX < N andalso thisY >= 0 andalso thisY < M then
                let
                    val squareTime = Array2.sub (time_array, thisX, thisY)
                    val checked = Array2.sub (checked_array, thisX, thisY)
                    val is_valid = if (squareTime <> ~2) andalso
                                   (bestTime = ~1 orelse thisTime < squareTime) andalso
                                   (not checked)
                                then true
                                else false
                    val is_better =
                        if (squareTime > bestTime)
                            then true
                        else if ((squareTime = bestTime) andalso (thisX+thisY < bestX+bestY))
                            then true
                        else false
                in
                    if is_valid then
                        let
                            val _ = Array2.update (checked_array, thisX, thisY, true)
                        in
                            if is_better then
                                (((thisX, thisY, thisTime, thisMoves) :: new_squares), (thisX, thisY, squareTime, thisMoves))
                            else
                                (((thisX, thisY, thisTime, thisMoves) :: new_squares), (bestX, bestY, bestTime, bestMoves))
                        end
                    else (new_squares, (bestX, bestY, bestTime, bestMoves))
                end
            else (new_squares, (bestX, bestY, bestTime, bestMoves))

        fun check_surround (X, Y, time, moves) new_squares (bestX, bestY, bestTime, bestMoves) =
            let
                (* add order D, L, R, U *)
                val (new_surround_squares, (bestX, bestY, bestTime, bestMoves)) = add_square ((X+1), Y, (time+1), (moves^"D")) [] (bestX, bestY, bestTime, bestMoves)
                val (new_surround_squares, (bestX, bestY, bestTime, bestMoves)) = add_square (X, (Y-1), (time+1), (moves^"L")) new_surround_squares (bestX, bestY, bestTime, bestMoves)
                val (new_surround_squares, (bestX, bestY, bestTime, bestMoves)) = add_square (X, (Y+1), (time+1), (moves^"R")) new_surround_squares (bestX, bestY, bestTime, bestMoves)
                val (new_surround_squares, (bestX, bestY, bestTime, bestMoves)) = add_square ((X-1), Y, (time+1), (moves^"U")) new_surround_squares (bestX, bestY, bestTime, bestMoves)
            in
                ((new_squares @ rev new_surround_squares), (bestX, bestY, bestTime, bestMoves))
            end

        fun cat_bfs [] [] (bestX, bestY, bestTime, bestMoves) =
            (bestTime, bestMoves)
          | cat_bfs [] new_squares (bestX, bestY, bestTime, bestMoves) =
            cat_bfs new_squares [] (bestX, bestY, bestTime, bestMoves)
          | cat_bfs squares new_squares (bestX, bestY, bestTime, bestMoves) =
            let
                val (X, Y, time, moves) = hd squares
                val rest_squares = tl squares
                val (new_squares, (bestX, bestY, bestTime, bestMoves)) = check_surround (X, Y, time, moves) new_squares (bestX, bestY, bestTime, bestMoves)
                (*val _ = print_bool_array checked_array*)
            in
                cat_bfs rest_squares new_squares (bestX, bestY, bestTime, bestMoves)
            end
    in
        cat_bfs [(catX, catY, ~1, "")] [] (catX, catY, ~1, "")
    end


fun savethecat fileName =
    let
        val (map_list, N, M) = parse fileName
        val map_array = Array2.fromList map_list
        val time_array = Array2.array (N, M, ~1)
        val ((catX, catY), pipes) = find_init map_array time_array
        val _ = flood_fill time_array N M pipes
        val checked_array = Array2.array (N, M, false)

        val (time, moves) = solve_cat (catX, catY) time_array checked_array N M

        (*for debugging*)
        val _ = print_char_array map_array
        val _ = print_int_array time_array

    in
        if (time = ~1 andalso (String.compare (moves, "") = EQUAL)) then
            print ("infinity\n" ^ "stay\n")
        else if time = ~1 then
            print ("infinity\n" ^ moves ^ "\n")
        else print (Int.toString (time-1) ^ "\n" ^ moves ^ "\n")
    end
