import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.ListIterator;

public class SaveTheCat {
    static World myworld = new World();

    static char[][] fileToMapArray (String filename) {
        ArrayList<char[]> myarraylist = new ArrayList<char[]>();
        File myfile = new File(filename);

        try {
            Scanner sc = new Scanner(myfile);

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                char[] line_array = line.toCharArray();
                myarraylist.add(line_array);
            }
        } catch (FileNotFoundException ex) {
             System.out.println("File " + filename + " not found");
             System.exit(1);
        }
        char[][] myarray = new char[myarraylist.size()][];
        myarray = myarraylist.toArray(myarray);
        return myarray;
    }

    static void update_square (int i, int j, int time) {
        if (i>=0 && i<myworld.N && j>=0 && j<myworld.M) {       // if valid
            int current_time = myworld.TimeArray[i][j];
            if (current_time == -1) {
                myworld.TimeArray[i][j] = time+1;
                myworld.pipes.add(new Square(i,j,time+1));
            }
        }
    }

    static void flood_fill () {
        for (int i=0; i < myworld.pipes.size(); i++) {
            Square pipe = myworld.pipes.get(i);
            update_square (pipe.X - 1, pipe.Y, pipe.time);
            update_square (pipe.X + 1, pipe.Y, pipe.time);
            update_square (pipe.X, pipe.Y - 1, pipe.time);
            update_square (pipe.X, pipe.Y + 1, pipe.time);
        }
    }

    static void check_square (int i, int j, int time, String moves) {
        if (i>=0 && i<myworld.N && j>=0 && j<myworld.M) {        // if valid
            int squaretime = myworld.TimeArray[i][j];
            if (squaretime !=-2 && !myworld.CheckedArray[i][j]) {  // if not checked and not obstacle
                //System.out.println ("checking " + i + "," + j);
                myworld.CheckedArray[i][j] = true;
                if (squaretime ==-1 || squaretime > time) {
                    myworld.newsquares.add(new Square(i, j, time+1, moves));
                    if ((squaretime > myworld.cat.time) ||
                        (squaretime == myworld.cat.time && (i+j < myworld.cat.X+myworld.cat.Y)) ||
                        (squaretime == myworld.cat.time && (i+j == myworld.cat.X+myworld.cat.Y) && (moves.compareTo(myworld.cat.moves) == 1))) {
                        myworld.cat = new Square(i,j,squaretime, moves);
                    }
                }
            }
        }
    }

    static void cat_bfs () {
        myworld.newsquares = new ArrayList <Square>();
        for (Square current_square : myworld.squares) {
            check_square (current_square.X + 1, current_square.Y, current_square.time, current_square.moves+"D");
            check_square (current_square.X, current_square.Y - 1, current_square.time, current_square.moves+"L");
            check_square (current_square.X, current_square.Y + 1, current_square.time, current_square.moves+"R");
            check_square (current_square.X - 1, current_square.Y, current_square.time, current_square.moves+"U");
        }
        myworld.squares = myworld.newsquares;
        if (myworld.squares.size() > 0) {
            cat_bfs();
        }
    }

    public static void main (String args[]) {
        myworld.MapArray = fileToMapArray (args[0]);
        myworld.Init();
        //myworld.PrintPipes();
        flood_fill();
        myworld.cat.time = myworld.TimeArray[myworld.cat.X][myworld.cat.Y];
        //myworld.Print();
        myworld.squares.add(new Square(myworld.cat.X,myworld.cat.Y,-1));
        cat_bfs();
        //myworld.Print();
        if (myworld.cat.time == -1) {
            System.out.println("infinity");
        } else {
            System.out.println(myworld.cat.time-1);
        }
        if (myworld.cat.moves == "") {
            System.out.println("stay");
        } else {
            System.out.println(myworld.cat.moves);
        }

    }

}
