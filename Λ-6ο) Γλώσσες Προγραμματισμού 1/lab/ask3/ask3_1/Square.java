class Square {
    int X;
    int Y;
    int time;
    String moves;

    Square () {
        X=0;
        Y=0;
        time=0;
        moves="";
    }

    Square (int newX, int newY, int newtime) {
        X=newX;
        Y=newY;
        time=newtime;
        moves="";
    }

    Square (int newX, int newY, int newtime, String newmoves) {
        X=newX;
        Y=newY;
        time=newtime;
        moves=newmoves;
    }

    void Print() {
        System.out.println("X=" + X + " Y=" + Y + " time=" + time + " moves=" + moves);
    }
}