#!/usr/bin/python3

import sys

# read N, M and MAP_ARRAY from file
def read_file (file, MAP_ARRAY) :
    fo = open(file, "r")
    for line in fo:
        list_line = list(line)
        list_line.pop()
        MAP_ARRAY.append(list_line)
    fo.close()
    N = len (MAP_ARRAY)
    M = len (MAP_ARRAY[0])
    return (N, M)

# create time array, find CatX, CatY and pipes
# set 0 for Cat position
# set -2 for obstacles (simplifies later checks)
# set everything else at -1
def init_time_array (N, M, TIME_ARRAY, pipes) :
    for i in range(N) :
        for j in range(M) :
            if MAP_ARRAY[i][j] == 'A' :
                CatX, CatY = i, j
            elif MAP_ARRAY[i][j] == 'X' :
                TIME_ARRAY[i][j] = -2
            elif MAP_ARRAY[i][j] == 'W' :
                TIME_ARRAY[i][j] = 0
                pipes.append((i,j,0))
    return CatX, CatY

# flood the time array
def flood_fill (TIME_ARRAY, pipes) :

    def update_time (thisx, thisy, thistime, pipes) :
        if (TIME_ARRAY[thisx][thisy] == -1) :
            TIME_ARRAY[thisx][thisy] = thistime
            pipes.append((thisx,thisy,thistime))

    for pipe in pipes :
        (x, y, time) = pipe
        if (x > 0) :
            update_time (x-1, y, time+1, pipes)
        if (y > 0) :
            update_time (x, y-1, time+1, pipes)
        if (x < N-1) :
            update_time (x+1, y, time+1, pipes)
        if (y < M-1) :
            update_time (x, y+1, time+1, pipes)
    return

def solve_cat (CatX, CatY, TIME_ARRAY, CHECKED_ARRAY, N, M) :

    def check_square (x, y, time, moves, squares, best) :
        squareTime = TIME_ARRAY[x][y]
        checked = CHECKED_ARRAY[x][y]
        (bestX, bestY, bestTime, bestMoves) = best
        new_best = best
        # if not obstacle, not overflowed and not checked
        if (squareTime != -2) and (bestTime == -1 or time < squareTime) and (not checked) :
            CHECKED_ARRAY[x][y] = True

            #print ("Valid square: ", (x,y,squareTime,moves)) #debug
            # if this square is better
            if (squareTime > bestTime) or ((squareTime == bestTime) and (x+y < bestX+bestY)) :
                new_best = (x, y, squareTime, moves)
                #print ("best changed to : ", new_best) #debug
            squares.append((x,y,time,moves))
        return new_best

    squares = [(CatX, CatY, -1, "")]
    best = (CatX, CatY, -1, "")
    for square in squares :
        # check order D, L, R, U
        (x, y, time, moves) = square
        if (x < N-1) :
            best = check_square (x+1, y, time+1, moves+"D", squares, best)
        if (y > 0) :
            best = check_square (x, y-1, time+1, moves+"L", squares, best)
        if (y < M-1) :
            best = check_square (x, y+1, time+1, moves+"R", squares, best)
        if (x > 0) :
            best = check_square (x-1, y, time+1, moves+"U", squares, best)
    return best

# main program
MAP_ARRAY = []
N, M = read_file (sys.argv[1], MAP_ARRAY)
TIME_ARRAY = [[-1] * M for _ in range (N)]
CHECKED_ARRAY = [[False] * M for _ in range (N)]
pipes = []
CatX, CatY = init_time_array (N, M, TIME_ARRAY, pipes)


# debug
# print ("initial pipes = ", pipes)

flood_fill (TIME_ARRAY, pipes)
best = solve_cat (CatX, CatY, TIME_ARRAY, CHECKED_ARRAY, N, M)
(bestX, bestY, bestTime, bestMoves) = best
#print ("Best tuple is: ", best)
if (bestTime == -1) :
    print("infinity")
else :
    print (bestTime-1)
if (bestMoves == "") :
    print ("stay")
else :
    print (bestMoves)

# debug
'''
print ("N = ", N, "M = ", M)
print ("Cat coordinates = ", (CatX, CatY))

print ("MAP_ARRAY\n")
for line in MAP_ARRAY :
    print (line, "\n")
print ("TIME_ARRAY\n")
for line in TIME_ARRAY :
    print (line, "\n")
print ("CHECKED_ARRAY\n")
for line in CHECKED_ARRAY :
    print (line, "\n")
'''