class State {
    int L;
    int R;
    String program;
    char lastf;
    boolean hast;

    /*State (int newL, int newR, String newprogram) {
        L=newL;
        R=newR;
        program=newprogram;
        lastf='\0';
        hast=false;
    }*/

    State (int newL, int newR, String newprogram, char newlastf, boolean newhast) {
        L=newL;
        R=newR;
        program=newprogram;
        lastf=newlastf;
        hast = newhast;
    }

    State (State prev) {
        L=prev.L;
        R=prev.R;
        program=prev.program;
        lastf=prev.lastf;
        hast=prev.hast;
    }
}