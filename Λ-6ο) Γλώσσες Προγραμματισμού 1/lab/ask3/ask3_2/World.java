import java.util.ArrayList;

class World {
    int Q=0;
    int[][] A = new int[0][0];
    String[] B = new String[0];

    void Print () {
        System.out.println("Q=" + Q);
        System.out.println("A:");
        for (int[] i : A) {
            for (int j : i) {
                System.out.print(j);
                System.out.print('\t');
            }
            System.out.println();
        }
        System.out.println();
        for (String s : B) {
            System.out.print(s);
            System.out.println();
        }
        System.out.println();
    }

}