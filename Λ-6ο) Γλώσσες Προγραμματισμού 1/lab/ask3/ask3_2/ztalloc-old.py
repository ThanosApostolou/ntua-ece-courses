#!/usr/bin/python3

import sys
import time

class State :
    L=0
    R=0
    program=""
    lastf=''
    hast=False
    def __init__(self, newL, newR, newprogram, newlastf, newhast) :
        self.L=int(newL)
        self.R=int(newR)
        self.program=newprogram
        self.lastf=newlastf
        self.hast=newhast

# Read from file
# Q is questions number
# A is a list of list of integers: [Lin, Rin, Lout, Rout] of each question
def read_file (file) :
    fo = open("ztalloc-testcases-public/"+file, "r")
    Q = int(fo.readline().split()[0])
    A = []
    for line in fo :
        listline=[]
        for val in line.split() :
            listline.append(int(val))
        A.append(listline)
    fo.close()
    return (Q, A)

# function h()
def h (x) :
    return int(int(x) / 2)
# function t()
def t (x) :
    return int(3*int(x)+1)

def find (states, Lout, Rout) :
    for current in states :
        # for debugging
        #print("checking program " + current.program + " L=" ,current.L, " R=" , current.R , " lastf=" , current.lastf , " hast=" , current.hast)
        #time.sleep(0.1)
        # determine impossible if string longer than 40 (need to find a better way)
        if (len(current.program) > 40) :
                return "IMPOSSIBLE"
        # if values are between Lout and Rout then answer found
        if (current.L >= Lout and current.R <= Rout) :
            if (current.program == "") :
                return "EMPTY"
            else :
                return current.program
        next = State(current.L, current.R, current.program, current.lastf, current.hast)
        if (next.L !=0 or next.R !=0) :
            if (not next.hast or next.lastf != 'h') :
                next.L = h(next.L)
                next.R = h(next.R)
                next.program = next.program + "h"
                next.lastf = 'h'
                if (next.L < 333333 or next.R < 333333) :
                    states.append(next)
        nextt = State(current.L, current.R, current.program, current.lastf, current.hast)
        if (nextt.L <= Lout and nextt.R <= Rout) :
            nextt.L = t(nextt.L)
            nextt.R = t(nextt.R)
            nextt.program += "t"
            nextt.lastf = 't'
            nextt.hast = True
            if (nextt.L <= 999999 and nextt.R <= 999999) :
                states.append(nextt)
    return "IMPOSSIBLE"

def answer (Lin, Rin, Lout, Rout) :
    first = State(Lin, Rin, "", '\0', False)
    states = [first]
    return find(states, Lout, Rout)

# main program
def main () :
    (Q, A) = read_file(sys.argv[1])
    # for each question print the answer
    for q in A :
        print (answer (q[0], q[1], q[2], q[3]))
    # for debugging
    #print (Q, A)

main()

'''
print ("N = ", N, "M = ", M)
print ("Cat coordinates = ", (CatX, CatY))

print ("MAP_ARRAY\n")
for line in MAP_ARRAY :
    print (line, "\n")
print ("TIME_ARRAY\n")
for line in TIME_ARRAY :
    print (line, "\n")
print ("CHECKED_ARRAY\n")
for line in CHECKED_ARRAY :
    print (line, "\n")
'''