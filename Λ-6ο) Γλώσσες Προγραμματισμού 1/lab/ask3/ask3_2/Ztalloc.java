import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.ListIterator;

public class Ztalloc {
    static World myworld = new World();

    static void fileToMapArray (String filename) {
        File myfile = new File(filename);
        try {
            Scanner sc = new Scanner(myfile);
            int Q = sc.nextInt();
            int myarray[][] = new int[Q][4];
            for (int i=0; i<Q; i++) {
                myarray[i][0] = sc.nextInt();
                myarray[i][1] = sc.nextInt();
                myarray[i][2] = sc.nextInt();
                myarray[i][3] = sc.nextInt();
            }
            myworld.Q = Q;
            myworld.A = myarray;
        } catch (FileNotFoundException ex) {
             System.out.println("File " + filename + " not found");
             System.exit(1);
        }
    }

    static int h (int x) {
        return x / 2;
    }

    static int t (int x) {
        return 3*x+1;
    }

    static String find (ArrayList<State> states, int Lout, int Rout) {
        for (int i=0; i < states.size(); i++) {
            State current = states.get(i);
            // for debugging
            //System.out.println("checking program " + current.program + " L=" + current.L + " R=" + current.R + " lastf=" + current.lastf + " hast=" + current.hast);
            //try { Thread.sleep(10); } catch(InterruptedException e) {}
            if (current.program.length() > 40) {
                return "IMPOSSIBLE";
            }
            if (current.L >= Lout && current.R <= Rout) {
                if (current.program.compareTo("") == 0) {
                    return "EMPTY";
                } else {
                    return current.program;
                }
            }
            State next = new State(current);
            if (next.L !=0 || next.R !=0) {
                if (!next.hast || next.lastf != 'h') {
                    next.L = h(next.L);
                    next.R = h(next.R);
                    next.program = next.program + "h";
                    next.lastf = 'h';
                    if (next.L < 333333 || next.R < 333333) {
                        states.add(next);
                    }
                }
            }
            next = new State(current);
            if (next.L <= Lout && next.R <= Rout) {
                next.L = t(next.L);
                next.R = t(next.R);
                next.program = next.program + "t";
                next.lastf = 't';
                next.hast = true;
                if (next.L <= 999999 && next.R <= 999999) {
                    states.add(next);
                }
            }
        }
        return "IMPOSSIBLE";
    }

    static String answer (int Lin, int Rin, int Lout, int Rout) {
        State first = new State(Lin, Rin, "", '\0', false);
        ArrayList<State> states = new ArrayList<State>();
        states.add(first);
        return find(states, Lout, Rout);
    }

    public static void main (String args[]) {
        fileToMapArray(args[0]);
        //myworld.B = new String[myworld.Q];
        String program = "";

        for (int i=0; i<myworld.Q; i++) {
            //myworld.B[i] = answer(myworld.A[i][0], myworld.A[i][1], myworld.A[i][2], myworld.A[i][3]);
            System.out.println( answer(myworld.A[i][0], myworld.A[i][1], myworld.A[i][2], myworld.A[i][3]));
        }
        //myworld.Print();
    }

}
