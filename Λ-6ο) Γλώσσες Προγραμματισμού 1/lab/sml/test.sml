(* listify [3,5,1,8,9,2,1,0,1,7,4] 7 => [[3,5,1],[8,9],[2,1,0,1],[7],[4]] *)
fun listify ls x =
    let
        (* create interior list *)
        fun create_interior [] acc less =
            ([], rev acc)
          | create_interior (h :: t) acc less =
            if (less andalso h < x) orelse (not less andalso h >= x) then
                create_interior t (h :: acc) less
            else
                ((h :: t), (rev acc))
        
        (* add each interior list *)
        fun traverse [] acc less =
            rev acc
          | traverse ls acc less =
            let
                val (remaining_ls, inside_list) = create_interior ls [] less
            in
                traverse remaining_ls (inside_list :: acc) (not less)
            end
    in
        traverse ls [] true 
    end

(* maxSumSublist [-2, 1, -3, 4, -1, 2, 1, -5, 4] => 6 *)
fun maxSumSublist L =
    let
        fun traverse (el, (prev_max, curr_max)) =
            let
                val max_here = Int.max(prev_max + el, el)
                val max = Int.max(curr_max, max_here)
            in
                (max_here, max)
            end
        val (_, result) = foldl traverse (0, 0) L
    in
        result
    end

(* datatype 'a tree = node of 'a * 'a tree * 'a tree | empty *)
datatype 'a tree = node of 'a * 'a tree * 'a tree | empty;
fun countUnbalanced T = 
    let
        fun sizeUnbalanced empty = (0, 0)
        | sizeUnbalanced (node (_, left, right)) =
            let
                val (sLeft, uLeft) = sizeUnbalanced left
                val (sRight, uRight) = sizeUnbalanced right
            in
                (
                sLeft + sRight + 1, (* number of nodes under this tree *)
                uLeft + uRight + (if sLeft = sRight then 0 else 1)
                )
            end
    in
        #2(sizeUnbalanced T)
    end

(* itermap (fn x => x+1) [1, 2, 3, 4, 5, 6, 7] => [1,3,5,7,9,11,13]*)
fun itermap f [] =
    []
  | itermap f ls =
    let
        fun apply_f el 0 =
            el
          | apply_f el counter =
            apply_f (f el) (counter-1)
            
        fun traverse [] acc counter =
            rev acc
          | traverse (el :: t) acc counter =
            let
                val new_el = apply_f el counter
            in
                traverse t (new_el :: acc) (counter+1)
            end
    in
        traverse ls [] 0
    end  

fun mergeSort nil = nil
  | mergeSort [e] = [e]
  | mergeSort theList =
    let
        fun halve nil = (nil, nil)
          | halve [a] = ([a], nil)
          | halve (a::b::cs) =
            let
                val (x, y) = halve cs
            in
                (a::x, b::y)
            end

        fun merge (nil, ys) = ys
          | merge (xs, nil) = xs
          | merge (x::xs, y::ys) =
            if x < y then x :: merge(xs, y::ys)
            else y :: merge(x::xs, ys)
            
        val (x, y) = halve theList
    in
        merge (mergeSort x, mergeSort y)
    end
