fun g x =
    let
        val inc = 1
        fun f y = y + inc
        fun h z =
            let
                val inc = 2
            in
                f z
            end
    in
        h x
    end

fun listify ls x =
    let
        (* Αν είναι το 1ο στοιχείο που εξετάζουμε *)
        fun traverse (el, [nil]) =
            if el < x then [[el]] else [[el], []]
        (* Αλλιώς, αν έχουν μπει ήδη στοιχεία *)
          | traverse (el, (lastNum :: lastGroup) :: rest) =
            if (lastNum < x) = (el < x) then
                (* Μπαίνει στην ίδια ομάδα *)
                (el :: lastNum :: lastGroup) :: rest
            else
                (* Δημιουργεί νέα ομάδα *)
                [el] :: (lastNum :: lastGroup) :: rest
        (* Τρέχουμε την traverse σειριακά στην ls μέσω foldl *)
        val result = foldl traverse [[]] ls
    in
        (* Αντιστρέφουμε τη σειρά ομάδων και των στοιχείων τους για να βγει το σωστό
        αποτέλεσμα *)
        rev (map rev result)
    end
    
fun mylistify ls x =
    let
        (* find a continuous list with min numbers *)
        fun create_inside [] acc less =
            ([], rev acc)
          | create_inside (h :: t) acc true =
            if h < x then
                create_inside t (h :: acc) true
            else
                ((h :: t), (rev acc))
          | create_inside (h :: t) acc false =
            if h >= x then
                create_inside t (h :: acc) false
            else
                ((h :: t), (rev acc))
                
        fun traverse [] acc less =
            rev acc
          | traverse ls acc less =
            let
                val (remaining_ls, inside_list) = create_inside ls [] less
            in 
                if less then
                    traverse remaining_ls (inside_list :: acc) false
                else
                    traverse remaining_ls (inside_list :: acc) true
            end
    in
        traverse ls [] true 
    end
