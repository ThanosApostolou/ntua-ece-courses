#!/usr/bin/env python3

print ("Hello world!")
a = 42
# this a is global
def reasonable():
    print(a)
def what(a):
    a = 17
    print(a)

reasonable() # prints 42
print(a) # prints 42
what(a) # prints 17
print(a) # prints 42, what???

s = "hello world"
print(s[4]) # prints 'o'
s = s[:1]
print(s)

s = (1, 2, "what", 3)
print(s[2]) # prints 2
s = tuple("hello world") # ('h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd')
print(s[4]) # prints 'o'
s = s[:4] + ("x",) + s[5:]
print(s)

s = [1, 2, "what", 3]
print(s)

A = [x+1 for x in range(10)]
print(A)
B = [10-x for x in range(10)]
print (B)
L = [A[i] + B[i] for i in range(10)]
print (A[0:2])
print(sum(L[3:5]))
