(* Read the file line by line and return the map as lists inside a list with N and M *)
fun parse file =
    let
        fun readInt input =
	    	Option.valOf (TextIO.scanStream (Int.scan StringCvt.DEC) input)

        (*
        * returns counter lines in reversed ordered which doesn't matter
        * each line has the number digits as chars in reversed order which simplifies later comparison
        *)
        fun readLines stream acc 0 =
            acc
          | readLines stream acc counter =
            let
                val final_line =
                    case TextIO.inputLine stream of
                        SOME line =>
                            let
                                val list_line = tl (rev (explode line))
                                (*val trimmed_line = List.take (list_line, (length list_line)-1)*)
                            in
                                list_line
                            end
                        | NONE      => []

            in
                readLines stream (final_line :: acc) (counter-1)
            end


        val stream = TextIO.openIn file
        (* read K, N, Q and consume new line *)
        val K = readInt stream
		val N = readInt stream
		val Q = readInt stream
		val _ = TextIO.inputLine stream

        (* X is list of list of number digits (chars)
        *  Y is list of list of wining number digits (chars)
        *)
        val X = readLines stream [] N
        val Y = readLines stream [] Q
    in
        (K, N, Q, X, Y)
    end

(* compare 2 numbers digit by digit
*  remember digits are in reversed order which is what we want!
*)
fun compare [] _ common =
    common
  | compare _ [] common =
    common
  | compare (Xi_dig :: Xi_tl) (Yi_dig :: Yi_tl) common =
    if (Xi_dig = Yi_dig) then
        compare Xi_tl Yi_tl (common+1)
    else common

fun find_winning [] Yi wining_list =
    wining_list
  | find_winning (Xi :: X_tl) Yi wining_list =
    let
        val common = compare Xi Yi 0
    in
        if (common > 0) then
            find_winning X_tl Yi (common :: wining_list)
        else
            find_winning X_tl Yi wining_list
    end

fun find_alternatives X [] alternatives_list =
    alternatives_list
  | find_alternatives X Y alternatives_list =
    let
        val Yi = hd Y
        val wining_list = find_winning X Yi []
    in
        find_alternatives X (tl Y) (wining_list :: alternatives_list)
    end

fun solve_lottery X Y =
    find_alternatives X Y []

fun lottery fileName =
    let
        val (K, N, Q, X, Y) = parse fileName
        val alternatives_list = solve_lottery X Y
    in
        (K, N, Q, X, Y, alternatives_list)
    end