(* Read the file line by line and return the map as lists inside a list with N and M *)
fun parse file =
    let
        fun readInt input =
	    	Option.valOf (TextIO.scanStream (Int.scan StringCvt.DEC) input)

        (*
        * returns counter lines
        * each line has the number digits as chars in reversed order which simplifies later comparison
        *)
        fun readLines stream acc 0 =
            rev acc
          | readLines stream acc counter =
            let
                val final_line =
                    case TextIO.inputLine stream of
                        SOME line =>
                            let
                                val list_line = tl (rev (explode line))
                                (*val trimmed_line = List.take (list_line, (length list_line)-1)*)
                            in
                                list_line
                            end
                        | NONE      => []

            in
                readLines stream (final_line :: acc) (counter-1)
            end


        val stream = TextIO.openIn file
        (* read K, N, Q and consume new line *)
        val K = readInt stream
		val N = readInt stream
		val Q = readInt stream
		val _ = TextIO.inputLine stream

        (* X is list of list of number digits (chars)
        *  Y is list of list of wining number digits (chars)
        *)
        val X = readLines stream [] N
        val Y = readLines stream [] Q
    in
        (K, N, Q, X, Y)
    end

(* compare 2 numbers and return common digits
*  remember digits are in reversed order which is what we want!
*)
fun compare [] _ common =
    common
  | compare _ [] common =
    common
  | compare (Xi_dig :: Xi_tl) (Yi_dig :: Yi_tl) common =
    if (Xi_dig = Yi_dig) then
        compare Xi_tl Yi_tl (common+1)
    else common

(*
* for given Yi calculate number of wining tickets and
* money mod (10^9 + 7)
*)
fun find_winning [] Yi won money =
    let
        val (_, mod_money) = IntInf.divMod (money, IntInf.pow(10, 9)+7)
    in
        (won, mod_money)
    end
  | find_winning (Xi :: X_tl) Yi won money =
    let
        val common = compare Xi Yi 0
    in
        if (common > 0) then
            find_winning X_tl Yi (won+1) (money + (IntInf.pow(2, common)-1))
        else
            find_winning X_tl Yi won money
    end

fun solve_lottery X [] =
    ()
  | solve_lottery X (Yi :: Y_tl) =
    let
        val (won, mod_money) = find_winning X Yi 0 0
        val _ = print(Int.toString (won) ^ " " ^ IntInf.toString (mod_money) ^ "\n");
    in
        solve_lottery X Y_tl
    end

fun lottery fileName =
    let
        val (K, N, Q, X, Y) = parse fileName
    in
        solve_lottery X Y
    end