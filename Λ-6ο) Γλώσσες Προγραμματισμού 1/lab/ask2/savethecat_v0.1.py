#!/usr/bin/python3

import sys

# read N, M and MAP_ARRAY from file
def read_file (file) :
    fo = open(file, "r")
    MAP_ARRAY = []
    for line in fo:
        line = line[:-1]
        MAP_ARRAY.append(list(line))
    fo.close()
    N = len (MAP_ARRAY)
    M = len (MAP_ARRAY[0])
    return (N, M, MAP_ARRAY)

# create time array, find CatX, CatY and pipes
# set 0 for Cat position
# set -2 for obstacles (simplifies later checks)
# set everything else at -1
def init_time_array (N, M) :
    TIME_ARRAY = [[-1 for _ in range (M)] for _ in range (N)]
    pipes = []
    for i in range(N) :
        for j in range(M) :
            if MAP_ARRAY[i][j] == 'A' :
                CatX, CatY = i, j
            elif MAP_ARRAY[i][j] == 'X' :
                TIME_ARRAY[i][j] = -2
            elif MAP_ARRAY[i][j] == 'W' :
                TIME_ARRAY[i][j] = 0
                pipes.append((i,j,0))
    return TIME_ARRAY, CatX, CatY, pipes

def flood_fill (TIME_ARRAY, pipes) :

    def update_time (thisx, thisy, thistime, pipes) :
        if (TIME_ARRAY[thisx][thisy] == -1) :
            TIME_ARRAY[thisx][thisy] = thistime
            pipes.append((thisx,thisy,thistime))

    for pipe in pipes :
        (x, y, time) = pipe
        if (x > 0) :
            update_time (x-1, y, time+1, pipes)
        if (y > 0) :
            update_time (x, y-1, time+1, pipes)
        if (x < N-1) :
            update_time (x+1, y, time+1, pipes)
        if (y < M-1) :
            update_time (x, y+1, time+1, pipes)
    return


(N, M, MAP_ARRAY) = read_file (sys.argv[1])
TIME_ARRAY, CatX, CatY, pipes = init_time_array (N, M)
CHECKED_ARRAY = [[False for _ in range (M)] for _ in range (N)]

# debug
# print ("initial pipes = ", pipes)

flood_fill (TIME_ARRAY, pipes)

# debug
print ("N = ", N, "M = ", M)
print ("Cat coordinates = ", (CatX, CatY))

print ("MAP_ARRAY\n")
for line in MAP_ARRAY :
    print (line, "\n")
print ("TIME_ARRAY\n")
for line in TIME_ARRAY :
    print (line, "\n")
print ("CHECKED_ARRAY\n")
for line in CHECKED_ARRAY :
    print (line, "\n")
