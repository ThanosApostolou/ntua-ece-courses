(* read input file and return n,k,[ribbon] *)
fun parse file =
    let
			(* A function to read an integer from specified input. *)
			fun readInt input = 
			Option.valOf (TextIO.scanStream (Int.scan StringCvt.DEC) input)

			(* Open input file. *)
    	val inStream = TextIO.openIn file

      (* Read an integer (number of countries) and consume newline. *)
			val n = readInt inStream
			val k = readInt inStream
			val _ = TextIO.inputLine inStream

      (* A function to read N integers from the open file. *)
			fun readInts 0 acc = acc (* Replace with 'rev acc' for proper order. *)
				| readInts i acc = readInts (i - 1) (readInt inStream :: acc)
    in
   		(n, k, readInts n [])
    end

(* return reduced list without losing any color, starting from head *)
fun find_min [] colors_freq =
			[]
  | find_min list colors_freq =
			let
				val index = (hd list) - 1
				val previous = Array.sub(colors_freq, index)
			in
				if Array.sub(colors_freq, index) = 1 then
					list
				else
					let
						val _ = Array.update(colors_freq, index, (previous-1))
					in
						find_min (tl list) colors_freq
					end
			end

(* return array with colors frequency *)
fun get_colors_freq (colors_freq, []) =
			colors_freq
  | get_colors_freq (colors_freq, ribbon) =
			let
				val index = (hd ribbon) - 1
				val previous = Array.sub(colors_freq, index)
				val _ = Array.update(colors_freq, index, (previous+1))
			in
				get_colors_freq (colors_freq, tl ribbon)
			end

(* check if there is any 0 color frequency *)
fun color_missing colors_freq 0 =
			false
  | color_missing colors_freq k =
			let
				val index = k - 1
			in
				if Array.sub(colors_freq, index) = 0 then
					true
				else
					color_missing colors_freq index
			end

(* solve the colors problem *)
fun solve (n, k, ribbon) =
	let
		val colors_freq = get_colors_freq (Array.array (k,0), ribbon)
	in
		(* exit early if a color is missing *)
		if (color_missing colors_freq k) then
			print(Int.toString 0 ^ "\n")
		else
			let
				(* create a copied array beacuse colors_freq seems to be passed by reference and it gets changed *)
				val colors_freq_cp = Array.tabulate (Array.length colors_freq, fn i => Array.sub (colors_freq, i))
				
				(* reduce ribbon from left, then from right, and get length *)
				val min_left_partial = find_min ribbon colors_freq
				val min_left_final = find_min (rev min_left_partial) colors_freq
				val min_left = length min_left_final

				(* reduce reversed ribbon from left, then from right, and get length *)
				val min_right_partial = find_min (rev ribbon) colors_freq_cp
				val min_right_final = find_min (rev min_right_partial) colors_freq_cp
				val min_right =  length min_right_final
			in
				if min_left < min_right then
					print(Int.toString min_left ^ "\n")
				else
					print(Int.toString min_right ^ "\n")
			end
	end

fun colors fileName = solve (parse fileName)
