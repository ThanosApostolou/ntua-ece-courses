(* Print a given array2, for debugging purposes
 * 1 function for char array and 1 function for int array *)
fun print_char_array A =
    let
        val N = Array2.nRows A
        val M = Array2.nCols A
        fun print_elements i j =
            if j = (M-1) andalso i <> (N-1) then (
                print(Char.toString (Array2.sub (A, i, j)));
                print("\n");
                print_elements (i+1) 0
            ) else if j = (M-1) andalso i = (N-1) then (
                print(Char.toString (Array2.sub (A, i, j)));
                print("\n")
            ) else (
                print(Char.toString (Array2.sub (A, i, j)));
                print("\t");
                print_elements i (j+1)
            )
    in
        print_elements 0 0;
        print("\n")
    end

fun print_int_array A =
    let
        val N = Array2.nRows A
        val M = Array2.nCols A
        fun print_elements i j =
            if j = (M-1) andalso i <> (N-1) then (
                print(Int.toString (Array2.sub (A, i, j)));
                print("\n");
                print_elements (i+1) 0
            ) else if j = (M-1) andalso i = (N-1) then (
                print(Int.toString (Array2.sub (A, i, j)));
                print("\n")
            ) else (
                print(Int.toString (Array2.sub (A, i, j)));
                print("\t");
                print_elements i (j+1)
            )
    in
        print_elements 0 0;
        print("\n")
    end

(* Read the file line by line and return the map as lists inside a list with N and M *)
fun parse file =
    let
        fun next_String input = (TextIO.inputAll input)
        val stream = TextIO.openIn file
        fun readLines stream acc N =
            case TextIO.inputLine stream of
                SOME line =>
                    let
                        val list_line = explode line
                        val trimed_line = List.take (list_line, (length list_line)-1)
                    in
                        readLines stream (trimed_line :: acc) (N+1)
                    end
                | NONE      => (rev acc, N, length (hd acc))
    in
        readLines stream [] 0
    end

(* temp old stuff
accumulate_line list_line =
                    if String.compare (line, "\n") = EQUAL then
                        readLines stream acc N
                    else readLines stream (rev (tl (rev (explode line))) :: acc) (N+1)

SOME line => readLines stream (acc @ [rev (tl (rev (explode line)))]) (N+1)
*)

fun find_init map_array time_array =
    let
        val N = Array2.nRows map_array
        val M = Array2.nCols map_array
        fun iterate i j (catX, catY) pipes =
            let
                val current = Array2.sub (map_array, i, j)
                val cat_coord = if current = #"A" then (i, j)
                                else (catX, catY)
                val pipes_list = if current = #"W" then (i, j, 0) :: pipes
                                 else pipes
                val _ = if current = #"W" then
                            Array2.update (time_array, i, j, 0)
                        else if current = #"X" then
                            Array2.update (time_array, i, j, ~2)
                        else ()
            in
                if i = (N-1) andalso j = (M-1) then
                    (cat_coord, rev pipes_list)
                else if i <> (N-1) andalso j = (M-1) then
                    iterate (i+1) 0 cat_coord pipes_list
                else
                    iterate i (j+1) cat_coord pipes_list
            end
    in
        iterate 0 0 (~1, ~1) []
    end

fun flood_fill time_array N M pipes =
    let
        (* update this coordinate if it's valid *)
        fun update_this thisX thisY thisTime pipes =
            if thisX >= 0 andalso thisX < N andalso thisY >= 0 andalso thisY < M then
                let
                    val current = Array2.sub (time_array, thisX, thisY)
                    val change = if current = ~1 then true
                                 else false
                in
                    if change then
                        let
                            val _ = Array2.update (time_array, thisX, thisY, thisTime)
                        in
                            (thisX, thisY, thisTime) :: pipes
                        end
                    else pipes
                end
            else pipes

        (* update all 4 nearest squares *)
        fun update_surround X Y time pipes repeat =
            if repeat = 4 then
                let
                    val pipes = update_this (X-1) Y (time+1) pipes
                in
                    update_surround X Y time pipes (repeat-1)
                end
            else if repeat = 3 then
                let
                    val pipes = update_this (X+1) Y (time+1) pipes
                in
                    update_surround X Y time pipes (repeat-1)
                end
            else if repeat = 2 then
                let
                    val pipes = update_this X (Y-1) (time+1) pipes
                in
                    update_surround X Y time pipes (repeat-1)
                end
            else if repeat = 1 then
                let
                    val pipes = update_this X (Y+1) (time+1) pipes
                in
                    update_surround X Y time pipes (repeat-1)
                end
            else
                rev pipes

        (* for each pipe, remove it from list and get new pipes until list is empty *)
        fun fill_from_pipes [] =
            ()
          | fill_from_pipes pipes =
            let
                val (X, Y, time) = hd pipes
                (* send rest pipes in reversed order *)
                val rest_pipes = rev (tl pipes)
                val pipes = update_surround X Y time rest_pipes 4
            in
                fill_from_pipes pipes
            end

    in
        fill_from_pipes pipes
    end

fun solve_cat (catX, catY) time_array checked_array N M =
    let
        fun add_square (thisX, thisY, thisTime, thisMoves) squares (bestTime, bestMoves) =
            if thisX >= 0 andalso thisX < N andalso thisY >= 0 andalso thisY < M then
                let
                    val squareTime = Array2.sub (time_array, thisX, thisY)
                    val checked = Array2.sub (checked_array, thisX, thisY)
                    val valid = if (squareTime <> ~2) andalso
                                   (thisTime < squareTime) andalso
                                   (not checked)
                                then true
                                else false
                    val is_better = if (squareTime > bestTime) orelse
                                       (squareTime = bestTime andalso String.compare (thisMoves, bestMoves) = LESS)
                                    then true
                                    else false
                    val _ = Array2.update (checked_array, thisX, thisY, true)
                in
                    if valid andalso is_better then
                        (((thisX, thisY, thisTime, thisMoves) :: squares), (squareTime, thisMoves))
                    else if valid then
                        (((thisX, thisY, thisTime, thisMoves) :: squares), (bestTime, bestMoves))
                    else (squares, (bestTime, bestMoves))
                end
            else (squares, (bestTime, bestMoves))

        fun check_surround (X, Y, time, moves) squares (bestTime, bestMoves) repeat =
            if repeat = 4 then
                let
                    val (squares, (bestTime, bestMoves)) = add_square ((X-1), Y, (time+1), (moves^"U")) squares (bestTime, bestMoves)
                in
                    check_surround (X, Y, time, moves) squares (bestTime, bestMoves) (repeat-1)
                end
            else if repeat = 3 then
                let
                    val (squares, (bestTime, bestMoves)) = add_square ((X+1), Y, (time+1), (moves^"D")) squares (bestTime, bestMoves)
                in
                    check_surround (X, Y, time, moves) squares (bestTime, bestMoves) (repeat-1)
                end
            else if repeat = 2 then
                let
                    val (squares, (bestTime, bestMoves)) = add_square (X, (Y-1), (time+1), (moves^"L")) squares (bestTime, bestMoves)
                in
                    check_surround (X, Y, time, moves) squares (bestTime, bestMoves) (repeat-1)
                end
            else if repeat = 1 then
                let
                    val (squares, (bestTime, bestMoves)) = add_square (X, (Y+1), (time+1), (moves^"R")) squares (bestTime, bestMoves)
                in
                    check_surround (X, Y, time, moves) squares (bestTime, bestMoves) (repeat-1)
                end
            else
                (rev squares, (bestTime, bestMoves))

        fun cat_bfs [] (bestTime, bestMoves) =
            ((bestTime-1), bestMoves)
          | cat_bfs squares (bestTime, bestMoves) =
            let
                val rest_squares = rev (tl squares)
                val (updated_squares, (bestTime, bestMoves)) = check_surround (hd squares) rest_squares (bestTime, bestMoves) 4
            in
                cat_bfs updated_squares (bestTime, bestMoves)
            end
    in
        cat_bfs [(catX, catY, 0, "")] (0, "")
    end


fun savethecat fileName =
    let
        val (map_list, N, M) = parse fileName
        val map_array = Array2.fromList map_list
        val time_array = Array2.array (N, M, ~1)
        val _ = print_char_array map_array
        val ((catX, catY), pipes) = find_init map_array time_array
        val _ = flood_fill time_array N M pipes
        val _ = print_int_array time_array
        val checked_array = Array2.array (N, M, false)
        val (time, moves) = solve_cat (catX, catY) time_array checked_array N M
    in
        (time,moves)
    end
