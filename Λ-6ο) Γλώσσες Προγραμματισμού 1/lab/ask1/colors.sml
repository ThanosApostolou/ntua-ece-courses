fun parse file =
    let
		(* A function to read an integer from specified input. *)
        fun readInt input = 
	    	Option.valOf (TextIO.scanStream (Int.scan StringCvt.DEC) input)

		(* Open input file. *)
    	val inStream = TextIO.openIn file

        (* Read an integer (number of countries) and consume newline. *)
		val n = readInt inStream
		val k = readInt inStream
		val _ = TextIO.inputLine inStream

        (* A function to read N integers from the open file. *)
		fun readInts 0 acc = acc (* Replace with 'rev acc' for proper order. *)
		  | readInts i acc = readInts (i - 1) (readInt inStream :: acc)
    in
   		(n, k, readInts n [])
    end

fun removeElem elem myList = List.filter (fn x => x <> elem) myList

fun find_min ribbon colors acc =
	if ribbon = [] andalso colors = [] then acc
	else if ribbon = [] andalso colors <> [] then []
	else if colors = [] then acc
	else find_min (tl ribbon) (removeElem (hd ribbon) colors) (acc @ [hd ribbon])

fun get_colors 0 =
	[]
  | get_colors k =
	k :: get_colors (k-1)

fun solve (n, k, ribbon) =
	let
		val colors = rev (get_colors k)
		val min_left_partial = find_min ribbon colors []
		val min_left = length (find_min (rev min_left_partial) colors [])
		val min_right_partial = find_min (rev ribbon) colors []
		val min_right = length (find_min (rev min_right_partial) colors [])
	in
		if min_left < min_right then
			print(Int.toString min_left ^ "\n")
		else print(Int.toString min_right ^ "\n")
	end

fun colors fileName = solve (parse fileName)
