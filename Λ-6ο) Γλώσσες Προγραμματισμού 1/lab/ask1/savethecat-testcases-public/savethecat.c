#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int N = 1;                  //N = # of rows
int M = 0;                  //M = # of columns

struct queueNode {                  //FIFO queue node
    int xCoord;
    int yCoord;
    char time;
    char *moves;
    struct queueNode *next;
};

struct queue {                      //FIFO queue
    struct queueNode *front;
    struct queueNode *last;
    int size;
};

void initQueue(struct queue *que) { //initialize queue
    que->front = NULL;
    que->last = NULL;
    que->size = 0;
}

void popQueue(struct queue *que) {  //pop front node of queue
    que->size--;
    free(que->front->moves);
    struct queueNode *tmp = que->front;
    que->front = que->front->next;
    free(tmp);
}
                                    //push node to the back of the queue
void pushQueue(struct queue *que, int xCoord, int yCoord, char time, char *moves) {
    if (xCoord >= 0 && xCoord <= N && yCoord >= 0 && yCoord <= M) {
        que->size++;
        if (que->front == NULL) {
            que->front = (struct queueNode *) malloc(sizeof(struct queueNode));
            que->front->xCoord = xCoord;
            que->front->yCoord = yCoord;
            que->front->time = time;
            que->front->moves = malloc(1);
            que->front->moves[0] = '\0';
            que->front->next = NULL;
            que->last = que->front;
        } else {
            que->last->next = (struct queueNode *) malloc(sizeof(struct queueNode));
            que->last->next->xCoord = xCoord;
            que->last->next->yCoord = yCoord;
            que->last->next->time = time;
            que->last->next->moves = malloc(strlen(que->front->moves) + strlen(moves) + 1);
            strcpy(que->last->next->moves, que->front->moves);
            strcat(que->last->next->moves, moves);
            que->last->next->next = NULL;
            que->last = que->last->next;
        }
    }
}

void floodFillMap(struct queue *que, struct queueNode *safety, char **map, bool *catSafe) {
    int xCoord, yCoord;
    char time;
    while (que->size != 0) {
        xCoord = que->front->xCoord;
        yCoord = que->front->yCoord;
        time = que->front->time;
        if (map[xCoord][yCoord] == '.' || map[xCoord][yCoord] == 'W' || map[xCoord][yCoord] == 'A'){
            if (map[xCoord][yCoord] == 'A') {
                *catSafe = false;
                safety->time = time - 1;
            }
            map[xCoord][yCoord] = time;
            pushQueue(que, xCoord - 1, yCoord, time + 1, "");
            pushQueue(que, xCoord + 1, yCoord, time + 1, "");
            pushQueue(que, xCoord, yCoord - 1, time + 1, "");
            pushQueue(que, xCoord, yCoord + 1, time + 1, "");
        }
        popQueue(que);
    }
}

void catBFS(struct queue *que, struct queueNode *safety, char **map) {
    int xCoord, yCoord;
    char time, datum;
    while (que->size != 0) {
        xCoord = que->front->xCoord;
        yCoord = que->front->yCoord;
        time = que->front->time;
        datum = map[xCoord][yCoord];
        if ((datum != 'X' && (datum >= time || datum == '.')) || ((datum == 'D' || datum == 'L' || datum == 'R' || datum == 'U') && (que->front->moves[strlen(que->front->moves)] < datum))) {
            if ((datum - 1 > safety->time && datum != '.' ) || ((datum - 1 == safety->time || datum == '.') && (xCoord < safety->xCoord || (xCoord == safety->xCoord && yCoord < safety->yCoord)))) {
                safety->xCoord = xCoord;
                safety->yCoord = yCoord;
                safety->time = datum - 1;
                safety->moves = realloc(safety->moves, strlen(que->front->moves) + 1);
                strcpy(safety->moves, que->front->moves);
            }
            map[xCoord][yCoord] = que->front->moves[strlen(que->front->moves)];
//            printf("map[%d][%d] = %d, time = %c, maxTime = %d, moveset = %s, cmoves = %s\n", xCoord, yCoord, map[xCoord][yCoord] - '0', time, safety->time - '0', safety->moves, que->front->moves);
            pushQueue(que, xCoord + 1, yCoord, time + 1, "D");
            pushQueue(que, xCoord, yCoord - 1, time + 1, "L");
            pushQueue(que, xCoord, yCoord + 1, time + 1, "R");
            pushQueue(que, xCoord - 1, yCoord, time + 1, "U");
        }
        popQueue(que);
    }
}

int main(int argc, char *argv[]) {
    int i, j, c;
    
    FILE *fp;
    fp = fopen(argv[1],"rb+");  //open the file
    if (fp == NULL) {           //error handling
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    
    while(!feof(fp)) {          //count lines & columns of map
        if ((c = fgetc(fp)) == '\n') {
            if ((c = fgetc(fp)) != EOF) {
                M = 1;
                N++;
            }
        } else {
            M++;
        }
    }
    
//    printf("%d %d\n", N, M);    //TESTING
    rewind(fp);                 //rewind file
    
    char **map = malloc(M * sizeof(*map));   //create 2D map
    for (i = 0; i < M; i++) {
        map[i] = malloc(N * sizeof(map));
    }
    
    struct queue que;
    initQueue(&que);
    struct queueNode safety;
    safety.moves = malloc(1);
    safety.moves[0] = '\0';
    
    for (i = 0; i < N; i++) {                   //fill map
        for (j = 0; j < M; j++) {
            c = fgetc(fp);
            while (c == '\n' || c == '\r') {
                c = fgetc(fp);
            }
            map[i][j] = c;
            if (c == 'W') {
                pushQueue(&que, i, j, '0', "");
            } else if (c == 'A') {
                safety.xCoord = i;
                safety.yCoord = j;
            }
        }
    }
    
    fclose(fp);
        
/*    for (i = 0; i < N; i++) {                   //print starting map (TESTING)
        for (j = 0; j < M; j++) {
           printf("%c", map[i][j]);
        }
        printf("\n");
    }
*/  //  printf("\n");
    
    bool catSafe = true;
    floodFillMap(&que, &safety, map, &catSafe);
    
/*    for (i = 0; i < N; i++) {                   //print flooded map (TESTING)
        for (j = 0; j < M; j++) {
           printf("%c", map[i][j]);
        }
        printf("\n");
    }
*///    printf("\n");
    initQueue(&que);
    pushQueue(&que, safety.xCoord, safety.yCoord, '0', "");
    catBFS(&que, &safety, map);
    if (catSafe) {
        printf("infinity\n");
    } else {
        printf("%d\n", safety.time - '0');
    }
    if (strlen(safety.moves) == 0) {
        printf("stay");
    } else {
        printf("%s", safety.moves);
    }
    free(safety.moves);
    
    return 0;
}
