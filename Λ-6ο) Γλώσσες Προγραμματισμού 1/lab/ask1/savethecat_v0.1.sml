(* Print a given array2, for debugging purposes
 * 1 function for char array and 1 function for int array *)
fun print_char_array A =
    let
        val N = Array2.nRows A
        val M = Array2.nCols A
        fun print_elements i j =
            if j = (M-1) andalso i <> (N-1) then (
                print(Char.toString (Array2.sub (A, i, j)));
                print("\n");
                print_elements (i+1) 0
            ) else if j = (M-1) andalso i = (N-1) then (
                print(Char.toString (Array2.sub (A, i, j)));
                print("\n")
            ) else (
                print(Char.toString (Array2.sub (A, i, j)));
                print("\t\t\t\t\t\t\t\t");
                print_elements i (j+1)
            )
    in
        print(Int.toString N ^ "  " ^ Int.toString M ^ "\n");
        print_elements 0 0;
        print("\n")
    end

fun print_int_array A =
    let
        val N = Array2.nRows A
        val M = Array2.nCols A
        fun print_elements i j =
            if j = (M-1) andalso i <> (N-1) then (
                print(Int.toString (Array2.sub (A, i, j)));
                print("\n");
                print_elements (i+1) 0
            ) else if j = (M-1) andalso i = (N-1) then (
                print(Int.toString (Array2.sub (A, i, j)));
                print("\n")
            ) else (
                print(Int.toString (Array2.sub (A, i, j)));
                print("\t");
                print_elements i (j+1)
            )
    in
        print(Int.toString N ^ "  " ^ Int.toString M ^ "\n");
        print_elements 0 0;
        print("\n")
    end

(* Read the file line by line and return the map as lists inside a list with N and M *)
fun parse file =
    let
        fun next_String input = (TextIO.inputAll input)
        val stream = TextIO.openIn file
        fun readLines stream acc N =
            let
                (* add line as list to accumuluator, without the new line char and only if it's not empty *)
                fun accumulate_line line =
                    if String.compare (line, "\n") = EQUAL then
                        readLines stream acc N
                    else readLines stream (rev (tl (rev (explode line))) :: acc) (N+1)
            in
                case TextIO.inputLine stream of
                    SOME line => accumulate_line line
                  | NONE      => (rev acc, N, length (hd acc))
            end
    in
        readLines stream [] 0
    end

(* solve the colors problem
SOME line => readLines stream (acc @ [rev (tl (rev (explode line)))]) (N+1)

fun solve (n, k, ribbon) =
	let
		val colors_freq = get_colors_freq (Array.array (k,0), ribbon)
	in
		(* exit early if a color is missing *)
		if (color_missing colors_freq k) then
			print(Int.toString 0 ^ "\n")
		else
			let
				(* create a copied array beacuse colors_freq seems to be passed by reference and it gets changed *)
				val colors_freq_cp = Array.tabulate (Array.length colors_freq, fn i => Array.sub (colors_freq, i))

				(* reduce ribbon from left, then from right, and get length *)
				val min_left_partial = find_min ribbon colors_freq
				val min_left_final = find_min (rev min_left_partial) colors_freq
				val min_left = length min_left_final

				(* reduce reversed ribbon from left, then from right, and get length *)
				val min_right_partial = find_min (rev ribbon) colors_freq_cp
				val min_right_final = find_min (rev min_right_partial) colors_freq_cp
				val min_right =  length min_right_final
			in
				if min_left < min_right then
					print(Int.toString min_left ^ "\n")
				else
					print(Int.toString min_right ^ "\n")
			end
	end*)

fun find_init map_array time_array =
    let
        val N = Array2.nRows map_array
        val M = Array2.nCols map_array
        fun iterate i j (catX, catY) pipes =
            let
                val current = Array2.sub (map_array, i, j)
                val cat_coord = if current = #"A" then (i, j)
                                else (catX, catY)
                val pipes_list = if current = #"W" then (i, j, 0) :: pipes
                                 else pipes
                val _ = if current = #"W" then
                            Array2.update (time_array, i, j, 0)
                        else if current = #"X" then
                            Array2.update (time_array, i, j, ~2)
                        else ()
            in
                if i = (N-1) andalso j = (M-1) then
                    (cat_coord, rev pipes_list)
                else if i <> (N-1) andalso j = (M-1) then
                    iterate (i+1) 0 cat_coord pipes_list
                else
                    iterate i (j+1) cat_coord pipes_list
            end
    in
        iterate 0 0 (~1, ~1) []
    end

fun fill_time_array time_array pipes =
    let
        val N = Array2.nRows time_array
        val M = Array2.nCols time_array

        fun update_surround X Y time new_pipes repeat=
            if X > 0 andalso repeat = 4 then
                let
                    val top = Array2.sub (time_array, (X-1), Y)
                    val change = if top = ~1 then true
                              else false
                in
                    if change then
                        let
                            val next_time = time+1
                            val _ = Array2.update (time_array, (X-1), Y, next_time)
                            val _ = print ("updating with " ^ Int.toString next_time ^ "\n")
                        in
                            update_surround X Y time (((X-1), Y, next_time) :: new_pipes) (repeat-1)
                        end
                    else
                        update_surround X Y time new_pipes (repeat-1)
                end
            else if repeat = 4 then update_surround X Y time new_pipes (repeat-1)
            else if X < (N-1) andalso repeat = 3 then
                let
                    val bot = Array2.sub (time_array, (X+1), Y)
                    val change = if bot = ~1 then true
                              else false
                in
                    if change then
                        let
                            val next_time = time+1
                            val _ = Array2.update (time_array, (X+1), Y, next_time)
                            val _ = print ("updating with " ^ Int.toString next_time ^ "\n")
                        in
                            update_surround X Y time (((X+1), Y, next_time) :: new_pipes) (repeat-1)
                        end
                    else
                        update_surround X Y time new_pipes (repeat-1)
                end
            else if repeat = 3 then update_surround X Y time new_pipes (repeat-1)
            else if Y > 0 andalso repeat = 2 then
                let
                    val left = Array2.sub (time_array, X, (Y-1))
                    val change = if left = ~1 then true
                              else false
                in
                    if change then
                        let
                            val next_time = time+1
                            val _ = Array2.update (time_array, X, (Y-1), next_time)
                            val _ = print ("updating with " ^ Int.toString next_time ^ "\n")
                        in
                            update_surround X Y time ((X, (Y-1), next_time) :: new_pipes) (repeat-1)
                        end
                    else
                        update_surround X Y time new_pipes (repeat-1)
                end
            else if repeat = 2 then update_surround X Y time new_pipes (repeat-1)
            else if Y < (M-1) andalso repeat = 1 then
                let
                    val left = Array2.sub (time_array, X, (Y+1))
                    val change = if left = ~1 then true
                              else false
                in
                    if change then
                        let
                            val next_time = time+1
                            val _ = Array2.update (time_array, X, (Y+1), next_time)
                            val _ = print ("updating with " ^ Int.toString next_time ^ "\n")
                        in
                            update_surround X Y time ((X, (Y+1), next_time) :: new_pipes) (repeat-1)
                        end
                    else
                        update_surround X Y time new_pipes (repeat-1)
                end
            else if repeat = 1 then update_surround X Y time new_pipes (repeat-1)
            else
                new_pipes

        fun fill_from_pipes [] =
            ()
          | fill_from_pipes pipes =
            let
                val (X, Y, time) = hd pipes
                val new_pipes = update_surround X Y time [] 4
            in
                fill_from_pipes ((tl pipes) @ new_pipes)
            end

    in
        fill_from_pipes pipes
    end

fun savethecat fileName =
    let
        val (map_list, N, M) = parse fileName
        val map_array = Array2.fromList map_list
        val time_array = Array2.array (N, M, ~1)
        val _ = print_array map_array
        val ((catX, catY), pipes) = find_init map_array time_array
        val _ = print_int_array time_array
        val _ = fill_time_array time_array pipes
        val _ = print_int_array time_array
        (* val map = fill_map all_string map *)
    in
        (N, M, (catX, catY), pipes)
        (* all_string, N, M, map *)
    end
