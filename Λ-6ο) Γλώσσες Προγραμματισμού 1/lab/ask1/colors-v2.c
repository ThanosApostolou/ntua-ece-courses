#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// COMPLEXITY
// (N) times to read ribbon, ribbon_reversed and colors_freq
// (j + N - j) == N to find_min
// Another N to find min for reversed array
// So I think we have something like O(4N) == O(N)

int find_min (int N, int K, int *ribbon, int *colors_freq) {
    int min_partial=N, min=N;
    int *colors_freq_cp;
    
    // copy colors_freq so that we leave the inital array untouched
    colors_freq_cp = (int*) malloc(K * sizeof(int));
    for (int i = 0; i < K; i++) {
        colors_freq_cp[i] = colors_freq[i];
    }

    // start to reduce frequency from the end
    // break when the frequency of the current color is the last
    for (int j = N-1; j > 0; j--) {
        if (colors_freq_cp[ribbon[j]-1] == 1) {
            min_partial = j+1;
            break;
        }
        colors_freq_cp[ribbon[j]-1]--;
    }

    // start to reduce frequency from the start, until previously last position
    // break when the frequency of the current color is the last
    min = min_partial;
    for (int i = 0; i < min_partial; i++) {
        if (colors_freq_cp[ribbon[i]-1] == 1) {
            min = min_partial - i;
            break;
        }
        colors_freq_cp[ribbon[i]-1]--;
    }

    return min;
}

int main(int argc, char *argv[]) {
    int *ribbon, *ribbon_reversed, *colors_freq;
    int N, K, min, min_left, min_right;
    FILE *fp;
    
    fp = fopen(argv[1],"r");                            //open the file
    if (fp == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    fscanf(fp, "%d", &N);                               //read ribbon size (N)
    ribbon = (int*) malloc(N * sizeof(int));            //create ribbon array
    ribbon_reversed = (int*) malloc(N * sizeof(int));   //create ribbon reversed array
    fscanf(fp, "%d", &K);                               //read number of colors (K)
    colors_freq = (int*) malloc(K * sizeof(int));       //create colors frequency array (int)
    for (int i = 0; i < K; i++) {
        colors_freq[i] = 0;
    }
    
    for (int i = 0; i < N; i++) {
        int temp;
        fscanf(fp, "%d", &temp); 
        ribbon[i] = temp;                               // fill ribbon array
        ribbon_reversed[N-i-1] = temp;                  // fill ribbon reversed array
        colors_freq[ribbon[i]-1]++;                     // fill colors frequency
    }
    fclose(fp);
    
    // check early if some color is missing (like in c3.txt)
    for (int i = 0; i < K; i++) {
        if (colors_freq[i] == 0) {
            printf("%d\n", 0);
            return 0;
        }
    }
    
    // find min
    min_left = find_min(N, K, ribbon, colors_freq);
    printf("Min_left is %d\n", min_left); // debug output
    // find min of reversed array
    min_right = find_min(N, K, ribbon_reversed, colors_freq);
    printf("Min_right is %d\n", min_right); // debug output

    if (min_left < min_right) {
        min = min_left;
    } else {
        min = min_right;
    }
    printf("%d\n", min);
    return 0;
}
