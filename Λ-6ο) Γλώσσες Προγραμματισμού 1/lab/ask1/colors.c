#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, char *argv[]) {
    int i, j, z;
    int N, K, min;
    int *ribbon;
    bool minFound;
    bool *colors;
    FILE *fp;
    
    fp = fopen(argv[1],"r");                    //open the file
    if (fp == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    fscanf(fp, "%d", &N);                       //read ribbon size (N)
    ribbon = (int*) malloc(N * sizeof(int));    //create ribbon array
    fscanf(fp, "%d", &K);                       //read number of colors (K)
    colors = (bool*) malloc(K * sizeof(bool));  //create colors array (boolean)
    for (i = 0; i < K; i++) {
        colors[i] = false;                      //initialize colors array
    }
    
    for (i = 0; i < N; i++) {
        fscanf(fp, "%d", &ribbon[i]);           //fill ribbon array
    }
    fclose(fp);
    min = 0;
    minFound = false;
    for (i = K; i < N + 1; i++) {               //i = the minimum size to check
        for (j = 0; j < N - i + 1; j++) {       //j = the (starting) part of the ribbon to check
            for (z = 0; z < i; z++) {           //z = the current point of the array to check
                colors[ribbon[j+z]-1] = true;
            }
            minFound = true;
            for (z = 0; z < K; z++) {
                if (colors[z] == false) {
                    minFound = false;
                } else {
                    colors[z] = false;
                }
            }
            if (minFound == true) {
                min = i;
                break;
            }
        }
        if (minFound == true) {
            break;
        }
    }
    printf("%d", min);
    return 0;
}
