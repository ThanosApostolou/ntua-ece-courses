#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int N = 1;                  //N = # of rows
int M = 0;                  //M = # of columns
bool catSafe = true;

struct queueNode {                  //FIFO queue node
    int xCoord;
    int yCoord;
    char time;
    char move;
    struct queueNode *next;
};

struct queue {                      //FIFO queue
    struct queueNode *front;
    struct queueNode *last;
    int size;
};

void initQueue(struct queue *que) { //initialize queue
    que->front = NULL;
    que->last = NULL;
    que->size = 0;
}

void popQueue(struct queue *que) {  //pop front node of queue
    que->size--;
    struct queueNode *tmp = que->front;
	que->front = que->front->next;
	free(tmp);
}
                                    //push node to the back of the queue
void pushQueue(struct queue *que, int xCoord, int yCoord, char time, char move) {
    if (xCoord >= 0 && xCoord <= N && yCoord >= 0 && yCoord <= M) {
        que->size++;
        if (que->front == NULL) {
            que->front = (struct queueNode *) malloc(sizeof(struct queueNode));
            que->front->xCoord = xCoord;
            que->front->yCoord = yCoord;
            que->front->time = time;
            que->front->move = move;
            que->front->next = NULL;
            que->last = que->front;
        } else {
            que->last->next = (struct queueNode *) malloc(sizeof(struct queueNode));
            que->last->next->xCoord = xCoord;
            que->last->next->yCoord = yCoord;
            que->last->next->time = time;
            que->last->next->move = move;
            que->last->next->next = NULL;
            que->last = que->last->next;
        }
    }
}

void floodFillMap(struct queue *que, char **map) {
    while (que->size != 0) {
        int xCoord = que->front->xCoord;
        int yCoord = que->front->yCoord;
        char time = que->front->time;
        if (map[xCoord][yCoord] == '.' || map[xCoord][yCoord] == 'W' || map[xCoord][yCoord] == 'A'){
            if (map[xCoord][yCoord] == 'A') {
                catSafe = false;
            }
            map[xCoord][yCoord] = time;
            pushQueue(que, xCoord - 1, yCoord, time + 1, '\0');
            pushQueue(que, xCoord + 1, yCoord, time + 1, '\0');
            pushQueue(que, xCoord, yCoord - 1, time + 1, '\0');
            pushQueue(que, xCoord, yCoord + 1, time + 1, '\0');
        }
        popQueue(que);
    }
}

void catBFS(struct queue *que, char **map, char *catMoves) {
    char maxTime = '0';
    int i = 0;
    while (que->size != 0) {
        int xCoord = que->front->xCoord;
        int yCoord = que->front->yCoord;
        char time = que->front->time;
        char move = que->front->move;
        if (map[xCoord][yCoord] != 'X' && (map[xCoord][yCoord] > time)) {
            if (map[xCoord][yCoord] - 1 > maxTime) {
                maxTime = map[xCoord][yCoord] - 1;
                if (move != '\0') {
                    catMoves[i] = move;
                    i++;
                }
                printf("map[%d][%d] = %c, time = %c, maxTime = %c\n", xCoord, yCoord, map[xCoord][yCoord], time, maxTime);
            }
            pushQueue(que, xCoord + 1, yCoord, time + 1, 'D');
            pushQueue(que, xCoord - 1, yCoord, time + 1, 'U');
            pushQueue(que, xCoord, yCoord - 1, time + 1, 'L');
            pushQueue(que, xCoord, yCoord + 1, time + 1, 'R');
        }
        popQueue(que);
    }
    catMoves[i] = '\0';
}
            

int main(int argc, char *argv[]) {
    int i, j;
    
    FILE *fp;
    fp = fopen(argv[1],"rb+");  //open the file
    if (fp == NULL) {           //error handling
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    int c;
    while(!feof(fp)) {          //count lines & columns of map
        c = fgetc(fp);
        if (c == '\n') {
            N++;
            M = -1;
        } else {
            M++;
        }
    }
    
    printf("%d %d\n", N, M);
    rewind(fp);                 //rewind file
    
    char **map = malloc(M * sizeof(*map));   //create 2D map
    for (i = 0; i < M; i++) {
        map[i] = malloc(N * sizeof(map));
    }
    int catXCoord = -1;
    int catYCoord = -1;
    for (i = 0; i < N; i++) {                   //fill map
        for (j = 0; j < M; j++) {
            c = fgetc(fp);
            while (c == '\n' || c == '\r') {
                c = fgetc(fp);
            }
            map[i][j] = c;
            if (c == 'A') {
                catXCoord = i;
                catYCoord = j;
            }
        }
    }
    
    fclose(fp);
        
    for (i = 0; i < N; i++) {                   //print starting map (TESTING)
        for (j = 0; j < M; j++) {
           printf("%c", map[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    //FLOOD FILL
    struct queue que;
    initQueue(&que);
    for (i = 0; i < N; i++) {
        for (j = 0; j < M; j++) {
            if (map[i][j] == 'W') {
                pushQueue(&que, i, j, '0', '\0');
            }
        }
    }
    
    floodFillMap(&que, map);
    
    for (i = 0; i < N; i++) {                   //print flooded map (TESTING)
        for (j = 0; j < M; j++) {
           printf("%c", map[i][j]);
        }
        printf("\n");
    }
    printf("%d\n", que.size);
    
    if (catSafe) {
        printf("infinity\n");
        printf("stay\n");
        return 0;
    }
    char *catMoves = malloc(N * M * sizeof(catMoves));
    pushQueue(&que, catXCoord, catYCoord, '0', '\0');
    catBFS(&que, map, catMoves);
    printf("%s\n", catMoves);
    
    return 0;
}
