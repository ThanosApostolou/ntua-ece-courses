hanoi(0, _Source, _Target, _Auxiliary, []).
hanoi(N, Source, Target, Auxiliary, Moves) :-
  N > 0,
  N1 is N-1,
  hanoi(N1, Source, Auxiliary, Target, Moves1),
  hanoi(N1, Auxiliary, Target, Source, Moves2),
  append(Moves1, [move(Source, Target) | Moves2], Moves).
