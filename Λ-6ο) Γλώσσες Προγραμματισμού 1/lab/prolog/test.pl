parent(kim, holly).
parent(margaret, kim).
parent(margaret, kent).
parent(esther, margaret).
parent(herbert, margaret).
parent(herbert, jean).

greatgrandparent(GGP, GGC):-
    parent(GGP, GP),
    parent(GP, P),
    parent(P, GGC).
    
ancestor(X, Y) :- parent(X, Y).
ancestor(X, Y) :-
    parent(Z, Y),
    ancestor(X, Z).
    
append([], L, L).
append([H|L1], L2, [H|L3]) :-
    append(L1, L2, L3).
    
running_sum([], [], _).
running_sum([X0 | X], [Y0 | Y], N) :-
    ( var(X0) ->
    X0 is Y0 - N,
    running_sum(X, Y, Y0)
    ;
    Y0 is X0 + N,
    running_sum(X, Y, Y0)
    ).
running_sum(L, S) :- running_sum(L, S, 0).
