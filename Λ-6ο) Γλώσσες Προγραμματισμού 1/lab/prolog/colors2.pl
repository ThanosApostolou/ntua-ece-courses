% Rule that reads input from file [Correct[
% Assigns first int to N (length of ribbon), second int to K (number of colors), and the rest of the ints to C (ribbon).
read_input(File, N, K, C) :-
    open(File, read, Stream),
    read_line(Stream, [N, K]),
    read_line(Stream, C).

read_line(Stream, L) :-
    read_line_to_codes(Stream, Line),
    atom_codes(Atom, Line),
    atomic_list_concat(Atoms, ' ', Atom),
    maplist(atom_number, Atoms, L).

% Rule that counts the occurances of an element in a list [Correct]
% (Element, List, Counter)
countElement(_, [], 0) :- !.
countElement(_, [], _).
countElement(Element, [Element|Tail], Counter) :-
    countElement(Element, Tail, Counter2),
    Counter is Counter2 + 1.
countElement(Element, [_|Tail], Counter) :-
    countElement(Element, Tail, Counter).

% Rule that creates a new list with the occurances of the numbers in a list
% (List, Length of list, Different numbers in list, List of occurances, current number we are counting)
createOccuranceList(_, _, X, [], X).
createOccuranceList(List, Length, Numbers, [Head|Tail], CurrentNumber) :-
    countElement(CurrentNumber, List, Head),
    CurrentNumber2 is CurrentNumber + 1,
    createOccuranceList(List, Length, Numbers, Tail, CurrentNumber2).

% Rule that duplicates a list [Correct]
dupList([],[]).
dupList([Original|Tail1], [Duplicate|Tail2]) :-
    Duplicate is Original,
    dupList(Tail1, Tail2).

% Rule that decreases an element of a list at a specified index [Correct]
decrease([Head|Tail], 0, [NewHead|Tail]) :-
    NewHead is Head - 1.
decrease([Head|Tail], Index, [Head|Tail2]) :-
    Index > 0,
    Index2 is Index - 1,
    decrease(Tail, Index2, Tail2),
    !.
decrease(List, _, List).

% Rule that counts the length of a list [Correct]
measureLength([], 0).
measureLength([_|Tail], X) :-
    measureLength(Tail, Y),
    X is Y + 1.

% Can't even explain [Correct]
findMin([Head|Tail], FreqList, [Head|Tail], FreqList1) :-
    Head1 is Head - 1,
    nth0(Head1, FreqList, 1),
    dupList(FreqList, FreqList1).
findMin([Head|Tail], FreqList, NewList, FreqList2) :-
    Head1 is Head - 1,
    \+ nth0(Head1, FreqList, 1),
    decrease(FreqList, Head1, FreqList1),
    findMin(Tail, FreqList1, NewList, FreqList2).

mini(X,Y,Z) :-
    Z is min(X,Y).

colors(File, Answer) :-
    read_input(File, RibbonLength, RibbonColors, Ribbon),
    RibbonColorsCopy is RibbonColors + 1,
    createOccuranceList(Ribbon, RibbonLength, RibbonColorsCopy, OccuranceList, 1),
    \+ member(0, OccuranceList),
    dupList(OccuranceList, OccuranceListCopy),
    findMin(Ribbon, OccuranceList, RibbonLeftPartial, OccuranceListEdited),
    reverse(RibbonLeftPartial, RibbonLeftPartialReversed),
    findMin(RibbonLeftPartialReversed, OccuranceListEdited, RibbonLeftFinal, OccuranceListEdited2),
    measureLength(RibbonLeftFinal, MinLeft),
    reverse(Ribbon, RibbonReversed),
    findMin(RibbonReversed, OccuranceListCopy, RibbonRightPartial, OccuranceListCopyEdited),
    reverse(RibbonRightPartial, RibbonRightPartialReversed),
    findMin(RibbonRightPartialReversed, OccuranceListCopyEdited, RibbonRightFinal, OccuranceListCopyEdited2),
    measureLength(RibbonRightFinal, MinRight),
    mini(MinLeft, MinRight, Answer).